#include <cassert>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <time.h>
#include <utility>
#include <boost/config.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/multi_array.hpp>

#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "discreteGeodesic.h"
 
void graph_geodesic( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, int queryVertex, std::vector< TypeReal > &geodesicDistance )
{
 	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
    

     // create a graph with a node for each vertex and an edge for each pair of adjacent vertices
    boost::multi_array<TypeReal,2> nodes(boost::extents[n_vertex][3]);
	std::vector< std::pair<int,int> > edges;
    for( int ii=0; ii< n_vertex; ii++)
    {
    	for(int idim=0; idim<3; idim++)
    	{ 
    		nodes[ii][idim] = vertices[ii*3+idim];
    	}
    }
    int count = 0;
    for(int ii=0; ii<n_faces; ii++)
    {
    	for(int jj=0; jj< face_nvertex[ii]; jj++)
    	{
    		int iv = face_vertex[count+jj];
    		int ivplus = face_vertex[count + ( (jj+1)%face_nvertex[ii] ) ];
			if( iv < ivplus )
			{
				edges.push_back( std::make_pair( iv, ivplus) );
			}
		}
		count += face_nvertex[ii];
    }    		
    

    
 
    // create graph for dijkstra
    
    //how to decide which version of the adjacency_list class to use in terms of OutEdgeList and VertexList
    //these control the data structures that represent the graph and affect the complexity of graph operations and algorithm

    //boost uses containers from the STL to represent the set of vertices and the adjacency structure
    // vecS = std::vector.
    // listS = std::list.
    // slistS = std::slist.
    // setS = std::set.
    // multisetS = std::multiset.
    // hash_setS = std::hash_set.
    
    // VertexList determines the container used to represent the vertex set. listS is a good choice if you need to add and remove vertices, otherwise vecS uses less memory

    // OutEdgeList determines the container used to store the out-edges (and possibly in-edges) for each vertex in the graph. 
    // If you want the adjacency_list to enforce the absence of parallel edges in the graph, then use the setS or hash_setS selectors. 
    // If you want to represent a multi-graph, or know that you will not insert parallel edges, then choose vecS, listS, or slistS to save space
    
    //The adjacency_list class can be used to represent both directed and undirected graphs, depending on the argument passed to the Directed template parameter.
    // directedS or bidirectionalS choose a directed graph. It specifies that the graph will provide the in_edges() function as well as the out_edges()
    // undirectedS selects an undirected graph. 
    using namespace boost;
    typedef boost::property < boost::edge_weight_t, TypeReal >Weight;
    typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::undirectedS, boost::no_property, Weight > UndirectedGraph;
    typedef boost::graph_traits < UndirectedGraph >::vertex_descriptor vertex_descriptor;
	typedef boost::graph_traits < UndirectedGraph >::edge_descriptor edge_descriptor;
    
    UndirectedGraph undigraph(n_faces);
    property_map < UndirectedGraph, boost::edge_weight_t >::type weightmap = get(edge_weight, undigraph);
  
	int n_edges = edges.size();
	for( int ii=0; ii< n_edges; ii++)
	{
		TypeReal dist = 0;
		int ivertex = edges[ii].first;
		int jvertex = edges[ii].second;
		for(int idim=0; idim<3; idim++)
		{
			dist += std::pow( nodes[ivertex][idim] -  nodes[jvertex][idim], 2); 
		}
		dist = std::sqrt(dist);
		vertex_descriptor u = vertex(ivertex, undigraph);
		vertex_descriptor v = vertex(jvertex, undigraph);
		add_edge(u, v, Weight(dist), undigraph);
	}
	printf("graph to run Dijkstra created\n");
	
	
	// define the source and run Dijkstra
	std::vector<vertex_descriptor> p(n_vertex);
	std::vector<TypeReal> d(n_vertex);
	vertex_descriptor source = boost::vertex(queryVertex, undigraph);
	boost::dijkstra_shortest_paths(undigraph, source, predecessor_map(&p[0]).distance_map(&d[0]));
	printf("Dijkstra finished\n");
	
	geodesicDistance = std::vector<TypeReal>( n_vertex, -1);
	for(int ii=0; ii<n_vertex; ii++)
	{
		vertex_descriptor visitor = vertex(ii, undigraph);
		geodesicDistance[ii] = d[visitor];
	}
	
	// find the shortest path to the sink
	/*vertex_descriptor visitor = vertex(sinkID, undigraph);
	std::vector<vertex_descriptor> shortest_path;
	shortest_path.push_back( visitor );
	while( visitor != source )
	{
		printf("distance node %d is %g, parent %d\n", visitor, d[visitor], p[visitor]);
		visitor = p[visitor];
		shortest_path.push_back( visitor );
	}*/
	
}