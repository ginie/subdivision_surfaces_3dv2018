#include <cassert>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cmath>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <time.h>
#include <utility>


#include "TypeDef.h"
#include "Settings.h"
#include "IO.h"

/*
#include <mrpt/gui/CDisplayWindow3D.h>
#include <mrpt/system/threads.h>
#include <mrpt/system/os.h>
#include <mrpt/opengl.h>
#include "visualization.h"
*/

#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "kdtree2.h"
#include "discreteGeodesic.h"
#include "tMeshHeatGeodesic.h"








void real2rgb( TypeReal *v, TypeReal *rgb, int sz)
{
	TypeReal vmax = v[0];
	TypeReal vmin = v[0];
	for(int ii=0; ii< sz; ii++)
	{
		if( v[ii] < vmin ){ vmin = v[ii]; }
		if( v[ii] > vmax ){ vmax = v[ii]; }
	}
	double dv = vmax - vmin;
	//printf("color range %f %f\n", vmin, vmax);
	for(int ii=0; ii< sz; ii++)
	{
		rgb[ii*3] = 1.0;
		rgb[ii*3+1] = 1.0;
		rgb[ii*3+2] = 1.0;
				
		if( v[ii] < (vmin + 0.25 * dv) )
		{
		  rgb[ii*3] = 0;
		  rgb[ii*3+1] = 4 * (v[ii] - vmin) / dv;
		}
		else if( v[ii] < (vmin + 0.5 * dv) )
		{
		  rgb[ii*3] = 0;
		  rgb[ii*3+2] = 1 + 4 * (vmin + 0.25 * dv - v[ii]) / dv;
		}
		else if( v[ii] < (vmin + 0.75 * dv) )
		{
		  rgb[ii*3] = 4 * (v[ii] - vmin - 0.5 * dv) / dv;
		  rgb[ii*3+2] = 0;
		} 
		else
		{
		  rgb[ii*3+1] = 1 + 4 * (vmin + 0.75 * dv - v[ii]) / dv;
		  rgb[ii*3+2] = 0;
		}
	}
	
}


void initialize_sources( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &u0 )
{

	int n_vertex = vertices.size()/3;

	u0 = std::vector<TypeReal>(n_vertex, 0);
	if( D.point.size() < 3 )
    {
		int queryVertex = D.sourceVertex;
		if( (queryVertex <0) || (queryVertex>= n_vertex) )
		{
			srand (time(NULL));
			queryVertex = rand() % n_vertex;
			printf("initializing with a random vertex %d\n", queryVertex);
		}
		else
		{
			printf("initializing with source vertex %d\n", queryVertex);
		}
		u0[queryVertex] = 1.0;
		
	}
	else
	{
		// locate the 3 closest points in the mesh and initialize u0 accordingly
		int kNN = 3;
		// create search data structures
		// Kdtree with vertices
		boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_vertex][3]);
		for(int ii=0; ii<n_vertex; ii++)
		{
			for (int idim=0; idim<3; idim++)
			{
				KdTreePoints[ii][idim] = vertices[ii*3+idim];
			}
		}
		// notice it is in C-standard layout. 
		kdtree2::KDTree* tree;
		kdtree2::KDTreeResultVector KdtreeRes; 
		tree = new kdtree2::KDTree(KdTreePoints,false); 
		tree->sort_results = true;
		
		
		std::vector<TypeReal> query(3);
		for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = D.point[idim];
		}
		
		printf("initializing with vertices closest to %f, %f, %f:\n", query[0], query[1], query[2]);
		tree->n_nearest(query, kNN, KdtreeRes);
		std::vector<TypeReal> weights(kNN);
		TypeReal sumWeights = 0;
		for(int ii=0; ii<kNN; ii++)
		{
			size_t neighID = KdtreeRes[ii].idx;
			
			TypeReal dist = 0;
			for( int jj=0; jj< 3; jj++ )
			{
				dist += std::pow( vertices[ neighID*3 + jj] - query[jj], 2);
			}
			dist = std::sqrt(dist);
			weights[ii] = std::exp( -dist );
			sumWeights += weights[ii];
		}
		// normalize the weights
		for(int ii=0; ii<kNN; ii++)
		{
			size_t neighID = KdtreeRes[ii].idx;
			u0[ neighID ] = weights[ii]/sumWeights;
			printf("\t %lu with weight %f\n", neighID, u0[ neighID ] );
		}
		

	}
}



int main(int argc, char *argv[]) {
  
	
	
	Settings D;
	bool close_holes = true;
    D.parseCommandLine(argc, argv);
    if(D.visualize>0){ D.printOptions();}
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    clock_t t0 = clock();
    
    
    std::vector<TypeReal> vertices;
    std::vector<int> face_nvertex, face_vertex;
    std::vector<TypeReal> dummy;
	
    // read input mesh
    std::string format = D.inFile.substr(D.inFile.size()-3,3);
    if( format.compare( std::string("off"))==0 ){
    	read_off( D.inFile, vertices, face_nvertex, face_vertex);    
    }else if( format.compare( std::string("ply"))==0 ){
    	read_ply( D.inFile, vertices, face_nvertex, face_vertex, dummy, true);
    }else{
    	printf("unknown input file type, expecting .ply or .off\n");
    	return(0);
    }
    int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
	
    // sample a random vertex of the surface
    std::vector<TypeReal> u0;
    initialize_sources( vertices, face_nvertex, face_vertex, D, u0 );
    std::vector< TypeReal > geodesicDistance( n_vertex );
    tMesh_heatGeodesic( vertices, face_nvertex, face_vertex, D, u0, geodesicDistance );
    
         
    /*-------------------------------------------- OUTPUT -----------------------------------------------*/
    // output geodesic as colormap
	std::vector< TypeReal> vcolors( geodesicDistance.size()*3 );
	real2rgb( &geodesicDistance[0], &vcolors[0], geodesicDistance.size() );
	std::string outfilename(D.outFile);
    write_obj_coloredmesh(  outfilename.append("_geodesic.obj"), vertices, face_nvertex, face_vertex, vcolors);	
    printf("min %f, max %f geodesic\n", *std::min_element(geodesicDistance.begin(), geodesicDistance.end() ), *std::max_element(geodesicDistance.begin(), geodesicDistance.end() ));
    
  
    std::vector< TypeReal> levelSets( geodesicDistance.size() );
    TypeReal maxDist = *std::max_element( geodesicDistance.begin(), geodesicDistance.end() );
    for(int ii=0; ii< geodesicDistance.size(); ii++)
    {
    	levelSets[ii] = std::cos( 2* M_PI *30 *geodesicDistance[ii]/maxDist );
    }
    real2rgb( &levelSets[0], &vcolors[0], levelSets.size() );
	outfilename.assign(D.outFile);
    write_obj_coloredmesh(  outfilename.append("_geodesic30LevelLines.obj"), vertices, face_nvertex, face_vertex, vcolors);	
    
    
    // discrete geodesic
    /*int queryVertex = std::distance( u0.begin(), std::max_element(u0.begin(), u0.end() ) );
    std::vector<TypeReal> discreteGeodesic;
    graph_geodesic( vertices, face_nvertex, face_vertex, queryVertex, discreteGeodesic );
    
	std::vector< TypeReal> vdcolors( discreteGeodesic.size()*3 );
	real2rgb( &discreteGeodesic[0], &vdcolors[0], discreteGeodesic.size());
	outfilename.assign(D.outFile);
    write_obj_coloredmesh(  outfilename.append("_discreteGeodesic.obj"), vertices, face_nvertex, face_vertex, vdcolors);	
	printf("min %f, max %f discrete geodesic\n", *std::min_element( discreteGeodesic.begin(), discreteGeodesic.end() ), *std::max_element(discreteGeodesic.begin(), discreteGeodesic.end() ));
    
	
	maxDist = *std::max_element( discreteGeodesic.begin(), discreteGeodesic.end() );
	levelSets.resize(discreteGeodesic.size());
    for(int ii=0; ii< discreteGeodesic.size(); ii++)
    {
    	levelSets[ii] = std::cos( 2* M_PI *30 *discreteGeodesic[ii]/maxDist );
    }
    real2rgb( &levelSets[0], &vdcolors[0], levelSets.size() );
	outfilename.assign(D.outFile);
    write_obj_coloredmesh(  outfilename.append("_discreteGeodesicLevelLines.obj"), vertices, face_nvertex, face_vertex, vdcolors);	
    */
    
    /* ------------------------------------- VISUALIZATIONS ---------------------------------------------*/
    
    // vector field
    /*printf("min %f, max %f geodesic\n", *std::min_element(geodesicDistance.begin(), geodesicDistance.end() ), *std::max_element(geodesicDistance.begin(), geodesicDistance.end() ));
	mrpt::gui::CDisplayWindow3D	winField("3-level subdivided surface",640,480);
	std::vector<TypeReal> centerF(0);
	render_vectorField( winField, &qPoint[0], &qField[0], qPoint.size()/3, centerF, true, true );
	winField.repaint();
	
    	

	// colored surface    
	mrpt::gui::CDisplayWindow3D	winA("Heat geodesic on 3-level subdivided surface",640,480);
	std::vector<TypeReal> face_geodesic( face_nvertex.size(), 0 );
	int count = 0;
	for(int ii=0; ii< face_nvertex.size(); ii++)
	{
		TypeReal color = 0;
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			color += geodesicDistance[ face_vertex[count] ];
			count += 1;
		}
		face_geodesic[ii] = color/face_nvertex[ii];
	}
	std::vector<TypeReal> center(0);
	render_colorControlMesh( winA, vertices, face_nvertex, face_vertex, center, face_geodesic, true, true);
    render_pointCloud( winA, &querySpoint[0], 1, center, 1, 1, 1, false );
    winA.repaint();
    //std::cin.ignore();
	
    
    	// colored surface    
	mrpt::gui::CDisplayWindow3D	winB("discrete geodesic on 3-level subdivided surface",640,480);
	std::vector<TypeReal> face_dgeodesic( face_nvertex.size(), 0 );
	count = 0;
	for(int ii=0; ii< face_nvertex.size(); ii++)
	{
		TypeReal color = 0;
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			color += discreteGeodesic[ face_vertex[count] ];
			count += 1;
		}
		face_dgeodesic[ii] = color/face_nvertex[ii];
	}
	std::vector<TypeReal> dcenter(0);
	render_colorControlMesh( winB, vertices, face_nvertex, face_vertex, dcenter, face_dgeodesic, true, true);
    render_pointCloud( winB, &querySpoint[0], 1, dcenter, 1, 1, 1, false );
    winB.repaint();
    std::cin.ignore();*/
    
    return 0;

}



