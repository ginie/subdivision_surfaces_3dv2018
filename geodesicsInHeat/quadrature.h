//
//  quadrature.h
//  
//
//  Created by virginia estellers on 1/17/17.
//
//

#ifndef quadrature_h
#define quadrature_h


//------------- sample at quadrature-----------------------------------------------------

void quadrature_referenceInterval(size_t n_quad, std::vector<TypeReal> &qpoint, std::vector<TypeReal> &qweight);

// Gaussian quadrature rule over face quad [0,1]^2
void quadrature_referenceQuad( size_t n_Uquad, size_t n_Vquad, std::vector<TypeReal> &qpoints, std::vector<TypeReal> &qweights);

#endif /* quadrature_h */
