

#ifndef __TRIANGULARMESH_HEATGEODESICS__
#define __TRIANGULARMESH_HEATGEODESICS__

void tMesh_heatGeodesic( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const Settings &D, std::vector<TypeReal> sources, std::vector< TypeReal > &phi );
#endif