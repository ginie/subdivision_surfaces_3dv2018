#include <string.h>
#include <vector>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <algorithm>

#include "TypeDef.h"
#include "IO.h"


void read_ply( std::string filename, std::vector<TypeReal> &verts, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &vertex_vars, bool read_faces=true)
{
    std::ifstream infile;
    infile.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
    try{
        infile.open(filename.c_str());
    }catch (std::ifstream::failure e){
        printf("Exception opening/reading file %s\n", filename.c_str());
        return;
    }
    std::string line;
    std::getline(infile, line);
    while (line.compare(0,15,"element vertex ")!=0)
    {
        std::getline(infile, line);
    }
    // read number of vertex
    int n_vertex = std::atoi( line.substr(15, std::string::npos ).c_str() );
    int n_vvars = 0;
    while (line.compare(0,13,"element face ")!=0)
    {
        std::getline(infile, line);
        n_vvars += 1;
    }
    n_vvars -= 4; // do not include the xyz coordinates
    // read number of faces
    int n_faces = std::atoi( line.substr(13, std::string::npos ).c_str() );
    while (line.compare(0,10,"end_header")!=0)
    {
        std::getline(infile, line);
    }
    // read the vertex positions
    verts.resize(n_vertex*3);
    vertex_vars.resize(n_vertex*n_vvars);
    for( int iv=0; iv< n_vertex; iv++ )
    {
        std::getline(infile, line);
        int count_ = 0;
        for( int idim=0; idim<3; idim++)
        {
            int count = line.find(" ",count_);
            verts[iv*3 + idim] = std::atof( line.substr(count_, count ).c_str() );
            count_ = count+1;
        }
        for( int idim=0; idim<n_vvars; idim++)
        {
            int count = line.find(" ",count_);
            vertex_vars[iv*n_vvars + idim] = std::atof( line.substr(count_, count ).c_str() );
            count_ = count+1;
        }
    }
    if (read_faces==true)
    {
        // read the face topology
        face_nvertex.resize(n_faces);
        face_vertex.clear();
        for( int iv=0; iv< n_faces; iv++ )
        {
            std::getline(infile, line);
            int count_ = line.find(" ") + 1;
            face_nvertex[iv] = std::atoi( line.substr(0, count_ ).c_str() );
            for( int idim=0; idim<face_nvertex[iv]; idim++)
            {
                int count = line.find(" ",count_);
                face_vertex.push_back( std::atof( line.substr(count_, count ).c_str() ) );
                count_ = count+1;
            }
        }
    }
    //printf("read initial control mesh with %d vertices and %d faces\n", n_vertex, n_faces);
    infile.close();
}

void read_off( std::string filename, std::vector<TypeReal> &verts, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
    std::ifstream infile;
    infile.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
    try{
        infile.open(filename.c_str());
    }catch (std::ifstream::failure e){
        printf("Exception opening/reading file %s\n", filename.c_str());
        return;
    }
    std::string line, readLine;
    std::getline(infile, line);
    if( line.compare(0,3,"OFF")!=0 )
    {
    	//printf("OFF files should start with OFF");
    }
    std::getline(infile, line);
    // ignore comments
    while (line.compare(0,1,"#")==0)
    {
        std::getline(infile, line);
    }
    // read number of vertex
    int delimiterPos_1 = line.find(" ", 0);
    int n_vertex = atoi(line.substr(0,delimiterPos_1+1).c_str());
    int delimiterPos_2 = line.find(" ", delimiterPos_1);
    int n_faces = atoi(line.substr(delimiterPos_1,delimiterPos_2 +1).c_str());
    int delimiterPos_3, delimiterPos_4;
    
    
    // read the vertex positions
    verts.resize(n_vertex*3);
    for (int n=0; n<n_vertex; n++)
	{
		getline(infile,readLine);
		delimiterPos_1 = readLine.find(" ", 0);
		verts[n*3] = atof(readLine.substr(0,delimiterPos_1).c_str());
		delimiterPos_2 = readLine.find(" ", delimiterPos_1+1);
		verts[n*3+1] = atof(readLine.substr(delimiterPos_1,delimiterPos_2 ).c_str());
		delimiterPos_3 = readLine.find(" ", delimiterPos_2+1);
		verts[n*3+2] = atof(readLine.substr(delimiterPos_2,delimiterPos_3 ).c_str());
	}

	face_nvertex.resize(n_faces);
    face_vertex.clear();

	for (int n=0; n<n_faces; n++)
	{
		getline(infile,readLine);
		delimiterPos_1 = readLine.find(" ", 0);
		face_nvertex[n] = atoi(readLine.substr(0,delimiterPos_1).c_str());
		for(int ii=0; ii< face_nvertex[n]; ii++)
		{
			delimiterPos_2 = readLine.find(" ", delimiterPos_1+1);
			face_vertex.push_back( atoi(readLine.substr(delimiterPos_1,delimiterPos_2 ).c_str()) );
			delimiterPos_1 = delimiterPos_2;
		}
	}
    
    //printf("read initial control mesh with %d vertices and %d faces\n", n_vertex, n_faces);
    infile.close();
}




void write_ply_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
    size_t nsamples = vertices.size()/3;
    size_t nfaces = face_nvertex.size();
    std::ofstream outfile(filename.c_str());
    outfile << "ply\nformat ascii 1.0\nelement vertex ";
    outfile << nsamples;
    outfile << "\nproperty float x\nproperty float y\nproperty float z\n";
    outfile << "element face " << nfaces;
    outfile << "\nproperty list uchar int vertex_indices\nend_header\n";
    for (size_t sample=0; sample<nsamples; sample++)
    {
        outfile << vertices[sample*3] << " "  << vertices[3*sample+1] << " " << vertices[3*sample+2] << "\n" ;
    }
    size_t count = 0;
    for (size_t face=0; face<nfaces; face++)
    {
        outfile << face_nvertex[face];
        int nv = face_nvertex[face];
        for( int ii=0; ii<nv; ii++){
            outfile << " " << face_vertex[count + ii];
        }
        outfile << "\n";
        count += face_nvertex[face];
    }
    
    outfile.close();
}






void write_ply_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &vars, std::vector<std::string> &var_names)
{
    size_t nsamples = vertices.size()/3;
    size_t nfaces = face_nvertex.size();
    size_t nvars = vars.size()/nsamples;
    std::ofstream outfile(filename.c_str());
    outfile << "ply\nformat ascii 1.0\nelement vertex ";
    outfile << nsamples;
    outfile << "\nproperty float x\nproperty float y\nproperty float z\n";
    for (size_t iv=0; iv<nvars; iv++)
    {
        outfile << "property float " << var_names[iv] << "\n";
    }
    outfile << "element face " << nfaces;
    outfile << "\nproperty list uchar int vertex_indices\nend_header\n";
    for (size_t sample=0; sample<nsamples; sample++)
    {
        outfile << vertices[sample*3] << " "  << vertices[3*sample+1] << " " << vertices[3*sample+2];
        for (size_t iv=0; iv<nvars; iv++)
        {
        	outfile << " " << vars[sample*nvars + iv];
        }
        outfile << "\n";
    }
    size_t count = 0;
    for (size_t face=0; face<nfaces; face++)
    {
        outfile << face_nvertex[face];
        int nv = face_nvertex[face];
        for( int ii=0; ii< nv; ii++){
            outfile << " " << face_vertex[count];
            count += 1;
        }
        outfile << "\n";
    }
    
    outfile.close();
    
}




void write_obj_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
    size_t nsamples = vertices.size()/3;
    size_t nfaces = face_nvertex.size();
    std::ofstream outfile(filename.c_str());
	outfile << "# OBJ\n";
	outfile << "# " << nsamples << " " << nfaces << "0\n";
    for (size_t sample=0; sample<nsamples; sample++)
    {
        outfile << "v " << vertices[sample*3] << " "  << vertices[3*sample+1] << " " << vertices[3*sample+2] << "\n" ;
    }
    size_t count = 0;
    for (size_t face=0; face<nfaces; face++)
    {
        outfile << "f";
        int nv = face_nvertex[face];
        for( int ii=0; ii<nv; ii++){
            outfile << " " << face_vertex[count + ii] + 1;
        }
        outfile << "\n";
        count += face_nvertex[face];
    }
    outfile.close();
}



void write_obj_coloredmesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, const std::vector<TypeReal> &vcolors)
{
    size_t nsamples = vertices.size()/3;
    size_t nfaces = face_nvertex.size();
    std::ofstream outfile(filename.c_str());
	outfile << "# OBJ\n";
	outfile << "# " << nsamples << " " << nfaces << "0\n";
	for (size_t sample=0; sample<nsamples; sample++)
	{
		outfile << "v " << vertices[sample*3] << " "  << vertices[3*sample+1] << " " << vertices[3*sample+2]  << " " << vcolors[sample*3] << " "  << vcolors[3*sample+1] << " " << vcolors[3*sample+2] << "\n" ;
	}
    size_t count = 0;
    for (size_t face=0; face<nfaces; face++)
    {
        outfile << "f";
        int nv = face_nvertex[face];
        for( int ii=0; ii<nv; ii++){
            outfile << " " << face_vertex[count + ii] + 1;
        }
        outfile << "\n";
        count += face_nvertex[face];
    }
    outfile.close();
}



void write_off_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
    size_t nvertex = vertices.size()/3;
    size_t nfaces = face_nvertex.size();
    std::ofstream outfile(filename.c_str());
	outfile << "OFF\n";
	outfile << nvertex << " " << nfaces << "0\n";
    for (size_t sample=0; sample<nvertex; sample++)
    {
        outfile << vertices[sample*3] << " "  << vertices[3*sample+1] << " " << vertices[3*sample+2] << "\n" ;
    }
    size_t count = 0;
    for (size_t face=0; face<nfaces; face++)
    {
        outfile << face_nvertex[face];
        int nv = face_nvertex[face];
        for( int ii=0; ii< nv; ii++){
            outfile << " " << face_vertex[count + ii];
        }
        outfile << "\n";
        count += face_nvertex[face];
    }
    outfile.close();
}

void write_off_colormesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, const std::vector<TypeReal> &vcolors )
{
    size_t nvertex = vertices.size()/3;
    size_t nfaces = face_nvertex.size();
    std::ofstream outfile(filename.c_str());
	outfile << "OFF\n";
	outfile << nvertex << " " << nfaces << "0\n";
    for (size_t sample=0; sample<nvertex; sample++)
    {
        outfile << vertices[sample*3] << " "  << vertices[3*sample+1] << " " << vertices[3*sample+2] << " " << vcolors[sample*3] << " "  << vcolors[3*sample+1] << " " << vcolors[3*sample+2] << " 1.0\n" ;
    }
    size_t count = 0;
    for (size_t face=0; face<nfaces; face++)
    {
        outfile << face_nvertex[face];
        int nv = face_nvertex[face];
        for( int ii=0; ii< nv; ii++){
            outfile << " " << face_vertex[count + ii];
        }
        outfile << "\n";
        count += face_nvertex[face];
    }
    outfile.close();
}
