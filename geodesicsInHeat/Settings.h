#ifndef __SETTINGS__
#define __SETTINGS__

class Settings
{
public:

    std::string outFile;
    std::string inFile;
    int quadrature;
    int	visualize;
    int sourceVertex;
    int solverIt;
    std::vector<double> point;

    Settings() :
        inFile(),
        outFile(),
        visualize(0),
        quadrature(3),
        sourceVertex(-1),
        point( std::vector<double>(0) ),
        solverIt(50)
    {
    }
    
    void error(const char *msg);
    void printOptions(void);
    void print_usage(void);
    void parseCommandLine(int argc, char **argv);
};

#endif //__SETTINGS__
