#include <cassert>
#include <cstdio>
#include <cfloat>
#include <algorithm>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <unordered_map>
#include <queue>
#include <utility>
#include <boost/multi_array.hpp>
#include <boost/config.hpp>
#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>

#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "PointcloudOp.h"
#include "kdtree2.h"


#ifndef __edgemap__
#define __edgemap__
struct pair_hash {
    template <class T1, class T2>
    std::size_t operator () (const std::pair<T1,T2> &p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        // Mainly for demonstration purposes, i.e. works but is overly simple
        // In the real world, use sth. like boost.hash_combine
        return h1 ^ h2;  
    }
};
typedef std::unordered_map< std::pair<int,int>, int, pair_hash > edgemap;
#endif


PointcloudOp::PointcloudOp(const std::vector<TypeReal> &_vertices, int estimate_geometry = 0)
{
	// initialize vertices, face_nvertex, face_vertex
	vertices =_vertices;
	size_t n_points = vertices.size()/3;
	int kNN = 12;
	int kNNgraph = 6;
	int n_neigh = kNN + 1;
	
	// create a kdtree to determine the nearest neighbours
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_points][3]);
	for(int ii=0; ii<n_points; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = vertices[ii*3+idim];
		}
	}
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
	tree = new kdtree2::KDTree(KdTreePoints,false); 
	tree->sort_results = true;  

	// if the pointcloud down not include normals,
	// estimate their orientation by PCA analysis
	// orient them by computing the minimim spanning tree
	edgemap edge_map;
	std::vector< int > edge_pairs;
	int num_edges = 0;
	
	
	if( estimate_geometry > 0) // create edges for the kNN graph
	{
		principalK1.resize(n_points, 0);
		principalD1.resize(n_points*3);
		principalK2.resize(n_points, 0);
		principalD2.resize(n_points*3);
		principalD3.resize(n_points*3);// normal direction
		confidence.resize(n_points, 0);
		for(int ipt=0; ipt<n_points; ipt++)
		{
			std::vector<TypeReal> query(3);
			for (int idim=0; idim<3; idim++)
			{ 
				query[idim] = vertices[ipt*3+idim];
			}
			tree->n_nearest(query, n_neigh, KdtreeRes);
	
			// for each point, use the vertex and its neighbourhood to estimate the geometry of the surface
			std::vector<int> closestP( n_neigh );
			for( int k=0; k<n_neigh; k++)
			{
				closestP[k] =  KdtreeRes[k].idx;
			}
			
			bool connected = false;
			for( int k=0; k<kNNgraph; k++)
			{
				if(ipt<closestP[1+k])
				{
					edge_pairs.push_back(ipt);
					edge_pairs.push_back(closestP[1+k]);
					num_edges += 1;
				}
				/*std::pair<int, int> edge_ikey = std::make_pair( ipt, closestP[1+k] );
				if( (edge_map.count(edge_ikey) == 0) )
				{
					edge_map.emplace( edge_ikey, num_edges );
					std::pair<int, int> edge_jkey = std::make_pair( closestP[1+k], ipt );
					edge_map.emplace( edge_jkey, num_edges );
					num_edges += 1;
					//printf("new edge in map %d %d\n", ipt, closestP[k+1]);
				}*/
			}


			// find the centroid of each cluster
			TypeReal centroid[3] = {0,0,0};
			Eigen::MatrixXf neigh_points( n_neigh, 3);
			Eigen::MatrixXf centered_points( n_neigh, 3);
			//printf("closests points to %f,%f,%f are:", points[ipt*3], points[ipt*3+1], points[ipt*3+2]);
			for( int k=0; k<n_neigh; k++)
			{
				for(int idim=0; idim<3; idim++)
				{
					neigh_points(k,idim) = vertices[ closestP[k]*3+idim ];
					centroid[idim] += neigh_points(k,idim);
				}
				//printf("\t %f %f %f at distance %f\n", points[ closestP[k]*3+0 ], points[ closestP[k]*3+1 ], points[ closestP[k]*3+2 ], distance2P[k] );
			}
			for(int idim=0; idim<3; idim++){ centroid[idim] /= n_neigh; }	
			
	
			// computer the covariance matrix
			Eigen::Matrix3f cov =  Eigen::Matrix3f::Zero();
			for( int k=0; k<n_neigh; k++)
			{
				Eigen::Vector3f aux;
				for(int idim=0; idim<3; idim++)
				{
					aux[idim] = (centroid[idim] - neigh_points(k,idim) );
					centered_points(k,idim) = neigh_points(k,idim) - centroid[idim];
				}
				cov += aux *(aux.transpose());
			}    
		
			// find principal components
			Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigensolver(cov);
			if (eigensolver.info() != Eigen::Success) abort();
			Eigen::Vector3f eigVals = eigensolver.eigenvalues();
			// sort eigenValues
			std::vector<int> map(3);
			for(int idim=0; idim<3; idim++){map[idim] = idim;}
			std::sort( map.begin(), map.end(), [&](int x,int y){ return eigVals[x]<eigVals[y];} );
			//matrix whose columns are the eigenvectors.
			Eigen::Matrix3f eigVects = eigensolver.eigenvectors();
			for(int idim=0; idim<3; idim++)
			{
				principalD1[ipt*3+idim] = eigVects(idim,map[2]);
				principalD2[ipt*3+idim] = eigVects(idim,map[1]);
			}
			principalD3[ipt*3] = principalD1[ipt*3+1]*principalD2[ipt*3+2] - principalD1[ipt*3+2]*principalD2[ipt*3+1];
			principalD3[ipt*3+1] = principalD1[ipt*3+2]*principalD2[ipt*3+0] - principalD1[ipt*3+0]*principalD2[ipt*3+2];
			principalD3[ipt*3+2] = principalD1[ipt*3+0]*principalD2[ipt*3+1] - principalD1[ipt*3+1]*principalD2[ipt*3+0];
		
		
			// project points and fit a parabola  
			if( estimate_geometry>1 )
			{
				Eigen::MatrixXf neigh_projection = centered_points * eigVects;
				TypeReal A11 = 0;
				TypeReal A12 = 0;
				TypeReal A22 = 0;
				TypeReal b1 = 0; TypeReal b2 = 0;
				for( int k=0; k<n_neigh; k++)
				{
					TypeReal x = neigh_projection(k,map[2]);
					TypeReal y = neigh_projection(k,map[1]);
					TypeReal z = neigh_projection(k,map[0]);			
					A11 += std::pow(x,4);
					A12 += std::pow(x*y,2);
					A22 += std::pow(y,4);
					b1 += std::pow(x,2) * z;
					b2 += std::pow(y,2) * z;
				} 
				
				// estimate principal curvatures solving system A*k = b
				TypeReal idetA = 1.0/( A11*A22 - std::pow(A12,2) );
				principalK1[ipt] = 2*std::abs( idetA* ( A22* b1 - A12*b2) );
				principalK2[ipt] = 2*std::abs( idetA* ( -A12* b1 + A11*b2) );
				
				// get confidence measures
				confidence[ipt] = 1.0;
			
			} // end if (estimate_geometry > 1)
			if( std::isnan(principalK1[ipt]) || std::isnan(principalK2[ipt]) || std::isnan(principalD1[ipt*3]) || std::isnan(principalD1[ipt*3+1]) || std::isnan(principalD1[ipt*3+2]) || std::isnan(principalD2[ipt*3]) || std::isnan(principalD2[ipt*3+1]) || std::isnan(principalD2[ipt*3+2])|| std::isnan(principalD3[ipt*3]) || std::isnan(principalD3[ipt*3+1]) || std::isnan(principalD3[ipt*3+2]) || std::isinf(principalK1[ipt]) || std::isinf(principalK2[ipt]) || std::isinf(principalD1[ipt*3]) || std::isinf(principalD1[ipt*3+1]) || std::isinf(principalD1[ipt*3+2]) || std::isinf(principalD2[ipt*3]) || std::isinf(principalD2[ipt*3+1]) || std::isinf(principalD2[ipt*3+2]) || std::isinf(principalD3[ipt*3]) || std::isinf(principalD3[ipt*3+1]) || std::isinf(principalD3[ipt*3+2]) )
			{
				//printf("nan in Jet point %ipt k1 = %f, d1 = %f %f %f, k2= %f, d2 = %f %f %f\n", principalK1[ipt], principalD1[ipt*3], principalD1[ipt*3+1], principalD1[ipt*3+2], principalK2[ipt], principalD2[ipt*3], principalD2[ipt*3+1], principalD2[ipt*3+2] );
				principalK1[ipt] = 0;
				principalD1[ipt*3] = 0;
				principalD1[ipt*3+1] = 0;
				principalD1[ipt*3+2] = 0;
				principalK2[ipt] = 0;
				principalD2[ipt*3] = 0;
				principalD2[ipt*3+1] = 0;
				principalD2[ipt*3+2] = 0;
				principalD3[ipt*3] = 0;
				principalD3[ipt*3+1] = 0;
				principalD3[ipt*3+2] = 0;    		
				confidence[ipt] = 0;
			}		
		}// end loop over points
    
		// compute weights for MST computation
		typedef std::pair<int, int> E;
		//printf("%d edges in knn graph\n", num_edges);
		E edges[ num_edges ];
		TypeReal weights[ num_edges ];
		/*for ( auto it = edge_map.begin(); it != edge_map.end(); ++it )
		{
			std::pair<int, int> edge_key = it->first;
			int iedge = it->second;
			int iv = edge_key.first;
			int jv = edge_key.second;
			if( iv < jv )
			{
				TypeReal ndisti = 0;
				TypeReal ndistj = 0;
				for(int idim=0; idim<3; idim++)
				{
					TypeReal aux = vertices[iv*3+idim] -vertices[jv*3+idim];
					ndisti += aux* principalD3[iv*3+idim];
					ndistj += aux* principalD3[jv*3+idim];
				}
				TypeReal w = std::abs(ndisti)+std::abs(ndistj);
				if( std::isnan(w) || std::isinf(w) ){
					weights[iedge] = std::numeric_limits<TypeReal>::max();
				}else{ weights[iedge] = w; }
				edges[iedge] = std::make_pair( iv, jv );
			}
		}*/
		for( int iedge=0; iedge< num_edges; iedge++ )
		{
			int iv = edge_pairs[2*iedge];
			int jv = edge_pairs[2*iedge+1];
			TypeReal ndisti = 0;
			TypeReal ndistj = 0;
			for(int idim=0; idim<3; idim++)
			{
				TypeReal aux = vertices[iv*3+idim] -vertices[jv*3+idim];
				ndisti += aux* principalD3[iv*3+idim];
				ndistj += aux* principalD3[jv*3+idim];
			}
			TypeReal w = std::abs(ndisti)+std::abs(ndistj);
			weights[iedge] = w;
			edges[iedge] = std::make_pair( iv, jv );
		}
		
		// minimum spanning tree
		
		using namespace boost;
		typedef adjacency_list < vecS, vecS, undirectedS,
		property<vertex_distance_t, TypeReal>, property < edge_weight_t, TypeReal > > Graph;
		typedef std::pair < int, int >E;

		Graph g(edges, edges + sizeof(edges) / sizeof(E), weights, n_points );
		property_map<Graph, edge_weight_t>::type weightmap = get(edge_weight, g);
		std::vector < graph_traits < Graph >::vertex_descriptor > p(num_vertices(g));
		prim_minimum_spanning_tree(g, &p[0]);
		std::vector< std::vector<int> > sons(n_points, std::vector<int>(0) );
		std::vector<int> sources;
		//printf("MST has size %d out of %d = %d points\n", p.size(), n_points, num_vertices(g));
		int count = 0;
		
		for (int i = 0; i < p.size(); i++)
		{
			if (p[i] != i)
			{
				sons[ p[i] ].push_back(i);
				//printf("parent %d is %d\n", p[i], i);
			}else
			{
				sources.push_back(i);
			}
		}
		int source = -1;
		for(int ii=0; ii< sources.size(); ii++)
		{ 
			int isource = sources[ii];
			if( sons[ isource ].size() >0 )
			{ 
				source = isource; 
				sources.erase( sources.begin()+ii );
				break;
			}
		}
		std::queue<int> next_vertices;
		next_vertices.push( source );
		while( ! next_vertices.empty() )
		{
			int iv = next_vertices.front();
			for(int k=0; k< sons[iv].size(); k++)
			{
				int jv = sons[iv][k];
				//printf("\tedge %d %d in MST\n", iv, jv);
				TypeReal ip = 0;
				for(int idim=0; idim<3; idim++)
				{
					ip += principalD3[iv*3+idim] * principalD3[jv*3+idim];
				}
				if( ip < 0 )
				{
					for(int idim=0; idim<3; idim++)
					{
						principalD3[jv*3+idim] *= -1.0;
					}
				}
				next_vertices.push(jv);
			}
			next_vertices.pop();
		}
	}

}









