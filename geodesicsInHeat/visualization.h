#ifndef __VISUALIZATION_HPP__
#define __VISUALIZATION_HPP__



void render_controlMesh( mrpt::gui::CDisplayWindow3D &win, std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &center, bool clear  );
void scalar2rgb(TypeReal v, TypeReal vmin, TypeReal vmax, float *r, float *g, float *b);
void render_colorControlMesh( mrpt::gui::CDisplayWindow3D &win, std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &center, std::vector<TypeReal> &scalarField, bool clear, bool normalize);
void render_vectorField( mrpt::gui::CDisplayWindow3D &win,TypeReal *points, TypeReal *field, int n_points, std::vector<TypeReal> &center, bool normalize, bool clear );
void render_pointCloud( mrpt::gui::CDisplayWindow3D &win, TypeReal *points, int n_points, std::vector<TypeReal> &center, double r, double g, double b, bool clear );
#endif