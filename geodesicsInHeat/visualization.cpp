#include <mrpt/gui/CDisplayWindow3D.h>
#include <mrpt/system/threads.h>
#include <mrpt/system/os.h>
#include <mrpt/opengl.h>


#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "sds.h"
#include "visualization.h"

void render_controlMesh( mrpt::gui::CDisplayWindow3D &win, std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &center, bool clear = false )
{
	
	mrpt::opengl::COpenGLScenePtr &theScene = win.get3DSceneAndLock();
	if(clear){ theScene->clear();}
	
	// CMesh3D
	mrpt::opengl::CMesh3DPtr obj = mrpt::opengl::CMesh3D::Create();
	obj->enableShowEdges(false);
	obj->enableShowFaces(true);
	obj->enableShowVertices(false);
	if( center.size()==0)
	{ 
		center.resize(3);
		TypeReal mini[3] = {vertices[0], vertices[1], vertices[2]};
		TypeReal maxi[3] = {vertices[0], vertices[1], vertices[2]};
		for(int ii=0; ii< vertices.size()/3; ii++)
		{
			for(int idim=0; idim<3; idim++)
			{
				mini[idim] = std::min( vertices[ii*3+idim] , mini[idim] );
				maxi[idim] = std::max( vertices[ii*3+idim] , maxi[idim] );
			}
		}
		center[0] = 0.5*(mini[0]+maxi[0]);
		center[1] = 0.5*(mini[1]+maxi[1]);
		center[2] = 0.5*(mini[2]+maxi[2]);
		TypeReal range;
		range = std::max( std::max(maxi[0]-mini[0], maxi[1]-mini[1]), maxi[2]-mini[2]);
		win.setCameraZoom(range*10);
	}
	obj->setLocation(center[0], center[1], center[2]);
	
	const unsigned int num_verts = vertices.size()/3;
	const unsigned int num_faces = face_nvertex.size();
	int *vert_per_face = new int[num_faces];
	int *face_verts = new int[4*num_faces];	
	// face information
	size_t count = 0;
	for (unsigned int ifc = 0; ifc<num_faces; ifc++)
	{
		vert_per_face[ifc] = face_nvertex[ifc];
		for (unsigned int iv = 0; iv<vert_per_face[ifc]; iv++)
		{
			face_verts[count+iv] = (unsigned int) face_vertex[count + iv];
		}
		count += face_nvertex[ifc];
	}		
	//Create vert coords
	float *vert_coords = new float [3*num_verts];
	for (unsigned int iv = 0; iv<num_verts; iv++)
	{
		for (unsigned int idim = 0; idim<3; idim++)
		{
			vert_coords[3*iv+idim] = (float) vertices[3*iv+idim];
		}
	}	
	obj->loadMesh(num_verts, num_faces, vert_per_face, face_verts, vert_coords);
	theScene->insert(obj);
	// IMPORTANT!!! IF NOT UNLOCKED, THE WINDOW WILL NOT BE UPDATED!
	win.unlockAccess3DScene();
}





void scalar2rgb(TypeReal v, TypeReal vmin, TypeReal vmax, float *r, float *g, float *b)
{
   r[0] = 1.f;
   g[0] = 1.f;
   b[0] = 1.f;
   float dv;

   if (v < vmin)
      v = vmin;
   if (v > vmax)
      v = vmax;
   dv = vmax - vmin;

   if (v < (vmin + 0.25 * dv)) {
      r[0] = 0;
      g[0] = 4 * (v - vmin) / dv;
   } else if (v < (vmin + 0.5 * dv)) {
      r[0] = 0;
      b[0] = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
   } else if (v < (vmin + 0.75 * dv)) {
      r[0] = 4 * (v - vmin - 0.5 * dv) / dv;
      b[0] = 0;
   } else {
      g[0] = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
      b[0] = 0;
   }

}


void render_colorControlMesh( mrpt::gui::CDisplayWindow3D &win, std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &center, std::vector<TypeReal> &scalarField, bool clear=false, bool normalize=true )
{

	mrpt::opengl::COpenGLScenePtr &theScene = win.get3DSceneAndLock();
	mrpt::opengl::CMesh3DPtr obj = mrpt::opengl::CMesh3D::Create();
	obj->enableShowEdges(false);
	obj->enableShowFaces(true);
	obj->enableShowVertices(false);
	if( center.size()==0)
	{ 
		center.resize(3);
		TypeReal mini[3] = {vertices[0], vertices[1], vertices[2]};
		TypeReal maxi[3] = {vertices[0], vertices[1], vertices[2]};
		for(int ii=0; ii< vertices.size()/3; ii++)
		{
			for(int idim=0; idim<3; idim++)
			{
				mini[idim] = std::min( vertices[ii*3+idim] , mini[idim] );
				maxi[idim] = std::max( vertices[ii*3+idim] , maxi[idim] );
			}
		}
		center[0] = 0.5*(mini[0]+maxi[0]);
		center[1] = 0.5*(mini[1]+maxi[1]);
		center[2] = 0.5*(mini[2]+maxi[2]);
		TypeReal range;
		range = std::max( std::max(maxi[0]-mini[0], maxi[1]-mini[1]), maxi[2]-mini[2]);
		win.setCameraZoom(range*10);
	}
	obj->setLocation(center[0], center[1], center[2]);	if(clear){ theScene->clear();}



	const unsigned int num_verts = vertices.size()/3;
	const unsigned int num_faces = face_nvertex.size();
	int *vert_per_face = new int[num_faces];
	int *face_verts = new int[4*num_faces];
	float *faceColor_r = new float[num_faces];
	float *faceColor_g = new float[num_faces];
	float *faceColor_b = new float[num_faces];
	float *faceColor_a = new float[num_faces];	
	// normalize scalar Field
	float minF = 0; float maxF = 1.0;
	if(normalize==true)
	{
		minF = (float) scalarField[0];
		maxF = (float) scalarField[0];
		for (unsigned int ifc = 0; ifc<num_faces; ifc++)
		{
			minF = minF > scalarField[ifc] ? scalarField[ifc] : minF;
			maxF = maxF < scalarField[ifc] ? scalarField[ifc] : maxF;
		}	
	}
	// face information
	size_t count = 0;
	for (unsigned int ifc = 0; ifc<num_faces; ifc++)
	{
		vert_per_face[ifc] = face_nvertex[ifc];
		scalar2rgb(scalarField[ifc], minF, maxF, &(faceColor_r[ifc]), &(faceColor_g[ifc]), &(faceColor_b[ifc]));
		faceColor_a[ifc] = 1.f;
		for (unsigned int iv = 0; iv<vert_per_face[ifc]; iv++)
		{
			face_verts[count+iv] = (unsigned int) face_vertex[count + iv];
		}
		count += face_nvertex[ifc];
	}		
	//Create vert coords
	float *vert_coords = new float [3*num_verts];
	for (unsigned int iv = 0; iv<num_verts; iv++)
	{
		for (unsigned int idim = 0; idim<3; idim++)
		{
			vert_coords[3*iv+idim] = (float) vertices[3*iv+idim];
		}
	}	
	obj->loadMesh(num_verts, num_faces, vert_per_face, face_verts, vert_coords);
	obj->setFaceColors(faceColor_r, faceColor_g, faceColor_b, faceColor_a, num_faces);
	theScene->insert(obj);

	// IMPORTANT!!! IF NOT UNLOCKED, THE WINDOW WILL NOT BE UPDATED!
	win.unlockAccess3DScene();
}

void render_pointCloud( mrpt::gui::CDisplayWindow3D &win, TypeReal *points, int n_points, std::vector<TypeReal> &center, double r=1, double g=0, double b=0, bool clear=true )
{

	mrpt::opengl::COpenGLScenePtr &theScene = win.get3DSceneAndLock();
	if(clear){ theScene->clear();}
	// add pointcloud
	mrpt::opengl::CPointCloudPtr objPC = mrpt::opengl::CPointCloud::Create();
	if( center.size()==0)
	{ 
		center.resize(3);
		TypeReal mini[3] = {points[0], points[1], points[2]};
		TypeReal maxi[3] = {points[0], points[1], points[2]};
		for(int ii=0; ii< n_points; ii++)
		{
			for(int idim=0; idim<3; idim++)
			{
				mini[idim] = std::min( points[ii*3+idim] , mini[idim] );
				maxi[idim] = std::max( points[ii*3+idim] , maxi[idim] );
			}
		}
		center[0] = 0.5*(mini[0]+maxi[0]);
		center[1] = 0.5*(mini[1]+maxi[1]);
		center[2] = 0.5*(mini[2]+maxi[2]);
		TypeReal range;
		range = std::max( std::max(maxi[0]-mini[0], maxi[1]-mini[1]), maxi[2]-mini[2]);
		win.setCameraZoom(range*10);
	}
	objPC->setLocation(center[0], center[1], center[2]);
	theScene->insert( objPC );
	objPC->setPointSize(3.0);
	objPC->enablePointSmooth();
	objPC->setColor(r,g,b);
	for (int i=0;i<n_points;i++)
	{
		objPC->insertPoint( points[i*3], points[i*3+1], points[i*3+2] );
	}

	// IMPORTANT!!! IF NOT UNLOCKED, THE WINDOW WILL NOT BE UPDATED!
	win.unlockAccess3DScene();
}



void render_vectorField( mrpt::gui::CDisplayWindow3D &win, TypeReal *points, TypeReal *field, int n_points, std::vector<TypeReal> &center, bool normalize=false, bool clear=false )
{

	mrpt::opengl::COpenGLScenePtr &theScene = win.get3DSceneAndLock();
	if(clear){ theScene->clear();}
	// add vectorField
	float scaleNorm = 0.05;
	float minX = (float) points[0];
	float maxX = (float) points[0];
	for (size_t i = 0; i<n_points; i++)
	{
		minX = minX > (float)points[i*3] ? (float)points[i*3] : minX;
		maxX = maxX < (float)points[i*3] ? (float)points[i*3] : maxX;
	}			
	float rangeX = maxX - minX;
	scaleNorm *= rangeX;
	float maxNorm = 0;
	float minNorm = std::sqrt(std::pow(field[0],2) + std::pow(field[1],2) + std::pow(field[2],2));

	mrpt::opengl::CVectorField3DPtr VFobj = mrpt::opengl::CVectorField3D::Create();
	if( center.size()!=3 )
	{ 
		center.resize(3);
		TypeReal mini[3] = {points[0], points[1], points[2]};
		TypeReal maxi[3] = {points[0], points[1], points[2]};
		for(int ii=0; ii< n_points; ii++)
		{
			for(int idim=0; idim<3; idim++)
			{
				mini[idim] = std::min( points[ii*3+idim] , mini[idim] );
				maxi[idim] = std::max( points[ii*3+idim] , maxi[idim] );
			}
		}
		center[0] = 0.5*(mini[0]+maxi[0]);
		center[1] = 0.5*(mini[1]+maxi[1]);
		center[2] = 0.5*(mini[2]+maxi[2]);
		TypeReal range;
		range = std::max( std::max(maxi[0]-mini[0], maxi[1]-mini[1]), maxi[2]-mini[2]);
		win.setCameraZoom(range*10);		
	}
	VFobj->setLocation(center[0], center[1], center[2]);
	mrpt::math::CMatrixFloat x(n_points,1), y(n_points,1), z(n_points,1);
	mrpt::math::CMatrixFloat vx(n_points,1), vy(n_points,1), vz(n_points,1);
	std::vector<float> normF(n_points,0);
	float inorm = 1.0;
	for (int i=0;i<n_points;i++)
	{
		x(i,0) = (float)points[i*3];
		y(i,0) = (float)points[i*3+1];
		z(i,0) = (float)points[i*3+2];
		if( normalize==true )
		{
			inorm = std::pow(field[i*3],2) + std::pow(field[i*3+1],2) + std::pow(field[i*3+2],2);
			if( inorm>0 )
			{
				inorm = 1.0/std::sqrt(inorm) * scaleNorm;
				normF[i] = std::sqrt(inorm);
				maxNorm = maxNorm < normF[i] ? normF[i] :maxNorm;
				minNorm = minNorm > normF[i] ? normF[i] :minNorm;
			}
		}
		vx(i,0) = (float)field[i*3] * inorm;
		vy(i,0) = (float)field[i*3+1] * inorm;
		vz(i,0) = (float)field[i*3+2] * inorm;
	}
	VFobj->setPointCoordinates(x,y,z);
	VFobj->setVectorField(vx,vy,vz);
	VFobj->setPointColor(1,0.3,0);
	VFobj->setVectorFieldColor(0,0,1);
	VFobj->setPointSize(5.0);
	VFobj->setLineWidth(5.0);
	VFobj->enableColorFromModule();
	VFobj->setMaxSpeedForColor(3.0);
	VFobj->setMotionFieldColormap(0,0,1,1,0,0);
	theScene->insert( VFobj );

	// IMPORTANT!!! IF NOT UNLOCKED, THE WINDOW WILL NOT BE UPDATED!
	win.unlockAccess3DScene();

}


