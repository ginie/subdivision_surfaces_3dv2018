#include <opensubdiv/far/topologyDescriptor.h>
#include <opensubdiv/far/primvarRefiner.h>
#include <opensubdiv/far/patchTableFactory.h>
#include <opensubdiv/far/patchMap.h>
#include <opensubdiv/far/ptexIndices.h>
#include <opensubdiv/far/stencilTable.h>
#include <opensubdiv/far/stencilTableFactory.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <time.h>
#include <utility>
#include <queue>


#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "quadrature.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "Settings.h"
#include "LaplaceBeltrami.h"
#include "heatGeodesic.h"




void rhs_Heat( const std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, size_t n_quad, const SpVect &u0, SpVect &rhs )
{

    int nverts = vertices.size()/3;
    int nverts0 = nverts;
    size_t nfaces = facenverts.size();
    rhs = SpVect::Zero(nverts);

    // the rhs is \int_{M} < \grad_M Basis_j, V >_{M}
        // where M is the manifold or subdivision surface)
        //               V is the normalized gradient of function u0

    // Vertex container implementation.
        struct Vertex {

                // Minimal required interface ----------------------
                Vertex() { }

                void Clear( void * =0 ) {
                         point[0] = point[1] = point[2] = 0.0f;
                }

                void AddWithWeight(Vertex const & src, TypeReal weight) {
                        point[0] += weight * src.point[0];
                        point[1] += weight * src.point[1];
                        point[2] += weight * src.point[2];
                }

                TypeReal point[3];
        };

        typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
        //OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE;
        //OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY;
        OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_AND_CORNER;
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;

    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(BoundaryOption);

    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);


    // quadrature over reference quad
    std::vector<TypeReal> qpoints, qweights;
    quadrature_referenceQuad( n_quad, n_quad, qpoints, qweights);
    int n_quadface = qweights.size();


    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));

    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 0;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));


    //printf("generating far patch table\n");
    // Generate a set of Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;//ENDCAP_NONE;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);



   // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    //printf("getting number of refiner verices and local points\n");
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();

    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));



    //Get all the stencils from the patchTable (necessary to obtain the weights for the LocalPoints)
    OpenSubdiv::Far::StencilTable const *stenciltab = patchTable->GetLocalPointStencilTable();
    // Allocate vertex primvar buffer (1 stencil for each vertex)
    const int nstencils = stenciltab->GetNumStencils();
    nverts = stenciltab->GetNumControlVertices();
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    // Adaptive refinement may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    //printf("nRefinedLevels is %d\n", nRefinedLevels);

    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    // Evaluate local points from interpolated vertex primvars.
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    nfaces = ptexIndices.GetNumFaces();

    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    float pWeights[20], dsWeights[20], dtWeights[20];
    for (size_t iface=0; iface<nfaces; iface++)
    {

        for( int iq=0; iq< n_quadface; iq++)
        {
                        // Locate the patch corresponding to the face ptex idx and (s,t)
                        TypeReal s = qpoints[iq*2];
                        TypeReal t = qpoints[iq*2+1];
                        // store the values of the vector field V
                        OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( iface, s, t );
                        assert(handle);


                        // Evaluate the patch weights, identify the CVs and compute the limit frame
                        // the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
                        // compute the weights of these linear combinations
                        patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights);

                        // get the control variables for this patch
                        OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);


                        //Compute the weights for the coordinates and the derivatives wrt the control vertices
                        //Set all weights to zero
                        std::vector<TypeReal> vect_wc(nverts0, 0.f);
                        std::vector<TypeReal> vect_wu1(nverts0, 0.f);
                        std::vector<TypeReal> vect_wu2(nverts0, 0.f);
                        TypeReal S[3] = {0, 0, 0};
                        TypeReal Ss[3] = {0, 0, 0};
                        TypeReal St[3] = {0, 0, 0};
                        std::queue<std::pair<int, TypeReal> > auxQueue;
                        std::queue<TypeReal> auxDu;
                        std::queue<TypeReal> auxDv;
                        for (int cv = 0; cv < cvs.size(); ++cv)
                        {
                                // evaluate the surface and tangent
                                for(size_t idim=0; idim<3; idim++)
                                {
                                        S[idim] += pWeights[cv]* verts[cvs[cv]].point[idim];
                                        Ss[idim] += dsWeights[cv]* verts[cvs[cv]].point[idim];
                                        St[idim] += dtWeights[cv]* verts[cvs[cv]].point[idim];
                                }

                                // get the value of the active basis
                                if (cvs[cv] < nverts0)
                                {
                                        vect_wc[ cvs[cv] ] += pWeights[cv];
                                        vect_wu1[ cvs[cv] ] += dsWeights[cv];
                                        vect_wu2[ cvs[cv] ] += dtWeights[cv];
                                }
                                else
                                {
                                        //printf("add2queue %d, %f, %f, %f\n", cvs[cv], pWeights[cv], dsWeights[cv], dtWeights[cv] );
                                        auxQueue.push( std::make_pair(cvs[cv], pWeights[cv]) );
                                        auxDu.push( dsWeights[cv] );
                                        auxDv.push( dtWeights[cv] );
                                }
                        }

                        // for indices that are auxiliary intermediate points, accumulate the contributions
                        while( !auxQueue.empty() )
                        {
                                std::pair<int, TypeReal> obj = auxQueue.front();
                                TypeReal du0 = auxDu.front();
                                TypeReal dv0 = auxDv.front();
                                int indx0 = obj.first;
                                TypeReal val0 = obj.second;
                                const unsigned int ind_offset = indx0 - nverts0;
                                size_t size_st = st[ind_offset].GetSize();
                                OpenSubdiv::Far::Index const *st_ind = st[ind_offset].GetVertexIndices();
                                float const *st_weights = st[ind_offset].GetWeights();
                                for (size_t s = 0; s < size_st; s++)
                                {
                                        int indx = st_ind[s];
                                        if( indx <nverts0 )
                                        {
                                                vect_wc[ indx ] += val0*st_weights[s];
                                                vect_wu1[ indx ] += du0*st_weights[s];
                                                vect_wu2[ indx ] += dv0*st_weights[s];
                                        }else
                                        {
                                                auxQueue.push( std::make_pair(indx, val0*st_weights[s]) );
                                                auxDu.push( du0*st_weights[s] );
                                                auxDv.push( dv0*st_weights[s] );
                                                //printf("add2queue %d, %f, %f, %f\n", indx, val, du0*st_weights[s], dv0*st_weights[s]  );

                                        }
                                }
                                auxQueue.pop();
                                auxDu.pop();
                                auxDv.pop();
                        }


                        // compute the metric tensor, its inverse and the are element
                        Eigen::Map< Eigen::Vector3f > Su(&Ss[0], 3);
                        Eigen::Map< Eigen::Vector3f > Sv(&St[0], 3);
                        Eigen::Matrix2f G(2,2);
                        G(0,0) = Su.dot(Su);
                        G(1,0) = Sv.dot(Su);
                        G(0,1) = G(1,0);
                        G(1,1) = Sv.dot(Sv);
                        // compute the psudoinverse instead of the inverse to protect against badly conditioned quads
                        TypeReal detG = G.determinant();
                        TypeReal area = std::sqrt(detG);

                        // compute the value of V in the basis of the tangent space
                        //Eigen::Vector2f Dv(2); Dv[0] = 0; Dv[1] = 0;
                        TypeReal val = 0;
                        for (int cv=0; cv<nverts0; cv++)
                        {
                                val +=  vect_wc[cv] * u0[cv];
                        }

                        // add it to the rhs
                        TypeReal mweight = qweights[iq] * area;
                        for (int cv=0; cv<nverts0; cv++)
                        {
                                if (vect_wc[cv] != 0.f)
                                {
                                        rhs[cv] += vect_wc[cv]* val * mweight;
                                }
                        }
                }// end quadrature over face
    }// end loop over face

}


void rhs_Poisson( const std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, size_t n_quad, const SpVect &u0, SpVect &rhs, std::vector<TypeReal> &qPoint, std::vector<TypeReal> &qField )
{

    int nverts = vertices.size()/3;
    int nverts0 = nverts;
    size_t nfaces = facenverts.size();
    rhs = SpVect::Zero(nverts);
    qPoint = std::vector<TypeReal>(n_quad*n_quad*nfaces*3, 0);
    qField = std::vector<TypeReal>(n_quad*n_quad*nfaces*3, 0);

    // the rhs is \int_{M} < \grad_M Basis_j, V >_{M}
        // where M is the manifold or subdivision surface)
        //               V is the normalized gradient of function u0

 // Vertex container implementation.
      struct Vertex {

                // Minimal required interface ----------------------
                Vertex() { }

                void Clear( void * =0 ) {
                         point[0] = point[1] = point[2] = 0.0f;
                }

                void AddWithWeight(Vertex const & src, TypeReal weight) {
                        point[0] += weight * src.point[0];
                        point[1] += weight * src.point[1];
                        point[2] += weight * src.point[2];
                }

                TypeReal point[3];
        };

        typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
        //OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE;
        //OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY;
        OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_AND_CORNER;



    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;

    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;

    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(BoundaryOption);

    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);


    // quadrature over reference quad
    std::vector<TypeReal> qpoints, qweights;
    quadrature_referenceQuad( n_quad, n_quad, qpoints, qweights);
    int n_quadface = qweights.size();


    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));

    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 0;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));


    //printf("generating far patch table\n");
    // Generate a set of Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;//ENDCAP_NONE;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);



   // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    //printf("getting number of refiner verices and local points\n");
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();

    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));



    //Get all the stencils from the patchTable (necessary to obtain the weights for the LocalPoints)
    OpenSubdiv::Far::StencilTable const *stenciltab = patchTable->GetLocalPointStencilTable();
    // Allocate vertex primvar buffer (1 stencil for each vertex)
    const int nstencils = stenciltab->GetNumStencils();
    nverts = stenciltab->GetNumControlVertices();
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    // Adaptive refinement may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    //printf("nRefinedLevels is %d\n", nRefinedLevels);

    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    // Evaluate local points from interpolated vertex primvars.
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    nfaces = ptexIndices.GetNumFaces();

    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    float pWeights[20], dsWeights[20], dtWeights[20];
    for (size_t iface=0; iface<nfaces; iface++)
    {

        for( int iq=0; iq< n_quadface; iq++)
        {
			// Locate the patch corresponding to the face ptex idx and (s,t)
			TypeReal s = qpoints[iq*2];
			TypeReal t = qpoints[iq*2+1];
			// store the values of the vector field V
			OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( iface, s, t );
			assert(handle);
	
	
			// Evaluate the patch weights, identify the CVs and compute the limit frame
			// the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
			// compute the weights of these linear combinations
			patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights);
	
			// get the control variables for this patch
			OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);
	
	
			//Compute the weights for the coordinates and the derivatives wrt the control vertices
			//Set all weights to zero
			std::vector<TypeReal> vect_wc(nverts0, 0.f);
			std::vector<TypeReal> vect_wu1(nverts0, 0.f);
			std::vector<TypeReal> vect_wu2(nverts0, 0.f);
			TypeReal S[3] = {0, 0, 0};
			TypeReal Ss[3] = {0, 0, 0};
			TypeReal St[3] = {0, 0, 0};
			std::queue<std::pair<int, TypeReal> > auxQueue;
			std::queue<TypeReal> auxDu;
			std::queue<TypeReal> auxDv;
			for (int cv = 0; cv < cvs.size(); ++cv)
			{
					// evaluate the surface and tangent
					for(size_t idim=0; idim<3; idim++)
					{
							S[idim] += pWeights[cv]* verts[cvs[cv]].point[idim];
							Ss[idim] += dsWeights[cv]* verts[cvs[cv]].point[idim];
							St[idim] += dtWeights[cv]* verts[cvs[cv]].point[idim];
					}
	
					// get the value of the active basis
					if (cvs[cv] < nverts0)
					{
							vect_wc[ cvs[cv] ] += pWeights[cv];
							vect_wu1[ cvs[cv] ] += dsWeights[cv];
							vect_wu2[ cvs[cv] ] += dtWeights[cv];
					}
					else
					{
							//printf("add2queue %d, %f, %f, %f\n", cvs[cv], pWeights[cv], dsWeights[cv], dtWeights[cv] );
							auxQueue.push( std::make_pair(cvs[cv], pWeights[cv]) );
							auxDu.push( dsWeights[cv] );
							auxDv.push( dtWeights[cv] );
					}
			}
	
			// for indices that are auxiliary intermediate points, accumulate the contributions
			while( !auxQueue.empty() )
			{
					std::pair<int, TypeReal> obj = auxQueue.front();
					TypeReal du0 = auxDu.front();
					TypeReal dv0 = auxDv.front();
					int indx0 = obj.first;
					TypeReal val0 = obj.second;
					const unsigned int ind_offset = indx0 - nverts0;
					size_t size_st = st[ind_offset].GetSize();
					OpenSubdiv::Far::Index const *st_ind = st[ind_offset].GetVertexIndices();
					float const *st_weights = st[ind_offset].GetWeights();
					for (size_t s = 0; s < size_st; s++)
					{
							int indx = st_ind[s];
							if( indx <nverts0 )
							{
									vect_wc[ indx ] += val0*st_weights[s];
									vect_wu1[ indx ] += du0*st_weights[s];
									vect_wu2[ indx ] += dv0*st_weights[s];
							}else
							{
									auxQueue.push( std::make_pair(indx, val0*st_weights[s]) );
									auxDu.push( du0*st_weights[s] );
									auxDv.push( dv0*st_weights[s] );
									//printf("add2queue %d, %f, %f, %f\n", indx, val, du0*st_weights[s], dv0*st_weights[s]  );
	
							}
					}
					auxQueue.pop();
					auxDu.pop();
					auxDv.pop();
			}
	
	
			// compute the metric tensor, its inverse and the are element
			Eigen::Map< Eigen::Vector3f > Su(&Ss[0], 3);
			Eigen::Map< Eigen::Vector3f > Sv(&St[0], 3);
			Eigen::Matrix2f G(2,2);
			G(0,0) = Su.dot(Su);
			G(1,0) = Sv.dot(Su);
			G(0,1) = G(1,0);
			G(1,1) = Sv.dot(Sv);
			// compute the psudoinverse instead of the inverse to protect against badly conditioned quads
			Eigen::Matrix2f iG = pseudoinverse(G, (TypeReal) 1e-4);
			TypeReal detG = G.determinant();
			TypeReal area = std::sqrt(detG);
	
			// compute the value of V in the basis of the tangent space
			Eigen::Vector2f Dv(2); Dv[0] = 0; Dv[1] = 0;
			//TypeReal val = 0;
			for (int cv=0; cv<nverts0; cv++)
			{
					//val +=  vect_wc[cv] * u0[cv];
					Dv[0] += vect_wu1[cv] * u0[cv];
					Dv[1] += vect_wu2[cv] * u0[cv];
			}
			TypeReal normV = Dv.dot( G* Dv );
			normV = std::sqrt( normV );
			Dv[1] = Dv[1]/normV;
			Dv[0] = Dv[0]/normV;
			
			int ipoint = iface*n_quadface + iq;
			for(int idim=0; idim<3; idim++)
			{
				qPoint[ ipoint*3 + idim ] = S[idim];
				qField[ ipoint*3 + idim ] = Dv[0]* Ss[idim] + Dv[1]* St[idim];
			}
	
			// add it to the rhs
			TypeReal mweight = qweights[iq] * area;
			for (int cv=0; cv<nverts0; cv++)
			{
				Eigen::Vector2f Du(2); Du[0] = vect_wu1[cv]; Du[1] = vect_wu2[cv];
				rhs[cv] += mweight * Du.dot( iG*Dv);
			}
		}// end quadrature over face
    }// end loop over face

}


void heat_geodesic( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> sources, std::vector< TypeReal > &phi, std::vector<TypeReal> &qPoint, std::vector<TypeReal> &qField )
{
        // implementation of
        // Geodesics in Heat: A New Approach to Computing Distance Based on Heat Flow. Crane,  Weischel, and Wardetzky, TOG 2013
        // on subdivision surfaces

    clock_t t0 = clock();
    int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
    size_t n_quad = D.quadrature;

    // create a Dirac function for the heat sorce at the query point
    Eigen::Map< SpVect > u0( &sources[0], n_vertex);
    SpVect x = SpVect::Random(n_vertex);
    Eigen::Map< SpVect > phiEigen(&phi[0], n_vertex);
    phiEigen = SpVect::Random(n_vertex);

    // compute the time step of the heat equation (tau)
    TypeReal maxSqrDist = 0;
    TypeReal meanDist = 0;
    int countMean = 0;
    int count = 0;
    for(int ii=0; ii< n_faces; ii++)
    {
        for(int jj=0; jj< face_nvertex[ ii]; jj++)
        {
                int iv = face_vertex[ count +jj];
                int ivplus = face_vertex[ count + ( (jj+1)%face_nvertex[ ii] ) ];
                int ivdiag = face_vertex[ count + ( (jj+2)%face_nvertex[ ii] ) ];  
                if( iv< ivplus)
                {
                	TypeReal dist = 0;
                	for(int idim=0; idim<3; idim++)
                	{
                        dist += std::pow( vertices[iv*3+idim] - vertices[ivplus*3+idim] , 2);
                    }
                    if( dist > maxSqrDist ){ maxSqrDist = dist; };
                    meanDist += std::sqrt( dist );
                    countMean += 1;
                }
                if( iv< ivdiag)
                {
                	TypeReal dist = 0;
                	for(int idim=0; idim<3; idim++)
                	{
                        dist += std::pow( vertices[iv*3+idim] - vertices[ivdiag*3+idim] , 2);
                    }
                    if( dist > maxSqrDist ){ maxSqrDist = dist; };
                    meanDist += std::sqrt( dist );
                    countMean += 1;
                }                
        }
        count += face_nvertex[ ii];
    }
    meanDist /= countMean;
    TypeReal tau = maxSqrDist;//std::sqrt(meanDist);
    clock_t t1 = clock();
    printf("time step %f computed in %f s\n", tau, (double)(t1-t0) / ((double)CLOCKS_PER_SEC));

    // compute the mass (M) and stiffness matrices (K)
    SpMat M, K;
    LaplaceBeltrami(vertices, face_nvertex, face_vertex, K, M, n_quad);
    clock_t t2 = clock();
    printf("mass and stiffness matrices computed in %f s\n", (double)(t2-t1) / ((double)CLOCKS_PER_SEC));

    // define the linear system for the Heat and Poisson problems and initialize the solvers
    SpMat Aheat = M + tau * K;
    SpMat Apoisson = K;
    
    // compute the right-hand size in the weak formulation of the Heat equation (b)
    SpVect b;
    rhs_Heat( vertices, face_nvertex, face_vertex, n_quad, u0,  b );
    
    // solve the linear systrem associated with a forward step on the heat equation: (M + tau K) x = b
    if( D.solverIt <= 0 )
    {
    	Eigen::SparseLU<SpMat> solverHeat;
		solverHeat.analyzePattern(Aheat);   // for this step the numerical values of A are not used
		solverHeat.factorize(Aheat);
		x = solverHeat.solve( b ); 
	}else
	{
        Eigen::ConjugateGradient<SpMat > solverHeat;
        solverHeat.setMaxIterations(D.solverIt);
        solverHeat.compute(Aheat);
        x = solverHeat.solveWithGuess( b, x );
    }
    clock_t t3 = clock();
    printf("Heat equation integrated %f s\n", (double)(t3-t2) / ((double)CLOCKS_PER_SEC));


    // define the right-hand size for the Poisson problem (c)
    SpVect c;
    rhs_Poisson( vertices, face_nvertex, face_vertex, n_quad, x, c, qPoint, qField );

    // solve the Poisson problem K v = c
    if( D.solverIt <= 0 )
    {
		Eigen::SparseLU<SpMat> solverPoisson;
		solverPoisson.analyzePattern(Apoisson);   // for this step the numerical values of A are not used
		solverPoisson.factorize(Apoisson);
		phiEigen = solverPoisson.solve(-c);
	}else
	{
        Eigen::ConjugateGradient<SpMat > solverPoisson;
        solverPoisson.setMaxIterations(D.solverIt);
        solverPoisson.compute(Apoisson);
        phiEigen = solverPoisson.solveWithGuess( -c, phiEigen );
    }    
    clock_t t4 = clock();
    printf("Poisson problem solved in %f s\n", (double)(t4-t3) / ((double)CLOCKS_PER_SEC));    
    
    // add the offset tau gurantees that the smallest distance is zero
    TypeReal offset = *std::min_element( phi.begin(), phi.end() );
    for(int ii=0; ii<n_vertex; ii++)
    {
    	phi[ii] += -offset;
    }
    

}