#ifndef __LAPLACEBELTRAMI__
#define __LAPLACEBELTRAMI__


// useful topological counts
// Laplace Beltrami operator
void LaplaceBeltrami(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, SpMat &stiffM, SpMat &massM, size_t n_quad);

#endif
