
#include <cassert>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <time.h>


#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "Settings.h"
#include "tMeshHeatGeodesic.h"


void triangularMesh_gradient( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, SpMat &grad, SpMat &div, SpMat &Ac, SpMat &cotanLaplacian )
{

	// The gradient operator grad is implemented as sparse matrix.
	// The gradient is computed with piecewise linear finite elements over the mesh. We assume a triangular mesh
	// Let f index the faces of the mesh and i the vertices, then grad(u)_f = \frac{1}{2 A_f} \sum_{ i \in f } u_i ( N_f \crossProduct e_i )
	// where N_f if the face normal, e_i is the edge opposite to vertex i, and A_f is the area of the face

	int n_faces = face_nvertex.size();
	int n_vertex = vertices.size()/3;
	
	// For each face, compute un-normalized normal through the formula e1 \crossProduct e2
	// Compute the area of each face as half the norm of the cross product.
	// Compute the set of unit-norm normals to each face.
	std::vector<TypeReal> face_normals( 3*n_faces );
	std::vector<TypeReal> face_areas( n_faces );
	std::vector<TypeReal> vertex_areas( n_vertex, 0 );
	for(int ii=0; ii< n_faces; ii++)
	{
		int ivertex = face_vertex[ ii*3];
		int jvertex = face_vertex[ ii*3 +1 ];
		int kvertex = face_vertex[ ii*3 + 2];
		Vect3 edge1, edge2;
		Eigen::Map<Vect3> normal(&face_normals[ii*3], 3);
		for(int idim=0; idim<3; idim++)
		{
			edge1[idim] = vertices[ jvertex*3 + idim] - vertices[ ivertex*3 + idim];
			edge2[idim] = vertices[ kvertex*3 + idim] - vertices[ jvertex*3 + idim];
		}
		normal = edge1.cross(edge2);
		face_areas[ii] = normal.norm();
		face_areas[ii] *= 0.5;
		normal.normalize();
		vertex_areas[ ivertex ] += face_areas[ii];
		vertex_areas[ jvertex ] += face_areas[ii];
		vertex_areas[ kvertex ] += face_areas[ii];
	}
	
	
	//Sparse matrix with area f the faces around each vertex
	std::vector<T> tripletList;
	for(int ii=0; ii< n_vertex; ii++)
	{
		vertex_areas[ii] /= 3.;
		tripletList.push_back( T( ii, ii, vertex_areas[ii] ) );
	}
	Ac.setFromTriplets( tripletList.begin(), tripletList.end() );

	//Populate the sparse entries of the matrices for the gradient operator
	tripletList.clear();
	std::vector<T> tripletListB;
	for(int ii=0; ii< n_faces; ii++)
	{	
		for(int jj=0; jj<3; jj++)
		{
			// indexes opposite to jj
			int ivertex = face_vertex[ ii*3 + jj ];
			int jvertex = face_vertex[ ii*3 + ((jj+1)%3) ];
			int kvertex = face_vertex[ ii*3 + ((jj+2)%3) ];
			Vect3 edge;
			for(int idim=0; idim<3; idim++)
			{
				edge[idim] = vertices[ kvertex*3 + idim] - vertices[ jvertex*3 + idim];
			}
			Eigen::Map<Vect3> normal(&face_normals[ii*3], 3);
			// vector N_f \crossProduct e_i
			Vect3 vals = normal.cross( edge );
			for(int idim=0; idim<3; idim++)
			{
				tripletList.push_back( T(ii*3+idim, ivertex, vals[idim]/( 2*face_areas[ii] ) ) );
				tripletListB.push_back( T(ivertex, ii*3+idim, vals[idim] ) );
			}
		}
	}
	grad.setFromTriplets( tripletList.begin(), tripletList.end() );
	div.setFromTriplets( tripletListB.begin(), tripletListB.end() );
	
	
	
	// cotan weights Laplacian
	tripletList.clear();
	for(int ii=0; ii< n_faces; ii++)
	{	
		for(int jj=0; jj<3; jj++)
		{
			int ivertex = face_vertex[ ii*3 + jj ];
			int jvertex = face_vertex[ ii*3 + ((jj+1)%3) ];
			int kvertex = face_vertex[ ii*3 + ((jj+2)%3) ];
			// edges adjacent to the vertex
			Vect3 edge1, edge2;
			for(int idim=0; idim<3; idim++)
			{
				edge1[idim] = vertices[ jvertex*3 + idim] - vertices[ ivertex*3 + idim];
				edge2[idim] = vertices[ kvertex*3 + idim] - vertices[ ivertex*3 + idim];
			}
			// compute cotangent
			edge1.normalize();
			edge2.normalize();
			Vect3 crossP = edge1.cross(edge2);
			TypeReal cos = edge1.dot(edge2);
			TypeReal sin = crossP.norm();
			TypeReal cot = 1.0;
			if( std::abs(sin) > std::numeric_limits<TypeReal>::epsilon() )
			{
				cot = cos/sin;
			}
			tripletList.push_back( T(jvertex, kvertex, cot ) );
			tripletList.push_back( T(kvertex, jvertex, cot ) );
		}
	}
	
	SpMat W( n_vertex, n_vertex );
	W.setFromTriplets( tripletList.begin(), tripletList.end() );
	
	SpVect ones = SpVect::Constant( n_vertex, 1.0);
	SpMat Wt = W.transpose();
	SpVect sumW = W * ones;
	SpMat diag( n_vertex, n_vertex);
	build_spsDiagMatrix( sumW, diag );
	cotanLaplacian = diag - W;
	
	// cotanLaplacian should be equal to div*grad
	SpMat res = cotanLaplacian - div*grad;
	res.pruned();
	printf("res should be zero, it has norm %f, and relative norm %f\n", res.squaredNorm(), res.norm()/ cotanLaplacian.norm() );
	
}




void tMesh_heatGeodesic( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const Settings &D, std::vector<TypeReal> sources, std::vector< TypeReal > &phi )
{

	int n_faces = face_nvertex.size();
	int n_vertex = vertices.size()/3;

	
	// implementation of
	// Geodesics in Heat: A New Approach to Computing Distance Based on Heat Flow. Crane,  Weischel, and Wardetzky, TOG 2013
	// on subdivision surfaces
	clock_t t0 = clock();
    // create a Dirac function for the heat sorce at the query point
    Eigen::Map< SpVect > u0( &sources[0], n_vertex);
    SpVect x = SpVect::Random(n_vertex);
    Eigen::Map< SpVect > phiEigen(&phi[0], n_vertex);
    phiEigen = SpVect::Random(n_vertex);

    // compute the time step of the heat equation (tau)
    TypeReal maxSqrDist = 0;
    TypeReal meanDist = 0;
    int countMean = 0;
    int count = 0;
    for(int ii=0; ii< n_faces; ii++)
    {
        for(int jj=0; jj< face_nvertex[ ii]; jj++)
        {
                int iv = face_vertex[ count +jj];
                int ivplus = face_vertex[ count + ( (jj+1)%face_nvertex[ ii] ) ];
                if( iv< ivplus)
                {
                	TypeReal dist = 0;
                	for(int idim=0; idim<3; idim++)
                	{
                        dist += std::pow( vertices[iv*3+idim] - vertices[ivplus*3+idim] , 2);
                    }
                    if( dist > maxSqrDist ){ maxSqrDist = dist; };
                    meanDist += std::sqrt( dist );
                    countMean += 1;
                }               
        }
        count += face_nvertex[ ii];
    }
    meanDist /= countMean;
    TypeReal tau = maxSqrDist;// std::pow(meanDist,2);//
    clock_t t1 = clock();
    printf("time step %f computed in %f s\n", tau, (double)(t1-t0) / ((double)CLOCKS_PER_SEC));
	
	// computre differential operators over the mesh
	SpMat grad( 3*n_faces, n_vertex);
	SpMat div( n_vertex, 3*n_faces );
	SpMat Ac( n_vertex, n_vertex );
	SpMat K( n_vertex, n_vertex );
	triangularMesh_gradient( vertices, face_nvertex, face_vertex, grad, div, Ac, K );
	clock_t t2 = clock();
	//SpMat K = div * grad;
    printf("differential operators %f s\n", (double)(t2-t1) / ((double)CLOCKS_PER_SEC));

    // define the linear system for the Heat and Poisson problems and initialize the solvers
    SpMat Aheat = Ac + tau * K;
    SpMat Apoisson = K;
    
    // solve the linear systrem associated with a forward step on the heat equation: (M + tau K) x = b
    if( D.solverIt <= 0 )
    {
    	Eigen::SparseLU<SpMat> solverHeat;
		solverHeat.analyzePattern(Aheat);   // for this step the numerical values of A are not used
		solverHeat.factorize(Aheat);
		x = solverHeat.solve( u0 ); 
	}else
	{
        Eigen::ConjugateGradient<SpMat > solverHeat;
        solverHeat.setMaxIterations(D.solverIt);
        solverHeat.compute(Aheat);
        x = solverHeat.solveWithGuess( u0, x );
    }
    clock_t t3 = clock();
    printf("Heat equation integrated %f s\n", (double)(t3-t2) / ((double)CLOCKS_PER_SEC));

    // normalize gradient vector
    SpVect X = grad * x;
    for(int ii=0; ii< X.size()/3; ii++)
    {
    	TypeReal norm = 0;
    	for(int idim=0; idim<3; idim++)
    	{
    		norm += std::pow( X[ii*3+idim] , 2);
    	}
    	norm = std::sqrt(norm);
    	if( norm>0 )
    	{
			for(int idim=0; idim<3; idim++)
			{
				 X[ii*3+idim] /= norm;
			}
		}
    }
    SpVect c = - div * X;
    // solve the Poisson problem K v = c
    if( D.solverIt <= 0 )
    {
		Eigen::SparseLU<SpMat> solverPoisson;
		solverPoisson.analyzePattern(Apoisson);   // for this step the numerical values of A are not used
		solverPoisson.factorize(Apoisson);
		phiEigen = solverPoisson.solve(c);
	}else
	{
        Eigen::ConjugateGradient<SpMat > solverPoisson;
        solverPoisson.setMaxIterations(D.solverIt);
        solverPoisson.compute(Apoisson);
        phiEigen = solverPoisson.solveWithGuess( c, phiEigen );
    }    
    clock_t t4 = clock();
    printf("Poisson problem solved in %f s\n", (double)(t4-t3) / ((double)CLOCKS_PER_SEC));    

    
    
    // add the offset tau gurantees that the smallest distance is zero
    TypeReal offset = *std::min_element( phi.begin(), phi.end() );
    for(int ii=0; ii<n_vertex; ii++)
    {
    	phi[ii] += -offset;
    }
	

}