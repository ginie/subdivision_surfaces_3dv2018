#ifndef __HEATGEODESICS__
#define __HEATGEODESICS__

void heat_geodesic( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> sources, std::vector< TypeReal > &phi, std::vector<TypeReal> &qPoint, std::vector<TypeReal> &qField );


#endif