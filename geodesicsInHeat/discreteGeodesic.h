#ifndef __DISCRETEGEODESIC__
#define __DISCRETEGEODESIC__

void graph_geodesic( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, int queryVertex, std::vector< TypeReal > &geodesicDistance );
#endif