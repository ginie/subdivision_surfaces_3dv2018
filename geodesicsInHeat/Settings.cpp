#include <cstring>
#include <cstdio>
#include <string>
#include <vector>
#include "TypeDef.h"
#include "Settings.h"
void Settings::error(const char *msg)
{
    fprintf(stderr,"getGeodesic | ERROR | %s\n",(msg)?msg:"");
}


void Settings::printOptions(void)
{
	printf("model parameters:\n");
}

void Settings::print_usage(void)
{
    fprintf(stderr,"USAGE\n");
    fprintf(stderr,"  getGeodesic [options] sdsFile outFile\n");
    printOptions();
}
void Settings::parseCommandLine(int argc, char **argv)
{
    if (argc==1)
    {
        print_usage();
    }
    int aux = 0;
    for (int i=1; i<argc; ++i)
    {
        if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); break; }
        else if (!strcmp(argv[i],"-VIZ") || !strcmp(argv[i],"-viz"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%d",&(visualize))<0) { error("parsing -viz"); }
        }
        else if (!strcmp(argv[i],"-quad") || !strcmp(argv[i],"-q"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%d",&(quadrature))<0) { error("parsing -quad"); }
        }      
        else if (!strcmp(argv[i],"-solIt") || !strcmp(argv[i],"-solverIt"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%d",&(solverIt))<0) { error("parsing -solverIt"); }
        } 
        else if (!strcmp(argv[i],"-source") || !strcmp(argv[i],"-sourceVertex"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%d",&(sourceVertex))<0) { error("parsing -sourceVertex"); }
        }             
        else if (!strcmp(argv[i],"-p") || !strcmp(argv[i],"-point"))
        {
        	point.resize(3);
            if ((++i)>=argc ) { error("parsing -point"); }else{ sscanf( argv[i],"%lf", &(point[0]) ); }
            if ((++i)>=argc ) { error("parsing -point"); }else{ sscanf(argv[i],"%lf", &(point[1]) ); }
            if ((++i)>=argc ) { error("parsing -point"); }else{ sscanf(argv[i],"%lf", &(point[2]) ); }
        }
        else if (!strcmp(argv[i],"-in") || !strcmp(argv[i],"-inFile"))
		{
			i++;
			inFile = std::string(argv[i]);
		}
		else if (!strcmp(argv[i],"-out") || !strcmp(argv[i],"-outFile"))
		{
			i++;
			outFile = std::string(argv[i]);
		}
        else if (argv[i][0]=='-') { error("unknown option"); }
    }
    if (inFile.size()==0)              { error("no inFile");  }
    if (outFile.size()==0)                { error("no outFile"); }
}