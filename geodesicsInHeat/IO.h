#ifndef __IO__
#define __IO__

void read_off( std::string filename, std::vector<TypeReal> &verts, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
void read_ply( std::string filename, std::vector<TypeReal> &verts, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &vertex_vars, bool read_faces);
void write_ply_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
void write_ply_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &vars, std::vector<std::string> &var_names);
void write_obj_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
void write_off_mesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
void write_off_colormesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, const std::vector<TypeReal> &vcolors );
void write_obj_coloredmesh( std::string filename, const std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, const std::vector<TypeReal> &vcolors);


#endif //__IO__
