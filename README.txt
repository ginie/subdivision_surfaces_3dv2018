UBUNTU:

1) CREATE NEW DIRECTORY
mkdir code && cd code

2) INSTALL OPENSUBDIV
sudo apt-get install libglew-dev libglfw3 libglfw3-dev
wget https://github.com/PixarAnimationStudios/OpenSubdiv/archive/master.zip
unzip master.zip && rm master.zip
cd OpenSubdiv-master
mkdir build && cd build
cmake -D NO_MAYA=1 -D NO_PTEX=1 -D NO_DOC=1 -D NO_OMP=1 -D NO_TBB=1 -D NO_CUDA=1 -D NO_OPENCL=1 -D NO_CLEW=1 -D GLEW_LOCATION=/usr/local/include/ -D GLFW_LOCATION=/usr/local/include/ -DNO_EXAMPLES=0 -DNO_TUTORIALS=0 .. 
make
cd ../..

3) INSTALL SHAPE-COMPRESS
sudo apt-get install libeigen3-dev libboost-dev
wget <> && tar xzf <>.tgz
cd mesh2SDS && make && cd ..
cd SDSutils && make && cd ..
cd geodesicsInHeat && make && cd ..

4) TEST CODE

# fit a subdivision surface to an input mesh
# example run: mesh2SDS/mesh2sds -n_controlPoints n -lp 1.3 -a alpha -b beta -maxIt max_number_of_MM_iterations -solverIt max_number_of_CG_iterations -eps epsilon -pointsFile inputMesh.ply -outFile output_controlMesh.ply
# input parameters: 	
#	-n or -n_controlPoints: number of vertices in the control mesh of the subdivision surface
#	-lp model paremeter q (power used to define the robust norms, set to 1 for the \ell_1-norm, 1.3 also works well)
#	-a model parameter alpha (weight of normal term)
#	-b model parameter beta (regularizer)
#	-eps optimization parameter epsilon used in the definition of the MM majorizer	
#	-maxIt maximum number of MM iterations
#	-solverIt maximum number of conjugate-gradients iterations used to solve the sparse linear system of equations, if set to 0 it uses a sparse LU decomposition to solve the system of equations
#	-p -pointsFile ply file (inputMesh.ply) with input triangular mesh
# 	-o -outFile ply file (output_controlMesh.ply) with the control mesh of the Catmull-Clark subdivision surface

mesh2SDS/mesh2sds -n_controlPoints 1000 -lp 1.3 -a 1e-1 -b 1e-1 -maxIt 500 -solverIt 10 -eps 1e-3 -viz 1001 -pointsFile cat0.ply -outFile cat0_SDS.ply


# fit a subdivision surface to an input mesh
# example run: mesh2SDS/points2sds -n_controlPoints n -lp 1.3 -a alpha -b beta -oLevel -BBExp 1.2 -maxIt max_number_of_MM_iterations -solverIt max_number_of_CG_iterations -eps epsilon -pointsFile inputPointcloud.ply -outFile output_controlMesh.ply
# input parameters: 	
#	-n or -n_controlPoints: number of vertices in the control mesh of the subdivision surface
#	-lp model paremeter q (power used to define the robust norms, usually set to 1 or 1.3)
#	-a model parameter alpha (weight of normal term)
#	-b model parameter beta (regularizer)
#	-eps optimization parameter epsilon used in the definition of the MM majorizer	
#	-oLevel depth of the octree used in Poisson reconstruction to initialize the surface fit
#	-BBexp	expansion factor used on the range of points to define the bounding-box split by the octree of Poisson reconstruction
#	-maxIt maximum number of MM iterations
#	-solverIt maximum number of conjugate-gradients iterations used to solve the sparse linear system of equations, if set to 0 it uses a sparse LU decomposition to solve the system of equations
#	-p -pointsFile ply file (inputMesh.ply) with input pointcloud
# 	-o -outFile ply file (output_controlMesh.ply) with the control mesh of the Catmull-Clark subdivision surface

mesh2SDS/points2sds -lp 1.3 -a 1e-2 -b 1e-2 -oLevel 7 -eps 1e-3 -BBexp 1.2 -n_controlPoints 1500 -viz 1001 -pointsFile head.ply -outFile sds_head.ply 



# compute the mass and stiffness matrices with qxq quadrature rule (it allows for 3x3, 4x4, and 5x5)
# example run: ./computeLaplaceBeltrami -quad q -sds inputControlMesh.ply -o outFile
# input parameters: 	
#	-quad q (q=3,4, or 5)
# 	-pFile ply file (inputControlMesh.ply) with the control mesh of the Catmull-Clark subdivision surface
# 	-oFile name of output file: the matrices are output as textfiles with name [outFile]_massMatrix.txt [outFile]_stiffnessMatrix.txt
# 		the sparse matrices are described by their size (first line) and triplets (one for each remaining line in the txt file)
# 		1st line: number_of_non_zeros n_rows n_cols
# 		for the rest of lines: row col value

SDSutils/computeLaplaceBeltrami -quad 4 -sds cat0_SDS.ply -o cat0_SDS


# given the control mesh of a subdivision surface, subdivide it n times
# example run: ./subdivide -uniR n inputControlMesh.ply outputSubdividedMesh.obj
# input parameters: 	
#	int n number of subdivisions of the control mesh
# 	-pFile ply file (inputControlMesh.ply) with the control mesh of the Catmull-Clark subdivision surface
# 	-oFile name of output file (.off, .obj, or .ply)

SDSutils/subdivide cat0_SDS.ply -uniR 3 cat0_SDS_ref3.obj



# compute the approximate geodesic from a point p with the Heat method of Crane et.al
# the technique first estimates the diffusion of a heat source at point p and then solve a Poisson equation
# the mass and stiffness matrices used for the diffusion and Poisson equations are computed by qxq quadrature aproximation
# example run: ./getGeodesic -quad q -point 0 0 0 inputControlMesh.ply outFile
# input parameters: 	
#	-quad q (q=3,4, or 5) quadrature rule
#	-point x y z coordinates of source point
#	-source index of the vertex used as source if -p point is not defined
#	-solverIt maximum number of conjugate-gradients iterations used to solve the sparse linear system of equations, if set to 0 it uses a sparse LU decomposition to solve the system of equations
# 	ply file (inputControlMesh.ply) with the control mesh of the Catmull-Clark subdivision surface
# 	-oFile the code outputs the geodesic (and its level lines) as a colored map on the subdivision surface, this is approximated by #	subdivising 3-times the control mesh and projecting the geodesic color on it to [outFile]_geodesic.obj

geodesicsInHeat/getGeodesic -quad 4 -source 1 -solverIt 1000 -inFile cat0_SDS.ply -outFile cat0_SDS







MAC-OS:

You can use linux instructions by using home-brew to install boost, eigen, and install OpenSubdiv with the following settings.

cmake -D NO_MAYA=1 -D NO_PTEX=1 -D NO_DOC=1 -D NO_OMP=1 -D NO_TBB=1 -D NO_CUDA=1 -D NO_OPENCL=1 -D NO_CLEW=1 -D GLFW_LOCATION=/usr/local/lib/ -DNO_EXAMPLES=0 -DNO_TUTORIALS=0 ..









WINDOWS: TBD



