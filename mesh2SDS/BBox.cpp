//
//  BBox.cpp
//  
//
//  Created by virginia estellers on 1/3/17.
//
//

#include "BBox.h"
// class constructor and functions
BoundingBox::BoundingBox(void)
{
    _min = std::vector<TypeReal>(3,0);
    _max = std::vector<TypeReal>(3,1.0);    
}

TypeReal BoundingBox::getSide(size_t i)
{
    return _max[i] - _min[i];
}
TypeReal BoundingBox::getMin(size_t i)
{
    return _min[i];
}
TypeReal BoundingBox::getMax(size_t i)
{
    return _max[i];
}
void BoundingBox::setMin(size_t i, TypeReal min)
{
    _min[i] = min;
}
void BoundingBox::setMax(size_t i, TypeReal max)
{
    _max[i] = max;
}





