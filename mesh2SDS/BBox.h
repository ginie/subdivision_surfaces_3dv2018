//
//  BBox.hpp
//  
//
//  Created by virginia estellers on 1/3/17.
//
//

#ifndef BBox_hpp
#define BBox_hpp

#include <stdio.h>
#include <vector>
#include "TypeDef.h"


class BoundingBox
{
private:
    std::vector<TypeReal> _min;
    std::vector<TypeReal> _max;
public:
    BoundingBox( const BoundingBox &other) : _min(other._min), _max(other._max) {}
    BoundingBox(void);
    TypeReal getSide(size_t i);
    TypeReal getMin(size_t i);
    TypeReal getMax(size_t i);
    void setMin(size_t i, TypeReal min);
    void setMax(size_t i, TypeReal max);
};



#endif /* BBox_hpp */
