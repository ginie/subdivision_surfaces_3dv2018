#include <utility>
#include <unordered_map>
#include <boost/multi_array.hpp>


#include "TypeDef.h"
#include "Settings.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "kdtree2.h"
#include "optimizationSubdivisionSurface.h"


bool ADD_METRIC = true;
TypeReal epsilon_a = 1e-6;
TypeReal epsilon_r = 1e-6;



void graph_Differences( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, SpMat &regMat, SpMat &regOp, bool diagonals)
{
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    //printf("computing graph Laplacian for control vertices\n");
    // adjacency matrix
    std::vector<size_t> AdjRows;
    std::vector<size_t> AdjCols;
    std::vector<TypeReal> AdjVals;
    size_t count = 0;
    size_t count_row = 0;
    TypeReal edge_weights[3] = { 1.0, (TypeReal)std::sqrt(0.5), 1.0};
    for( size_t iface=0; iface<n_faces; iface++ )
    {
        for(int ivertex=0; ivertex<face_nvertex[iface]; ivertex++)
        {
            size_t vertex1 = face_vertex[count+ivertex];
            if(diagonals)
            {
            	int iedge = 0;
                for(int jvertex=ivertex+1; jvertex<face_nvertex[iface]; jvertex++)
                {
                    size_t vertex2 = face_vertex[count+jvertex];                  
                    // each row compute the difference between two neighbours
                    for( size_t idim=0; idim<3; idim++)
                    {
                        AdjCols.push_back( vertex1*3 + idim);
                        AdjVals.push_back( 1.0 );
                       // AdjVals.push_back( edge_weights[iedge] );
                        AdjRows.push_back( count_row*3 + idim);
                        AdjCols.push_back( vertex2*3 + idim);
                        AdjVals.push_back( -1.0 );
                        //AdjVals.push_back( -edge_weights[iedge] );
                        AdjRows.push_back( count_row*3 + idim);
                    }
                    count_row += 1;
                    iedge += 1;
                }
            }
            else
            {
                size_t offset = (ivertex+1)%face_nvertex[iface];
                size_t vertex2 = face_vertex[count+offset];
                // each row compute the difference between two neighbours
                for( size_t idim=0; idim<3; idim++)
                {
                    AdjCols.push_back( vertex1*3 + idim);
                    AdjVals.push_back( 1.0 );
                    AdjRows.push_back( count_row*3 + idim);
                    AdjCols.push_back( vertex2*3 + idim);
                    AdjVals.push_back( -1.0 );
                    AdjRows.push_back( count_row*3 + idim);	
                }			
                count_row += 1;
            }
        }	
        count += face_nvertex[iface];
    }	
    
    regOp = SpMat(count_row*3, 3*n_vertex);
    build_spsMatrix(AdjRows, AdjCols, AdjVals, regOp);
    regMat = regOp.transpose()*regOp;
}


void graph_Differences1d( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, SpMat &regMat, SpMat &regOp, bool diagonals)
{
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    //printf("computing graph Laplacian for control vertices\n");
    // adjacency matrix
    std::vector<size_t> rows;
    std::vector<size_t> cols;
    std::vector<TypeReal> vals;
    size_t count = 0;
    size_t count_row = 0;
    TypeReal edge_weights[3] = { 1.0, (TypeReal)std::sqrt(0.5), 1.0};
    for( size_t iface=0; iface<n_faces; iface++ )
    {
        for(int ivertex=0; ivertex<face_nvertex[iface]; ivertex++)
        {
            size_t vertex1 = face_vertex[count+ivertex];
            if(diagonals)
            {
            	int iedge = 0;
                for(int jvertex=ivertex+1; jvertex<face_nvertex[iface]; jvertex++)
                {
                    size_t vertex2 = face_vertex[count+jvertex];
                    rows.push_back(count_row);
                    cols.push_back(vertex1);
                    vals.push_back(1.0);
                    rows.push_back(count_row);
                    cols.push_back(vertex2);
                    vals.push_back(-1.0);                    
                    count_row += 1;
                    iedge += 1;
                }
            }
            else
            {
                size_t offset = (ivertex+1)%face_nvertex[iface];
                size_t vertex2 = face_vertex[count+offset];
				rows.push_back(count_row);
				cols.push_back(vertex1);
				vals.push_back(1.0);
				rows.push_back(count_row);
				cols.push_back(vertex2);
				vals.push_back(-1.0); 			
                count_row += 1;
            }
        }	
        count += face_nvertex[iface];
    }	
    
    regOp = SpMat(count_row, n_vertex);
    build_spsMatrix(rows, cols, vals, regOp);
    regMat = regOp.transpose()*regOp;
}






void KdSearch_closestPointValue( std::vector<TypeReal> &points, std::vector<TypeReal> &point_values, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &vertex_values)
{

    size_t n_points = points.size()/3;
    size_t n_splat = 3;
    size_t n_faces = face_nvertex.size();
    size_t n_vertex = vertices.size()/3;
    size_t dim_values = point_values.size()/n_points;
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    // create search data structures
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_vertex][3]);
	for(int ii=0; ii<n_vertex; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = vertices[ii*3+idim];
		}
	}
	// notice it is in C-standard layout. 
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
    tree = new kdtree2::KDTree(KdTreePoints,false); 
    tree->sort_results = true;
    
    std::vector<TypeReal> vertex_weights(n_vertex, 0);
    vertex_values = std::vector<TypeReal>( n_vertex* dim_values, 0);
    for( size_t ii=0; ii< n_points; ii++ )
    {
    	// for each point, find the closest surface sample
    	std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = points[ii*3+idim];
		}
		tree->n_nearest(query, n_splat, KdtreeRes);
		for( int jj=0; jj<n_splat; jj++)
		{
			int vertexID = KdtreeRes[jj].idx;
			TypeReal weight = std::exp(- KdtreeRes[jj].dis );
			vertex_weights[ vertexID ] += weight;
			for(int idim=0; idim<dim_values; idim++)
			{
				vertex_values[ vertexID + idim* n_vertex ] +=  weight * point_values[ ii + idim*n_points ];
			}
		}		
    }
    
    for(int ii=0; ii<n_vertex; ii++)
	{
		if( vertex_weights[ii] <= std::numeric_limits<TypeReal>::epsilon() ){ continue; }
		for (int idim=0; idim<dim_values; idim++)
		{
			vertex_values[ ii+idim* n_vertex ] /= vertex_weights[ii];
		}
	}
    

}






TypeReal KdSearch_closestParameter2Surface( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params)
{

    size_t n_points = points.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t n_vertex = vertices.size()/3;
    // Generate uniform samples on each face
    size_t s_samples = 5;
    size_t t_samples = 5;
    size_t n_samples = s_samples * t_samples * n_faces;
    std::vector<int> Sf_params;
    std::vector<TypeReal> Ss_params, St_params;
    UniformParameterSample( n_faces, s_samples, t_samples, Sf_params, Ss_params, St_params);
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    // sample points (and normals) of the surface
    std::vector<TypeReal> Spoints, Ss, St;
    SampleLimitSurface(vertices, face_nvertex, face_vertex, Sf_params, Ss_params, St_params, Spoints, Ss, St);

    // create search data structures
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_samples][3]);
	for(int ii=0; ii<n_samples; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = Spoints[ii*3+idim];
		}
	}
	// notice it is in C-standard layout. 
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
    tree = new kdtree2::KDTree(KdTreePoints,false); 
    tree->sort_results = true;
    
    TypeReal energy_data = 0;
    f_params.resize(n_points);
    s_params.resize(n_points);
    t_params.resize(n_points);
    for( size_t ii=0; ii< n_points; ii++ )
    {
    	// for each point, find the closest surface sample
    	std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = points[ii*3+idim];
		}
		tree->n_nearest(query, 2, KdtreeRes);
		size_t c0sample = KdtreeRes[0].idx;	
		size_t c1sample = KdtreeRes[1].idx;
		TypeReal c = 0;
		if(  Sf_params[c0sample] == Sf_params[c1sample] )
		{
			TypeReal ip0 = 0;
			TypeReal ip1 = 0;
			for(int idim=0; idim<3; idim++)
			{
				TypeReal s0p = points[ii*3+idim] - Spoints[c0sample*3+idim];
				TypeReal s1p = points[ii*3+idim] - Spoints[c1sample*3+idim];
				TypeReal s0s1 = Spoints[c1sample*3+idim] - Spoints[c0sample*3+idim];
				ip0 += s0p * s0s1;
				ip1 += s1p * (-s0s1);
			}
			if( (ip0 > 0) && (ip1>0) )
			{
				c = ip0/std::max( ip0+ip1, eps );
			}			
		}
		f_params[ii] = Sf_params[c0sample];
		s_params[ii] = (1-c) * Ss_params[c0sample] + c * Ss_params[c1sample];
		t_params[ii] = (1-c) * St_params[c0sample] + c * St_params[c1sample]; 
        // in the first case, this is only an upper bound
        energy_data += std::pow(points[ii*3] - Spoints[c0sample*3],2) + std::pow(points[ii*3+1] - Spoints[c0sample*3+1],2) + std::pow(points[ii*3+2] - Spoints[c0sample*3+2],2);
        

    }
    energy_data;
    //printf("energy_data by search %f\n", energy_data);
    return energy_data;
}

// pointcloud with variables
TypeReal KdSearch_closestParameter2Surface( std::vector<TypeReal> &points, std::vector<TypeReal> &pointvars, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params, std::vector<TypeReal> &vertexvars)
{

    size_t n_points = points.size()/3;
    size_t dimvars = pointvars.size()/n_points;
    size_t n_faces = face_nvertex.size();
    size_t n_vertex = vertices.size()/3;
    // Generate uniform samples on each face
    size_t s_samples = 5;
    size_t t_samples = 5;
    size_t n_samples = s_samples * t_samples * n_faces;
    std::vector<int> Sf_params;
    std::vector<TypeReal> Ss_params, St_params;
    UniformParameterSample( n_faces, s_samples, t_samples, Sf_params, Ss_params, St_params);
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    // sample points (and normals) of the surface
    std::vector<TypeReal> Spoints, Ss, St;
    SampleLimitSurface(vertices, face_nvertex, face_vertex, Sf_params, Ss_params, St_params, Spoints, Ss, St);

    // create search data structures
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_samples][3]);
	for(int ii=0; ii<n_samples; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = Spoints[ii*3+idim];
		}
	}
	// notice it is in C-standard layout. 
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
    tree = new kdtree2::KDTree(KdTreePoints,false); 
    tree->sort_results = true;
    
    TypeReal energy_data = 0;
    f_params.resize(n_points);
    s_params.resize(n_points);
    t_params.resize(n_points);
    for( size_t ii=0; ii< n_points; ii++ )
    {
    	// for each point, find the closest surface sample
    	std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = points[ii*3+idim];
		}
		tree->n_nearest(query, 2, KdtreeRes);
		size_t c0sample = KdtreeRes[0].idx;	
		size_t c1sample = KdtreeRes[1].idx;
		TypeReal c = 0;
		if(  Sf_params[c0sample] == Sf_params[c1sample] )
		{
			TypeReal ip0 = 0;
			TypeReal ip1 = 0;
			for(int idim=0; idim<3; idim++)
			{
				TypeReal s0p = points[ii*3+idim] - Spoints[c0sample*3+idim];
				TypeReal s1p = points[ii*3+idim] - Spoints[c1sample*3+idim];
				TypeReal s0s1 = Spoints[c1sample*3+idim] - Spoints[c0sample*3+idim];
				ip0 += s0p * s0s1;
				ip1 += s1p * (-s0s1);
			}
			if( (ip0 > 0) && (ip1>0) )
			{
				c = ip0/std::max( ip0+ip1, eps );
			}			
		}
		f_params[ii] = Sf_params[c0sample];
		s_params[ii] = (1-c) * Ss_params[c0sample] + c * Ss_params[c1sample];
		t_params[ii] = (1-c) * St_params[c0sample] + c * St_params[c1sample]; 
        // in the first case, this is only an upper bound
        energy_data += std::pow(points[ii*3] - Spoints[c0sample*3],2) + std::pow(points[ii*3+1] - Spoints[c0sample*3+1],2) + std::pow(points[ii*3+2] - Spoints[c0sample*3+2],2);
        

    }
    // transfer pointvars to vertices of closest face
    // create search data structures
	boost::multi_array<TypeReal,2> KdTreeVertices(boost::extents[n_points][3]);
	for(int ii=0; ii<n_points; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreeVertices[ii][idim] = points[ii*3+idim];
		}
	}
	// notice it is in C-standard layout. 
	size_t n_closestPoint = 5;
    tree = new kdtree2::KDTree(KdTreeVertices,false); 
    tree->sort_results = true;
    vertexvars.resize(n_vertex*dimvars);
    for( size_t ii=0; ii< n_vertex; ii++ )
    {
    	// for each point, find the closest surface sample
    	std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = vertices[ii*3+idim];
		}
		tree->n_nearest(query, n_closestPoint, KdtreeRes);
		std::vector<int> closestPoint(n_closestPoint);
		std::vector<int> pweights(n_closestPoint);
		TypeReal sum = 0;
		for(int jj=0; jj<n_closestPoint; jj++)
		{
			closestPoint[jj] = KdtreeRes[jj].idx;	
			pweights[jj] = std::exp( - KdtreeRes[jj].dis );
			sum += pweights[jj];
		}
		for(int jj=0; jj<n_closestPoint; jj++)
		{
			 pweights[jj] /= sum;
			 for(int idim=0; idim<dimvars; idim++)
			 {
			 	 vertexvars[ii + n_vertex*idim] = pweights[jj] * pointvars[ closestPoint[jj] + n_points*idim ];
			 }
		}	
	}
    energy_data;
    //printf("energy_data by search %f\n", energy_data);
    return energy_data;
}

       

void refinedMeshGeometry( std::vector<TypeReal> &points, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &vertex_nring, std::vector<int> &vertex_0ring, std::vector<int> &vertex_ring, std::vector<int> &vertex_nfaces, std::vector<int> &vertex_0face, std::vector<int> &vertex_face, std::vector<TypeReal> &principalK1, std::vector<TypeReal> &principalK2, std::vector<TypeReal> &principalD1, std::vector<TypeReal> &principalD2, std::vector<TypeReal> &principalD3, std::vector<TypeReal> &metric)
{

	size_t n_points = points.size()/3;
	principalK1.resize(n_points);
	principalD1.resize(n_points*3);
	principalK2.resize(n_points);
	principalD2.resize(n_points*3);
	principalD3.resize(n_points*3);// normal direction
	metric.resize(n_points*3);
	for(int ipt=0; ipt<n_points; ipt++)
	{
		// for each point, use the vertex and its neighbourhood to estimate the geometry of the surface
		int n_neigh = vertex_nring[ipt] + 1;
		std::vector<size_t> closestP( n_neigh );
		closestP[0] = ipt;
		int k0 = vertex_0ring[ipt];
		for( int k=1; k<n_neigh; k++)
		{
			closestP[k] =  vertex_ring[ k0 + k - 1];
		}		
		
		
		// compute area of faces around vertex
		k0 = vertex_0face[ipt];
		for( int k=0; k<vertex_nfaces[ipt]; k++)
		{
			int iface =  vertex_face[ k0 + k];
			TypeReal v01[3];
			TypeReal v03[3];
			for(int idim=0; idim<3; idim++) // all the refined faces are quads
			{
				v01[idim] = points[ face_vertex[4*iface+1]*3 + idim] - points[ face_vertex[4*iface]*3 + idim];
				v03[idim] = points[ face_vertex[4*iface+3]*3 + idim] - points[ face_vertex[4*iface]*3 + idim];
			}
			TypeReal crossP[3] = { v01[1]*v03[2] - v01[2]*v03[1], v01[2]*v03[0] - v01[0]*v03[2], v01[0]*v03[1] - v01[1]*v03[0] };
			
			metric[ipt] += 0.5*( std::pow(crossP[0], 2) + std::pow(crossP[1], 2) + std::pow(crossP[2], 2) ); 
		}		
		
		// find the centroid of each cluster
		TypeReal centroid[3] = {0,0,0};
		Eigen::MatrixXf neigh_points( n_neigh, 3);
		Eigen::MatrixXf centered_points( n_neigh, 3);
		//printf("closests points to %f,%f,%f are:", points[ipt*3], points[ipt*3+1], points[ipt*3+2]);
		for( int k=0; k<n_neigh; k++)
		{
			for(int idim=0; idim<3; idim++)
			{
				neigh_points(k,idim) = points[ closestP[k]*3+idim ];
				centroid[idim] += neigh_points(k,idim);
			}
			//printf("\t %f %f %f at distance %f\n", points[ closestP[k]*3+0 ], points[ closestP[k]*3+1 ], points[ closestP[k]*3+2 ], distance2P[k] );
		}
		for(int idim=0; idim<3; idim++){ centroid[idim] /= n_neigh; }
    
		// computer the covariance matrix
		Eigen::Matrix3f cov =  Eigen::Matrix3f::Zero();
		for( int k=0; k<n_neigh; k++)
		{
			Eigen::Vector3f aux;
			for(int idim=0; idim<3; idim++)
			{
				aux[idim] = (centroid[idim] - neigh_points(k,idim) );
				centered_points(k,idim) = neigh_points(k,idim) - centroid[idim];
			}
			cov += aux *(aux.transpose());
		}    
    
		// find principal components
		Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigensolver(cov);
		if (eigensolver.info() != Eigen::Success) abort();
		Eigen::Vector3f eigVals = eigensolver.eigenvalues();
		// sort eigenValues
		std::vector<int> map(3);
		for(int idim=0; idim<3; idim++){map[idim] = idim;}
		std::sort( map.begin(), map.end(), [&](int x,int y){ return eigVals[x]<eigVals[y];} );
		//matrix whose columns are the eigenvectors.
        Eigen::Matrix3f eigVects = eigensolver.eigenvectors();
        for(int idim=0; idim<3; idim++)
		{
			principalD1[ipt*3+idim] = eigVects(idim,map[2]);
			principalD2[ipt*3+idim] = eigVects(idim,map[1]);
		}
		principalD3[ipt*3] = principalD1[ipt*3+1]*principalD2[ipt*3+2] - principalD1[ipt*3+2]*principalD2[ipt*3+1];
		principalD3[ipt*3+1] = principalD1[ipt*3+2]*principalD2[ipt*3+0] - principalD1[ipt*3+0]*principalD2[ipt*3+2];
		principalD3[ipt*3+2] = principalD1[ipt*3+0]*principalD2[ipt*3+1] - principalD1[ipt*3+1]*principalD2[ipt*3+0];

    	// project points and fit a parabola
    	Eigen::MatrixXf neigh_projection = centered_points * eigVects;
    	TypeReal A11 = 0;
    	TypeReal A12 = 0;
    	TypeReal A22 = 0;
    	TypeReal b1 = 0; TypeReal b2 = 0;
    	for( int k=0; k<n_neigh; k++)
		{
			TypeReal x = neigh_projection(k,map[2]);
			TypeReal y = neigh_projection(k,map[1]);
			TypeReal z = neigh_projection(k,map[0]);			
			A11 += std::pow(x,4);
			A12 += std::pow(x*y,2);
			A22 += std::pow(y,4);
			b1 += std::pow(x,2) * z;
			b2 += std::pow(y,2) * z;
		} 
		
		// estimate principal curvatures solving system A*k = b
		TypeReal idetA = 1.0/( A11*A22 - std::pow(A12,2) );
    	principalK1[ipt] = 2*std::abs( idetA* ( A22* b1 - A12*b2) );
    	principalK2[ipt] = 2*std::abs( idetA* ( -A12* b1 + A11*b2) );
    	

    	
    	if( std::isnan(principalK1[ipt]) || std::isnan(principalK2[ipt]) || std::isnan(principalD1[ipt*3]) || std::isnan(principalD1[ipt*3+1]) || std::isnan(principalD1[ipt*3+2]) || std::isnan(principalD2[ipt*3]) || std::isnan(principalD2[ipt*3+1]) || std::isnan(principalD2[ipt*3+2]) )
    	{
    		//printf("nan in refined point %ipt k1 = %f, d1 = %f %f %f, k2= %f, d2 = %f %f %f\n", principalK1[ipt], principalD1[ipt*3], principalD1[ipt*3+1], principalD1[ipt*3+2], principalK2[ipt], principalD2[ipt*3], principalD2[ipt*3+1], principalD2[ipt*3+2] );
    	    principalK1[ipt] = 0;
    		principalD1[ipt*3] = 0;
    		principalD1[ipt*3+1] = 0;
    		principalD1[ipt*3+2] = 0;
    		principalK2[ipt] = 0;
    		principalD2[ipt*3] = 0;
    		principalD2[ipt*3+1] = 0;
    		principalD2[ipt*3+2] = 0;
    		principalD3[ipt*3] = 0;
    		principalD3[ipt*3+1] = 0;
    		principalD3[ipt*3+2] = 0;    		
    	}
    }	
	
}


TypeReal KdSearch_closestRefinedVertex( std::vector<TypeReal> &points, std::vector<TypeReal> refinedVertices, std::vector<int> &closestRvertex, SpMat &project2closestVertex)
{
    size_t n_points = points.size()/3;
    size_t n_rvertex = refinedVertices.size()/3;
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    // create search data structures
    boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_rvertex][3]);
	for(int ii=0; ii<n_rvertex; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = refinedVertices[ii*3+idim];
		}
	}
	// notice it is in C-standard layout. 
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
    tree = new kdtree2::KDTree(KdTreePoints,false); 
    tree->sort_results = true;

    TypeReal energy_data = 0;
    std::vector<size_t> rows, cols;
    std::vector<TypeReal> vals(n_points*3, 1.0);
    closestRvertex = std::vector<int>(n_points);
    for( size_t ii=0; ii< n_points; ii++ )
    {
    	// for each point, find the closest surface sample
    	std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = points[ii*3+idim];
		}
		tree->n_nearest(query, 1, KdtreeRes);
		size_t c0sample = KdtreeRes[0].idx;	
		closestRvertex[ii] = (int) c0sample;
		for(size_t idim=0; idim<3; idim++){
			rows.push_back( ii*3 + idim);
			cols.push_back( c0sample*3 + idim);
		}
        // in the first case, this is only an upper bound
        energy_data += std::pow(points[ii*3] - refinedVertices[c0sample*3],2) + std::pow(points[ii*3+1] - refinedVertices[c0sample*3+1],2) + std::pow(points[ii*3+2] - refinedVertices[c0sample*3+2],2);

    }
    energy_data;
    
    project2closestVertex = SpMat( n_points*3, n_rvertex*3 );
	build_spsMatrix( rows, cols, vals, project2closestVertex);
    
    return energy_data;
}







void vertex_ring(const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &vertex_nring, std::vector<int> &vertex_0ring, std::vector<int> &vertexring, std::vector<int> &vertex_nfaces, std::vector<int> &vertex_0face, std::vector<int> &vertex_face )
{
	int n_faces = face_nvertex.size();
	int n_vertex = *std::max_element(face_vertex.begin(), face_vertex.end());
	n_vertex += 1;
	if( n_vertex*3 != vertices.size() ){ printf("error computing n_vertex\n");}
	
	//traverse the faces
	std::vector< std::vector<int> > vertexfaces( n_vertex, std::vector<int>(0) );
	std::vector<int> face_0vertex(n_faces);
	int count = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		// traverse the vertices of each face
		face_0vertex[ii] = count;
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			int ivertex = face_vertex[count];
			vertexfaces[ ivertex ].push_back( ii );
			count += 1;
		}
	}
	
	// for each vertex accumulate the vertices of the faces incident to the vertex
	// find uniques to define the ring
	vertexring.clear();
	vertex_nring = std::vector<int>(n_vertex, 0);
	vertex_0ring = std::vector<int>(n_vertex, 0);
	vertex_nfaces.resize(n_vertex);
	vertex_0face.resize(n_vertex);
	vertex_face.clear();
	count = 0;
	for(int ii=0; ii< n_vertex; ii++)
	{
		std::vector<int > vring;
		std::vector<int> ifaces = vertexfaces[ii];
		vertex_nfaces[ii] = ifaces.size();
		vertex_0face[ii] = count;
		// traverse the faces iniciden to the vertex
		for(int jj=0; jj< vertexfaces[ii].size(); jj++)
		{
			int iface =  ifaces[jj];
			vertex_face.push_back( iface);
			int iv0 = face_0vertex[iface];
			for(int kk=0; kk<face_nvertex[iface]; kk++)
			{
				int iv = face_vertex[iv0 + kk];
				if(iv != ii)
				{
					vring.push_back( iv );
				}
			}
			count += 1;
		}
		// find uniques
		std::sort(vring.begin(), vring.end() );
		auto end = std::unique(vring.begin(), vring.end() );
		vertex_0ring[ii] = vertexring.size();
		vertex_nring[ii] = std::distance( vring.begin(), end) ;
		vertexring.insert(vertexring.end(), vring.begin(), end);
			
	}
}



/*TypeReal find_closest_Spoint(const std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const std::vector<int> &adjacency, const std::vector<int> &changeCoord, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, std::vector<TypeReal>& closestSpoint, size_t max_iter = 10)
{
    // evaluate the residual
    size_t n_vertex = vertices.size()/3;
    size_t n_points = points.size()/3;
    size_t n_faces = face_nvertex.size();
    
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
    	//printf("point %d sample at %d/%d, %f %f\n", ipoint, fparams[ipoint], n_faces, sparams[ipoint], tparams[ipoint] );
    	if( std::isnan(fparams[ipoint]) ) { printf("nan fparams %lu\n", ipoint); }
    	if( std::isnan(sparams[ipoint]) ) { printf("nan sparams %lu\n", ipoint); }
    	if( std::isnan(tparams[ipoint]) ) { printf("nan tparams %lu\n", ipoint); }
    	if( (fparams[ipoint]>=n_faces) || (fparams[ipoint]<0) ) { printf("fparams %lu outside domain\n", ipoint); }
    	if( (sparams[ipoint]> 1.0) || (sparams[ipoint]<0) ) { printf("sparams %lu outside domain\n", ipoint); }
    	if( (tparams[ipoint]>1.0) || (tparams[ipoint]<0) ) { printf("tparams %lu outside domain\n", ipoint); }
    }
    
    //printf("%d vertex, %d points, size params %d %d %d", n_vertex, n_points, fparams.size(), sparams.size(), tparams.size() );
    std::vector<int> f_(fparams);
    std::vector<TypeReal> s_(sparams);
    std::vector<TypeReal> t_(tparams);
    std::vector<TypeReal> lambda( n_points, 2e0 );
    std::vector<TypeReal> S, Ss, St;
    SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
    std::vector<TypeReal> energy(n_points,  0 );
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
    	for(int idim=0; idim<3; idim++)
    	{
    		energy[ipoint] += std::pow(points[ipoint*3+idim] - S[ipoint*3+idim],2);
    	}
    }
    std::vector<bool> stop( n_points, false );
    std::vector<TypeReal> Js(n_points*3);
    std::vector<TypeReal> Jt(n_points*3);
    std::vector<TypeReal> Hss(n_points);
    std::vector<TypeReal> Htt(n_points);
    std::vector<TypeReal> Hst(n_points);
    for( size_t iter=0; iter< max_iter; iter++ )
    {
        // sample the surface
        if(iter>0)
        {
        	SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
        }
        for (size_t ipoint=0; ipoint< n_points; ipoint++)
        {
            if(stop[ipoint]==false)
            {
                TypeReal resx = points[ipoint*3] - S[ipoint*3];
                TypeReal resy = points[ipoint*3+1] - S[ipoint*3+1];
                TypeReal resz = points[ipoint*3+2] - S[ipoint*3+2];
                
                // update damping factor
                TypeReal energy_point = resx*resx + resy*resy + resz*resz;
                if( (energy[ipoint] >= energy_point) || (iter==0) )
                {
                    // energy decreases, so update the fotnote parameters and Hessian approximation for the next iteration
                    lambda[ipoint] = 0.5*lambda[ipoint];
                    s_[ipoint] = sparams[ipoint];
                    t_[ipoint] = tparams[ipoint];
                    f_[ipoint] = fparams[ipoint];
                    energy[ipoint] = energy_point;
                    // compute the MINUS gradient of f w.r.t parameters (s,t)
                    Js[ipoint] = Ss[ipoint*3] * resx + Ss[ipoint*3+1] * resy + Ss[ipoint*3+2] * resz;
                    Jt[ipoint] = St[ipoint*3] * resx + St[ipoint*3+1] * resy + St[ipoint*3+2] * resz;
                    // cheack stopping criterion
                    if( Js[ipoint]*Js[ipoint] + Jt[ipoint]*Jt[ipoint] < 1e-6* energy_point )
                    {
                        stop[ipoint] = true;
                        //printf("stopped point %lu at iteration %lu with squared distance %f and normGrad %f\n", ipoint, iter, energy_point, Js*Js + Jt*Jt );
                    }
                    Hss[ipoint] = Ss[ipoint*3]*Ss[ipoint*3] + Ss[ipoint*3+1]*Ss[ipoint*3+1] + Ss[ipoint*3+2]*Ss[ipoint*3+2];
                    Hst[ipoint] = Ss[ipoint*3]*St[ipoint*3] + Ss[ipoint*3+1]*St[ipoint*3+1] + Ss[ipoint*3+2]*St[ipoint*3+2];
                    Htt[ipoint] = St[ipoint*3]*St[ipoint*3] + St[ipoint*3+1]*St[ipoint*3+1] + St[ipoint*3+2]*St[ipoint*3+2];
                    
                }else
                {
                    // try smaller damping factor, the Hessian approximation remains the previous one but the linear system changes
                    sparams[ipoint] = s_[ipoint];
                    tparams[ipoint] = t_[ipoint];
                    fparams[ipoint] = f_[ipoint];
                    lambda[ipoint] = lambda[ipoint]*2;
                }
                // solve system
                TypeReal Hss_k = (1+ lambda[ipoint]) * Hss[ipoint];
                TypeReal Htt_k = (1+ lambda[ipoint]) * Htt[ipoint];
                TypeReal Hst_k = Hst[ipoint];
                TypeReal detH = Hss_k*Htt_k - Hst_k*Hst_k;
                TypeReal idetH = std::abs(detH)>1e-12 ? 1.0/detH : 1.0;
                // solve linear system to obtain descent direction (qs, qt)
                TypeReal qs = idetH* ( Htt_k*Js[ipoint] - Hst_k*Jt[ipoint] );
                TypeReal qt = idetH* ( Hss_k*Jt[ipoint] - Hst_k*Js[ipoint] );
                // update
                sparams[ipoint] = sparams[ipoint]+ qs;
                tparams[ipoint] = tparams[ipoint]+ qt;
                reparametrize_sample( adjacency, changeCoord, fparams, sparams, tparams, ipoint );
                //printf("sample assigned point %f %f in face %d\n", sparams[ipoint], tparams[ipoint], fparams[ipoint]);
            }
        }
        //printf("newton iteration %d/%d finished\n", iter, max_iter);
    }
    
    // output the closest point in the surface
    SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, closestSpoint, Ss, St);
    //printf("return the closest point in the surface\n");

    TypeReal squared_distance = 0;
    for(size_t ipoint=0; ipoint< n_points; ipoint++){
        squared_distance += std::pow(  points[ipoint*3] - closestSpoint[ipoint*3], 2 ) + std::pow(  points[ipoint*3+1] - closestSpoint[ipoint*3+1], 2 ) + std::pow(  points[ipoint*3+2] - closestSpoint[ipoint*3+2], 2 );
    }
    //printf("energy by optimization %f\n", squared_distance);
    return squared_distance;
}*/




void estimate_diff2metrics( TypeReal *Ss, TypeReal *St, TypeReal *Sss, TypeReal *Sst, TypeReal *Stt, TypeReal *normals, TypeReal *metric, TypeReal *meanCurvature, TypeReal *principalCurvature1, TypeReal *principalCurvature2, TypeReal *principalDirection1, TypeReal *principalDirection2, size_t n_samples )
{
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    for (int ii=0; ii< n_samples; ii++)
    {
        TypeReal n[3];
        // compute normal direction
        n[0] = Ss[ii*3+1]*St[ii*3+2] - Ss[ii*3+2]*St[ii*3+1];
        n[1] = Ss[ii*3+2]*St[ii*3] - Ss[ii*3]*St[ii*3+2];
        n[2] = Ss[ii*3]*St[ii*3+1] - Ss[ii*3+1]*St[ii*3];
        TypeReal inorm = ( std::pow(n[0],2) + std::pow(n[1],2) + std::pow(n[2],2) );
        if( inorm< 1e-16 )
        {
            inorm = 1;
        }else{ inorm = 1.0/std::sqrt(inorm);}
        TypeReal E, F, G; E = 0; F = 0; G = 0;
        TypeReal e, f, g; e = 0; f = 0; g = 0;
        for(int idim=0; idim<3; idim++)
        {
            // normalize it
            n[idim] *= inorm;
            normals[ii*3+idim] = n[idim];
            // compute first fondamental form I
            //[  s_uu \dot n   S_uv \dot n ] = [ E F ]
            //[  s_vu \dot n   S_vv \dot n ]   [ F G ]
            E += Ss[ii*3+idim]*Ss[ii*3+idim];
            F += Ss[ii*3+idim]*St[ii*3+idim];
            G += St[ii*3+idim]*St[ii*3+idim];
            // compute second fondamental form II
            //[  s_uu \dot n   S_uv \dot n ] = [ e f ]
            //[  s_vu \dot n   S_vv \dot n ]   [ f g ]
            e += n[idim]*Sss[ii*3+idim];
            f += n[idim]*Sst[ii*3+idim];
            g += n[idim]*Stt[ii*3+idim];
        }
        // compute metric and curvatures
        metric[ii] = E * G - std::pow( F, 2 );
        if( std::isnan(metric[ii]) ==true){ printf("nan for metric at sample %d\n", ii);}
        //gCurvature[ii] = (f * g - std::pow( f, 2 ))/metric;
        meanCurvature[ii] = 0.5*( e * G + g * E - 2* f * F )/metric[ii];
        
        // compute the shape operator ( I^{-1} II ) and its eigen-decomposition
        TypeReal A11 = ( e * G - f * F )/std::max(metric[ii],eps);
        TypeReal A12 = ( f * G - g * F)/std::max(metric[ii],eps);
        TypeReal A21 = ( f * E - e * F )/std::max(metric[ii],eps);
        TypeReal A22 = ( g * E - f * F )/std::max(metric[ii],eps);
        TypeReal det = A11*A22 - A12 * A21;
        TypeReal trace = A11 + A22;
        TypeReal res = 0.25*std::pow(trace,2) - det;
        TypeReal k1 = 0.5*trace;
        TypeReal k2 = 0.5*trace;
        if(res>0)
        {
            k1 += std::sqrt(std::max(res, eps) );
            k2 -= std::sqrt(std::max(res, eps) );
        }
        TypeReal coef1[2] = {1.0, 0};
        TypeReal coef2[2] = {0.0, 1.0};
        if( (A21!= 0) && ( k1!= k2) ){
            coef1[0] = k1 - A22;
            coef1[1] = A21;
            coef2[0] = k2 - A22;
            coef2[1] = A21;
        }
        else if ((A12 != 0) && (k1!=k2))
        {
            coef1[1] = k1 - A11;
            coef1[0] = A12;
            coef2[1] = k2 - A11;
            coef2[0] = A12;
        }
        // compute principal radius
        principalCurvature1[ii] = k1;
        principalCurvature2[ii] = k2;
        if( std::isnan(k1) ==true){ printf("nan for k1 at sample %d\n", ii);}
        if( std::isnan(k2) ==true){ printf("nan for k2 at sample %d\n", ii);}
        TypeReal norm1 = 0;
        TypeReal norm2 = 0;
        
        // compute principal directions
        for(int idim=0; idim<3; idim++)
        {
            principalDirection1[ii*3+idim] = coef1[0] *Ss[ii*3+idim] + coef1[1] *St[ii*3+idim];
            principalDirection2[ii*3+idim] = coef2[0] *Ss[ii*3+idim] + coef2[1] *St[ii*3+idim];
            norm1 += std::pow( principalDirection1[ii*3+idim], 2);
            norm2 += std::pow( principalDirection2[ii*3+idim], 2);
        }
        norm1 = std::sqrt( std::max(norm1, eps) );
        norm2 = std::sqrt( std::max(norm2, eps) );
        // normalize them
        for(int idim=0; idim<3; idim++)
        {
            principalDirection1[ii*3+idim] /= std::max(norm1, eps);
            principalDirection2[ii*3+idim] /= std::max(norm2, eps);
        }
        if( (std::isnan(principalDirection1[ii*3]) ==true) || (std::isnan(principalDirection1[ii*3+1]) ==true) || (std::isnan(principalDirection1[ii*3+2]) ==true) ){ printf("nan for principal direction 1 at sample %d\n", ii);}
        if( (std::isnan(principalDirection2[ii*3]) ==true) || (std::isnan(principalDirection2[ii*3+1]) ==true) || (std::isnan(principalDirection2[ii*3+2]) ==true) ){ printf("nan for principal direction 2 at sample %d\n", ii);}
    }
}



void sample_diffmetrics( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const std::vector<int> &f_params, const std::vector<TypeReal> &s_params, const std::vector<TypeReal> &t_params, std::vector<TypeReal> &spoints, std::vector<TypeReal> &normals, std::vector<TypeReal> &metric, std::vector<TypeReal> &meanCurvature, std::vector<TypeReal> &principalRadius1, std::vector<TypeReal> &principalRadius2, std::vector<TypeReal> &principalDirection1, std::vector<TypeReal> &principalDirection2 )
{
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    size_t n_samples = f_params.size();
    std::vector<TypeReal> points, Ss, St, Sss, Sst, Stt;
    //printf("sampling surface and its 1st and second derivatives...");
    SampleD2LimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, points, Ss, St, Sss, Sst, Stt);
    //printf("done\n");
    spoints = points;
    normals.resize(n_samples*3);
    metric.resize(n_samples);
    meanCurvature.resize(n_samples);
    principalRadius1.resize(n_samples);
    principalRadius2.resize(n_samples);
    principalDirection1.resize(n_samples*3);
    principalDirection2.resize(n_samples*3);
    TypeReal k1, k2;
    for (int ii=0; ii< n_samples; ii++)
    {
        estimate_diff2metrics( &(Ss[ii*3]), &(St[ii*3]), &(Sss[ii*3]), &(Sst[ii*3]), &(Stt[ii*3]), &(normals[ii*3]), &(metric[ii]), &(meanCurvature[0]),&k1, &k2, &(principalDirection1[ii*3]), &(principalDirection2[ii*3]), 1 );
        principalRadius1[ii] = 1.0/k1;
        principalRadius2[ii] = 1.0/k2;
    }
}



void build_normalProjection( const std::vector<TypeReal> &normals, SpMat &Pn)
{
    size_t n_points = normals.size()/3;
    std::vector<size_t> Nrows(n_points*3);
    std::vector<size_t> Ncols(n_points*3);
    std::vector<TypeReal> Nvals(n_points*3);
    for( size_t ipoint=0; ipoint<n_points; ipoint++)
    {
        for(size_t idim=0; idim<3; idim++)
        {
            Nrows[ipoint*3 + idim] = ipoint;
            Ncols[ipoint*3 + idim] = 3*ipoint + idim;
            Nvals[ipoint*3 + idim] = normals[ipoint*3+idim];
        }
    }
    build_spsMatrix(Nrows, Ncols, Nvals, Pn);
}
 
 

 
  
 /*---------------------------------------------------------------------------------------------------------------------*/
  
/*TypeReal Distance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals)
{

 	 
 	 
 	 
 	 
	clock_t t0 = clock();
	
	
	
	// get the quad control mesh of a regular Catmull-Clark subdivision surface by one level of subdivision
	// the refinement operator is the sparse linear matrix L0
	


    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t maxIterations = D.maxIterations;
	size_t perDimsamples = (size_t)std::floor( std::sqrt( n_points *1.0 / n_faces ) );
	size_t n_samples = perDimsamples * perDimsamples * n_faces;
	TypeReal alpha = D.alpha;
	TypeReal beta = D.beta/n_faces*n_points;
	TypeReal epsilon = D.epsilon;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	
	TypeReal surface_area = area_surface(vertices, face_nvertex, face_vertex);
    
    // regularizers
    SpMat G, sqrtG, G1d, R;
   	graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
   	R = beta* G;
	
	
    // create projection matrices for normals
    SpMat Pn(n_points, 3*n_points);
    build_normalProjection( normals, Pn);

	
	
    // visualization window
	//mrpt::gui::CDisplayWindow3D	winA("quadtraticSDM_curvature_fit",640,480);

    
    
    // start minimization
	// variables fixed during loop
   // the order of coefficients in local approximarion matrix is A11, A12, A13, A21, A22, A23, A31, A32, A33
    // as they remain fix, we can pre-compute them
    size_t localrows[9] = { 0, 0, 0, 1, 1, 1, 2, 2, 2};
    size_t localcols[9] = { 0, 1, 2, 0, 1, 2, 0, 1, 2};
	std::vector<int> f_params;
	std::vector<TypeReal> s_params, t_params; 
	std::vector<size_t> S2Pcols(n_points*9);
	std::vector<size_t> S2Prows(n_points*9);
	for( size_t ii=0; ii< n_points; ii++ )
	{
		for( size_t jj=0; jj<9; jj++)
		{
			S2Prows[ ii*9 + jj ] = ii*3 + localrows[jj];
			S2Pcols[ ii*9 + jj ] = ii*3 + localcols[jj];
		}
	}    
	//printf("done\n");
	TypeReal energy;
    Eigen::Map< SpVect > pointsEigen(&points[0], 3*n_points);
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    SpVect verticesEigen_(verticesEigen);
    TypeReal energy_ = std::numeric_limits<TypeReal>::max();
	for( size_t iiSD=0; iiSD< D.maxIterations; iiSD++)
	{
		
		
		
		// global variables for linear systems and optimization
        SpMat Alsqr = R;	
        SpVect rhs = SpVect::Zero(3*n_vertex);		
		TypeReal energy_regularizer = verticesEigen.dot( R*verticesEigen );
		TypeReal energy_data = 0;
		
		
	
		//------------------------------------------------------------------------------------------------//
		//----------------------------- S2P quadratic MAJORIZATION --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		std::vector<TypeReal> closestSpoint0;
		// find closest surface point to each sample
		KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, f_params, s_params, t_params );
		SpMat L, Ds, Dt;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, L, Ds, Dt, true);	
		SpVect closestSpoint = L*verticesEigen;
		SpVect res = pointsEigen - closestSpoint;
		TypeReal energyS2P_data = 0;
		// given the closest sample to each point in the pointc loud, construct the quadratic approximation to the squared distance function of the subdivision surface
		std::vector<TypeReal> spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2, principalDirection1, principalDirection2;
		sample_diffmetrics( vertices, face_nvertex, face_vertex, f_params, s_params, t_params, spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2,principalDirection1, principalDirection2 );
		std::vector<TypeReal> S2PAvals(n_points*9);
		for( size_t ii=0; ii< n_points; ii++ )
		{
			TypeReal dist = std::pow( res[ii*3], 2) + std::pow( res[ii*3+1], 2) + std::pow( res[ii*3+2], 2);
			if( std::isnan(dist) ){printf("energy S2P is nan at sample %lu\n", ii);}
			dist = std::sqrt( dist + epsilon );
			energyS2P_data += std::pow(dist, D.lp);
			// quadratic squared distance approx
			size_t count = ii*9;
			TypeReal r1 = std::abs(principalRadius1[ii]);
			TypeReal r2 = std::abs(principalRadius2[ii]);			
			TypeReal sc1 = 0;
			TypeReal sc2 = 0;
			TypeReal sc3 = 1.0;
			if( dist> eps )
			{
				TypeReal dp_1 = std::pow( dist, D.lp - 1.0);
				TypeReal dp_2 = std::pow( dist, D.lp - 2.0);
				sc1 = D.lp * dp_1 / ( dist + r1 );
				sc2 = D.lp * dp_1 / ( dist + r2 );
				sc3 = D.lp * dp_2;		
			}
			for( size_t jj=0; jj<3; jj++)
			{
				for( size_t kk=0; kk<3; kk++)
				{
					TypeReal approx = sc1* principalDirection1[ii*3+jj]* principalDirection1[ii*3+kk] + sc2* principalDirection2[ii*3+jj]* principalDirection2[ii*3+kk] + sc3 *normals[ii*3+jj]*normals[ii*3+kk];
					S2PAvals[count + jj*3 + kk ] = approx;
				}
			}
		}
		SpMat AS2P( n_points*3, n_points*3 );
		build_spsMatrix( S2Prows, S2Pcols, S2PAvals, AS2P);		
		SpMat Lt = L.transpose();
		SpMat S2PAdata =  Lt* AS2P * L;
		SpVect S2Prhs = Lt* ( AS2P * pointsEigen);     
		SpMat Ps = Pn * Ds;
		SpMat Pt = Pn * Dt;
		SpVect PsNormal = Ps * verticesEigen;
		SpVect PtNormal = Pt * verticesEigen;
		std::vector<TypeReal> wsN(n_points);
		std::vector<TypeReal> wtN(n_points);
		for(int ii=0; ii<n_points; ii++)
		{
			wsN[ii] = D.lp/ std::pow( std::abs(PsNormal[ii]) + epsilon, 2.0 - D.lp );
			wtN[ii] = D.lp/ std::pow( std::abs(PtNormal[ii]) + epsilon, 2.0 - D.lp );	
		}
		SpMat DiagNs(n_points,n_points);
		SpMat DiagNt(n_points,n_points);
		build_spsDiagMatrix( wsN, DiagNs);
		build_spsDiagMatrix( wtN, DiagNt);	
		SpMat PnS = Ps.transpose()* DiagNs*Ps + Pt.transpose()* DiagNt *Pt;
		Alsqr += S2PAdata + alpha *PnS;		
		rhs += S2Prhs;
		energy_data += energyS2P_data;
		energy_data += alpha * verticesEigen.dot( PnS*verticesEigen );
		
        energy = energy_data + energy_regularizer;
		if(D.visualize>0){ printf("average energy %f\n", energy/n_points);}
		
		
		
		
		//------------------------------------------------------------------------------------------------//
		//---------------------------------------- MINIMIZATION------------------------------------------//
		//--------------------------------------------------------- --------------------------------------//
		SpVect x;
		Eigen::ConjugateGradient<SpMat > solver;
		solver.setMaxIterations(D.solverIterations);
		solver.compute(Alsqr);
		x = solver.solveWithGuess( rhs, verticesEigen );		

		
		SpVect verticesCandidate = x;
		energy_regularizer = verticesCandidate.dot( R*verticesCandidate );
		energy_data = 0;
		PsNormal = Ps * verticesCandidate;
		PtNormal = Pt * verticesCandidate;
		//
		SpVect S = L*verticesCandidate;
		res = S - pointsEigen;
		energyS2P_data = 0;
		TypeReal energy_normals = 0;
		for(int ii=0; ii<res.size()/3; ii++)
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(res[ii*3+idim],2);
			}
			energyS2P_data += std::pow(dist + epsilon, D.lp/2);
			energy_normals += std::pow(std::abs( PsNormal[ii] ) +epsilon, D.lp) + std::pow(std::abs( PtNormal[ii] )+epsilon, D.lp);
		}		
		energy_data +=  energyS2P_data;
		energy_data += alpha * energy_normals;
		energy = energy_data + energy_regularizer;
		if( energy_ > energy )
		{
			verticesEigen = verticesCandidate;
		}        
		
		
		//------------------------------------------------------------------------------------------------//
		//--------------------------------------- CHECK CONVERGENCE --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        x = verticesEigen - verticesEigen_;
        if( (  x.dot(x) <  epsilon_a ) && ( x.dot(x) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iiSD);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iiSD);}
            break;
        }
        energy_ = energy;
        verticesEigen_ = verticesEigen;
		if(D.visualize>0){ printf("iteration %lu, energy data + regularizer: %f + %f = %f\n", iiSD, energy_data, energy_regularizer, energy); }       

        
                

		
	}
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("quadraticSD_curvature_fit took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
 
	return energy;
 }*/
 
 
void normalize_variables( std::vector<TypeReal> &point_values, size_t n_points, std::vector<TypeReal> &minVars, std::vector<TypeReal> &maxVars)
{
	size_t dim = point_values.size()/n_points;

	minVars = std::vector<TypeReal>( dim, std::numeric_limits<TypeReal>::max() );
    maxVars = std::vector<TypeReal>( dim, std::numeric_limits<TypeReal>::min() );
    for(int ii=0; ii<n_points; ii++)
	{
		for(int idim=0; idim< dim; idim++)
		{
			if( maxVars[idim] < point_values[ ii + idim*n_points] ){ maxVars[idim] = point_values[ ii + idim*n_points]; };
			if( minVars[idim] > point_values[ ii + idim*n_points] ){  minVars[idim] = point_values[ ii + idim*n_points]; }
		}
	}	
	for(int idim=0; idim< dim; idim++)
	{
		TypeReal irange = 1.0;
		if( maxVars[idim] > minVars[idim] ){ irange = 1.0/( maxVars[idim] - minVars[idim] ); }
		for(int ii=0; ii<n_points; ii++)
		{
			point_values[ ii + idim*n_points] = (point_values[ ii + idim*n_points] - minVars[idim] ) * irange;
		}
	}
}

void denormalize_variables( std::vector<TypeReal> &point_values, std::vector<TypeReal> minVars, std::vector<TypeReal> maxVars)
{
	size_t dim = minVars.size();
	size_t n_points = point_values.size()/dim;

	for(int ii=0; ii<n_points; ii++)
	{
		for(int idim=0; idim< dim; idim++)
		{
			point_values[ ii + idim*n_points] =  minVars[idim] + point_values[ ii + idim*n_points]*( maxVars[idim] - minVars[idim]);
		}
	}	
}
 

// colored PC
TypeReal ColorDistance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &sds_color, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> &colors, std::vector<int> &boundary_vertices)
{

	
 	if(D.visualize>1){ printf("%d boundary vertices\n", boundary_vertices.size()); }
 	 
 	// parse input
 	size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t dimcolor = colors.size()/n_points;
    TypeReal pct_randomSamples = D.randomization;
	TypeReal epsilon_a = 1e-6;
	TypeReal epsilon_r = 1e-4;
	TypeReal rho = 1e0;
	size_t ADMMIter = 100;
    printf("colored surface, size vars %lu\n", dimcolor);
    
	clock_t t0 = clock();
	std::vector<TypeReal> center;
	
	std::vector<int> random_samples(n_points);
	for(int ii=0; ii< n_points; ii++){ random_samples[ii] = ii; }
	// get the quad control mesh of a regular Catmull-Clark subdivision surface by one level of subdivision
	// the refinement operator is the sparse linear matrix L0


    // parse parameters
    size_t maxIterations = D.maxIterations;
    size_t n_samples = (int) std::floor(n_points* pct_randomSamples);    
	TypeReal alpha = D.alpha;
	TypeReal beta = D.beta/n_faces*n_points*pct_randomSamples;
	TypeReal gamma = D.gamma/D.delta *n_faces*n_points*pct_randomSamples;
	TypeReal epsilon = D.epsilon;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	
	TypeReal surface_area = area_surface(vertices, face_nvertex, face_vertex);
    
    // regularizers
    SpMat G, sqrtG, G1d, sqrtG1d, R;
   	graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
   	graph_Differences1d( vertices, face_nvertex, face_vertex, G1d, sqrtG1d, true);
   	R = beta* G;
	
   	
    // normalize point variables to [0,1] (to preserve their range)
    std::vector<TypeReal> minVars, maxVars;
    normalize_variables( colors, n_points, minVars, maxVars);
 
	
	// initialize vertex values
	KdSearch_closestPointValue( points, colors, vertices, face_nvertex, face_vertex, sds_color);
	std::vector<TypeReal> s_params, t_params;
	std::vector<int> f_params;
	//KdSearch_closestParameter2Surface( points, colors, vertices, face_nvertex, face_vertex, f_params, s_params, t_params, sds_color );



    // create matrix for vertex constraints
	int n_Bvertex = boundary_vertices.size();
	SpMat C(3*n_Bvertex, 3*n_vertex);
	std::vector<size_t> rows( 3*n_Bvertex );
	std::vector<size_t> cols( 3*n_Bvertex );
	std::vector<TypeReal> vals( 3*n_Bvertex, 1.0 );
    SpVect fix_vertices( n_Bvertex *3);
    for(int ii=0; ii<n_Bvertex; ii++)
    {
    	int bvertex = boundary_vertices[ii];
    	//printf("fixed vertex %d\n", bvertex);
    	for(int idim=0; idim<3; idim++)
    	{
    		fix_vertices[ii*3 + idim] = vertices[ 3*bvertex+ idim];
    		cols[ii*3+idim] = bvertex*3 + idim;
			rows[ii*3+idim] = 3*ii + idim;
    	}
    }
    build_spsMatrix( rows, cols, vals, C);
    SpMat Ct = C.transpose();
    SpMat CtC = Ct*C;
    //for(int ii=0; ii<rows.size(); ii++)
    //{
    //	printf("%d %d %f\n", rows[ii], cols[ii], vals[ii]);
    //}
	
    // visualization window
	//mrpt::gui::CDisplayWindow3D	winA("quadtraticSDM_curvature_fit",640,480);

    
    
    // start minimization
	// variables fixed during loop
    // the order of coefficients in local approximarion matrix is A11, A12, A13, A21, A22, A23, A31, A32, A33
    // as they remain fix, we can pre-compute them
    size_t localrows[9] = { 0, 0, 0, 1, 1, 1, 2, 2, 2};
    size_t localcols[9] = { 0, 1, 2, 0, 1, 2, 0, 1, 2};
	std::vector<size_t> S2Pcols(n_samples*9);
	std::vector<size_t> S2Prows(n_samples*9);
	for( size_t ii=0; ii< n_samples; ii++ )
	{
		for( size_t jj=0; jj<9; jj++)
		{
			S2Prows[ ii*9 + jj ] = ii*3 + localrows[jj];
			S2Pcols[ ii*9 + jj ] = ii*3 + localcols[jj];
		}
	}    
	

	
	//printf("done\n");
	TypeReal energy;
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    //sds_color = std::vector<TypeReal>(dimcolor*n_vertex);
    Eigen::Map< Mat > colorEigen(&sds_color[0], n_vertex, dimcolor);
    //Mat colorEigen = Mat::Random(n_vertex, dimcolor);
    SpVect verticesEigen_(verticesEigen);
    TypeReal energy_ = std::numeric_limits<TypeReal>::max();
    SpVect x = verticesEigen;
    SpVect lambdas = SpVect::Zero( 3*n_Bvertex);
	for( size_t iiSD=0; iiSD< D.maxIterations; iiSD++)
	{
		
		
		
		// global variables for linear systems and optimization
        SpMat Alsqr = R;	
        SpVect rhs = SpVect::Zero(3*n_vertex);		
		TypeReal energy_regularizer = verticesEigen.dot( R*verticesEigen );
		TypeReal energy_data = 0;
		
		
		// randomize
		std::random_shuffle ( random_samples.begin(), random_samples.end() );
		std::vector<TypeReal> ipoints( 3*n_samples );
		std::vector<TypeReal> inormals( 3*n_samples );
		Mat icolors( n_samples, dimcolor );
		for(int ii=0; ii<n_samples; ii++)
		{
			for(int idim=0; idim<3; idim++)
			{
				ipoints[ii*3+idim] = points[ random_samples[ii]*3 + idim ];
				inormals[ii*3+idim] = normals[ random_samples[ii]*3 + idim ];
			}
			for(int idim=0; idim<dimcolor; idim++)
			{
				icolors(ii,idim) = colors[ random_samples[ii] + idim*n_points ];
			}
		}
		Eigen::Map< SpVect > ipointsEigen(&ipoints[0], 3*n_samples);
		
		// create projection matrices for normals
		SpMat Pn(n_samples, 3*n_samples);
		build_normalProjection( inormals, Pn);
		
		
		
		//------------------------------------------------------------------------------------------------//
		//----------------------------- S2P quadratic MAJORIZATION --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		std::vector<TypeReal> closestSpoint0;
		// find closest surface point to each sample
		KdSearch_closestParameter2Surface( ipoints, vertices, face_nvertex, face_vertex, f_params, s_params, t_params );

		SpMat L, Ds, Dt, B, DBs, DBt;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, L, Ds, Dt, true);	
		SampleLimitBasis(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, B, DBs, DBt, false);

		SpVect closestSpoint = L*verticesEigen;
		SpVect res = ipointsEigen - closestSpoint;
		TypeReal energyS2P_data = 0;
		// given the closest sample to each point in the pointc loud, construct the quadratic approximation to the squared distance function of the subdivision surface
		std::vector<TypeReal> spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2, principalDirection1, principalDirection2;
		sample_diffmetrics( vertices, face_nvertex, face_vertex, f_params, s_params, t_params, spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2,principalDirection1, principalDirection2 );
		std::vector<TypeReal> S2PAvals(n_points*9);
		for( size_t ii=0; ii< n_samples; ii++ )
		{
			TypeReal dist = std::pow( res[ii*3], 2) + std::pow( res[ii*3+1], 2) + std::pow( res[ii*3+2], 2);
			if( std::isnan(dist) ){printf("energy S2P is nan at sample %lu\n", ii);}
			dist = std::sqrt( dist + epsilon );
			energyS2P_data += std::pow(dist, D.lp);
			// quadratic squared distance approx
			size_t count = ii*9;
			TypeReal r1 = std::abs(principalRadius1[ii]);
			TypeReal r2 = std::abs(principalRadius2[ii]);			
			TypeReal sc1 = 0;
			TypeReal sc2 = 0;
			TypeReal sc3 = 1.0;
			if( dist> eps )
			{
				TypeReal dp_1 = std::pow( dist, D.lp - 1.0);
				TypeReal dp_2 = std::pow( dist, D.lp - 2.0);
				sc1 = D.lp * dp_1 / ( dist + r1 );
				sc2 = D.lp * dp_1 / ( dist + r2 );
				sc3 = D.lp * dp_2;		
			}
			for( size_t jj=0; jj<3; jj++)
			{
				for( size_t kk=0; kk<3; kk++)
				{
					TypeReal approx = sc1* principalDirection1[ii*3+jj]* principalDirection1[ii*3+kk] + sc2* principalDirection2[ii*3+jj]* principalDirection2[ii*3+kk] + sc3 *normals[ii*3+jj]*normals[ii*3+kk];
					S2PAvals[count + jj*3 + kk ] = approx;
				}
			}
		}
		SpMat AS2P( n_samples*3, n_samples*3 );
		build_spsMatrix( S2Prows, S2Pcols, S2PAvals, AS2P);		
		SpMat Lt = L.transpose();
		SpMat S2PAdata =  Lt* AS2P * L;
		SpVect S2Prhs = Lt* ( AS2P * ipointsEigen);     
		SpMat Ps = Pn * Ds;
		SpMat Pt = Pn * Dt;
		SpVect PsNormal = Ps * verticesEigen;
		SpVect PtNormal = Pt * verticesEigen;
		std::vector<TypeReal> wsN(n_samples);
		std::vector<TypeReal> wtN(n_samples);
		for(int ii=0; ii<n_samples; ii++)
		{
			wsN[ii] = D.lp/ std::pow( std::abs(PsNormal[ii]) + epsilon, 2.0 - D.lp );
			wtN[ii] = D.lp/ std::pow( std::abs(PtNormal[ii]) + epsilon, 2.0 - D.lp );	
		}
		SpMat DiagNs(n_samples,n_samples);
		SpMat DiagNt(n_samples,n_samples);
		build_spsDiagMatrix( wsN, DiagNs);
		build_spsDiagMatrix( wtN, DiagNt);	
		SpMat PnS = Ps.transpose()* DiagNs*Ps + Pt.transpose()* DiagNt *Pt;
		Alsqr += S2PAdata + alpha *PnS;		
		rhs += S2Prhs;
		energy_data += energyS2P_data;
		energy_data += alpha * verticesEigen.dot( PnS*verticesEigen );
		
        energy = energy_data + energy_regularizer;
		
        
		
		
		//------------------------------------------------------------------------------------------------//
		//---------------------------------------- MINIMIZATION------------------------------------------//
		//--------------------------------------------------------- --------------------------------------//
		if( n_Bvertex>0 ){ rho = 1e1; ADMMIter = 100; }else{  rho = 0; ADMMIter = 1; }
		SpMat Asol = Alsqr + rho*CtC;
		Eigen::SimplicialCholesky<SpMat> solverD;
		Eigen::ConjugateGradient<SpMat, Eigen::Lower|Eigen::Upper> solverCG;
		if( D.solverIterations >0 )
		{
			solverCG.setMaxIterations(D.solverIterations);
			solverCG.compute( Asol );
		}
		else
		{	if( iiSD==0 ){ solverD.analyzePattern( Asol ); }
			solverD.compute( Asol );
		}
		for(int iiADMM=0; iiADMM<100; iiADMM++)
		{
			// update primal variable
			SpVect x_ = x;
			SpVect rhs_sol= rhs + Ct*( rho * fix_vertices - lambdas );			
			if( D.solverIterations >0 ){ x = solverCG.solveWithGuess( rhs_sol, x );}else{	x = solverD.solve( rhs_sol ); }
			
			// compute residual
			SpVect resp = C*x - fix_vertices;
			SpVect resd = Alsqr*x - rhs + Ct*lambdas;
			SpVect resx = x - x_;
			
			// update lagrange multipliers
			lambdas = lambdas + rho * resp;
			
			// update rho
			if( resp.norm() * resd.size() > 10* resd.norm()* resp.size() )
			{
				rho *= 2;
				Asol = Alsqr + rho*CtC;
				if( D.solverIterations >0 ){ solverCG.compute(Asol);}else{ solverD.solve(Asol); }
			}else if( resd.norm() * resp.size() > 10* resp.norm()* resd.size() )
			{
				rho /= 2;
				Asol = Alsqr + rho*CtC;
				if( D.solverIterations >0 ){ solverCG.compute(Asol);}else{ solverD.solve(Asol); }				
			}
			
			// stopping criterion
			if( (res.norm() < epsilon_a *res.size() ) || (resx.norm() < epsilon_a *resx.size() ) || (resx.norm() < epsilon_r * x.norm() )  ){ break;}
		}
		
		// color
		Mat ccolors = B * colorEigen;
		std::vector<TypeReal> cweights(n_samples, 1.0);
		for( int ii=0; ii<n_samples; ii++)
		{
			TypeReal w = 0;
			for(int idim=0; idim<dimcolor; idim++)
			{
				w += std::pow( icolors(ii, idim) - ccolors(ii, idim), 2);
			}
			if( w> epsilon)
			{
				w = std::sqrt(w);
				cweights[ii] = D.lp/ std::pow( w + epsilon, 2.0 - D.lp );
			}
		}
		SpMat DiagC(n_samples,n_samples);
		build_spsDiagMatrix( cweights, DiagC);
		SpMat Bt = B.transpose();
		SpMat Asolc = Bt * DiagC * B + gamma * G1d;
		//Eigen::ConjugateGradient<SpMat, Eigen::Lower|Eigen::Upper> cSolver(Asolc);
		//cSolver.setMaxIterations(D.solverIterations);
		Eigen::SimplicialCholesky<SpMat> cSolver;
		cSolver.analyzePattern( Asolc ); 
		cSolver.compute( Asolc );
		Mat rhsc = Bt * DiagC * icolors;
		colorEigen = cSolver.solve( rhsc ); 
		//colorEigen = cSolver.solveWithGuess( rhsc, colorEigen );
		for(int ii=0; ii<colorEigen.rows(); ii++)
		{
			for(int idim=0; idim< colorEigen.cols(); idim++)
			{
				colorEigen(ii, idim) = std::min( std::max(  colorEigen(ii, idim),(TypeReal) 0.0 ), (TypeReal)1.0 );
			}
		}
		

		// project boundary vertices into their initial position
		TypeReal norm_constraint = 0;
		for(int ii=0; ii<n_Bvertex; ii++)
		{
			int bvertex = boundary_vertices[ii];
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow( fix_vertices[ii*3 + idim] - x[ 3*bvertex+ idim], 2);
			}
			norm_constraint += std::sqrt(dist);
		}
		norm_constraint /= std::max(n_Bvertex,1);
		printf("average distance to constrained vertices %f\n", norm_constraint);
		
		
		SpVect verticesCandidate = x;
		energy_regularizer = verticesCandidate.dot( R*verticesCandidate );
		energy_data = 0;
		PsNormal = Ps * verticesCandidate;
		PtNormal = Pt * verticesCandidate;
		//
		SpVect S = L*verticesCandidate;
		res = S - ipointsEigen;
		energyS2P_data = 0;
		TypeReal energy_normals = 0;
		for(int ii=0; ii<res.size()/3; ii++)
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(res[ii*3+idim],2);
			}
			energyS2P_data += std::pow(dist + epsilon, D.lp/2);
			energy_normals += std::pow(std::abs( PsNormal[ii] ) +epsilon, D.lp) + std::pow(std::abs( PtNormal[ii] )+epsilon, D.lp);
		}		
		energy_data +=  energyS2P_data;
		energy_data += alpha * energy_normals;
		energy = energy_data + energy_regularizer;
		if( energy_ > energy )
		{
			verticesEigen = verticesCandidate;
		}        
		
		
		
 
		
		
		//------------------------------------------------------------------------------------------------//
		//--------------------------------------- CHECK CONVERGENCE --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        x = verticesEigen - verticesEigen_;
        if( (  x.dot(x) <  epsilon_a ) && ( x.dot(x) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iiSD);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iiSD);}
            break;
        }
        energy_ = energy;
        verticesEigen_ = verticesEigen;
		if(D.visualize>0){ printf("iteration %lu, energy data + regularizer: %f + %f = %f\n", iiSD, energy_data/D.randomization, energy_regularizer/D.randomization, energy/D.randomization); }       

        
	}
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("quadraticSD_curvature_fit took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
 
	/*for(int ii=0; ii<n_vertex; ii++)
	{
		for(int idim=0; idim<dimcolor; idim++)
		{
			sds_color[ ii + idim*n_vertex ] = colorEigen(ii, idim); 
		}
	}*/
	denormalize_variables( sds_color, minVars, maxVars);
	denormalize_variables( colors, minVars, maxVars);
	
	return energy;
}

 
 
// WORKS
TypeReal Distance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<int> &boundary_vertices)
{

	
 	if(D.visualize>1){ printf("%d boundary vertices\n", boundary_vertices.size()); }
 	 
 	// parse input
 	size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    TypeReal pct_randomSamples = D.randomization;
	TypeReal epsilon_a = 1e-6;
	TypeReal epsilon_r = 1e-4;
	TypeReal rho = 1e0;
	size_t ADMMIter = 100;
    
    
	clock_t t0 = clock();
	std::vector<TypeReal> center;
	
	std::vector<int> random_samples(n_points);
	for(int ii=0; ii< n_points; ii++){ random_samples[ii] = ii; }
	// get the quad control mesh of a regular Catmull-Clark subdivision surface by one level of subdivision
	// the refinement operator is the sparse linear matrix L0


    // parse parameters
    size_t maxIterations = D.maxIterations;
    size_t n_samples = (int) std::floor(n_points* pct_randomSamples);    
	TypeReal alpha = D.alpha;
	TypeReal beta = D.beta/n_faces*n_points*pct_randomSamples;
	TypeReal epsilon = D.epsilon;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	
	TypeReal surface_area = area_surface(vertices, face_nvertex, face_vertex);
    
    // regularizers
    SpMat G, sqrtG, G1d, R;
   	graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
   	R = beta* G;
	
   	
	


    // create matrix for vertex constraints
	int n_Bvertex = boundary_vertices.size();
	SpMat C(3*n_Bvertex, 3*n_vertex);
	std::vector<size_t> rows( 3*n_Bvertex );
	std::vector<size_t> cols( 3*n_Bvertex );
	std::vector<TypeReal> vals( 3*n_Bvertex, 1.0 );
    SpVect fix_vertices( n_Bvertex *3);
    for(int ii=0; ii<n_Bvertex; ii++)
    {
    	int bvertex = boundary_vertices[ii];
    	//printf("fixed vertex %d\n", bvertex);
    	for(int idim=0; idim<3; idim++)
    	{
    		fix_vertices[ii*3 + idim] = vertices[ 3*bvertex+ idim];
    		cols[ii*3+idim] = bvertex*3 + idim;
			rows[ii*3+idim] = 3*ii + idim;
    	}
    }
    build_spsMatrix( rows, cols, vals, C);
    SpMat Ct = C.transpose();
    SpMat CtC = Ct*C;
    //for(int ii=0; ii<rows.size(); ii++)
    //{
    //	printf("%d %d %f\n", rows[ii], cols[ii], vals[ii]);
    //}
	
    // visualization window
	//mrpt::gui::CDisplayWindow3D	winA("quadtraticSDM_curvature_fit",640,480);

    
    
    // start minimization
	// variables fixed during loop
    // the order of coefficients in local approximarion matrix is A11, A12, A13, A21, A22, A23, A31, A32, A33
    // as they remain fix, we can pre-compute them
    size_t localrows[9] = { 0, 0, 0, 1, 1, 1, 2, 2, 2};
    size_t localcols[9] = { 0, 1, 2, 0, 1, 2, 0, 1, 2};
	std::vector<int> f_params;
	std::vector<TypeReal> s_params, t_params; 
	std::vector<size_t> S2Pcols(n_samples*9);
	std::vector<size_t> S2Prows(n_samples*9);
	for( size_t ii=0; ii< n_samples; ii++ )
	{
		for( size_t jj=0; jj<9; jj++)
		{
			S2Prows[ ii*9 + jj ] = ii*3 + localrows[jj];
			S2Pcols[ ii*9 + jj ] = ii*3 + localcols[jj];
		}
	}    
	

	
	//printf("done\n");
	TypeReal energy;
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    SpVect verticesEigen_(verticesEigen);
    TypeReal energy_ = std::numeric_limits<TypeReal>::max();
    SpVect x = verticesEigen;
    SpVect lambdas = SpVect::Zero( 3*n_Bvertex);
	for( size_t iiSD=0; iiSD< D.maxIterations; iiSD++)
	{
		
		
		
		// global variables for linear systems and optimization
        SpMat Alsqr = R;	
        SpVect rhs = SpVect::Zero(3*n_vertex);		
		TypeReal energy_regularizer = verticesEigen.dot( R*verticesEigen );
		TypeReal energy_data = 0;
		
		
		// randomize
		std::random_shuffle ( random_samples.begin(), random_samples.end() );
		std::vector<TypeReal> ipoints( 3*n_samples );
		std::vector<TypeReal> inormals( 3*n_samples );
		for(int ii=0; ii<n_samples; ii++)
		{
			for(int idim=0; idim<3; idim++)
			{
				ipoints[ii*3+idim] = points[ random_samples[ii]*3 + idim ];
				inormals[ii*3+idim] = normals[ random_samples[ii]*3 + idim ];
			}
		}
		Eigen::Map< SpVect > ipointsEigen(&ipoints[0], 3*n_samples);
		
		// create projection matrices for normals
		SpMat Pn(n_samples, 3*n_samples);
		build_normalProjection( inormals, Pn);
		
		
		
		//------------------------------------------------------------------------------------------------//
		//----------------------------- S2P quadratic MAJORIZATION --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		std::vector<TypeReal> closestSpoint0;
		// find closest surface point to each sample
		KdSearch_closestParameter2Surface( ipoints, vertices, face_nvertex, face_vertex, f_params, s_params, t_params );
		SpMat L, Ds, Dt;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, L, Ds, Dt, true);	
		SpVect closestSpoint = L*verticesEigen;
		SpVect res = ipointsEigen - closestSpoint;
		TypeReal energyS2P_data = 0;
		// given the closest sample to each point in the pointc loud, construct the quadratic approximation to the squared distance function of the subdivision surface
		std::vector<TypeReal> spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2, principalDirection1, principalDirection2;
		sample_diffmetrics( vertices, face_nvertex, face_vertex, f_params, s_params, t_params, spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2,principalDirection1, principalDirection2 );
		std::vector<TypeReal> S2PAvals(n_points*9);
		for( size_t ii=0; ii< n_samples; ii++ )
		{
			TypeReal dist = std::pow( res[ii*3], 2) + std::pow( res[ii*3+1], 2) + std::pow( res[ii*3+2], 2);
			if( std::isnan(dist) ){printf("energy S2P is nan at sample %lu\n", ii);}
			dist = std::sqrt( dist + epsilon );
			energyS2P_data += std::pow(dist, D.lp);
			// quadratic squared distance approx
			size_t count = ii*9;
			TypeReal r1 = std::abs(principalRadius1[ii]);
			TypeReal r2 = std::abs(principalRadius2[ii]);			
			TypeReal sc1 = 0;
			TypeReal sc2 = 0;
			TypeReal sc3 = 1.0;
			if( dist> eps )
			{
				TypeReal dp_1 = std::pow( dist, D.lp - 1.0);
				TypeReal dp_2 = std::pow( dist, D.lp - 2.0);
				sc1 = D.lp * dp_1 / ( dist + r1 );
				sc2 = D.lp * dp_1 / ( dist + r2 );
				sc3 = D.lp * dp_2;		
			}
			for( size_t jj=0; jj<3; jj++)
			{
				for( size_t kk=0; kk<3; kk++)
				{
					TypeReal approx = sc1* principalDirection1[ii*3+jj]* principalDirection1[ii*3+kk] + sc2* principalDirection2[ii*3+jj]* principalDirection2[ii*3+kk] + sc3 *normals[ii*3+jj]*normals[ii*3+kk];
					S2PAvals[count + jj*3 + kk ] = approx;
				}
			}
		}
		SpMat AS2P( n_samples*3, n_samples*3 );
		build_spsMatrix( S2Prows, S2Pcols, S2PAvals, AS2P);		
		SpMat Lt = L.transpose();
		SpMat S2PAdata =  Lt* AS2P * L;
		SpVect S2Prhs = Lt* ( AS2P * ipointsEigen);     
		SpMat Ps = Pn * Ds;
		SpMat Pt = Pn * Dt;
		SpVect PsNormal = Ps * verticesEigen;
		SpVect PtNormal = Pt * verticesEigen;
		std::vector<TypeReal> wsN(n_samples);
		std::vector<TypeReal> wtN(n_samples);
		for(int ii=0; ii<n_samples; ii++)
		{
			wsN[ii] = D.lp/ std::pow( std::abs(PsNormal[ii]) + epsilon, 2.0 - D.lp );
			wtN[ii] = D.lp/ std::pow( std::abs(PtNormal[ii]) + epsilon, 2.0 - D.lp );	
		}
		SpMat DiagNs(n_samples,n_samples);
		SpMat DiagNt(n_samples,n_samples);
		build_spsDiagMatrix( wsN, DiagNs);
		build_spsDiagMatrix( wtN, DiagNt);	
		SpMat PnS = Ps.transpose()* DiagNs*Ps + Pt.transpose()* DiagNt *Pt;
		Alsqr += S2PAdata + alpha *PnS;		
		rhs += S2Prhs;
		energy_data += energyS2P_data;
		energy_data += alpha * verticesEigen.dot( PnS*verticesEigen );
		
        energy = energy_data + energy_regularizer;
		
		
		
		
		//------------------------------------------------------------------------------------------------//
		//---------------------------------------- MINIMIZATION------------------------------------------//
		//--------------------------------------------------------- --------------------------------------//
		if( n_Bvertex>0 ){ rho = 1e1; ADMMIter = 100; }else{  rho = 0; ADMMIter = 1; }
		SpMat Asol = Alsqr + rho*CtC;
		Eigen::ConjugateGradient<SpMat, Eigen::Lower|Eigen::Upper> solver;
		solver.compute( Asol );
		solver.setMaxIterations(D.solverIterations);
		for(int iiADMM=0; iiADMM<100; iiADMM++)
		{
			// update primal variable
			SpVect x_ = x;
			SpVect rhs_sol= rhs + Ct*( rho * fix_vertices - lambdas );			
			x = solver.solveWithGuess( rhs_sol, x );
			
			// compute residual
			SpVect resp = C*x - fix_vertices;
			SpVect resd = Alsqr*x - rhs + Ct*lambdas;
			SpVect resx = x - x_;
			
			// update lagrange multipliers
			lambdas = lambdas + rho * resp;
			
			// update rho
			if( resp.norm() * resd.size() > 10* resd.norm()* resp.size() )
			{
				rho *= 2;
				Asol = Alsqr + rho*CtC;
				solver.compute(Asol);
			}else if( resd.norm() * resp.size() > 10* resp.norm()* resd.size() )
			{
				rho /= 2;
				Asol = Alsqr + rho*CtC;
				solver.compute(Asol);				
			}
			
			// stopping criterion
			if( (res.norm() < epsilon_a *res.size() ) || (resx.norm() < epsilon_a *resx.size() ) || (resx.norm() < epsilon_r * x.norm() )  ){ break;}
		}
		
				

		// project boundary vertices into their initial position
		TypeReal norm_constraint = 0;
		for(int ii=0; ii<n_Bvertex; ii++)
		{
			int bvertex = boundary_vertices[ii];
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow( fix_vertices[ii*3 + idim] - x[ 3*bvertex+ idim], 2);
			}
			norm_constraint += std::sqrt(dist);
		}
		norm_constraint /= std::max(n_Bvertex,1);
		printf("average distance to constrained vertices %f\n", norm_constraint);
		
		
		SpVect verticesCandidate = x;
		energy_regularizer = verticesCandidate.dot( R*verticesCandidate );
		energy_data = 0;
		PsNormal = Ps * verticesCandidate;
		PtNormal = Pt * verticesCandidate;
		//
		SpVect S = L*verticesCandidate;
		res = S - ipointsEigen;
		energyS2P_data = 0;
		TypeReal energy_normals = 0;
		for(int ii=0; ii<res.size()/3; ii++)
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(res[ii*3+idim],2);
			}
			energyS2P_data += std::pow(dist + epsilon, D.lp/2);
			energy_normals += std::pow(std::abs( PsNormal[ii] ) +epsilon, D.lp) + std::pow(std::abs( PtNormal[ii] )+epsilon, D.lp);
		}		
		energy_data +=  energyS2P_data;
		energy_data += alpha * energy_normals;
		energy = energy_data + energy_regularizer;
		if( energy_ > energy )
		{
			verticesEigen = verticesCandidate;
		}        
		
		
				
		
		//------------------------------------------------------------------------------------------------//
		//--------------------------------------- CHECK CONVERGENCE --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        x = verticesEigen - verticesEigen_;
        if( (  x.dot(x) <  epsilon_a ) && ( x.dot(x) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iiSD);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iiSD);}
            break;
        }
        energy_ = energy;
        verticesEigen_ = verticesEigen;
		if(D.visualize>0){ printf("iteration %lu, energy data + regularizer: %f + %f = %f\n", iiSD, energy_data/D.randomization, energy_regularizer/D.randomization, energy/D.randomization); }       

        
	}
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("quadraticSD_curvature_fit took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
 
	return energy;
}

 
//DOES NOT WORK
/*TypeReal Distance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<int> &boundary_vertices)
{

	
 	if(D.visualize>1){ printf("%d boundary vertices\n", boundary_vertices.size()); }
 	 
 	// parse input
 	size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    TypeReal pct_randomSamples = D.randomization;
	TypeReal epsilon_a = 1e-6;
	TypeReal epsilon_r = 1e-4;

    
    
	clock_t t0 = clock();
	mrpt::gui::CDisplayWindow3D	winP("boundary vertices",640,480); 
	std::vector<TypeReal> center;
	
	std::vector<int> random_samples(n_points);
	for(int ii=0; ii< n_points; ii++){ random_samples[ii] = ii; }
	// get the quad control mesh of a regular Catmull-Clark subdivision surface by one level of subdivision
	// the refinement operator is the sparse linear matrix L0


    // parse parameters
    size_t maxIterations = D.maxIterations;
    size_t n_samples = (int) std::floor(n_points* pct_randomSamples);    
	TypeReal alpha = D.alpha;
	TypeReal beta = D.beta/n_faces*n_points*pct_randomSamples;
	TypeReal epsilon = D.epsilon;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	
	TypeReal surface_area = area_surface(vertices, face_nvertex, face_vertex);
    
    // regularizers
    SpMat G, sqrtG, G1d, R;
   	graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
   	R = beta* G;
	
   	
	


    // create matrix for vertex constraints
	int n_Bvertex = boundary_vertices.size();
	std::vector<size_t> rows( 6*n_Bvertex );
	std::vector<size_t> cols( 6*n_Bvertex );
	std::vector<TypeReal> vals( 6*n_Bvertex, 1.0 );
    SpVect fix_vertices( n_Bvertex *3);
    for(int ii=0; ii<n_Bvertex; ii++)
    {
    	int bvertex = boundary_vertices[ii];
    	//printf("fixed vertex %d\n", bvertex);
    	for(int idim=0; idim<3; idim++)
    	{
    		fix_vertices[ii*3 + idim] = vertices[ 3*bvertex+ idim];
    		size_t r = 3*n_vertex + 3*ii + idim;
    		size_t c = bvertex*3 + idim;
    		cols[ii*6+idim] = c;
			rows[ii*6+idim] = r;
    		cols[ii*6+3+idim] = r;
			rows[ii*6+3+idim] = c;			
    	}
    }

    // start minimization
	// variables fixed during loop
    // the order of coefficients in local approximarion matrix is A11, A12, A13, A21, A22, A23, A31, A32, A33
    // as they remain fix, we can pre-compute them
    size_t localrows[9] = { 0, 0, 0, 1, 1, 1, 2, 2, 2};
    size_t localcols[9] = { 0, 1, 2, 0, 1, 2, 0, 1, 2};
	std::vector<int> f_params;
	std::vector<TypeReal> s_params, t_params; 
	std::vector<size_t> S2Pcols(n_samples*9);
	std::vector<size_t> S2Prows(n_samples*9);
	for( size_t ii=0; ii< n_samples; ii++ )
	{
		for( size_t jj=0; jj<9; jj++)
		{
			S2Prows[ ii*9 + jj ] = ii*3 + localrows[jj];
			S2Pcols[ ii*9 + jj ] = ii*3 + localcols[jj];
		}
	}    
	

	
	//printf("done\n");
	TypeReal energy;
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    SpVect verticesEigen_(verticesEigen);
    TypeReal energy_ = std::numeric_limits<TypeReal>::max();
    SpVect x( 3*(n_vertex+ n_Bvertex) );
	x.head(3*n_vertex) = verticesEigen;
	x.tail(3*n_Bvertex) = SpVect::Zero(3*n_Bvertex);    
	for( size_t iiSD=0; iiSD< D.maxIterations; iiSD++)
	{
		
		
		
		// global variables for linear systems and optimization
        SpMat Alsqr = R;	
        SpVect rhs = SpVect::Zero(3*n_vertex);		
		TypeReal energy_regularizer = verticesEigen.dot( R*verticesEigen );
		TypeReal energy_data = 0;
		
		
		// randomize
		std::random_shuffle ( random_samples.begin(), random_samples.end() );
		std::vector<TypeReal> ipoints( 3*n_samples );
		std::vector<TypeReal> inormals( 3*n_samples );
		for(int ii=0; ii<n_samples; ii++)
		{
			for(int idim=0; idim<3; idim++)
			{
				ipoints[ii*3+idim] = points[ random_samples[ii]*3 + idim ];
				inormals[ii*3+idim] = normals[ random_samples[ii]*3 + idim ];
			}
		}
		Eigen::Map< SpVect > ipointsEigen(&ipoints[0], 3*n_samples);
		
		// create projection matrices for normals
		SpMat Pn(n_samples, 3*n_samples);
		build_normalProjection( inormals, Pn);
		
		
		
		//------------------------------------------------------------------------------------------------//
		//----------------------------- S2P quadratic MAJORIZATION --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		std::vector<TypeReal> closestSpoint0;
		// find closest surface point to each sample
		KdSearch_closestParameter2Surface( ipoints, vertices, face_nvertex, face_vertex, f_params, s_params, t_params );
		SpMat L, Ds, Dt;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, L, Ds, Dt, true);	
		SpVect closestSpoint = L*verticesEigen;
		SpVect res = ipointsEigen - closestSpoint;
		TypeReal energyS2P_data = 0;
		// given the closest sample to each point in the pointc loud, construct the quadratic approximation to the squared distance function of the subdivision surface
		std::vector<TypeReal> spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2, principalDirection1, principalDirection2;
		sample_diffmetrics( vertices, face_nvertex, face_vertex, f_params, s_params, t_params, spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2,principalDirection1, principalDirection2 );
		std::vector<TypeReal> S2PAvals(n_points*9);
		for( size_t ii=0; ii< n_samples; ii++ )
		{
			TypeReal dist = std::pow( res[ii*3], 2) + std::pow( res[ii*3+1], 2) + std::pow( res[ii*3+2], 2);
			if( std::isnan(dist) ){printf("energy S2P is nan at sample %lu\n", ii);}
			dist = std::sqrt( dist + epsilon );
			energyS2P_data += std::pow(dist, D.lp);
			// quadratic squared distance approx
			size_t count = ii*9;
			TypeReal r1 = std::abs(principalRadius1[ii]);
			TypeReal r2 = std::abs(principalRadius2[ii]);			
			TypeReal sc1 = 0;
			TypeReal sc2 = 0;
			TypeReal sc3 = 1.0;
			if( dist> eps )
			{
				TypeReal dp_1 = std::pow( dist, D.lp - 1.0);
				TypeReal dp_2 = std::pow( dist, D.lp - 2.0);
				sc1 = D.lp * dp_1 / ( dist + r1 );
				sc2 = D.lp * dp_1 / ( dist + r2 );
				sc3 = D.lp * dp_2;		
			}
			for( size_t jj=0; jj<3; jj++)
			{
				for( size_t kk=0; kk<3; kk++)
				{
					TypeReal approx = sc1* principalDirection1[ii*3+jj]* principalDirection1[ii*3+kk] + sc2* principalDirection2[ii*3+jj]* principalDirection2[ii*3+kk] + sc3 *normals[ii*3+jj]*normals[ii*3+kk];
					S2PAvals[count + jj*3 + kk ] = approx;
				}
			}
		}
		SpMat AS2P( n_samples*3, n_samples*3 );
		build_spsMatrix( S2Prows, S2Pcols, S2PAvals, AS2P);		
		SpMat Lt = L.transpose();
		SpMat S2PAdata =  Lt* AS2P * L;
		SpVect S2Prhs = Lt* ( AS2P * ipointsEigen);     
		SpMat Ps = Pn * Ds;
		SpMat Pt = Pn * Dt;
		SpVect PsNormal = Ps * verticesEigen;
		SpVect PtNormal = Pt * verticesEigen;
		std::vector<TypeReal> wsN(n_samples);
		std::vector<TypeReal> wtN(n_samples);
		for(int ii=0; ii<n_samples; ii++)
		{
			wsN[ii] = D.lp/ std::pow( std::abs(PsNormal[ii]) + epsilon, 2.0 - D.lp );
			wtN[ii] = D.lp/ std::pow( std::abs(PtNormal[ii]) + epsilon, 2.0 - D.lp );	
		}
		SpMat DiagNs(n_samples,n_samples);
		SpMat DiagNt(n_samples,n_samples);
		build_spsDiagMatrix( wsN, DiagNs);
		build_spsDiagMatrix( wtN, DiagNt);	
		SpMat PnS = Ps.transpose()* DiagNs*Ps + Pt.transpose()* DiagNt *Pt;
		Alsqr += S2PAdata + alpha *PnS;		
		rhs += S2Prhs;
		energy_data += energyS2P_data;
		energy_data += alpha * verticesEigen.dot( PnS*verticesEigen );
		
        energy = energy_data + energy_regularizer;
		
 		
		//------------------------------------------------------------------------------------------------//
		//---------------------------------------- MINIMIZATION------------------------------------------//
		//--------------------------------------------------------- --------------------------------------//
		SpMat Aconst_lsqr( 3*(n_vertex+ n_Bvertex), 3*(n_vertex+ n_Bvertex));
		SpVect rhs_const( 3*(n_vertex+ n_Bvertex) );		
		{
			rhs_const.head(3*n_vertex) = rhs;
			rhs_const.tail(3*n_Bvertex) = fix_vertices;
			size_t count = 6*n_Bvertex;
		    size_t nnz = Alsqr.nonZeros();
			rows.resize( 6*n_Bvertex + nnz );
			cols.resize( 6*n_Bvertex + nnz );
			vals.resize( 6*n_Bvertex + nnz, 0 );			
			for(int k=0; k<Alsqr.outerSize(); ++k)
			{
				for (SpMat::InnerIterator it(Alsqr,k); it; ++it)
				{
					vals[count] = it.value();
					rows[count] = it.row();
					cols[count] = it.col();
					count += 1;
				}
			}
			build_spsMatrix( rows, cols, vals, Aconst_lsqr);
			SpMat Aaux = Aconst_lsqr.transpose();
			SpMat Apseudo = Aaux*Aconst_lsqr;
			SpVect rhs_pseudo = Aaux * rhs_const;
			Eigen::ConjugateGradient<SpMat, Eigen::Lower|Eigen::Upper> solver(Apseudo);
			solver.setMaxIterations(D.solverIterations);
			x = solver.solveWithGuess( rhs_pseudo, x );			
		}

		// project boundary vertices into their initial position
		TypeReal norm_constraint = 0;
		for(int ii=0; ii<n_Bvertex; ii++)
		{
			int bvertex = boundary_vertices[ii];
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow( fix_vertices[ii*3 + idim] - x[ 3*bvertex+ idim], 2);
				x[ 3*bvertex+ idim] = fix_vertices[ii*3 + idim];
			}
			norm_constraint += std::sqrt(dist);
		}
		norm_constraint /= std::max(n_Bvertex,1);
		printf("average distance to constrained vertices %f\n", norm_constraint);
		
		
		SpVect verticesCandidate = x.head(3*n_vertex);
		energy_regularizer = verticesCandidate.dot( R*verticesCandidate );
		energy_data = 0;
		PsNormal = Ps * verticesCandidate;
		PtNormal = Pt * verticesCandidate;
		//
		SpVect S = L*verticesCandidate;
		res = S - ipointsEigen;
		energyS2P_data = 0;
		TypeReal energy_normals = 0;
		for(int ii=0; ii<res.size()/3; ii++)
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(res[ii*3+idim],2);
			}
			energyS2P_data += std::pow(dist + epsilon, D.lp/2);
			energy_normals += std::pow(std::abs( PsNormal[ii] ) +epsilon, D.lp) + std::pow(std::abs( PtNormal[ii] )+epsilon, D.lp);
		}		
		energy_data +=  energyS2P_data;
		energy_data += alpha * energy_normals;
		energy = energy_data + energy_regularizer;
		if( energy_ > energy )
		{
			verticesEigen = verticesCandidate;
		}        
		
		
		
		if(D.visualize>0)
		{ 
			render_controlMesh( winP, vertices, face_nvertex, face_vertex, center, false );
			render_pointCloud( winP, &fix_vertices[0], n_Bvertex, center, 1, 0, 0, false );
			winP.repaint();
			//std::cin.ignore();
		}
 
		
		
		//------------------------------------------------------------------------------------------------//
		//--------------------------------------- CHECK CONVERGENCE --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        x = verticesEigen - verticesEigen_;
        if( (  x.dot(x) <  epsilon_a ) && ( x.dot(x) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iiSD);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iiSD);}
            break;
        }
        energy_ = energy;
        verticesEigen_ = verticesEigen;
		if(D.visualize>0){ printf("iteration %lu, energy data + regularizer: %f + %f = %f\n", iiSD, energy_data/D.randomization, energy_regularizer/D.randomization, energy/D.randomization); }       

        
	}
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("quadraticSD_curvature_fit took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
 
	return energy;
 }		*/
 

TypeReal projectSimplex( const std::vector<TypeReal> &what, std::vector<TypeReal> &weights, TypeReal h, TypeReal lbound, TypeReal ubound )
{
    int n_points = weights.size();
    int maxIter = 100;
    TypeReal epsilon = 1e-6;
    
    // estimate optimal lambda by biscetion method
    // variables to keep track of the interval
    TypeReal a = lbound;
    TypeReal b = ubound;
    TypeReal fa = 0;
    TypeReal fb = 0;
    for(int ii=0; ii<n_points; ii++ )
    {
        fa += std::min( std::max( what[ii]- a, (TypeReal)0.0), (TypeReal)1.0);
        fb += std::min( std::max( what[ii]- b, (TypeReal)0.0), (TypeReal)1.0);
    }
    fa -= h;
    fb -= h;
    // find interval with a root in it
    while(fa *fb >0 )        //Check if a root exists between a and b
    {
        TypeReal range = b - a;
        a -= range/2;
        b += range/2;
        
        fa = 0;
        fb = 0;
        for(int ii=0; ii<n_points; ii++ )
        {
            fa += std::min( std::max( what[ii]- a, (TypeReal)0.0), (TypeReal)1.0);
            fb += std::min( std::max( what[ii]- b, (TypeReal)0.0), (TypeReal)1.0);
        }
        fa -= h;
        fb -= h;
    }
    // bisection
    for( int ii=0; ii< maxIter; ii++ )
    {
        TypeReal c = (a+b)/2;
        TypeReal fc = 0;
        for(int ii=0; ii<n_points; ii++ )
        {
            fc += std::min( std::max( what[ii]- c, (TypeReal)0.0), (TypeReal)1.0);
        }
        fc -= h;
        
        
        if( fa * fc < 0 )
        {
            b = c;
            fb = fc;
        }
        else
        {
            a = c;
            fa = fc;
        }
        
        
        if( (b-a < epsilon) || ( fc==0 ) )
        {
            break;
        }
        
    }
    
    TypeReal lambda_opt = (a + b)/2;
    for(int ii=0; ii<n_points; ii++ )
    {
        weights[ii] = std::max( std::min( what[ii] - lambda_opt, (TypeReal)1.0), (TypeReal)0.0);
    }
    
    return lambda_opt;
}

 
 
TypeReal Distance_trimmedLSQ( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> &weights)
{

 	 
 	 
 	 
 	printf("trimmedLSQR"); 
	clock_t t0 = clock();
	
	
	// get the quad control mesh of a regular Catmull-Clark subdivision surface by one level of subdivision
	// the refinement operator is the sparse linear matrix L0
	


    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t maxIterations = D.maxIterations;
	size_t perDimsamples = (size_t)std::floor( std::sqrt( n_points *1.0 / n_faces ) );
	size_t n_samples = perDimsamples * perDimsamples * n_faces;
	TypeReal alpha = D.alpha;
	TypeReal beta = D.beta/n_faces*n_points;
	TypeReal epsilon = D.epsilon;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	
	TypeReal surface_area = area_surface(vertices, face_nvertex, face_vertex);
    
    // regularizers
    SpMat G, sqrtG, G1d, R;
   	graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
   	R = beta* G;
	
   	
   	weights = std::vector<TypeReal>( n_points, 1.0);
   	TypeReal trimmed_th = n_points * D.lp;
   	TypeReal lambda_lbound = 0.0;
	TypeReal lambda_ubound = 1.0;
	
    // create projection matrices for normals
    SpMat Pn(n_points, 3*n_points);
    build_normalProjection( normals, Pn);

	
	
    // visualization window
	//mrpt::gui::CDisplayWindow3D	winA("quadtraticSDM_curvature_fit",640,480);
	SpMat Id(3*n_vertex,3*n_vertex);
	build_spsDiagMatrix( std::vector<TypeReal>(3*n_vertex, 1.0), Id);
    
    
    // start minimization
	// variables fixed during loop
   // the order of coefficients in local approximarion matrix is A11, A12, A13, A21, A22, A23, A31, A32, A33
    // as they remain fix, we can pre-compute them
    size_t localrows[9] = { 0, 0, 0, 1, 1, 1, 2, 2, 2};
    size_t localcols[9] = { 0, 1, 2, 0, 1, 2, 0, 1, 2};
	std::vector<int> f_params;
	std::vector<TypeReal> s_params, t_params; 
	std::vector<size_t> S2Pcols(n_points*9);
	std::vector<size_t> S2Prows(n_points*9);
	for( size_t ii=0; ii< n_points; ii++ )
	{
		for( size_t jj=0; jj<9; jj++)
		{
			S2Prows[ ii*9 + jj ] = ii*3 + localrows[jj];
			S2Pcols[ ii*9 + jj ] = ii*3 + localcols[jj];
		}
	}    
	//printf("done\n");
	TypeReal energy;
	TypeReal step_size = 0.01;
    Eigen::Map< SpVect > pointsEigen(&points[0], 3*n_points);
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    SpVect verticesEigen_(verticesEigen);
    TypeReal energy_ = std::numeric_limits<TypeReal>::max();
	for( size_t iiSD=0; iiSD< D.maxIterations; iiSD++)
	{
		
		
		
		// global variables for linear systems and optimization
        SpMat Alsqr = R;	
        SpVect rhs = SpVect::Zero(3*n_vertex);		
		TypeReal energy_regularizer = verticesEigen.dot( R*verticesEigen );
		TypeReal energy_data = 0;
		
		
	
		//------------------------------------------------------------------------------------------------//
		//----------------------------- S2P quadratic MAJORIZATION --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		std::vector<TypeReal> closestSpoint0;
		// find closest surface point to each sample
		KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, f_params, s_params, t_params );
		SpMat L, Ds, Dt;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, L, Ds, Dt, true);	
		SpVect closestSpoint = L*verticesEigen;
		SpVect res = pointsEigen - closestSpoint;
		TypeReal energyS2P_data = 0;
		
		// given the closest sample to each point in the pointc loud, construct the quadratic approximation to the squared distance function of the subdivision surface
		std::vector<TypeReal> spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2, principalDirection1, principalDirection2;
		sample_diffmetrics( vertices, face_nvertex, face_vertex, f_params, s_params, t_params, spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2,principalDirection1, principalDirection2 );
		std::vector<TypeReal> S2PAvals(n_points*9);
		for( size_t ii=0; ii< n_points; ii++ )
		{
			TypeReal dist = std::pow( res[ii*3], 2) + std::pow( res[ii*3+1], 2) + std::pow( res[ii*3+2], 2);
			if( std::isnan(dist) ){printf("energy S2P is nan at sample %lu\n", ii);}
			energyS2P_data += weights[ii] * dist;
			dist = std::sqrt( dist + epsilon );
			// quadratic squared distance approx
			size_t count = ii*9;
			TypeReal r1 = std::abs(principalRadius1[ii]);
			TypeReal r2 = std::abs(principalRadius2[ii]);			
			TypeReal sc1 = dist / ( dist + r1 );
			TypeReal sc2 = dist / ( dist + r2 );
			for( size_t jj=0; jj<3; jj++)
			{
				for( size_t kk=0; kk<3; kk++)
				{
					TypeReal approx = sc1* principalDirection1[ii*3+jj]* principalDirection1[ii*3+kk] + sc2* principalDirection2[ii*3+jj]* principalDirection2[ii*3+kk] + normals[ii*3+jj]*normals[ii*3+kk];
					S2PAvals[count + jj*3 + kk ] = weights[ii] * approx;
				}
			}
		}
		SpMat AS2P( n_points*3, n_points*3 );
		build_spsMatrix( S2Prows, S2Pcols, S2PAvals, AS2P);		
		SpMat Lt = L.transpose();
		SpMat S2PAdata =  Lt* AS2P * L;
		SpVect S2Prhs = Lt* ( AS2P * pointsEigen);     
		SpMat Ps = Pn * Ds;
		SpMat Pt = Pn * Dt;
		SpVect PsNormal = Ps * verticesEigen;
		SpVect PtNormal = Pt * verticesEigen;
		SpMat DiagW(n_points,n_points);
		build_spsDiagMatrix( weights, DiagW);	
		SpMat PnS = Ps.transpose()* DiagW*Ps + Pt.transpose()* DiagW *Pt;
		Alsqr += S2PAdata + alpha *PnS;	
		rhs += S2Prhs;
		energy_data += energyS2P_data;
		energy_data += alpha * verticesEigen.dot( PnS*verticesEigen );
		
        energy = energy_data + energy_regularizer;
		if(D.visualize>0){ printf("average energy %f\n", energy/n_points);}
		
		// given the closest sample to each point in the pointc loud, construct the quadratic approximation to the squared distance function of the subdivision surface
		/*SpVect PsNormal = Pn * Ds *verticesEigen;
		SpVect PtNormal = Pn * Dt *verticesEigen;
		for( size_t ii=0; ii< n_points; ii++ )
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow( res[ii*3+idim], 2);
				res[ii*3+idim] *= weights[ii];
			}
			energy_data += weights[ii] * ( dist + alpha * ( std::pow( PsNormal[ii],2) + std::pow( PtNormal[ii],2) ) ) ;
			PsNormal[ii] *= weights[ii];
			PtNormal[ii] *= weights[ii];
		}
		SpMat DiagW(n_points,n_points);
		build_spsDiagMatrix( weights, DiagW);
		SpVect grad_x = -L.transpose()*res + alpha*( Ds.transpose() * PsNormal  + Dt.transpose() * PtNormal );	
		rhs += - grad_x;*/
		
        energy = energy_data + energy_regularizer;
		if(D.visualize>0){ printf("average energy %f\n", energy/n_points);}
		
		
		
		//------------------------------------------------------------------------------------------------//
		//---------------------------------------- MINIMIZATION------------------------------------------//
		//--------------------------------------------------------- --------------------------------------//
		//Alsqr += 1.0/step_size * Id;
		//rhs += 1.0/step_size * verticesEigen;
		SpVect x;
		Eigen::ConjugateGradient<SpMat > solver;
		solver.setMaxIterations(D.solverIterations);
		solver.compute(Alsqr);
		x = solver.solveWithGuess( rhs, verticesEigen );	
		std::vector<TypeReal> residuals(n_points, 0);

		
		SpVect verticesCandidate = x;
		energy_regularizer = verticesCandidate.dot( R*verticesCandidate );
		energy_data = 0;
		PsNormal = Pn * Ds * verticesCandidate;
		PtNormal = Pn * Dt * verticesCandidate;
		//
		SpVect S = L*verticesCandidate;
		res = S - pointsEigen;
		for(int ii=0; ii<res.size()/3; ii++)
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(res[ii*3+idim],2);
			}
			TypeReal Pnorm = std::pow( PsNormal[ii], 2 ) + std::pow( PtNormal[ii], 2);
			residuals[ii] = dist + alpha*Pnorm;
		}
		// minimize over weights
		std::vector<TypeReal> what(weights);
		for(int ii=0; ii<res.size()/3; ii++)
		{
			what[ii] -= step_size*residuals[ii];
		}
		TypeReal lambda_opt = projectSimplex( what, weights, trimmed_th, lambda_lbound, lambda_ubound );
		if( lambda_opt>0 )
		{
			lambda_lbound = lambda_opt/2;
			lambda_ubound = lambda_opt*2;
		}
		else if( lambda_opt<0 )
		{
			lambda_lbound = lambda_opt*2;
			lambda_ubound = lambda_opt/2;
		}
		// compute energy
		TypeReal sumWeights = 0;
		for(int ii=0; ii<res.size()/3; ii++)
		{
			sumWeights += weights[ii];
			energy_data +=  weights[ii]*residuals[ii];
		}
		energy = energy_data + energy_regularizer;
		if( energy_ > energy )
		{
			verticesEigen = verticesCandidate;
		}        
		
		
		//------------------------------------------------------------------------------------------------//
		//--------------------------------------- CHECK CONVERGENCE --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        x = verticesEigen - verticesEigen_;
        if( (  x.dot(x) <  epsilon_a ) && ( x.dot(x) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iiSD);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iiSD);}
            break;
        }
        energy_ = energy;
        verticesEigen_ = verticesEigen;
		if(D.visualize>0){ printf("iteration %lu, sumWeights %f, energy data + regularizer: %f + %f = %f\n", iiSD, sumWeights/n_points, energy_data, energy_regularizer, energy); }       

        
                

		
	}
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("quadraticSD_curvature_fit took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
 
	return energy;
 }
 
 
 
 
 
 
 
 
 
 
 
void approxDistance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, int n_ref )
{

 		 
	clock_t t0 = clock();
	
	
	// get the quad control mesh of a regular Catmull-Clark subdivision surface by one level of subdivision
	// the refinement operator is the sparse linear matrix L0
	
	
	
    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t maxIterations = D.maxIterations;
	TypeReal beta = D.beta/n_faces*n_points;
	TypeReal epsilon = D.epsilon;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	TypeReal surface_area = area_surface(vertices, face_nvertex, face_vertex);
	

	// Generate linear operators that compute the vertices of the refined mesh and its topology
	SpMat uL, uLt, emptyuL;
	std::vector<TypeReal> rVertices;
	std::vector<int> rFace_nvertex, rFace_vertex;
	if( n_ref >0 )
	{
		UniformRefineSurfaceOperators(vertices, face_nvertex, face_vertex, n_ref, rVertices, rFace_nvertex, rFace_vertex, emptyuL, uL);
	}else
	{
		uL = SpMat(3*n_vertex, 3*n_vertex);
		build_spsDiagMatrix(std::vector<TypeReal>(3*n_vertex, 1.0), uL);
		rVertices = vertices;
		rFace_nvertex = face_nvertex;
		rFace_vertex = face_vertex;
	}
	size_t n_rvertex = rVertices.size()/3;
	uLt = uL.transpose();
	
	// compute neighbour connectivity of the refined mesh
	std::vector<int> rVertex_nring, rVertex_0ring, rVertex_ring, vertex_nfaces, vertex_0faces, vertex_faces;
	vertex_ring( rVertices, rFace_nvertex, rFace_vertex, rVertex_nring, rVertex_0ring, rVertex_ring, vertex_nfaces, vertex_0faces, vertex_faces );
	//printf("vertex rings of the refined mesh computed\n");


    

    
    // regularizers
    SpMat G, sqrtG, G1d, R;
   	graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
   	R = beta* G;
		
	
    // start minimization
	// variables fixed during loop
   // the order of coefficients in local approximarion matrix is A11, A12, A13, A21, A22, A23, A31, A32, A33
    // as they remain fix, we can pre-compute them
    size_t localrows[9] = { 0, 0, 0, 1, 1, 1, 2, 2, 2};
    size_t localcols[9] = { 0, 1, 2, 0, 1, 2, 0, 1, 2};
	std::vector<size_t> S2Pcols(n_points*9);
	std::vector<size_t> S2Prows(n_points*9);
	for( size_t ii=0; ii< n_points; ii++ )
	{
		for( size_t jj=0; jj<9; jj++)
		{
			S2Prows[ ii*9 + jj ] = ii*3 + localrows[jj];
			S2Pcols[ ii*9 + jj ] = ii*3 + localcols[jj];
		}
	}    
	//printf("done\n");

    Eigen::Map< SpVect > pointsEigen(&points[0], 3*n_points);
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    Eigen::Map<SpVect> rVerticesEigen( &rVertices[0],3*n_rvertex);
    SpVect verticesEigen_(verticesEigen);
    TypeReal energy_ = std::numeric_limits<TypeReal>::max();
    std::vector<TypeReal> costFunction;
	for( size_t iiSD=0; iiSD< D.maxIterations; iiSD++)
	{
		
		
		
		// global variables for linear systems and optimization
        SpMat Alsqr = R;	
        SpVect rhs = SpVect::Zero(3*n_vertex);		
        rVerticesEigen = uL * verticesEigen;
		TypeReal energy_regularizer = verticesEigen.dot( R*verticesEigen );
		TypeReal energy_data = 0;
		TypeReal energy;
		

		
		// compute SDS approximation at each refined vertex
		std::vector<TypeReal> rprincipalK1, rprincipalK2, rprincipalD1, rprincipalD2, rprincipalD3, metric;
		refinedMeshGeometry( rVertices, rFace_nvertex, rFace_vertex, rVertex_nring, rVertex_0ring, rVertex_ring, vertex_nfaces, vertex_0faces, vertex_faces, rprincipalK1, rprincipalK2, rprincipalD1, rprincipalD2, rprincipalD3, metric);

		
		//--------------------------------------------------------------------------------------------------//
		//------------------------------ S2P quadratic MAJORIZATION ---------------------------------------//
		//---------------------------------------------------------- ---------------------------------------//		
		
		
		
		// find closest refined vertex to each point
		SpMat sample2closestPoint;
		std::vector<int> closestRvertex;
		KdSearch_closestRefinedVertex( points, rVertices, closestRvertex, sample2closestPoint);
		// creates a projection matrix sample2closestPoint
		SpMat L = sample2closestPoint*uL;
		SpVect closestSpoint = L*verticesEigen;
		SpVect res = pointsEigen - closestSpoint;
		TypeReal energyS2P_data = 0;
		// given the refined vertex closest to each point, construct quadratic approximation to the squared distance function of the refined mesh		
		std::vector<TypeReal> S2PAvals(n_points*9);
		for( size_t ii=0; ii< n_points; ii++ )
		{
			TypeReal dist = std::pow( res[ii*3], 2) + std::pow( res[ii*3+1], 2) + std::pow( res[ii*3+2], 2);
			if( std::isnan(dist) ){printf("energy S2P is nan at sample %lu\n", ii);}
			dist = std::sqrt( dist +epsilon);
			energyS2P_data += std::pow(dist, D.lp);
			// quadratic squared distance approx
			int i0 = closestRvertex[ii];
			//printf("closest refined vertex to point %d is %d\n", ii, i0);
			TypeReal r1 = 1.0/std::max(  std::abs(rprincipalK1[i0]) , eps);
			TypeReal r2 = 1.0/std::max(  std::abs(rprincipalK2[i0]) , eps);
			// quadratic squared distance approx
			size_t count = ii*9;
			TypeReal sc1 = 0;
			TypeReal sc2 = 0;
			TypeReal sc3 = 1.0;
			if( dist> eps )
			{
				TypeReal dp_1 = std::pow( dist, D.lp - 1.0);
				TypeReal dp_2 = std::pow( dist, D.lp - 2.0);
				sc1 = D.lp * dp_1 / ( dist + r1 );
				sc2 = D.lp * dp_1 / ( dist + r2 );
				sc3 = D.lp * dp_2;	
			}
			for( size_t jj=0; jj<3; jj++)
			{		
				for( size_t kk=0; kk<3; kk++)
				{
					S2PAvals[count + jj*3 + kk ] = ( sc1* rprincipalD1[i0*3+jj]* rprincipalD1[i0*3+kk] + sc2* rprincipalD2[i0*3+jj]* rprincipalD2[i0*3+kk] + sc3 *rprincipalD3[i0*3+jj]*rprincipalD3[i0*3+kk] );
				}
			}	
		}
		SpMat AS2P( n_points*3, n_points*3 );
		build_spsMatrix( S2Prows, S2Pcols, S2PAvals, AS2P);		
		SpMat Lt = L.transpose();
		SpMat S2PAdata =  Lt* AS2P * L;
		SpVect S2Prhs = Lt* ( AS2P * pointsEigen);     
		Alsqr += S2PAdata;		
		rhs += S2Prhs;
		energy_data += energyS2P_data;

		

		//--------------------------------------------------------------------------------------------------//
		//----------------------------------------- MINIMIZATION- -------------------------------------//
		//---------------------------------------------------------- ---------------------------------------//
		SpVect x;
		Eigen::ConjugateGradient<SpMat > solver;
		solver.setMaxIterations(D.solverIterations);
		solver.compute(Alsqr);
		x = solver.solveWithGuess( rhs, verticesEigen );		
		
		
		SpVect verticesCandidate = x;	
		energy_regularizer = verticesCandidate.dot( R*verticesCandidate );
		energy_data = 0;		//
		SpVect S = L*verticesCandidate;
		res = S - pointsEigen;
		energyS2P_data = 0;	
		for(int ii=0; ii<res.size()/3; ii++)
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(res[ii*3+idim],2);
			}
			energyS2P_data += std::pow(dist + epsilon, 0.5*D.lp);
		}		
		energy_data +=  energyS2P_data;
		energy = energy_data + energy_regularizer;		
		if( energy_ > energy )
		{
			verticesEigen = verticesCandidate;
		}
        
		
		
		//--------------------------------------------------------------------------------------------------//
		//---------------------------------------- CHECK CONVERGENCE ---------------------------------------//
		//---------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        x = verticesEigen - verticesEigen_;
        if( (  x.dot(x) <  epsilon_a ) && ( x.dot(x) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iiSD);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iiSD);}
            break;
        }
        energy_ = energy;
        verticesEigen_ = verticesEigen;
		if(D.visualize>0){ printf("iteration %lu, energy data + regularizer: %f + %f = %f\n", iiSD, energy_data, energy_regularizer, energy);      }  

        
		
	}
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("fit took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
 
 }

 
 
 
 
   
TypeReal sqrDistance_1storderApprox( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals)
{

 	 
 	 
	clock_t t0 = clock();
	
	
	
	// get the quad control mesh of a regular Catmull-Clark subdivision surface by one level of subdivision
	// the refinement operator is the sparse linear matrix L0
	


    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t maxIterations = D.maxIterations;
	size_t perDimsamples = (size_t)std::floor( std::sqrt( n_points *1.0 / n_faces ) );
	size_t n_samples = perDimsamples * perDimsamples * n_faces;
	TypeReal beta = D.beta/n_faces*n_points;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	TypeReal surface_area = area_surface(vertices, face_nvertex, face_vertex);

    
    // regularizers
    SpMat G, sqrtG, G1d, R;
   	graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
   	R = beta* G;
	
	
     // start minimization
	// variables fixed during loop
   // the order of coefficients in local approximarion matrix is A11, A12, A13, A21, A22, A23, A31, A32, A33
    // as they remain fix, we can pre-compute them
    size_t localrows[9] = { 0, 0, 0, 1, 1, 1, 2, 2, 2};
    size_t localcols[9] = { 0, 1, 2, 0, 1, 2, 0, 1, 2};
	std::vector<int> f_params;
	std::vector<TypeReal> s_params, t_params; 
	std::vector<size_t> S2Pcols(n_points*9);
	std::vector<size_t> S2Prows(n_points*9);
	for( size_t ii=0; ii< n_points; ii++ )
	{
		for( size_t jj=0; jj<9; jj++)
		{
			S2Prows[ ii*9 + jj ] = ii*3 + localrows[jj];
			S2Pcols[ ii*9 + jj ] = ii*3 + localcols[jj];
		}
	}    
	//printf("done\n");

    Eigen::Map< SpVect > pointsEigen(&points[0], 3*n_points);
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    SpVect verticesEigen_(verticesEigen);
    TypeReal energy_ = std::numeric_limits<TypeReal>::max();
    TypeReal energy;
	for( size_t iiSD=0; iiSD< D.maxIterations; iiSD++)
	{
		
		
		
		// global variables for linear systems and optimization
        SpMat Alsqr = R;	
        SpVect rhs = SpVect::Zero(3*n_vertex);		
		TypeReal energy_regularizer = verticesEigen.dot( R*verticesEigen );
		TypeReal energy_data = 0;
		
		
	
		//------------------------------------------------------------------------------------------------//
		//----------------------------- S2P quadratic MAJORIZATION --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		std::vector<TypeReal> closestSpoint0;
		// find closest surface point to each sample
		KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, f_params, s_params, t_params );
		SpMat L, Ds, Dt;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, L, Ds, Dt, true);	
		SpVect closestSpoint = L*verticesEigen;
		SpVect res = pointsEigen - closestSpoint;
		TypeReal energyS2P_data = 0;
		// given the closest sample to each point in the pointc loud, construct the quadratic approximation to the squared distance function of the subdivision surface
		std::vector<TypeReal> spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2, principalDirection1, principalDirection2;
		sample_diffmetrics( vertices, face_nvertex, face_vertex, f_params, s_params, t_params, spoints, normals, metrics, meanCurvature, principalRadius1, principalRadius2,principalDirection1, principalDirection2 );
		std::vector<TypeReal> S2PAvals(n_points*9);
		std::vector<TypeReal> dists(n_points, 0);
		TypeReal maxDist = eps;
		for( size_t ii=0; ii< n_points; ii++ )
		{
			TypeReal dist = std::pow( res[ii*3], 2) + std::pow( res[ii*3+1], 2) + std::pow( res[ii*3+2], 2);
			if( std::isnan(dist) ){printf("energy S2P is nan at sample %lu\n", ii);}
			energyS2P_data += dist;
			dists[ii] = std::sqrt( dist );	
			if( dists[ii] >maxDist ){ maxDist = dists[ii]; }
		}
		for( size_t ii=0; ii< n_points; ii++ )
		{
			// quadratic squared distance approx
			size_t count = ii*9;	
			TypeReal sc1 = 0;
			TypeReal sc2 = 0;
			TypeReal sc3 = 1.0;
			if( dists[ii]> eps )
			{
				TypeReal mu = dists[ii]/(2.0*maxDist);
				sc1 = mu;
				sc2 = mu;
				sc3 = 1.0 - mu;			
			}
			for( size_t jj=0; jj<3; jj++)
			{
				for( size_t kk=0; kk<3; kk++)
				{
					TypeReal approx = sc1* principalDirection1[ii*3+jj]* principalDirection1[ii*3+kk] + sc2* principalDirection2[ii*3+jj]* principalDirection2[ii*3+kk] + sc3 *normals[ii*3+jj]*normals[ii*3+kk];
					S2PAvals[count + jj*3 + kk ] = approx;
				}
			}
		}
		SpMat AS2P( n_points*3, n_points*3 );
		build_spsMatrix( S2Prows, S2Pcols, S2PAvals, AS2P);		
		SpMat Lt = L.transpose();
		SpMat S2PAdata =  Lt* AS2P * L;
		SpVect S2Prhs = Lt* ( AS2P * pointsEigen);     
		Alsqr += S2PAdata;		
		rhs += S2Prhs;
		energy_data += energyS2P_data;
		
        energy = energy_data + energy_regularizer;
		if(D.visualize>0){ printf("average energy %f\n", energy/n_points);}
		
		
		
		
		//------------------------------------------------------------------------------------------------//
		//---------------------------------------- MINIMIZATION------------------------------------------//
		//--------------------------------------------------------- --------------------------------------//
		SpVect x;
		Eigen::ConjugateGradient<SpMat > solver;
		solver.setMaxIterations(D.solverIterations);
		solver.compute(Alsqr);
		x = solver.solveWithGuess( rhs, verticesEigen );		

		
		SpVect verticesCandidate = x;
		energy_regularizer = verticesCandidate.dot( R*verticesCandidate );
		energy_data = 0;
		//
		SpVect S = L*verticesCandidate;
		res = S - pointsEigen;
		energyS2P_data = 0;
		TypeReal energy_normals = 0;
		for(int ii=0; ii<res.size()/3; ii++)
		{
			TypeReal dist = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(res[ii*3+idim],2);
			}
			energyS2P_data += dist;
		}		
		energy_data +=  energyS2P_data;
		energy = energy_data + energy_regularizer;
		if( energy_ > energy )
		{
			verticesEigen = verticesCandidate;
		}        
		
		
		//------------------------------------------------------------------------------------------------//
		//--------------------------------------- CHECK CONVERGENCE --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        x = verticesEigen - verticesEigen_;
        if( (  x.dot(x) <  epsilon_a ) && ( x.dot(x) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iiSD);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iiSD);}
            break;
        }
        energy_ = energy;
        verticesEigen_ = verticesEigen;
		if(D.visualize>0){ printf("iteration %lu, energy data + regularizer: %f + %f = %f\n", iiSD, energy_data, energy_regularizer, energy);    }    

        
                

		
	}
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("quadraticSD_curvature_fit took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
	return energy;
 }
 
 
  
 /*---------------------------------------------------------------------------------------------------------------------*/
 /*------------------------------------   lBFGS       ------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
 
 
TypeReal compute_gradient( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, const SpMat &Pn, const SpMat &regM, Settings D, SpVect &grad )
{
    
    // evaluate the residual
    size_t n_vertex = vertices.size()/3;
    size_t n_points = points.size()/3;
    TypeReal epsilon = D.epsilon;
    
    TypeReal lambda = 1.0;// assume the regulatizer already includes the scaling
    TypeReal alpha = D.alpha;
    TypeReal p = D.lp;
   
    // map from std vectors to eigen variables
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    Eigen::Map< SpVect > pointsEigen(&points[0], 3*n_points);
    
    // sample the surface with current estimates to 
    SpMat L, Ds, Dt, Dss, Dst, Dtt;
    //tsampling limit surface
    SampleD2LimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, L, Ds, Dt, Dss, Dst, Dtt);
    SpVect S = L* verticesEigen;
    SpVect Ss = Ds* verticesEigen;
    SpVect St = Dt* verticesEigen;
    SpVect Pnss = Pn * Dss * verticesEigen;
    SpVect Pnst = Pn * Dst * verticesEigen;
    SpVect Pntt = Pn * Dtt * verticesEigen;    
    SpMat Pns = Pn * Ds;
    SpMat Pnt = Pn * Dt;
    
    //compute the residuals
    SpVect res_data = ( S - pointsEigen );
    SpVect res_snormal =  Pn * Ss;
    SpVect res_tnormal =  Pn * St;
    grad.resize(3*n_vertex + 2*n_points);
    TypeReal energy = 0.5* verticesEigen.dot( regM *verticesEigen );
    for(int ii=0; ii< n_points; ii++)
    {
       TypeReal norm = 0;
       for(int idim=0; idim<3; idim++)
       {
       	   norm += std::pow( res_data[ii*3+idim], 2);   
       }
       norm = std::sqrt(norm + epsilon);
       TypeReal norm_2mp = std::pow( norm, 2.0 - p );
       TypeReal ipns = std::pow( std::abs(res_snormal[ii]) + epsilon, 2.0 - p );
       TypeReal ipnt = std::pow( std::abs(res_tnormal[ii]) + epsilon, 2.0 - p );
       for(int idim=0; idim<3; idim++)
       {
       	   res_data[ii*3+idim] *= p / norm_2mp;   
       }       
       res_snormal[ii] *= p / ipns;
       res_tnormal[ii] *= p / ipnt;  
       energy += std::pow( norm, p ) + alpha * std::pow( std::abs(res_snormal[ii]) + epsilon, p ) + alpha * std::pow( std::abs(res_tnormal[ii]) + epsilon, p );
    }

    // partial derivatives w.r.t. (s,t) parameters
    for(int ii=0; ii< n_points; ii++)
    {   
    	TypeReal ips = 0;
    	TypeReal ipt = 0;
    	for(int idim=0; idim<3; idim++)
    	{
    		ips += res_data[ii*3+idim] *Ss[ii*3+idim];	
    		ipt += res_data[ii*3+idim] *St[ii*3+idim];	
    	}    	
    	grad[3*n_vertex +ii] = ips + alpha* ( res_snormal[ii] * Pnss[ii] + res_tnormal[ii] * Pnst[ii] );
    	grad[3*n_vertex + n_points + ii] = ipt + alpha* ( res_snormal[ii] * Pnst[ii] + res_tnormal[ii] * Pntt[ii] );	
    }
    // partial derivatives w.r.t. control points
    grad.head(3*n_vertex) =  L.transpose() * res_data + alpha * ( Pns.transpose() * res_snormal + Pnt.transpose() * res_tnormal ) + regM *verticesEigen;
    
    // evaluate the function
    
    
    return energy;
    
}
 
 
 
void lpOptimization_lBFGS(  std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals )
{

	clock_t t0 = clock();

    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t memory = D.solverIterations;    
    size_t maxLineSearch = 30;
    TypeReal epsilon_descent = -1e-3;
    TypeReal epsilon_sdp = 1e-6;	
    TypeReal epsilon = D.epsilon;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	

	
	// precompute adjacency of faces with respect to (s,t)-parameters
    std::vector<int> changeCoord, adjacency;
    compute_params_adjacency(vertices, face_nvertex, face_vertex, adjacency, changeCoord );
    
    
    // regularizers
    SpMat G, sqrtG;
    TypeReal beta = D.beta/n_faces*n_points;
    graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
    SpMat R = beta* G;
    
    
   
    // create projection matrices for normals
    SpMat Pn(n_points, 3*n_points);
    build_normalProjection( normals, Pn);
    
    
    // initialize the surface footnote parameters by search +
    std::vector<int> fparams;
    std::vector<TypeReal> sparams, tparams;
    KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
	
    
    
    
    
    size_t n_vars = n_vertex*3 + n_points*2;
    size_t M = std::min(memory, D.maxIterations);
	// memory as circular list
    std::vector<SpVect> Ss( M, SpVect::Zero(n_vars) );
    std::vector<SpVect> Ys( M, SpVect::Zero(n_vars) );
    std::vector<TypeReal> Rhos( M, 0 );
    // initialize variables
    SpVect x(n_vars);
    memcpy(&x[0], &vertices[0], n_vertex*3*sizeof(TypeReal));
    memcpy(&x[n_vertex*3], &sparams[0], n_points*sizeof(TypeReal));
    memcpy(&x[n_vertex*3 + n_points], &tparams[0], n_points*sizeof(TypeReal));
    SpVect x_(x);
    SpVect g(n_vars);
    SpVect g_ = SpVect::Zero(n_vars);
    SpMat H0(n_vars,n_vars);
    // line-search parameters of Zhang & Hager (siam J. on. Opt. 2004)
    std::vector<int> f_(fparams);
    std::vector<TypeReal> s_(sparams);
    std::vector<TypeReal> t_(tparams);
    std::vector<TypeReal> vertices_(vertices);
    TypeReal energy_, energy;
    TypeReal lsC = 1.0;
    TypeReal lsQ = 1.0;
    std::vector<TypeReal> costFunction;
    size_t mem_count = 0;
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    SpVect verticesEigen_(verticesEigen);
    for( size_t iter=0; iter< D.maxIterations; iter++)
	{
		
        size_t current = mem_count % M;
        TypeReal step_size = 1e0;
        
        // compute gradien w.r.t. parameters
        SpVect diagH(n_vars);
        // store descent direction is in z
        SpVect z;
        if( mem_count < M )
        {
            energy_ = compute_gradient( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, Pn, R, D, g );
            costFunction.push_back( energy_ );
            // compute current value of cost function to initialize line-search parameters
            lsC = energy_;
            if(D.visualize>0){ if(mem_count==0){ printf("initial energy %f\n", energy_);}}
            
            // descent direction given by the gradient
            z = -g;
            step_size = 1e-1;
        }
        else
        {
            //step_size = step_size0;
            if(D.visualize>0){ if(mem_count==M){ printf("stopping gradient descent, using the memory\n"); }}
            step_size = 1e0; // line-search starts with 1e0 to allow a full Newton step
            // compute uphill direction with the two-loop procedure
            SpVect q(n_vars);
            q = -g;
            std::vector<TypeReal> alphas(M);
            // backward loop
            for( size_t ii=0; ii<std::min(M, iter); ii++)
            {
                size_t id = ((current-1-ii) + M )%M;
                alphas[ii] = Rhos[id] * ( q.dot( Ss[id] ) );
                q = q - alphas[ii]*Ys[id];
            }
            
            // forward loop
            // initialize Hessian with a multiple of identity
            size_t id = ((current-1) + M )%M;
            TypeReal denom = Ys[id].dot( Ys[id] );
            TypeReal num = Ys[id].dot( Ss[id] );
            z = num/std::max(denom, eps) * q;
            for( size_t ii=0; ii<std::min(M, iter); ii++)
            {
                size_t id = ((current-M +ii) + M )%M;
                TypeReal betaLBFGS = Rhos[id] * (z.dot( Ys[id] ) );
                z = z + (alphas[M-1-ii] - betaLBFGS)* Ss[ id ];
            }
        }
		
		
        // check sufficient descent direction
        if( z.dot(g) > -epsilon_descent * std::sqrt( z.dot(z) * g.dot(g) ) ){ printf("not a descent direction\n");}
        
        
        // line search starts with 1e0 to allow for a full Newton step close to the optimum
        
        int deltaf;
        for(size_t ls_it=0; ls_it<maxLineSearch; ls_it++)
        {
            deltaf = 0;
            // update footnote parameters wit current time step
            for (size_t ipoint=0; ipoint< n_points; ipoint++)
            {
                sparams[ipoint] = std::min( std::max( sparams[ipoint] + step_size * z[3*n_vertex+ipoint] , (TypeReal)0.0 ), (TypeReal)1.0) ;
                tparams[ipoint] = std::min( std::max( tparams[ipoint] + step_size * z[3*n_vertex+n_points+ipoint] , (TypeReal)0.0 ), (TypeReal)1.0) ;
                // check the local parameter values stay within the domain of the face
                int df = reparametrize_sample( adjacency, changeCoord, fparams, sparams, tparams, ipoint );
                deltaf += df;
                x[3*n_vertex + ipoint] = sparams[ipoint];
                x[3*n_vertex + n_points + ipoint] = tparams[ipoint];
            }
            // update the control vertices wit current time step
            for (size_t iv=0; iv< 3*n_vertex; iv++)
            {
                vertices[iv] = vertices[iv] + step_size * z[iv];
                x[ iv ] = vertices[iv];
            }
            
            
            // compute energy and gradient for current step size
            SpVect gk;
            energy = compute_gradient( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, Pn, R, D, gk );
            //printf("\t deltaf %d\n", deltaf);
            
            
            // check Wolfe conditions for descent direction z and step size alpha
            // Armijo condition  f(x_k - alpha*z ) < f_k + delta* alpha * \nabla f( x_k ) * z
            //lsC = energy_; uncomment that for standard wolfe conditions instead of Zhang & Hager's non-decreasing line-search
            bool first_WC = (energy < lsC + 1e-4 * step_size * g.dot(z) )? true : false;
            // curvature condition \nabla f( x_k + alpha*z ) * z > sigma * \nabla f(x_k) * z
            bool second_WC = ( gk.dot(z) > 0.9 * g.dot(z) ) ? true : false;
            
            
             // update step size
            if( ( first_WC && second_WC) || (ls_it==maxLineSearch-1) )
            {
            	step_size = step_size*2.0;
                if(D.visualize>0){ printf("\t energy %f at iteration %lu\n", energy, iter);}
                // update line-search parameters
                lsC = (0.85*lsQ*lsC + energy_)/( 0.85*lsQ + 1 );
                lsQ = 0.85*lsQ + 1;
                g = gk;
                break;
            }else{
                sparams = s_;
                tparams = t_;
                fparams = f_;
                vertices = vertices_;
                step_size = step_size*0.5;
            }
        }
            
        // update memory
        SpVect resX = x- x_;
        SpVect resY = g- g_;
        if( resX.dot( resY) > epsilon_sdp * resY.dot(resY) ) // update memory only when we can ensure that the inverse Hessian will be SPD
        {
        	Ss[current] = resX;
        	Ys[current] = resY;
        	Rhos[current] = 1.0/Ss[current].dot(Ys[current]);
        	mem_count += 1;
        }
        
		//------------------------------------------------------------------------------------------------//
		//--------------------------------------- CHECK CONVERGENCE --------------------------------------//
		//--------------------------------------------------------- --------------------------------------//		
		// stopping criterion
		if( iter>M){
        //if( (resX.dot( resX) <  epsilon_a * n_vars ) && ( resX.dot(resX) <  epsilon_r*x.dot(x) ) && (deltaf == 0) )
        SpVect deltav = verticesEigen - verticesEigen_;
        if( (  deltav.dot(deltav) <  epsilon_a ) && ( deltav.dot(deltav) <  epsilon_r*verticesEigen.dot(verticesEigen) )  )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because surface does not change\n", iter);}
            break;
        }
        else if( ( std::abs( energy-energy_)< epsilon_r* std::abs(energy) ) )
        {
            if(D.visualize>0){ printf("stopped SD1 optimization at iteration %lu because energy does not change\n", iter);}
            break;
        }
        }
        // update old values
        x_ = x;
        g_ = g;
		s_ = sparams;
		t_ = tparams;
		f_ = fparams;
		vertices_ = vertices;
		verticesEigen_ = verticesEigen;
        energy_ = energy;
        costFunction.push_back(energy_);        
        
		
	}
	
	
	clock_t t1 = clock();
	if(D.visualize>0){ printf("lBFGS optimization took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
 
 } 
 
 
 
 /* --------------------------------------------- LM -------------------------------------------*/

 
// optimize subdivision surface, vertices  
void computeLM_JacobianResidual( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, const SpMat &Pn, const SpMat &regM, SpMat &J, SpVect &res )
{
    // energy to minimize is E(x) = sum_{i=1}^{n} [ y_i - f_i(x) ]^2
    // the gradient of E is  -J^T [ y - f ] = -J^T r
    // where the residual r = [y_i - f_i(x)] is \in R^n
    // and the jacobian of f J has entries J_{i,j} = \frac{ \partial f_i }{ \partial x_j } and is \in R^{n \times m}
    //
    // to update the current solution x_{k+1} = x_{k} + \Delta
    // Levenberg combines  gradient descent update ( \Delta = 1/lambda J^T r ) with a Gauss-Newton approximation to the Hessian  (J^T J) \Delta = J^T r 
    // into 			( J^T J + lambda I ) \Delta = J^T r
    // Marquardt takes into account curvature informtaion in the gradient-descent direction with 
    //					( J^T J + lambda diag(J^T J) ) \Delta = J^T r
    // This routine computes J and r for energy with
    // n_points   data terms 	[ point_i - S( s_i, t_i) ]^2
    // n_points   data terms 	[ normal_i \dot \frac{ \partial S}{ \partial s}(s_i, t_i) ]^2  
	// n_points   data terms 	[ normal_i \dot \frac{ \partial S}{ \partial t}(s_i, t_i) ]^2  
	// 			  regularizer    x^t regM^T regM x       
    
    // evaluate the residual
    size_t n_vertex = vertices.size()/3;
    size_t n_points = points.size()/3;
    // map from std vectors to eigen variables
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    Eigen::Map< SpVect > pointsEigen(&points[0], 3*n_points);
    
    // sample the surface with current estimates to 
    SpMat L, Ds, Dt, Dss, Dst, Dtt;
    //tsampling limit surface
    SampleD2LimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, L, Ds, Dt, Dss, Dst, Dtt);
    SpVect S = L* verticesEigen;
    SpVect Ss = Ds* verticesEigen;
    SpVect St = Dt* verticesEigen;
    SpVect Sss = Dss * verticesEigen;
    SpVect Sst = Dst * verticesEigen;
    SpVect Stt = Dtt * verticesEigen;    

    // compute residual
    res.resize(5*n_points+regM.rows());
    res.head(3*n_points) = pointsEigen - S;
    Eigen::Map< SpVect > res_ns( &res[3*n_points], n_points);
    res_ns = - Pn * Ss; 
    Eigen::Map< SpVect > res_nt( &res[4*n_points], n_points);
    res_nt = - Pn * St;
    res.tail(regM.rows()) = - regM*verticesEigen;
    


    // create the sparse matrix of the joint jacobian     
    SpMat PDs = Pn *Ds;
    SpMat PDt = Pn * Dt;
    SpVect PSss = Pn *Sss;
    SpVect PSst = Pn * Sst; 
    SpVect PStt = Pn * Stt; 
    size_t nnz = L.nonZeros()+ regM.nonZeros() + PDs.nonZeros() + PDt.nonZeros();
    std::vector<size_t> rows( 10*n_points + nnz );
    std::vector<size_t> cols( 10*n_points + nnz );
    std::vector<TypeReal> vals( 10*n_points + nnz );
    size_t count = 0;
    // compute the derivatives w.r.t. vertices, first block column of Jacobian matrix
    // for residual (surface - point)
    for(int k=0; k<L.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(L,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row();
            cols[count] = it.col();
            count += 1;
        }
    }// for residual pointNormal.dot( surface_derivative_s )
    for(int k=0; k<PDs.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(PDs,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row() + 3*n_points;
            cols[count] = it.col();
            count += 1;
        }
    }// for residual pointNormal.dot( surface_derivative_t )
    for(int k=0; k<PDt.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(PDt,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row() + 4*n_points;
            cols[count] = it.col();
            count += 1;
        }
    }// for residual associated with the regularizer * vertices
    for(int k=0; k<regM.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(regM,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row() + 5*n_points;
            cols[count] = it.col();
            count += 1;
        }
    }   
    // compute the derivatives w.r.t. parametric variables, second and third block columns of Jacobian matrix (they are tri-diagonal)
    // for residual ( surface - point)
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
        for(size_t idim=0; idim<3; idim++)
        {
            rows[ count ] = 3*ipoint + idim;
            cols[ count ] = 3*n_vertex + ipoint;
            vals[ count ] = Ss[ 3*ipoint + idim];
            count += 1;
            rows[ count ] = 3*ipoint + idim;
            cols[ count ] = 3*n_vertex + n_points + ipoint;
            vals[ count ] = St[ 3*ipoint + idim];
            count += 1;
        }
    }
    // for residual pointNormal.dot( surface_derivative_s )
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
		rows[ count ] = ipoint + 3*n_points;
		cols[ count ] = 3*n_vertex + ipoint;
		vals[ count ] = PSss[ ipoint ];
		count += 1;
		rows[ count ] = ipoint + 3*n_points;
		cols[ count ] = 3*n_vertex + n_points + ipoint;
		vals[ count ] = PSst[ ipoint];
		count += 1;
    }    
    // for residual pointNormal.dot( surface_derivative_t )
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
		rows[ count ] = ipoint + 4*n_points;
		cols[ count ] = 3*n_vertex + ipoint;
		vals[ count ] = PSst[ ipoint ];
		count += 1;
		rows[ count ] = ipoint + 4*n_points;
		cols[ count ] = 3*n_vertex + n_points + ipoint;
		vals[ count ] = PStt[ ipoint];
		count += 1;
    }        
    J.resize(5*n_points + regM.rows(), 2*n_points+3*n_vertex);
    build_spsMatrix(rows, cols, vals, J);
    
}


// no color
TypeReal LeastSquaresOptimization_LM( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals )
{

	clock_t t0 = clock();
	int MMiterations = 1000;
    size_t maxLineSearch = 20;
    TypeReal damping_factor = 1e0;  
    int restartPeriod = MMiterations + 1;//1;
    
    
    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();

	TypeReal beta = D.beta/n_faces*n_points;
    TypeReal alpha = D.alpha;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();

	
	// precompute adjacency of faces with respect to (s,t)-parameters
    std::vector<int> changeCoord, adjacency;
    compute_params_adjacency(vertices, face_nvertex, face_vertex, adjacency, changeCoord );
    
    
    // regularizers
    SpMat G, sqrtG;
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    //printf("precomputing quad regularizer ...");
    graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
    //printf("done\n");
    SpMat R = beta* G;
    //sqrtG = std::sqrt(0.5) * std::sqrt(beta ) * sqrtG;
    sqrtG = std::sqrt(beta ) * sqrtG;
    
    
   
    // create projection matrices for normals
    SpMat Pn(n_points, 3*n_points);
    build_normalProjection( normals, Pn);
    
    
    // initialize the surface footnote parameters by search +
    std::vector<int> fparams;
    std::vector<TypeReal> sparams, tparams;
    KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
	
    
    // initialize weights
    int nLM = 5*n_points + sqrtG.rows();
    std::vector<TypeReal> weights(nLM, 1.0);
    TypeReal energyMM = 0;
    {
    	std::vector<TypeReal> S, St, Ss;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
		energyMM =  0.5*verticesEigen.dot( R*verticesEigen );
		for (size_t ipoint=0; ipoint< n_points; ipoint++)
		{
			TypeReal dist = 0;
			TypeReal ips = 0;
			TypeReal ipt = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += (S[ ipoint*3+idim ] - points[ ipoint*3+idim ], 2);
				ips += Ss[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
				ipt += St[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
			}
			dist = dist>eps ? std::sqrt( dist ) : eps;
			ips = std::abs(ips);
			ipt = std::abs(ipt);
			TypeReal wpoint =  D.lp *std::pow( dist+ D.epsilon, D.lp - 2.0);
			for(int idim=0; idim<3; idim++)
			{
				weights[ ipoint*3 + idim ] = wpoint;
			}
			weights[ 3*n_points + ipoint ] = alpha * D.lp *std::pow( ips + D.epsilon, D.lp - 2.0);
			weights[ 4*n_points + ipoint ] =  alpha * D.lp *std::pow( ipt + D.epsilon, D.lp - 2.0);
			energyMM += std::pow( dist, D.lp ) + alpha * ( std::pow(ips, D.lp) + std::pow(ipt, D.lp) );
		}
	}

    
    // update verticesEigen_ after each MM iteration
    for(int itMM=0; itMM< MMiterations; itMM++)
    {
    	// keep track of previous solution;
    	std::vector<TypeReal> weights_(weights);
    	SpVect verticesEigen_(verticesEigen); 
    	TypeReal energyMM_ = energyMM;
    	
    	// initialize variables
    	size_t n_vars = 2*n_points+3*n_vertex;
    	SpMat diagW(nLM, nLM );
    	build_spsDiagMatrix(weights,diagW);
		SpVect x = SpVect::Zero(n_vars);
		SpMat J, Jt;
		SpVect res;
		TypeReal energy = 0;
		for( size_t iter=0; iter< D.maxIterations; iter++)
		{
			
			
			
			// keep track of previous solution
			SpVect x_(x);
			TypeReal energy_ = energy;
			std::vector<int> f_(fparams);
			std::vector<TypeReal> s_(sparams);
			std::vector<TypeReal> t_(tparams);
			std::vector<TypeReal> vertices_(vertices);
			vertices_ = vertices;			
		
			//------------------------------------------------------------------------------------------------//
			//---------------------- compute residuals and Jacobian of residuals -----------------------------//
			//--------------------------------------------------------- --------------------------------------//		
			// energy to minimize is E(x) = sum_{i=1}^{n} w_i[ y_i - f_i(x) ]^2
			// the gradient of E is  -J^T diagW [ y - f ] = -J^T diagW r
			// where the residual r = [y_i - f_i(x)] is \in R^n
			// and the jacobian of f J has entries J_{i,j} = \frac{ \partial f_i }{ \partial x_j } and is \in R^{n \times m}
			//
			// to update the current solution x_{k+1} = x_{k} + \Delta
			// Levenberg combines  gradient descent update ( \Delta = 1/lambda J^T diagW r ) with a Gauss-Newton approximation to the Hessian  (J^T diagW J) \Delta = J^T diagW r 
			// into 			( J^T diagW J + lambda I ) \Delta = J^T diagW r
			// Marquardt takes into account curvature infomrtaion in the gradient-descent direction with 
			//					( J^T diagW J + lambda diag(J^T diagW J) ) \Delta = J^T diagW r
			// the routine computeLM_JacobianResidual computes J and r
			if( iter==0 )
			{
				computeLM_JacobianResidual( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, Pn, sqrtG, J, res );
				energy_ = res.dot( diagW * res );
				Jt = J.transpose();
			}
			
			
			
			//------------------------------------------------------------------------------------------------//
			//---------------------------------------- LINE  SEARCH ------------------------------------------//
			//--------------------------------------------------------- --------------------------------------//
			bool criticalPoint = false;
			for(size_t ls_it=0; ls_it<maxLineSearch; ls_it++)
			{
				
				// LM Hessian approximation
				SpMat JtJ = Jt* diagW * J;
				SpVect diagElems = JtJ.diagonal();
				SpMat diagH( n_vars, n_vars );
				for (size_t ipoint=0; ipoint< n_vars; ipoint++)
				{
					if( diagElems[ipoint]< 1e-6 )
					{
						diagElems[ipoint] = 1e-6;
					}
				}
				build_spsDiagMatrix(diagElems,diagH);
				SpMat H = JtJ + damping_factor * diagH;
				
				// compute the rhs = - gradient
				SpVect rhs = Jt * diagW * res;
				TypeReal max_rhs = 0;
				// if the gradient is small stop
				for(int kk=0; kk< rhs.size(); kk++){ if(max_rhs < std::abs(rhs[kk]) ) { max_rhs = std::abs(rhs[kk]); } }
				if( max_rhs < epsilon_a ) { criticalPoint = true; energy = energy_; break; }
				
				// solve linear system to compute descent direction q
				Eigen::ConjugateGradient<SpMat > solver;
				solver.setMaxIterations(D.solverIterations);
				solver.compute(H);
				x = solver.solveWithGuess( rhs, x );

	
				
				
				//updating parameters
				for (size_t ipoint=0; ipoint< n_points; ipoint++)
				{
					//sparams[ipoint] = std::min( std::max( sparams[ipoint]+ x[3*n_vertex+ipoint] , (TypeReal)0.0 ), (TypeReal)1.0);
					//tparams[ipoint] = std::min( std::max( tparams[ipoint]+ x[3*n_vertex+ipoint+n_points] , (TypeReal)0.0 ), (TypeReal)1.0);
					sparams[ipoint] = sparams[ipoint]+ x[3*n_vertex+ipoint];	
					tparams[ipoint] = tparams[ipoint]+ x[3*n_vertex+ipoint+n_points];
					// check the local parameter values stay within the domain of the correct face
					reparametrize_sample( adjacency, changeCoord, fparams, sparams, tparams, ipoint );
					
				}
				// updating the control vertices
				for (size_t iv=0; iv< 3*n_vertex; iv++)
				{
					vertices[iv] = vertices[iv] + x[iv];
				}
				
				
				// compute energy and gradient for current step size
				SpMat J_new, Jt_new;
				SpVect res_new;
				computeLM_JacobianResidual( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, Pn, sqrtG, J_new, res_new );
				energy = res_new.dot( diagW * res_new );
				Jt_new = J_new.transpose();
				
				
				
				// admit/delete update and adapt damping factor 
				if( (energy < energy_)  || (ls_it==maxLineSearch-1)  || std::isnan(energy) )
				{
					/* restart if energy is nan or we have reached maximum number of line-serach iterations without decreasing the energy */
					if( (energy_< energy) || std::isnan(energy) )
					{
						if(D.visualize>1000){ printf("line-search failed with energy %f at iteration %lu, re-initializing by search to energy ", energy, ls_it); }
						KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
						computeLM_JacobianResidual( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, Pn, sqrtG, J, res );
						energy = res.dot( diagW * res );
						if(D.visualize>1000){ printf("%f\n", energy); }
						Jt = J.transpose();
					}
					else // accept the update if energy < energy_ and not isnan(energy)
					{
						if(D.visualize>1000){ printf("line-search stopped at iteration %lu\n", ls_it); }
						damping_factor = 0.5*damping_factor;
						Jt = Jt_new;
						J = J_new;
						res = res_new;						
					}
					break;
				}
				else // if the energy has nor decreased and we have not reached the end of line-search, delete the update and increase damping factor
				{
					sparams = s_;
					tparams = t_;
					fparams = f_;
					vertices = vertices_;
					damping_factor = damping_factor*2;
				}
	
			}// end line-search

			
			
			
			//------------------------------------------------------------------------------------------------//
			//--------------------------------------- CHECK CONVERGENCE LM------------------------------------//
			//--------------------------------------------------------- --------------------------------------//		
			// stopping criterion
			SpVect hx = x.head(3*n_vertex);
			TypeReal deltaParams = 0;
			TypeReal normParams = 0;
			bool changeFace = false;
			for (size_t iv=0; iv< n_points; iv++)
			{
				if( fparams[iv]!=f_[iv] ){ changeFace = true; }
				deltaParams += std::pow(sparams[iv] - s_[iv],2) + std::pow(tparams[iv]-t_[iv], 2);
				normParams += std::pow(sparams[iv],2) + std::pow(tparams[iv], 2);
			}
			// re-esimate whenever the assignement changes quad
			/*if( changeFace == true )
			{
				if(D.visualize>100){ printf("re-estimate closest surface parameters by search\n");}
				KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
				computeLM_JacobianResidual( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, Pn, sqrtG, J, res );
				energy = res.dot( diagW * res );
				Jt = J.transpose();
			}*/
			
			if( criticalPoint== true  )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu at critical point\n", iter);}
				break;
			}
			else if( (  hx.dot(hx) + deltaParams <  epsilon_a*10* ( hx.size() + n_points)  ) && ( hx.dot(hx) + deltaParams <  epsilon_r*10*( verticesEigen.dot(verticesEigen) + normParams ) )  &&  ( changeFace == false ) )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because surface deltaV %f deltaParams %f does not change\n", iter, hx.dot(hx), deltaParams );}
				break;
			}
			else if( ( std::abs( energy-energy_)< epsilon_r*10* std::abs(energy) ) )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because energy %f does not change\n", iter, energy); }
				break;
			}
			else if( energy >= energy_)
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because energy %f increases\n", iter, energy); }
				break;
			}
		   if(D.visualize>100){ printf("LM iteration %lu, LM energy %f\n", iter, energy);   }     
		   
		   	
		}// end LM iterations for weighted-least squares problem
		
		
		// update MM weights
		std::vector<TypeReal> S, St, Ss;
		if( (itMM % restartPeriod) == restartPeriod - 1 )
		{
			KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
		}
		SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
		energyMM =  0.5*verticesEigen.dot( R*verticesEigen );
		for (size_t ipoint=0; ipoint< n_points; ipoint++)
		{
			TypeReal dist = 0;
			TypeReal ips = 0;
			TypeReal ipt = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += (S[ ipoint*3+idim ] - points[ ipoint*3+idim ], 2);
				ips += Ss[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
				ipt += St[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
			}
			dist >0 ? std::sqrt( dist ) : 0.0;
			ips = std::abs(ips);
			ipt = std::abs(ipt);
			TypeReal wpoint =  D.lp *std::pow( dist+ D.epsilon, D.lp - 2.0);
			for(int idim=0; idim<3; idim++)
			{
				weights[ ipoint*3 + idim ] = wpoint;
			}
			weights[ 3*n_points + ipoint ] = alpha * D.lp *std::pow( ips + D.epsilon, D.lp - 2.0);
			weights[ 4*n_points + ipoint ] =  alpha * D.lp *std::pow( ipt + D.epsilon, D.lp - 2.0);
			energyMM += std::pow( dist, D.lp ) + alpha * ( std::pow(ips, D.lp) + std::pow(ipt, D.lp) );
		}
		
		
		
		//--------------------------------------------------------------------------------------------------//
		//---------------------------------------- CHECK MM CONVERGENCE ------------------------------------//
		//---------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        SpVect deltaV = verticesEigen - verticesEigen_;
		TypeReal max_deltaW = 0;
		if(D.visualize>10){ printf("MM iteration %d, MM energy %f, maxDeltaW %f\n", itMM, energyMM, max_deltaW);   }     
		for (size_t ipoint=0; ipoint< weights.size(); ipoint++)
		{
			TypeReal deltaW = std::abs(weights[ipoint] - weights_[ipoint] );
			if( max_deltaW < deltaW ) { max_deltaW = deltaW; }
		}        
        if( (  deltaV.dot(deltaV) <  epsilon_a * deltaV.size()  ) && ( deltaV.dot(deltaV) <  epsilon_r*verticesEigen.dot(verticesEigen) )  &&  ( max_deltaW<epsilon_a ))
        {
            if(D.visualize>10){ printf("stopped MM optimization at iteration %d because surface with deltaV %f deltaW %f does not change\n", itMM, deltaV.dot(deltaV), max_deltaW );}
            energyMM_ = energyMM;
            break;
        }
        else if( ( std::abs( energyMM-energyMM_)< epsilon_r* std::abs(energyMM) ) )//&&  ( std::abs( energyMM-energyMM_)< epsilon_a ) )
        {
            if(D.visualize>10){ printf("stopped MM optimization at iteration %d because energy %f does not change\n", itMM, energyMM);}
            energyMM_ = energyMM;
            break;
        }

		
	}
	clock_t t1 = clock();
	if(D.visualize>10){ printf("MM - LM optimization took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
	
	return energyMM;
 } 
 
 
 

// optimize colored subdivision surface, vertices and color coefficients
void computeLM_JacobianResidual( std::vector<TypeReal> &points, std::vector<TypeReal> &pointscolor, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, std::vector<TypeReal> &sds_color, const SpMat &Pn, const SpMat &regM, const SpMat &regC, SpMat &J, SpVect &res )
{
    // energy to minimize is E(x) = sum_{i=1}^{n} [ y_i - f_i(x) ]^2
    // the gradient of E is  -J^T [ y - f ] = -J^T r
    // where the residual r = [y_i - f_i(x)] is \in R^n
    // and the jacobian of f J has entries J_{i,j} = \frac{ \partial f_i }{ \partial x_j } and is \in R^{n \times m}
    // the order of the variables is: 
    //				3*n_vertex 		vertices
    //				n_points		sparams
    //				n_points		tparams
    //				3*n_vertex		colors
    // the order of the objective with data terms: 
    // 				n_points  		data terms 	[ point_i - S( s_i, t_i) ]^2
    // 				n_points   		data terms 	[ normal_i \dot \frac{ \partial S}{ \partial s}(s_i, t_i) ]^2  
	// 				n_points   		data terms 	[ normal_i \dot \frac{ \partial S}{ \partial t}(s_i, t_i) ]^2 
	// 				n_points   		data terms 	[ pointcolor_i -C(s_i, t_i) ]^2 
	// 			  	regM.rows  		regularizer    vertices^t regM^T regM vertices  
	// 				regC.rows   	regularizer  color^t regC^T regC color 
	// where:
	// 		S(s_i, t_i) = \sum_{1<=j<= m} B_j(s_i, t_i) vertices_j
	// 		C(s_i, t_i) = \sum_{1<=j<= m} B_j(s_i, t_i) sds_color_j
	//		B_1, B_2, ..., B_m are the spline basis associated with the subdivision surface
	//
	//
    // to update the current solution x_{k+1} = x_{k} + \Delta
    // Levenberg combines  gradient descent update ( \Delta = 1/lambda J^T r ) with a Gauss-Newton approximation to the Hessian  (J^T J) \Delta = J^T r 
    // into 			( J^T J + lambda I ) \Delta = J^T r
    // Marquardt takes into account curvature informtaion in the gradient-descent direction with 
    //					( J^T J + lambda diag(J^T J) ) \Delta = J^T r
    // This routine computes J and r for the above energy
  

    
    // evaluate the residual
    size_t n_vertex = vertices.size()/3;
    size_t n_points = points.size()/3;
    size_t dimcolor = pointscolor.size()/n_points;
    // map from std vectors to eigen variables
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    Eigen::Map< SpVect > pointsEigen(&points[0], 3*n_points);
    Eigen::Map< Mat > pointscolorEigen(&pointscolor[0], n_points, dimcolor);
    Eigen::Map< Mat > colorEigen(&sds_color[0], n_vertex, dimcolor );
    
    // sample the surface with current estimates to 
    SpMat L, Ds, Dt, Dss, Dst, Dtt;
    SampleD2LimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, L, Ds, Dt, Dss, Dst, Dtt);
    SpMat B, Bs, Bt;
    SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, true);
    SpVect S = L* verticesEigen;
    SpVect Ss = Ds* verticesEigen;
    SpVect St = Dt* verticesEigen;
    SpVect Sss = Dss * verticesEigen;
    SpVect Sst = Dst * verticesEigen;
    SpVect Stt = Dtt * verticesEigen;
	Mat C = B* colorEigen; 
	Mat Cs = Bs * colorEigen; 
	Mat Ct = Bt * colorEigen; 
	//printf("C, Cs, Ct computed\n");
 

    // compute residual
    size_t size_regC = regC.rows();
    size_t size_regM = regM.rows();
    res.resize( (5+dimcolor)*n_points + size_regM + dimcolor*size_regC );
    res.head(3*n_points) = pointsEigen - S;
    Eigen::Map< SpVect > res_ns( &res[3*n_points], n_points);
    res_ns = - Pn * Ss; 
    Eigen::Map< SpVect > res_nt( &res[4*n_points], n_points);
    res_nt = - Pn * St;
    Eigen::Map< Mat > res_c( &res[5*n_points], n_points, dimcolor);
    res_c = pointscolorEigen - C;
    Eigen::Map< SpVect > res_dv( &res[(5+dimcolor)*n_points], size_regM);
    res_dv = - regM*verticesEigen;
    Eigen::Map< Mat > res_regC( &res[ (5+dimcolor)*n_points + size_regM ], size_regC, dimcolor);
    res_regC = - regC*colorEigen;
    //printf("residual computed\n");


    // create the sparse matrix of the joint jacobian     
    SpMat PDs = Pn *Ds;
    SpMat PDt = Pn * Dt;
    SpVect PSss = Pn *Sss;
    SpVect PSst = Pn * Sst; 
    SpVect PStt = Pn * Stt; 
    size_t nnz = L.nonZeros() + regM.nonZeros() + PDs.nonZeros() + PDt.nonZeros() + dimcolor*( B.nonZeros() + regC.nonZeros() );
    std::vector<size_t> rows( (5+dimcolor)*2*n_points + nnz );
    std::vector<size_t> cols( (5+dimcolor)*2*n_points + nnz );
    std::vector<TypeReal> vals( (5+dimcolor)*2*n_points + nnz );
    size_t count = 0;
    // compute the derivatives w.r.t. vertices, first block column of Jacobian matrix
    // for residual (surface - point)
    for(int k=0; k<L.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(L,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row();
            cols[count] = it.col();
            count += 1;
        }
    }// for residual pointNormal.dot( surface_derivative_s )
    size_t term_offset = 3*n_points;
    for(int k=0; k<PDs.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(PDs,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row() + term_offset;
            cols[count] = it.col();
            count += 1;
        }
    }// for residual pointNormal.dot( surface_derivative_t )
    term_offset = 4*n_points;
    for(int k=0; k<PDt.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(PDt,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row() + term_offset;
            cols[count] = it.col();
            count += 1;
        }
    }// for residual associated with the regularizer * vertices
    term_offset = (5+dimcolor)*n_points;
    for(int k=0; k<regM.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(regM,k); it; ++it)
        {
            vals[count] = it.value();
            rows[count] = it.row() + term_offset;
            cols[count] = it.col();
            count += 1;
        }
    }   
    //printf("derivatives w.r.t. vertices\n");
    // compute the derivatives w.r.t. parametric variables, second and third block columns of Jacobian matrix (they are tri-diagonal)
    // for residual ( surface - point)
    size_t var_offset = 3*n_vertex;
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
        for(size_t idim=0; idim<3; idim++)
        {
            rows[ count ] = 3*ipoint + idim;
            cols[ count ] = ipoint + var_offset;
            vals[ count ] = Ss[ 3*ipoint + idim];
            count += 1;
            rows[ count ] = 3*ipoint + idim;
            cols[ count ] = ipoint + var_offset + n_points;
            vals[ count ] = St[ 3*ipoint + idim];
            count += 1;
        }
    }
    // for residual pointNormal.dot( surface_derivative_s )
    term_offset = 3*n_points;
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
		rows[ count ] = ipoint + term_offset;
		cols[ count ] = ipoint + var_offset;
		vals[ count ] = PSss[ ipoint ];
		count += 1;
		rows[ count ] = ipoint + term_offset;
		cols[ count ] = ipoint + var_offset + n_points;
		vals[ count ] = PSst[ ipoint];
		count += 1;
    }    
    // for residual pointNormal.dot( surface_derivative_t )
    term_offset = 4*n_points;
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
		rows[ count ] = ipoint + term_offset;
		cols[ count ] = ipoint + var_offset;
		vals[ count ] = PSst[ ipoint ];
		count += 1;
		rows[ count ] = ipoint + term_offset;
		cols[ count ] = ipoint + var_offset + n_points;
		vals[ count ] = PStt[ ipoint];
		count += 1;
    }  
    // for residual ( Color - pointcolor)
    term_offset = 5*n_points;
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
        for(size_t idim=0; idim<dimcolor; idim++)
        {
            rows[ count ] = ipoint + idim*n_points + term_offset;
            cols[ count ] = ipoint + var_offset;
            vals[ count ] = Cs(ipoint, idim);
            count += 1;
            rows[ count ] = ipoint + idim*n_points + term_offset;
            cols[ count ] = ipoint + var_offset + n_points;
            vals[ count ] = Ct(ipoint, idim);
            count += 1;
        }
    }
    //printf("derivatives w.r.t. parametric variables\n");
    // compute the derivatives w.r.t. sds_color, last block column of Jacobian matrix
    // for residual ( Color - pointcolor)
    term_offset = 5*n_points;
    var_offset = 3*n_vertex + 2*n_points;
    for(int k=0; k<B.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(B,k); it; ++it)
        {
        	TypeReal val = it.value();
        	size_t row = it.row();
        	size_t col = it.col();
        	for(int idim=0; idim<dimcolor; idim++)
        	{
				vals[count] = val;
				rows[count] = row + idim*n_points + term_offset;
				cols[count] = col + idim*n_vertex + var_offset;
				count += 1;
            }
        }
    }
    // for residual associated with the gradS * sds_color
    term_offset = (5+dimcolor)*n_points + size_regM;
    for(int k=0; k<regC.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(regC,k); it; ++it)
        {
        	TypeReal val = it.value();
        	size_t row = it.row();
        	size_t col = it.col();
        	for(int idim=0; idim<dimcolor; idim++)
        	{
        		vals[count] = val;
        		rows[count] = row + idim*size_regC + term_offset;
        		cols[count] = col + idim*n_vertex + var_offset;
        		count += 1;
        	}
        }
    }
    //printf("derivatives w.r.t. color variables\n");
    J.resize( (5+dimcolor)*n_points + size_regM + dimcolor*size_regC, 2*n_points + (3+dimcolor)*n_vertex );
    build_spsMatrix(rows, cols, vals, J);
    //printf("built J matrix\n");
    
}





 
// colored pointcloud
TypeReal LeastSquaresOptimization_LM( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &sds_color, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> pointscolor )
 {

	clock_t t0 = clock();
	int MMiterations = 100;
    size_t maxLineSearch = 20;
    TypeReal damping_factor = 1e0;  
    int restartPeriod = MMiterations+1;//1;

    
    
    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t dimcolor = pointscolor.size()/n_points;
    

	TypeReal beta = D.beta/n_faces*n_points;
	TypeReal delta = D.delta;
    TypeReal alpha = D.alpha;
    TypeReal gamma = D.gamma/n_faces*n_points;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();

	
	// precompute adjacency of faces with respect to (s,t)-parameters
    std::vector<int> changeCoord, adjacency;
    compute_params_adjacency(vertices, face_nvertex, face_vertex, adjacency, changeCoord );
    
    
    // regularizers
    SpMat G, sqrtG, G1d, sqrtG1d;
    Eigen::Map< SpVect > verticesEigen(&vertices[0], 3*n_vertex);
    Eigen::Map< Mat > colorEigen(&sds_color[0], n_vertex, dimcolor);
    Eigen::Map< Mat > pointscolorEigen(&pointscolor[0], n_points, dimcolor);
    
    //printf("precomputing quad regularizer ...");
    graph_Differences( vertices, face_nvertex, face_vertex, G, sqrtG, true);
    graph_Differences1d( vertices, face_nvertex, face_vertex, G1d, sqrtG1d, true);
    size_t size_sqrtG = sqrtG.rows();
    size_t size_sqrtG1d = sqrtG1d.rows();
    //printf("done\n");
    
    
    // normalize point variables to [0,1] (to preserve their range)
    std::vector<TypeReal> minVars, maxVars, dummyMax, dummyMin;
    normalize_variables( pointscolor, n_points, minVars, maxVars);
    
    // create projection matrices for normals
    SpMat Pn(n_points, 3*n_points);
    build_normalProjection( normals, Pn);
    
    

	
    
    // initialize the surface footnote parameters and vertex values by search 
    std::vector<int> fparams;
    std::vector<TypeReal> sparams, tparams;
	KdSearch_closestParameter2Surface( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color );

	
	// initialize color
	SpMat B, Bs, Bt;
	SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, false);
	{
		Mat ccolors = B * colorEigen;
		SpMat Bt = B.transpose();
		SpMat Asolc = Bt * B + gamma * G1d;
		Eigen::SimplicialCholesky<SpMat> cSolver;
		cSolver.analyzePattern( Asolc ); 
		cSolver.compute( Asolc );
		Mat rhsc = Bt * pointscolorEigen;
		colorEigen = cSolver.solve( rhsc ); 
		for(int ii=0; ii<colorEigen.rows(); ii++)
		{
			for(int idim=0; idim< colorEigen.cols(); idim++)
			{
				colorEigen(ii, idim) = std::min( std::max(  colorEigen(ii, idim),(TypeReal) 0.0 ), (TypeReal)1.0 );
			}
		}
	}
    
    // initialize weights
    int nLM = 5*n_points + size_sqrtG + dimcolor*( n_points + size_sqrtG1d );
    std::vector<TypeReal> weights(nLM, 0.5* gamma );
    TypeReal energyMM = 0;
    TypeReal addMM = 0;
    {
    	std::vector<TypeReal> S, St, Ss;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
		SpVect Gv = sqrtG * verticesEigen;
		Mat C = B * colorEigen; 
		Mat Gc = sqrtG1d * colorEigen; 
		energyMM =  0; 
		for (size_t ipoint=0; ipoint< n_points; ipoint++)
		{
			TypeReal dist = 0;
			TypeReal ips = 0;
			TypeReal ipt = 0;
			TypeReal distC = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(S[ ipoint*3+idim ] - points[ ipoint*3+idim ], 2);
				ips += Ss[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
				ipt += St[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
			}
			for(int idim=0; idim<dimcolor; idim++)
			{
				distC += std::pow(C(ipoint,idim) - pointscolor[ipoint+idim*n_points ], 2);
			}
			dist = dist > eps ? std::sqrt( dist ) : eps;
			distC = distC > eps ? std::sqrt( distC ) : eps;
			ips = std::abs(ips);
			ipt = std::abs(ipt);
			TypeReal wpoint =  0.5 * D.lp *std::pow( dist + D.epsilon, D.lp - 2.0);
			TypeReal wcolor =  0.5 * delta * D.lp *std::pow( distC + D.epsilon, D.lp - 2.0);
			for(int idim=0; idim<3; idim++)
			{
				weights[ ipoint*3 + idim ] = wpoint;
			}
			for(int idim=0; idim<dimcolor; idim++)
			{
				weights[ ipoint + idim*n_points + 5*n_points ] = wcolor;
			}
			addMM += (1.0 - 0.5*D.lp)* std::pow( dist, D.lp );
			addMM += delta * (1.0 - 0.5*D.lp)* std::pow( distC, D.lp );
			weights[ 3*n_points + ipoint ] = 0.5 * alpha * D.lp *std::pow( ips + D.epsilon, D.lp - 2.0);
			addMM += alpha * (1.0 - 0.5*D.lp)* std::pow( ips, D.lp );
			weights[ 4*n_points + ipoint ] =  0.5 * alpha * D.lp *std::pow( ipt + D.epsilon, D.lp - 2.0);
			addMM += alpha * (1.0 - 0.5*D.lp)* std::pow( ipt, D.lp );
			energyMM += std::pow( dist, D.lp ) + alpha * ( std::pow(ips, D.lp) + std::pow(ipt, D.lp) )+ delta * std::pow( distC, D.lp );
		}
		TypeReal energyRegV = Gv.dot(Gv);
		energyMM += 0.5* beta * energyRegV;
		for (size_t ii=0; ii< size_sqrtG; ii++)
		{
			weights[ (5+dimcolor)*n_points + ii ] = 0.5 * beta;
		}
		Eigen::Map< SpVect > vectGc( &Gc(0,0), size_sqrtG1d*dimcolor );
		TypeReal energyRegC = vectGc.dot( vectGc );
		energyMM += 0.5* gamma * energyRegC;
	}
	printf("initial energy %f\n", energyMM);
	
    
    // update verticesEigen_ after each MM iteration
    TypeReal normColor = 0;
    for(int itMM=0; itMM< MMiterations; itMM++)
    {
    	// keep track of previous solution;
    	std::vector<TypeReal> weights_(weights);
    	SpVect verticesEigen_(verticesEigen); 
    	Mat colorEigen_(colorEigen); 
    	TypeReal energyMM_ = energyMM;
    	
    	// initialize weight and displacement variables
    	size_t n_vars = 2*n_points + (3+dimcolor)*n_vertex;
    	SpMat diagW( nLM, nLM );
    	build_spsDiagMatrix(weights,diagW);
		SpVect x = SpVect::Zero(n_vars);
		SpMat J, Jt;
		SpVect res;
		TypeReal energy = 0;
		for( size_t iter=0; iter< D.maxIterations; iter++)
		{
			
			// keep track of previous solution
			SpVect x_(x);
			TypeReal energy_ = energy;
			std::vector<int> f_(fparams);
			std::vector<TypeReal> s_(sparams);
			std::vector<TypeReal> t_(tparams);
			std::vector<TypeReal> c_(sds_color);
			std::vector<TypeReal> vertices_(vertices);		
		
			//------------------------------------------------------------------------------------------------//
			//---------------------- compute residuals and Jacobian of residuals -----------------------------//
			//--------------------------------------------------------- --------------------------------------//		
			// energy to minimize is E(x) = sum_{i=1}^{n} w_i[ y_i - f_i(x) ]^2
			// the gradient of E is  -J^T diagW [ y - f ] = -J^T diagW r
			// where the residual r = [y_i - f_i(x)] is \in R^n
			// and the jacobian of f J has entries J_{i,j} = \frac{ \partial f_i }{ \partial x_j } and is \in R^{n \times m}
			//
			// to update the current solution x_{k+1} = x_{k} + \Delta
			// Levenberg combines  gradient descent update ( \Delta = 1/lambda J^T diagW r ) with a Gauss-Newton approximation to the Hessian  (J^T diagW J) \Delta = J^T diagW r 
			// into 			( J^T diagW J + lambda I ) \Delta = J^T diagW r
			// Marquardt takes into account curvature infomrtaion in the gradient-descent direction with 
			//					( J^T diagW J + lambda diag(J^T diagW J) ) \Delta = J^T diagW r
			// the routine computeLM_JacobianResidual computes J and r
			if( iter==0 )
			{
				computeLM_JacobianResidual( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, Pn, sqrtG, sqrtG1d, J, res );
				energy_ = res.dot( diagW * res );
				Jt = J.transpose();
			}
			
			
			
			//------------------------------------------------------------------------------------------------//
			//---------------------------------------- LINE  SEARCH ------------------------------------------//
			//--------------------------------------------------------- --------------------------------------//
			bool criticalPoint = false;
			for(size_t ls_it=0; ls_it<maxLineSearch; ls_it++)
			{
				
				// LM Hessian approximation
				SpMat JtJ = Jt* diagW * J;
				SpVect diagElems = JtJ.diagonal();
				SpMat diagH( n_vars, n_vars );
				for (size_t ipoint=0; ipoint< n_vars; ipoint++)
				{
					if( diagElems[ipoint]< 1e-6 )
					{
						diagElems[ipoint] = 1e-6;
					}
				}
				build_spsDiagMatrix(diagElems,diagH);
				SpMat H = JtJ + damping_factor * diagH;
				
				// compute the rhs = - gradient
				SpVect rhs = Jt * diagW * res;
				TypeReal max_rhs = 0;
				// if the gradient is small stop
				for(int kk=0; kk< rhs.size(); kk++){ if(max_rhs < std::abs(rhs[kk]) ) { max_rhs = std::abs(rhs[kk]); } }
				if( max_rhs < epsilon_a ) { criticalPoint = true; energy = energy_; break; }
				
				// solve linear system to compute descent direction q
				Eigen::ConjugateGradient<SpMat > solver;
				solver.setMaxIterations(D.solverIterations);
				solver.compute(H);
				x = solver.solveWithGuess( rhs, x );

	
				
				// updating the control vertices
				for (size_t iv=0; iv< 3*n_vertex; iv++)
				{
					vertices[iv] = vertices[iv] + x[iv];
				}				
				//updating (f, s,t)-parameters
				for (size_t ipoint=0; ipoint< n_points; ipoint++)
				{
					sparams[ipoint] = sparams[ipoint]+ x[3*n_vertex+ipoint];	
					tparams[ipoint] = tparams[ipoint]+ x[3*n_vertex+ipoint+n_points];
					// check the local parameter values stay within the domain of the correct face
					reparametrize_sample( adjacency, changeCoord, fparams, sparams, tparams, ipoint );					
				}
				// updating the color
				for (size_t iv=0; iv< dimcolor*n_vertex; iv++)
				{
					sds_color[iv] = sds_color[iv] + x[iv+3*n_vertex+2*n_points];
					sds_color[iv] = std::min( std::max( sds_color[iv] , (TypeReal)0.0 ), (TypeReal)1.0);
				}
				
				
				// compute energy and gradient for current step size
				SpMat J_new, Jt_new;
				SpVect res_new;
				computeLM_JacobianResidual( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, Pn, sqrtG, sqrtG1d, J_new, res_new );
				energy = res_new.dot( diagW * res_new );
				Jt_new = J_new.transpose();
				
				
				// admit/delete update and adapt damping factor 
				if( (energy < energy_)  || (ls_it==maxLineSearch-1)  || std::isnan(energy) )
				{
					/* restart if energy is nan or we have reached maximum number of line-serach iterations without decreasing the energy */
					if( (energy_< energy) || std::isnan(energy) )
					{
						if(D.visualize>1000){ printf("line-search failed with energy %f at iteration %lu, re-initializing by search to energy ", energy + addMM, ls_it); }
						//KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
						KdSearch_closestParameter2Surface( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color );
						// reinitialize color
						SpMat B, Bs, Bt;
						SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, false);
						{
							Mat ccolors = B * colorEigen;
							SpMat Bt = B.transpose();
							SpMat Asolc = Bt * B + gamma * G1d;
							Eigen::SimplicialCholesky<SpMat> cSolver;
							cSolver.analyzePattern( Asolc ); 
							cSolver.compute( Asolc );
							Mat rhsc = Bt * pointscolorEigen;
							colorEigen = cSolver.solve( rhsc ); 
							for(int ii=0; ii<colorEigen.rows(); ii++)
							{
								for(int idim=0; idim< colorEigen.cols(); idim++)
								{
									colorEigen(ii, idim) = std::min( std::max(  colorEigen(ii, idim),(TypeReal) 0.0 ), (TypeReal)1.0 );
								}
							}
						}						
						computeLM_JacobianResidual( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, Pn, sqrtG, sqrtG1d, J, res );
						energy = res.dot( diagW * res );
						if(D.visualize>1000){ printf("%f\n", energy+ addMM); }
						Jt = J.transpose();
					}
					else // accept the update if energy < energy_ and not isnan(energy)
					{
						if(D.visualize>1000){ printf("line-search stopped at iteration %lu\n", ls_it); }
						damping_factor = 0.5*damping_factor;
						Jt = Jt_new;
						J = J_new;
						res = res_new;						
					}
					break;
				}
				else // if the energy has nor decreased and we have not reached the end of line-search, delete the update and increase damping factor
				{
					sparams = s_;
					tparams = t_;
					fparams = f_;
					sds_color = c_;
					vertices = vertices_;
					damping_factor = damping_factor*2;
				}
	
			}// end line-search
			
			
			
			//------------------------------------------------------------------------------------------------//
			//--------------------------------------- CHECK CONVERGENCE LM------------------------------------//
			//--------------------------------------------------------- --------------------------------------//		
			// stopping criterion
			SpVect hx = x.head(3*n_vertex);
			SpVect hc = x.tail(dimcolor*n_vertex);
			TypeReal deltaParams = 0;
			TypeReal normParams = 0;
			bool changeFace = false;
			for (size_t iv=0; iv< n_points; iv++)
			{
				if( fparams[iv]!=f_[iv] ){ changeFace = true; }
				deltaParams += std::pow(sparams[iv] - s_[iv],2) + std::pow(tparams[iv]-t_[iv], 2);
				normParams += std::pow(sparams[iv],2) + std::pow(tparams[iv], 2);
			}
			// re-esimate whenever the assignement changes quad
			/*if( changeFace == true )
			{
				if(D.visualize>100){ printf("re-estimate closest surface parameters by search\n");}
				KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
				computeLM_JacobianResidual( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, Pn, sqrtG, J, res );
				energy = res.dot( diagW * res );
				Jt = J.transpose();
			}*/
			normColor = 0; for(int kk=0; kk< sds_color.size(); kk++){ normColor += std::pow( sds_color[kk], 2); }			
			if( criticalPoint== true  )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu at critical point\n", iter);}
				break;
			}
			else if( (  hx.dot(hx) + hc.dot(hc) + deltaParams <  epsilon_a*10* ( hx.size() + n_points + hc.size())  ) && ( hx.dot(hx) + hc.dot(hc) + deltaParams <  epsilon_r*10*( verticesEigen.dot(verticesEigen) + normColor + normParams ) )  &&  ( changeFace == false ) )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because surface deltaV %f deltaParams %f does not change\n", iter, hx.dot(hx), deltaParams );}
				break;
			}
			else if( ( std::abs( energy-energy_)< epsilon_r*10* std::abs(energy) ) )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because energy %f does not change\n", iter, energy+addMM); }
				break;
			}
			else if( energy >= energy_)
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because energy %f increases\n", iter, energy); }
				break;
			}
		   if(D.visualize>100){ printf("LM iteration %lu, energy %f\n", iter, energy+addMM);   }     
		   	
		}
		
		
		// update MM weights
		energyMM = 0;
    	addMM = 0;
    	std::vector<TypeReal> S, St, Ss;
		SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
		SpVect Gv = sqrtG * verticesEigen;
		SpMat B, Bs, Bt;
		SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, false);
		Mat C = B * colorEigen; 
		Mat Gc = sqrtG1d * colorEigen; 
		
		energyMM =  0; 
		for (size_t ipoint=0; ipoint< n_points; ipoint++)
		{
			TypeReal dist = 0;
			TypeReal ips = 0;
			TypeReal ipt = 0;
			TypeReal distC = 0;
			for(int idim=0; idim<3; idim++)
			{
				dist += std::pow(S[ ipoint*3+idim ] - points[ ipoint*3+idim ], 2);
				ips += Ss[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
				ipt += St[ ipoint*3+idim ] * normals[ ipoint*3 + idim ]; 
			}
			for(int idim=0; idim<dimcolor; idim++)
			{
				distC += std::pow(C(ipoint,idim) - pointscolor[ipoint+idim*n_points ], 2);
			}
			dist = dist > eps ? std::sqrt( dist ) : eps;
			distC = distC > eps ? std::sqrt( distC ) : eps;
			ips = std::abs(ips);
			ipt = std::abs(ipt);
			TypeReal wpoint =  0.5 * D.lp *std::pow( dist + D.epsilon, D.lp - 2.0);
			TypeReal wcolor =  0.5 * delta * D.lp *std::pow( distC + D.epsilon, D.lp - 2.0);
			for(int idim=0; idim<3; idim++)
			{
				weights[ ipoint*3 + idim ] = wpoint;
			}
			for(int idim=0; idim<dimcolor; idim++)
			{
				weights[ ipoint + idim*n_points + 5*n_points ] = wcolor;
			}
			addMM += (1.0 - 0.5*D.lp)* std::pow( dist, D.lp );
			addMM += delta * (1.0 - 0.5*D.lp)* std::pow( distC, D.lp );
			weights[ 3*n_points + ipoint ] = 0.5 * alpha * D.lp *std::pow( ips + D.epsilon, D.lp - 2.0);
			addMM += alpha * (1.0 - 0.5*D.lp)* std::pow( ips, D.lp );
			weights[ 4*n_points + ipoint ] =  0.5 * alpha * D.lp *std::pow( ipt + D.epsilon, D.lp - 2.0);
			addMM += alpha * (1.0 - 0.5*D.lp)* std::pow( ipt, D.lp );
			energyMM += std::pow( dist, D.lp ) + alpha * ( std::pow(ips, D.lp) + std::pow(ipt, D.lp) )+ delta * std::pow( distC, D.lp );
		}
		TypeReal energyRegV = Gv.dot(Gv);
		energyMM += 0.5* beta * energyRegV;
		Eigen::Map< SpVect > vectGc( &Gc(0,0), size_sqrtG1d*dimcolor );
		TypeReal energyRegC = vectGc.dot( vectGc );
		energyMM += 0.5* gamma * energyRegC;		

		
		//--------------------------------------------------------------------------------------------------//
		//---------------------------------------- CHECK MM CONVERGENCE ------------------------------------//
		//---------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        SpVect deltaV = verticesEigen - verticesEigen_;
        Mat deltaC0 = colorEigen - colorEigen_;
        Eigen::Map< SpVect > deltaC( &deltaC0(0,0), dimcolor*n_vertex );
		TypeReal max_deltaW = 0;
		for (size_t ipoint=0; ipoint< weights.size(); ipoint++)
		{
			TypeReal deltaW = std::abs(weights[ipoint] - weights_[ipoint] );
			if( max_deltaW < deltaW ) { max_deltaW = deltaW; }
		}        
		if(D.visualize>10){ printf("MM iteration %d, energy %f, maxDeltaW %f\n", itMM, energyMM, max_deltaW);   } 
        if( (  deltaV.dot(deltaV) <  epsilon_a * deltaV.size()  ) && ( deltaV.dot(deltaV) <  epsilon_r*verticesEigen.dot(verticesEigen) ) && (  deltaV.dot(deltaV) <  epsilon_a * deltaV.size()  ) && ( deltaC.dot(deltaC) <  epsilon_r*normColor )  &&  ( max_deltaW<epsilon_a ))
        {
            if(D.visualize>10){ printf("stopped MM optimization at iteration %d because surface with deltaV %f deltaC %f deltaW %f does not change\n", itMM, deltaV.dot(deltaV), deltaC.dot(deltaC), max_deltaW );}
            energyMM_ = energyMM;
            break;
        }
        else if( ( std::abs( energyMM-energyMM_)< epsilon_r* std::abs(energyMM) ) )//&&  ( std::abs( energyMM-energyMM_)< epsilon_a ) )
        {
            if(D.visualize>10){ printf("stopped MM optimization at iteration %d because energy %f does not change\n", itMM, energyMM);}
            energyMM_ = energyMM;
            break;
        }
		    
		
	}
	clock_t t1 = clock();
	if(D.visualize>10){ printf("MM - LM optimization took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
	
	// color to its normal range
	denormalize_variables( sds_color, minVars, maxVars);
	
	return energyMM;
 } 
 
 
 

 
 
// only optimize for colors of subdivision
// optimize color field of a subdivision surface, vertices are fixed
void computeColorLM_JacobianResidual( std::vector<TypeReal> &pointscolor, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, std::vector<TypeReal> &sds_color, const SpMat &regC, SpMat &J, SpVect &res )
{
    // energy to minimize is E(x) = sum_{i=1}^{n} [ y_i - f_i(x) ]^2
    // the gradient of E is  -J^T [ y - f ] = -J^T r
    // where the residual r = [y_i - f_i(x)] is \in R^n
    // and the jacobian of f J has entries J_{i,j} = \frac{ \partial f_i }{ \partial x_j } and is \in R^{n \times m}
    // the order of the variables is: 
    //				n_points		sparams
    //				n_points		tparams
    //				3*n_vertex		colors
    // the order of the objective with data terms: 
	// 				n_points   		data terms 	[ pointcolor_i -C(s_i, t_i) ]^2 
	// 				regC.rows   	regularizer  color^t regC^T regC color 
	// where:
	// 		C(s_i, t_i) = \sum_{1<=j<= m} B_j(s_i, t_i) sds_color_j
	//		B_1, B_2, ..., B_m are the spline basis associated with the subdivision surface
	//
	//
    // to update the current solution x_{k+1} = x_{k} + \Delta
    // Levenberg combines  gradient descent update ( \Delta = 1/lambda J^T r ) with a Gauss-Newton approximation to the Hessian  (J^T J) \Delta = J^T r 
    // into 			( J^T J + lambda I ) \Delta = J^T r
    // Marquardt takes into account curvature informtaion in the gradient-descent direction with 
    //					( J^T J + lambda diag(J^T J) ) \Delta = J^T r
    // This routine computes J and r for the above energy
  

    
    // evaluate the residual
    size_t n_vertex = vertices.size()/3;
    size_t dimcolor = sds_color.size()/n_vertex;
    size_t n_points = pointscolor.size()/dimcolor;
    // map from std vectors to eigen variables
    Eigen::Map< Mat > pointscolorEigen(&pointscolor[0], n_points, dimcolor);
    Eigen::Map< Mat > colorEigen(&sds_color[0], n_vertex, dimcolor );
    
    // sample the surface with current estimates to 
    SpMat B, Bs, Bt;
    SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, true);
	Mat C = B* colorEigen; 
	Mat Cs = Bs * colorEigen; 
	Mat Ct = Bt * colorEigen; 
	//printf("C, Cs, Ct computed\n");
 

    // compute residual
    size_t size_regC = regC.rows();
    res.resize( dimcolor*( n_points + size_regC) );
    Eigen::Map< Mat > res_c( &res[0], n_points, dimcolor);
    res_c = pointscolorEigen - C;
    Eigen::Map< Mat > res_regC( &res[ dimcolor*n_points ], size_regC, dimcolor);
    res_regC = -regC*colorEigen;
    //printf("residual computed\n");


    // create the sparse matrix of the joint jacobian     
    size_t nnz = dimcolor*( B.nonZeros() + regC.nonZeros() );
    std::vector<size_t> rows( dimcolor*2*n_points + nnz );
    std::vector<size_t> cols( dimcolor*2*n_points + nnz );
    std::vector<TypeReal> vals( dimcolor*2*n_points + nnz );
    size_t count = 0;
    // compute the derivatives w.r.t. parametric variables, second and third block columns of Jacobian matrix (they are tri-diagonal)
    // for residual ( Color - pointcolor)
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
        for(size_t idim=0; idim<dimcolor; idim++)
        {
            rows[ count ] = ipoint + idim*n_points;
            cols[ count ] = ipoint;
            vals[ count ] = Cs(ipoint, idim);
            count += 1;
            rows[ count ] = ipoint + idim*n_points;
            cols[ count ] = ipoint + n_points;
            vals[ count ] = Ct(ipoint, idim);
            count += 1;
        }
    }
    //printf("derivatives w.r.t. parametric variables\n");
    // compute the derivatives w.r.t. sds_color, last block column of Jacobian matrix
    // for residual ( Color - pointcolor)
    size_t var_offset = 2*n_points;
    for(int k=0; k<B.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(B,k); it; ++it)
        {
        	TypeReal val = it.value();
        	size_t row = it.row();
        	size_t col = it.col();
        	for(int idim=0; idim<dimcolor; idim++)
        	{
				vals[count] = val;
				rows[count] = row + idim*n_points;
				cols[count] = col + idim*n_vertex + var_offset;
				count += 1;
            }
        }
    }
    // for residual associated with the gradS * sds_color
    size_t term_offset = dimcolor*n_points;
    for(int k=0; k<regC.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(regC,k); it; ++it)
        {
        	TypeReal val = it.value();
        	size_t row = it.row();
        	size_t col = it.col();
        	for(int idim=0; idim<dimcolor; idim++)
        	{
        		vals[count] = val;
        		rows[count] = row + idim*size_regC + term_offset;
        		cols[count] = col + idim*n_vertex + var_offset;
        		count += 1;
        	}
        }
    }
    //printf("derivatives w.r.t. color variables\n");
    J.resize( dimcolor*( n_points + size_regC), 2*n_points + dimcolor*n_vertex );
    build_spsMatrix(rows, cols, vals, J);    
}



TypeReal SDScolorOptimization_LM( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &sds_color, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> pointscolor )
 {

	clock_t t0 = clock();
	int MMiterations = 100;
    size_t maxLineSearch = 20;
    TypeReal damping_factor = 1e0;  
    int restartPeriod = MMiterations+1;//1;

    
    
    // parse input size and parameters
    size_t n_points = points.size()/3;
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t dimcolor = pointscolor.size()/n_points;
    

	TypeReal delta = D.delta;
    TypeReal gamma = D.gamma/n_faces*n_points;
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();

	
	// precompute adjacency of faces with respect to (s,t)-parameters
    std::vector<int> changeCoord, adjacency;
    compute_params_adjacency(vertices, face_nvertex, face_vertex, adjacency, changeCoord );
    
    
    // regularizers
    SpMat G1d, sqrtG1d;
    Eigen::Map< Mat > colorEigen(&sds_color[0], n_vertex, dimcolor);
    Eigen::Map< Mat > pointscolorEigen(&pointscolor[0], n_points, dimcolor);
    
    //printf("precomputing quad regularizer ...");
    graph_Differences1d( vertices, face_nvertex, face_vertex, G1d, sqrtG1d, true);
    size_t size_sqrtG1d = sqrtG1d.rows();
    //printf("done\n");
    
    
    // normalize point variables to [0,1] (to preserve their range)
    std::vector<TypeReal> minVars, maxVars, sds_minVars, sds_maxVars;
    normalize_variables( pointscolor, n_points, minVars, maxVars);
    normalize_variables( sds_color, n_points, sds_minVars, sds_maxVars);
    
    // initialize the surface footnote parameters and vertex values by search 
    std::vector<int> fparams;
    std::vector<TypeReal> sparams, tparams;
	KdSearch_closestParameter2Surface( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color );

	
	// initialize color
	SpMat B, Bs, Bt;
	SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, false);
	/*{
		Mat ccolors = B * colorEigen;
		SpMat Bt = B.transpose();
		SpMat Asolc = Bt * B + gamma * G1d;
		Eigen::SimplicialCholesky<SpMat> cSolver;
		cSolver.analyzePattern( Asolc ); 
		cSolver.compute( Asolc );
		Mat rhsc = Bt * pointscolorEigen;
		colorEigen = cSolver.solve( rhsc ); 
		for(int ii=0; ii<colorEigen.rows(); ii++)
		{
			for(int idim=0; idim< colorEigen.cols(); idim++)
			{
				colorEigen(ii, idim) = std::min( std::max(  colorEigen(ii, idim),(TypeReal) 0.0 ), (TypeReal)1.0 );
			}
		}
	}*/
    
    // initialize weights
    int nLM = dimcolor*( n_points + size_sqrtG1d );
    std::vector<TypeReal> weights(nLM, 0.5* gamma );
    TypeReal energyMM = 0;
    TypeReal addMM = 0;
    {
		Mat C = B * colorEigen; 
		Mat Gc = sqrtG1d * colorEigen; 
		energyMM =  0; 
		for (size_t ipoint=0; ipoint< n_points; ipoint++)
		{
			TypeReal distC = 0;
			for(int idim=0; idim<dimcolor; idim++)
			{
				distC += std::pow( C(ipoint,idim) - pointscolor[ ipoint + idim*n_points ], 2);
			}
			distC = distC > eps ? std::sqrt( distC ) : eps;
			TypeReal wcolor =  0.5 * delta * D.lp *std::pow( distC + D.epsilon, D.lp - 2.0);
			for(int idim=0; idim<dimcolor; idim++)
			{
				weights[ ipoint + idim*n_points ] = wcolor;
			}
			addMM += delta * (1.0 - 0.5*D.lp)* std::pow( distC, D.lp );
			energyMM += delta * std::pow( distC, D.lp );
		}
		Eigen::Map< SpVect > vectGc( &Gc(0,0), size_sqrtG1d*dimcolor );
		TypeReal energyRegC = vectGc.dot( vectGc );
		energyMM += 0.5* gamma * energyRegC;
	}
	printf("initial energy %f\n", energyMM);
	
    
    // update verticesEigen_ after each MM iteration
    TypeReal normColor = 0;
    for(int itMM=0; itMM< MMiterations; itMM++)
    {
    	// keep track of previous solution;
    	std::vector<TypeReal> weights_(weights);
    	Mat colorEigen_(colorEigen); 
    	TypeReal energyMM_ = energyMM;
    	
    	// initialize weight and displacement variables
    	size_t n_vars = 2*n_points + dimcolor*n_vertex;
    	SpMat diagW( nLM, nLM );
    	build_spsDiagMatrix(weights,diagW);
		SpVect x = SpVect::Zero(n_vars);
		SpMat J, Jt;
		SpVect res;
		TypeReal energy = 0;
		for( size_t iter=0; iter< D.maxIterations; iter++)
		{
			
			// keep track of previous solution
			SpVect x_(x);
			TypeReal energy_ = energy;
			std::vector<int> f_(fparams);
			std::vector<TypeReal> s_(sparams);
			std::vector<TypeReal> t_(tparams);
			std::vector<TypeReal> c_(sds_color);	
		
			//------------------------------------------------------------------------------------------------//
			//---------------------- compute residuals and Jacobian of residuals -----------------------------//
			//--------------------------------------------------------- --------------------------------------//		
			// energy to minimize is E(x) = sum_{i=1}^{n} w_i[ y_i - f_i(x) ]^2
			// the gradient of E is  -J^T diagW [ y - f ] = -J^T diagW r
			// where the residual r = [y_i - f_i(x)] is \in R^n
			// and the jacobian of f J has entries J_{i,j} = \frac{ \partial f_i }{ \partial x_j } and is \in R^{n \times m}
			//
			// to update the current solution x_{k+1} = x_{k} + \Delta
			// Levenberg combines  gradient descent update ( \Delta = 1/lambda J^T diagW r ) with a Gauss-Newton approximation to the Hessian  (J^T diagW J) \Delta = J^T diagW r 
			// into 			( J^T diagW J + lambda I ) \Delta = J^T diagW r
			// Marquardt takes into account curvature infomrtaion in the gradient-descent direction with 
			//					( J^T diagW J + lambda diag(J^T diagW J) ) \Delta = J^T diagW r
			// the routine computeLM_JacobianResidual computes J and r
			if( iter==0 )
			{
				computeColorLM_JacobianResidual( pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, sqrtG1d, J, res );
				energy_ = res.dot( diagW * res );
				Jt = J.transpose();
			}
			
			
			
			//------------------------------------------------------------------------------------------------//
			//---------------------------------------- LINE  SEARCH ------------------------------------------//
			//--------------------------------------------------------- --------------------------------------//
			bool criticalPoint = false;
			for(size_t ls_it=0; ls_it<maxLineSearch; ls_it++)
			{
				
				// LM Hessian approximation
				SpMat JtJ = Jt* diagW * J;
				SpVect diagElems = JtJ.diagonal();
				SpMat diagH( n_vars, n_vars );
				for (size_t ipoint=0; ipoint< n_vars; ipoint++)
				{
					if( diagElems[ipoint]< 1e-6 )
					{
						diagElems[ipoint] = 1e-6;
					}
				}
				build_spsDiagMatrix(diagElems,diagH);
				SpMat H = JtJ + damping_factor * diagH;
				
				// compute the rhs = - gradient
				SpVect rhs = Jt * diagW * res;
				TypeReal max_rhs = 0;
				// if the gradient is small stop
				for(int kk=0; kk< rhs.size(); kk++){ if(max_rhs < std::abs(rhs[kk]) ) { max_rhs = std::abs(rhs[kk]); } }
				if( max_rhs < epsilon_a ) { criticalPoint = true; energy = energy_; break; }
				
				// solve linear system to compute descent direction q
				Eigen::ConjugateGradient<SpMat > solver;
				solver.setMaxIterations(D.solverIterations);
				solver.compute(H);
				x = solver.solveWithGuess( rhs, x );

	
				
				
				//updating (f, s,t)-parameters
				for (size_t ipoint=0; ipoint< n_points; ipoint++)
				{
					sparams[ipoint] = sparams[ipoint]+ x[ipoint];	
					tparams[ipoint] = tparams[ipoint]+ x[ipoint+n_points];
					// check the local parameter values stay within the domain of the correct face
					reparametrize_sample( adjacency, changeCoord, fparams, sparams, tparams, ipoint );					
				}
				// updating the color
				for (size_t iv=0; iv< dimcolor*n_vertex; iv++)
				{
					sds_color[iv] = sds_color[iv] + x[iv+2*n_points];
					sds_color[iv] = std::min( std::max( sds_color[iv] , (TypeReal)0.0 ), (TypeReal)1.0);
				}
				
				
				// compute energy and gradient for current step size
				SpMat J_new, Jt_new;
				SpVect res_new;
				computeColorLM_JacobianResidual( pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, sqrtG1d, J_new, res_new );
				energy = res_new.dot( diagW * res_new );
				Jt_new = J_new.transpose();
				
				
				// admit/delete update and adapt damping factor 
				if( (energy < energy_)  || (ls_it==maxLineSearch-1)  || std::isnan(energy) )
				{
					/* restart if energy is nan or we have reached maximum number of line-serach iterations without decreasing the energy */
					if( (energy_< energy) || std::isnan(energy) )
					{
						if(D.visualize>1000){ printf("line-search failed with energy %f at iteration %lu, re-initializing by search to energy ", energy + addMM, ls_it); }
						KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
						KdSearch_closestPointValue( points, pointscolor, vertices, face_nvertex, face_vertex, sds_color);
						//KdSearch_closestParameter2Surface( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color );
						// reinitialize color
						/*SpMat B, Bs, Bt;
						SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, false);
						{
							Mat ccolors = B * colorEigen;
							SpMat Bt = B.transpose();
							SpMat Asolc = Bt * B + gamma * G1d;
							Eigen::SimplicialCholesky<SpMat> cSolver;
							cSolver.analyzePattern( Asolc ); 
							cSolver.compute( Asolc );
							Mat rhsc = Bt * pointscolorEigen;
							colorEigen = cSolver.solve( rhsc ); 
							for(int ii=0; ii<colorEigen.rows(); ii++)
							{
								for(int idim=0; idim< colorEigen.cols(); idim++)
								{
									colorEigen(ii, idim) = std::min( std::max(  colorEigen(ii, idim),(TypeReal) 0.0 ), (TypeReal)1.0 );
								}
							}
						}*/						
						computeColorLM_JacobianResidual( pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, sqrtG1d, J, res );
						energy = res.dot( diagW * res );
						if(D.visualize>1000){ printf("%f\n", energy+ addMM); }
						Jt = J.transpose();
					}
					else // accept the update if energy < energy_ and not isnan(energy)
					{
						if(D.visualize>1000){ printf("line-search stopped at iteration %lu\n", ls_it); }
						damping_factor = 0.5*damping_factor;
						Jt = Jt_new;
						J = J_new;
						res = res_new;						
					}
					break;
				}
				else // if the energy has nor decreased and we have not reached the end of line-search, delete the update and increase damping factor
				{
					sparams = s_;
					tparams = t_;
					fparams = f_;
					sds_color = c_;
					damping_factor = damping_factor*2;
				}
	
			}// end line-search
			
			
			
			//------------------------------------------------------------------------------------------------//
			//--------------------------------------- CHECK CONVERGENCE LM------------------------------------//
			//--------------------------------------------------------- --------------------------------------//		
			// stopping criterion
			SpVect hc = x.tail(dimcolor*n_vertex);
			TypeReal deltaParams = 0;
			TypeReal normParams = 0;
			bool changeFace = false;
			for (size_t iv=0; iv< n_points; iv++)
			{
				if( fparams[iv]!=f_[iv] ){ changeFace = true; }
				deltaParams += std::pow(sparams[iv] - s_[iv],2) + std::pow(tparams[iv]-t_[iv], 2);
				normParams += std::pow(sparams[iv],2) + std::pow(tparams[iv], 2);
			}
			// re-esimate whenever the assignement changes quad
			/*if( changeFace == true )
			{
				if(D.visualize>100){ printf("re-estimate closest surface parameters by search\n");}
				KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, fparams, sparams, tparams );
				computeLM_JacobianResidual( points, pointscolor, vertices, face_nvertex, face_vertex, fparams, sparams, tparams, sds_color, Pn, sqrtG, J, res );
				energy = res.dot( diagW * res );
				Jt = J.transpose();
			}*/
			normColor = 0; for(int kk=0; kk< sds_color.size(); kk++){ normColor += std::pow( sds_color[kk], 2); }			
			if( criticalPoint== true  )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu at critical point\n", iter);}
				break;
			}
			else if( (  hc.dot(hc) + deltaParams <  epsilon_a*10* (  n_points + hc.size())  ) && (  hc.dot(hc) + deltaParams <  epsilon_r*10*(  normColor + normParams ) )  &&  ( changeFace == false ) )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because surface deltaC %f deltaParams %f does not change\n", iter, hc.dot(hc), deltaParams );}
				break;
			}
			else if( ( std::abs( energy-energy_)< epsilon_r*10* std::abs(energy) ) )
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because energy %f does not change\n", iter, energy+addMM); }
				break;
			}
			else if( energy >= energy_)
			{
				if(D.visualize>100){ printf("stopped LM optimization at iteration %lu because energy %f increases\n", iter, energy); }
				break;
			}
		   if(D.visualize>100){ printf("LM iteration %lu, energy %f\n", iter, energy+addMM);   }     
		   	
		}
		
		
		// update MM weights
		energyMM = 0;
    	addMM = 0;
		SpMat B, Bs, Bt;
		SampleLimitBasis(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, B, Bs, Bt, false);
		Mat C = B * colorEigen; 
		Mat Gc = sqrtG1d * colorEigen; 		
		energyMM =  0; 
		for (size_t ipoint=0; ipoint< n_points; ipoint++)
		{
			TypeReal distC = 0;
			for(int idim=0; idim<dimcolor; idim++)
			{
				distC += std::pow(C(ipoint,idim) - pointscolor[ipoint+idim*n_points ], 2);
			}
			distC = distC > eps ? std::sqrt( distC ) : eps;
			TypeReal wcolor =  0.5 * delta * D.lp *std::pow( distC + D.epsilon, D.lp - 2.0);
			for(int idim=0; idim<dimcolor; idim++)
			{
				weights[ ipoint + idim*n_points ] = wcolor;
			}
			addMM += delta * (1.0 - 0.5*D.lp)* std::pow( distC, D.lp );
			energyMM += delta * std::pow( distC, D.lp );
		}
		Eigen::Map< SpVect > vectGc( &Gc(0,0), size_sqrtG1d*dimcolor );
		TypeReal energyRegC = vectGc.dot( vectGc );
		energyMM += 0.5* gamma * energyRegC;		

		
		//--------------------------------------------------------------------------------------------------//
		//---------------------------------------- CHECK MM CONVERGENCE ------------------------------------//
		//---------------------------------------------------------- --------------------------------------//		
		// stopping criterion
        Mat deltaC0 = colorEigen - colorEigen_;
        Eigen::Map< SpVect > deltaC( &deltaC0(0,0), dimcolor*n_vertex );
		TypeReal max_deltaW = 0;
		for (size_t ipoint=0; ipoint< weights.size(); ipoint++)
		{
			TypeReal deltaW = std::abs(weights[ipoint] - weights_[ipoint] );
			if( max_deltaW < deltaW ) { max_deltaW = deltaW; }
		}        
		if(D.visualize>10){ printf("MM iteration %d, energy %f, maxDeltaW %f\n", itMM, energyMM, max_deltaW);   } 
        if( ( deltaC.dot(deltaC) <  epsilon_r*normColor )  &&  ( max_deltaW<epsilon_a ))
        {
            if(D.visualize>10){ printf("stopped MM optimization at iteration %d because surface with deltaC %f deltaW %f does not change\n", itMM, deltaC.dot(deltaC), max_deltaW );}
            energyMM_ = energyMM;
            break;
        }
        else if( ( std::abs( energyMM-energyMM_)< epsilon_r* std::abs(energyMM) ) )//&&  ( std::abs( energyMM-energyMM_)< epsilon_a ) )
        {
            if(D.visualize>10){ printf("stopped MM optimization at iteration %d because energy %f does not change\n", itMM, energyMM);}
            energyMM_ = energyMM;
            break;
        }
		    
		
	}
	clock_t t1 = clock();
	if(D.visualize>10){ printf("MM - LM optimization took %f s\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));}
	
	// color to its normal range
	denormalize_variables( sds_color, minVars, maxVars);
	denormalize_variables( pointscolor, minVars, maxVars);
	
	return energyMM;
 }
 
