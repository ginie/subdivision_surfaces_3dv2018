#include <cstring>
#include <cstdio>
#include <string>
#include "TypeDef.h"
#include "Settings.h"
void Settings::error(const char *msg)
{
    fprintf(stderr,"mesh2sds | ERROR | %s\n",(msg)?msg:"");
}


void Settings::printOptions(void)
{
	printf("model parameters:\n");
	printf("            -n|-n_controlPoints	<%lu>\n", n_controlPoints);
	printf("           -lp|-norm	<%f>\n", lp);
    printf("          -eps|-epsilon  <%f>\n", epsilon);
    printf("            -a|-alpha  <%f>\n", alpha);  
    printf("            -b|-beta  <%f>\n", beta); 
    printf("            -g|-gamma  <%f>\n", gamma); 
    printf("            -r|-random  <%f>\n", randomization); 
	printf("  multiscale optmimization:\n");    
    printf("       -oLevel|-octreeLevel  <%lu>\n", octreeLevel);        
    printf("         -uniR|-uniformRefinement	<%lu>\n", uniR);  
    printf("  adaptive coarseninga and splitting of the control mesh:\n");  
    printf("         -quad|-quadrature  <%lu>\n", quadrature);
    printf("      -n_split|-n_splits  <%lu>\n", n_splits);
    printf("   -split_step| <%lu>\n", split_step);
    printf("  -n_collapses| <%lu>\n", n_collapses);
    printf("-collapse_step| <%lu>\n", collapse_step);    
    printf("MM and CG iterations:\n"); 
    printf("        -maxIt|-maxIterations  <%lu>\n", maxIterations);
    printf("     -solverIt|-solverIterations  <%lu>\n", solverIterations);
    printf("        -BBexp|<%f>\n", BBexp);
    printf("         -pFile|-p  %s\n", cloudFile.c_str());
    printf("         -oFile|-o  %s\n", outFile.c_str());
    printf("       -sdsFile|-sds %s\n", iniFile.c_str());
    
}

void Settings::print_usage(void)
{
    fprintf(stderr,"USAGE\n");
    fprintf(stderr,"  mes2SDS [options] meshFile outFile\n");
    printOptions();
}
void Settings::parseCommandLine(int argc, char **argv)
{
    if (argc==1)
    {
        print_usage();
    }
    int aux = 0;
    for (int i=1; i<argc; ++i)
    {
        if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); break; }
        else if (!strcmp(argv[i],"-BBexp")  || !strcmp(argv[i],"-BBExp"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(BBexp))<0) { error("parsing -BBexp"); }
        }
        else if (!strcmp(argv[i],"-eps")  || !strcmp(argv[i],"-epsilon"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(epsilon))<0) { error("parsing -epsilon"); }
        }
        else if (!strcmp(argv[i],"-a")  || !strcmp(argv[i],"-alpha"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(alpha))<0) { error("parsing -alpha"); }
        }
        else if (!strcmp(argv[i],"-b")  || !strcmp(argv[i],"-beta"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(beta))<0) { error("parsing -beta"); }
        } 
        else if (!strcmp(argv[i],"-d")  || !strcmp(argv[i],"-delta"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(delta))<0) { error("parsing -delta"); }
        } 
        else if (!strcmp(argv[i],"-g")  || !strcmp(argv[i],"-gamma"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(gamma))<0) { error("parsing -gamma"); }
        }         
        else if (!strcmp(argv[i],"-r")  || !strcmp(argv[i],"-random"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(randomization))<0) { error("parsing -random"); }
        }         
        else if (!strcmp(argv[i],"-oLevel")  || !strcmp(argv[i],"-octreeLevel"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(octreeLevel))<1) { error("parsing octreeLevel"); }
        }  
        else if (!strcmp(argv[i],"-quad")  || !strcmp(argv[i],"-quadrature"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(quadrature))<1) { error("parsing -quadrature"); }
        }
        else if (!strcmp(argv[i],"-uniR")  || !strcmp(argv[i],"-uniformRefinement"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(uniR))<1) { error("parsing -uniR"); }
        }              
        else if (!strcmp(argv[i],"-lp")  || !strcmp(argv[i],"-norm"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%f",&(lp))<0) { error("parsing -lp"); }
        }          
        else if (!strcmp(argv[i],"-maxIt") || !strcmp(argv[i],"-maxIterations"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(maxIterations))<0) { error("parsing -maxIterations"); }
        }      
        else if (!strcmp(argv[i],"-solverIt") || !strcmp(argv[i],"-solverIterations"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(solverIterations))<0) { error("parsing -solverIterations"); }
        }        
        else if (!strcmp(argv[i],"-VIZ") || !strcmp(argv[i],"-viz"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%d",&(visualize))<0) { error("parsing -viz"); }
        }
        else if (!strcmp(argv[i],"-LM") || !strcmp(argv[i],"-Levenberg"))
        {
            alg = 2;
        }         
        else if (!strcmp(argv[i],"-1storder") || !strcmp(argv[i],"-firstorder"))
        {
            alg = 1;
        }  
        else if (!strcmp(argv[i],"-n_split")  || !strcmp(argv[i],"-n_splits"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(n_splits))<1) { error("parsing n_splits"); }
        }        
        else if (!strcmp(argv[i],"-split_step")  || !strcmp(argv[i],"-split_steps"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(split_step))<1) { error("parsing split_step"); }
        }          
         else if (!strcmp(argv[i],"-n_collapse")  || !strcmp(argv[i],"-n_collapses"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(n_collapses))<1) { error("parsing n_collapses"); }
        }        
        else if (!strcmp(argv[i],"-collapse_step")  || !strcmp(argv[i],"-collapse_steps"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(collapse_step))<1) { error("parsing collapse_step"); }
        }  
        else if (!strcmp(argv[i],"-n")  || !strcmp(argv[i],"-n_controlPoints"))
        {
            if ((++i)>=argc || sscanf(argv[i],"%lu",&(n_controlPoints))<1) { error("parsing n_controlPoints"); }
        } 
	else if (!strcmp(argv[i],"-p") || !strcmp(argv[i],"-pointsFile"))
	{
		i++;
		cloudFile = std::string(argv[i]);
	}        
	else if (!strcmp(argv[i],"-sds") || !strcmp(argv[i],"-sdsFile"))
	{
		i++;
		iniFile = std::string(argv[i]);
	}
	else if (!strcmp(argv[i],"-o") || !strcmp(argv[i],"-outFile"))
	{
		i++;
		outFile = std::string(argv[i]);
	}
        else if (argv[i][0]=='-') { error("unknown option"); }
    }
    if (cloudFile.size()==0)              { error("no inFile");  }
    if (outFile.size()==0)                { error("no outFile"); }
}
