#include <cassert>
#include <cmath>
#include <cstring>
#include <cstddef>
#include <fstream>
#include <map>
#include <string.h>
#include <cstdlib>
#include <vector>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include "PerfectMatching.h"
#include "quadBlossom.h"

using namespace std;

size_t numV=0, numE=0, numF=0;
double **vert=NULL;
size_t **face=NULL;
size_t **edge_p=NULL;
size_t **edge_d=NULL;
double *edge_cost=NULL;
size_t *matching=NULL;

inline double pow2(double x) {return x*x;}

double angle_dist(double alpha, double beta) {
  double diff = beta - alpha;
  if (diff<0)    diff = -diff;
  if (diff>M_PI) diff = 2*M_PI - diff;
  diff = pow2(tan(diff));
  if (isnan(diff) || (diff>1e42)) diff = 1e42;
  return diff;
}

void cleanup() {
  if (numV>0) {
    numV=0;
    delete[] vert[0];
    delete[] vert;
  }
  if (numE>0) {
    numE=0;
    delete[] edge_p[0];
    delete[] edge_p;
    delete[] edge_d[0];
    delete[] edge_d;
    if (edge_cost!=NULL) {
      delete[] edge_cost;
      edge_cost = NULL;
    }
  }
  if (numF>0) {
    numF=0;
    delete[] face[0];
    delete[] face;
    if (matching!=NULL) {
      delete[] matching;
      matching = NULL;
    }
  }
}









void pass_triMesh( const std::vector<double> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex)
{
	numV = vertices.size()/3;
    numF = face_nvertex.size();
    
   
    // read the vertex positions
	// create new data
	vert    = new double*[numV];
	vert[0] = new double[numV*3];
	for (size_t i=1; i<numV; i++)
	{
		vert[i] = vert[0]+3*i;
	}
    for( int iv=0; iv< numV; iv++ )
    {
        for( int idim=0; idim<3; idim++)
        {
            vert[iv][idim] = vertices[iv*3+idim];
        }
    }

    
    
    // read face data
	// create new data
	face    = new size_t*[numF];
	face[0] = new size_t[numF*3];
	for (size_t i=1; i<numF; i++)
	{
		face[i] = face[0]+3*i;
	}

	// read the face topology
	int count = 0;
	for( int iv=0; iv< numF; iv++ )
	{
		for( int idim=0; idim<3; idim++)
		{
			face[iv][idim] = face_vertex[ count ];
			count = count+1;
		}
	}

}


void pass_triMesh( std::vector<float> vertices, std::vector<int> face_nvertex, std::vector<int> face_vertex)
{
	numV = vertices.size()/3;
    numF = face_nvertex.size();
    
   
    // read the vertex positions
	// create new data
	vert    = new double*[numV];
	vert[0] = new double[numV*3];
	for (size_t i=1; i<numV; i++)
	{
		vert[i] = vert[0]+3*i;
	}
    for( int iv=0; iv< numV; iv++ )
    {
        for( int idim=0; idim<3; idim++)
        {
            vert[iv][idim] =  (double)vertices[iv*3+idim];
        }
    }

    
    
    // read face data
	// create new data
	face    = new size_t*[numF];
	face[0] = new size_t[numF*3];
	for (size_t i=1; i<numF; i++)
	{
		face[i] = face[0]+3*i;
	}

	// read the face topology
	int count = 0;
	for( int iv=0; iv< numF; iv++ )
	{
		for( int idim=0; idim<3; idim++)
		{
			face[iv][idim] = face_vertex[ count ];
			count = count+1;
		}
	}

}

void createGraph() {
  // clear old data 
  size_t szE = 3*numF; // up to 2x the memory needed
  if (numE>0) {
    numE = 0;
    delete[] edge_p[0];
    delete[] edge_p;
    delete[] edge_d[0];
    delete[] edge_d;
  }
  // create new data
  edge_p    = new size_t*[szE];
  edge_p[0] = new size_t[szE*2];
  edge_d    = new size_t*[szE];
  edge_d[0] = new size_t[szE*2];
  for (size_t i=1; i<szE; i++) {
    edge_p[i] = edge_p[0]+2*i;
    edge_d[i] = edge_d[0]+2*i;
  }
  size_t *face_count = new size_t[szE];
  map<pair<size_t,size_t>,size_t> boundary;
  map<pair<size_t,size_t>,size_t>::iterator itBound;
  // process faces
  for (size_t f_id=0; f_id<numF; f_id++) {
    for (size_t i=0; i<3; i++) {
      size_t j = (i+1)%3;
      size_t u = face[f_id][i];
      size_t v = face[f_id][j];
      size_t e_id;
      itBound  = boundary.find(make_pair(u,v));
      if (itBound!=boundary.end()) { // manifold violation
        printf("ERROR: The edge (%zu,%zu) is used by two faces (with same orientation).\n",u,v);
        cleanup();
        delete[] face_count;
        exit(1);
      }
      itBound  = boundary.find(make_pair(v,u));
      if (itBound==boundary.end()) { // add edge to boundary
        e_id = numE++;
        boundary[make_pair(u,v)] = e_id;
        edge_p[e_id][0]  = u;
        edge_p[e_id][1]  = v;
        edge_d[e_id][0]  = f_id;
        face_count[e_id] = 1;
        continue;
      }
      e_id = itBound->second;
      edge_d[e_id][1] = f_id;
      face_count[e_id]++;
      if (face_count[e_id]>2) {
        printf("ERROR: The edge (%zu,%zu) is used by two faces (with same orientation).\n",u,v);
        cleanup();
        delete[] face_count;
        exit(1);
      }
    }
  }
  // create inner graph
  size_t numE_outer = 2*numE-3*numF;
  size_t numE_inner = numE - numE_outer;
  size_t **edgeI_p, **edgeI_d;
  edgeI_p    = new size_t*[numE_inner];
  edgeI_p[0] = new size_t[numE_inner*2];
  edgeI_d    = new size_t*[numE_inner];
  edgeI_d[0] = new size_t[numE_inner*2];
  for (size_t i=1; i<numE_inner; i++) {
    edgeI_p[i] = edgeI_p[0]+2*i;
    edgeI_d[i] = edgeI_d[0]+2*i;
  }
  size_t counter=0;
  for (size_t i=0; i<numE; i++) {
    if (face_count[i]==2) {
      for (size_t j=0;j<2;j++)  {
        edgeI_p[counter][j] = edge_p[i][j];
        edgeI_d[counter][j] = edge_d[i][j];
      }
      counter++;
    }
  }
  delete[] face_count;
  delete[] edge_p[0];
  delete[] edge_p;
  delete[] edge_d[0];
  delete[] edge_d;
  numE   = numE_inner;
  edge_p = edgeI_p;
  edge_d = edgeI_d;
}


void createEdgeCost() {
  // clear old data 
  if (edge_cost!=NULL) {
    delete[] edge_cost;
  }
  // create new data
  edge_cost = new double[numE];
  // create auxiliary data
  double **norm  = new double*[numF];
  double **angle = new double*[numF];
  norm[0]  = new double[numF*3];
  angle[0] = new double[numF*3];
  for (size_t i=1; i<numF; i++) {
    norm[i]  = norm[0] +3*i;
    angle[i] = angle[0]+3*i;
  }
  // process auxiliary data
  double edgeTri[3][3];
  double normTri[3];
  for (size_t f_id=0; f_id<numF; f_id++) {
    // fill edgeTri and normTri
    for (size_t i=0; i<3; i++) {
      size_t j = (i+1)%3;
      normTri[i] = 0.0;
      for (size_t dim=0; dim<3; dim++) {
        edgeTri[i][dim] = vert[face[f_id][j]][dim] - vert[face[f_id][i]][dim]; 
        normTri[i] += pow2(edgeTri[i][dim]);
      }
      normTri[i] = sqrt(normTri[i]);
      assert(normTri[i]>0);
    }
    // compute angle
    double minAngle = M_PI;
    double maxAngle = 0;
    size_t minAngleI, maxAngleI;
    for (size_t i=0; i<3; i++) {
      size_t j = (i+1)%3;
      double prod = 0.0;
      for (size_t dim=0; dim<3; dim++) {
        prod += edgeTri[i][dim]*edgeTri[j][dim];
      }
      angle[f_id][j] = acos(-prod/(normTri[i]*normTri[j]));
      if (angle[f_id][j]<minAngle) {
        minAngle  = angle[f_id][j];
        minAngleI = j;
      } 
      if (sin(angle[f_id][j])>sin(maxAngle)) {
        maxAngle  = angle[f_id][j];
        maxAngleI = j;
      }
    }
    // repair smallest angle
    switch (minAngleI) {
      case 0:
        minAngle = M_PI - angle[f_id][1] - angle[f_id][2]   ;
        break;
      case 1:
        minAngle = M_PI - angle[f_id][2] - angle[f_id][0];
        break;
      case 2:
        minAngle = M_PI - angle[f_id][0] - angle[f_id][1];
        break;
    }
    angle[f_id][minAngleI] = minAngle; 
    // use largest angle for normal computation
    size_t maxAngleJ = (maxAngleI+1)%3;
    norm[f_id][0] = 
        edgeTri[maxAngleI][1]*edgeTri[maxAngleJ][2] -
        edgeTri[maxAngleI][2]*edgeTri[maxAngleJ][1];
    norm[f_id][1] = 
        edgeTri[maxAngleI][2]*edgeTri[maxAngleJ][0] -
        edgeTri[maxAngleI][0]*edgeTri[maxAngleJ][2];
    norm[f_id][2] = 
        edgeTri[maxAngleI][0]*edgeTri[maxAngleJ][1] -
        edgeTri[maxAngleI][1]*edgeTri[maxAngleJ][0];
    double norm_len  = 0.0;
    for (size_t dim=0; dim<3; dim++) {
      norm_len += pow2(norm[f_id][dim]);
    }
    norm_len = sqrt(norm_len);
    assert(norm_len>0.0);
    for (size_t dim=0; dim<3; dim++) {
      norm[f_id][dim] /= norm_len;
    }
  }
  // compute edge costs
  for (size_t e_id=0; e_id<numE; e_id++) {
    size_t f1 = edge_d[e_id][0];
    size_t f2 = edge_d[e_id][1];
    size_t u1 = edge_p[e_id][0];
    size_t u2 = edge_p[e_id][1];
    size_t i1[3];
    size_t i2[3];
    if (f1>f2) {
      size_t tmp = f1;
      f1 = f2; f2=tmp;
    }
    // orientation preserving search
    i1[0] = 0;
    while (face[f1][i1[0]]!=u1) i1[0]++;
    i1[1] = (i1[0]+1)%3;
    i1[2] = (i1[1]+1)%3;
    // orientation reversing search
    i2[0] = 0;
    while (face[f2][i2[0]]!=u1) i2[0]++;
    i2[1] = (i2[0]+2)%3;
    i2[2] = (i2[1]+2)%3;
    // angle after merge
    double alpha = angle[f1][i1[0]] + angle[f2][i2[0]];
    double beta  = angle[f1][i1[1]] + angle[f2][i2[1]];
    double gamma = angle[f1][i1[2]];
    double delta = angle[f2][i2[2]];
    // angle between normals
    double prod=0.0;
    for (size_t dim=0; dim<3; dim++) {
      prod += norm[f1][dim]*norm[f2][dim];
    }
    double eta = acos(prod);
    edge_cost[e_id] = 0.25*
      (
        angle_dist(alpha, M_PI_2) + angle_dist(beta,  M_PI_2) + 
        angle_dist(gamma, M_PI_2) + angle_dist(delta, M_PI_2)
      ) + 
      angle_dist(0, eta);
    assert(edge_cost[e_id]>=0);
  }
  // cleanup
  delete[] norm[0];
  delete[] angle[0];
  delete[] norm;
  delete[] angle;
}


void checkPerfect() {
  // check for an even amount of faces
  if (numF%2==1) {
    printf("\nERROR: meshes with %zu triangles cannot be processed.\n",numF);
    cleanup();
    exit(1);
  }
  // compute the degree of each face (deg<3 are boundary faces)
  size_t *degree = new size_t[numF];
  memset(degree, 0, sizeof(size_t)*numF);
  for (size_t e_id=0; e_id<numE; e_id++) {
    degree[edge_d[e_id][0]]++;
    degree[edge_d[e_id][1]]++;
  }
  // count boundary faces (deg1 => corners; deg2 => edges)
  size_t numD1=0, numD2=0;
  for (size_t f_id=0; f_id<numF; f_id++) {
    if (degree[f_id]==3) continue;
    if (degree[f_id]==2) { numD2++; continue;}
    if (degree[f_id]==1) { numD1++; continue;}
    assert(false);
  }
  // assign boundary faces
  size_t *deg1=NULL, *deg2=NULL;
  if (numD1>0) deg1 = new size_t[numD1];
  if (numD2>0) deg2 = new size_t[numD2];
  if ((numD1>0) || (numD2>0)) {
    for (size_t f_id=0,i1=0,i2=0; f_id<numF; f_id++) {
      if (degree[f_id]==3) continue;
      if (degree[f_id]==2) { deg2[i2++] = f_id; continue;}
      if (degree[f_id]==1) { deg1[i1++] = f_id; continue;}
    }
  }
  delete[] degree;
  // test for cubic graph
  if ((numD1==0) && (numD2==0)) return;
  // generate auxiliary perfect matching problem
  PerfectMatching *pm = new PerfectMatching(numF, numE + numD1 + numD2/2);
  pm->options.verbose = false;
  for (size_t e_id=0; e_id<numE; e_id++) {
    pm->AddEdge(edge_d[e_id][0], edge_d[e_id][1], 0.0);
  }
  size_t numE_aux = 0;
  // process corners first
  if (numD1>0) {
    if (numD1>2) { // cyclic connection of corners
      for (size_t c_id=0; c_id<numD1; c_id++) {
        pm->AddEdge(deg1[c_id], deg1[(c_id+1)%numD1], 1.0);
        numE_aux++;
      }
    } else {
      assert(numD2>=2*numD1);
      for (size_t c_id=0; c_id<numD1; c_id++) {
        pm->AddEdge(deg1[c_id], deg2[--numD2], 1.0);
        pm->AddEdge(deg1[c_id], deg2[--numD2], 1.0);
        numE_aux+=2;
      }
    }
  }
  // process edges
  if (numD2>0) {
    for (size_t i=0; i<numD2; i+=2) {
      pm->AddEdge(deg2[i], deg2[i+1], 1.0);
      numE_aux++;
    }
  }
  if (deg1!=NULL) delete[] deg1;
  if (deg2!=NULL) delete[] deg2;
  pm->Solve();
  for (size_t i=0; i<numE_aux; i++) {
    if (pm->GetSolution(numE+i)) {
      printf("ERROR: The mesh cannot be processed.\n");
      cleanup();
      delete pm;
      exit(1);
    }
  }
  delete pm;
}


void perfectMatching() {
  // clear old data 
  if (matching!=NULL) {
    delete[] matching;
  }
  // create new data
  matching = new size_t[numF];
  // compute perfect matching 
  PerfectMatching *pm = new PerfectMatching(numF, numE);
  pm->options.verbose = false;
  for (size_t e_id=0; e_id<numE; e_id++) {
    pm->AddEdge(edge_d[e_id][0], edge_d[e_id][1], edge_cost[e_id]);
  }
  pm->Solve();
  // extract matching 
  for (size_t f_id=0; f_id<numF; f_id++) {
    matching[f_id] = pm->GetMatch(f_id);
  }
  delete pm;
}



void get_quadMesh(std::vector<double> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
	vertices.resize(3*numV);	
	for (size_t v_id=0; v_id<numV; v_id++)
	{
		for (size_t dim=0; dim<3; dim++)
		{
			vertices[ v_id*3 + dim] = vert[v_id][dim];
		}
	}
	face_nvertex.clear();
	face_vertex.clear();
	for (size_t f1=0; f1<numF; f1++)
	{
		size_t f2 = matching[f1];
		if (f2<f1) continue;
		size_t i,j;
		size_t u1 = face[f1][i=0];
		size_t u2;
		for (j=0;j<3;j++) {
		  u2 = face[f2][j];
		  if (u1==u2) break;
		}
		if (u1!=u2)
		{
		  u1 = face[f1][i=1];
		  for (j=0;j<3;j++)
		  {
			u2 = face[f2][j];
			if (u1==u2) break;
		  }
		}
		assert(u1==u2);
		if (face[f1][(i+1)%3]==face[f2][(j+2)%3])
		{
			i = (i+1) % 3;
		} else if (face[f1][(i+2)%3]==face[f2][(j+1)%3])
		{
			j = (j+1) % 3;
		}
		face_nvertex.push_back(4);
		face_vertex.push_back( (int) face[f1][i] );
		face_vertex.push_back( (int) face[f1][(i+1)%3] );
		face_vertex.push_back( (int) face[f2][j] );
		face_vertex.push_back( (int) face[f2][(j+1)%3] );
	}

}


void get_quadMesh(std::vector<float> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
	vertices.resize(3*numV);	
	for (size_t v_id=0; v_id<numV; v_id++)
	{
		for (size_t dim=0; dim<3; dim++)
		{
			vertices[ v_id*3 + dim] = (float)vert[v_id][dim];
		}
	}
	face_nvertex.clear();
	face_vertex.clear();
	for (size_t f1=0; f1<numF; f1++)
	{
		size_t f2 = matching[f1];
		if (f2<f1) continue;
		size_t i,j;
		size_t u1 = face[f1][i=0];
		size_t u2;
		for (j=0;j<3;j++) {
		  u2 = face[f2][j];
		  if (u1==u2) break;
		}
		if (u1!=u2)
		{
		  u1 = face[f1][i=1];
		  for (j=0;j<3;j++)
		  {
			u2 = face[f2][j];
			if (u1==u2) break;
		  }
		}
		assert(u1==u2);
		if (face[f1][(i+1)%3]==face[f2][(j+2)%3])
		{
			i = (i+1) % 3;
		} else if (face[f1][(i+2)%3]==face[f2][(j+1)%3])
		{
			j = (j+1) % 3;
		}
		face_nvertex.push_back(4);
		face_vertex.push_back( (int) face[f1][i] );
		face_vertex.push_back( (int) face[f1][(i+1)%3] );
		face_vertex.push_back( (int) face[f2][j] );
		face_vertex.push_back( (int) face[f2][(j+1)%3] );
	}
}


void quadBlossom(std::vector<double> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, bool visualize=false)
{
	pass_triMesh(vertices, face_nvertex, face_vertex);
	// extract graph and check for existence of perfect matching
	if(visualize>0){ printf("creating graph for perfect matching..."); }
	createGraph();
	if(visualize>0){ printf("done\nchecking if perfect-matching problem can be defined...");}
	checkPerfect();
	if(visualize>0){printf("done\ncomputing edge cost...");}
	createEdgeCost();
	if(visualize>0){printf("done\ncomputing perfect matching...");}
	perfectMatching();
	// writing OFF file
	get_quadMesh(vertices, face_nvertex, face_vertex);
	cleanup();
}


void quadBlossom(std::vector<float> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, bool visualize=false)
{
	pass_triMesh(vertices, face_nvertex, face_vertex);
	// extract graph and check for existence of perfect matching
	if(visualize>0){ printf("creating graph for perfect matching..."); }
	createGraph();
	if(visualize>0){ printf("done\nchecking if perfect-matching problem can be defined...");}
	checkPerfect();
	if(visualize>0){printf("done\ncomputing edge cost...");}
	createEdgeCost();
	if(visualize>0){printf("done\ncomputing perfect matching...");}
	perfectMatching();
	// writing OFF file
	get_quadMesh(vertices, face_nvertex, face_vertex);
	cleanup();
}




