#ifndef __VISUALIZATION_H__
#define __VISUALIZATION_H__


//#include "TypeDef.hpp"
#include "EigenTypeDef.h"

void render_controlMesh( mrpt::gui::CDisplayWindow3D &win, const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &center, bool clear  );
void render_controlMesh_EV( mrpt::gui::CDisplayWindow3D &win, const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &center, bool clear, int evalence);
void scalar2rgb(TypeReal v, TypeReal vmin, TypeReal vmax, float *r, float *g, float *b);
//void render_colorControlMesh( mrpt::gui::CDisplayWindow3D &win, const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const std::vector<TypeReal> &scalarField, bool clear, bool normalize, int ioffset_x, int ioffset_y, int ioffset_z, std::string label );
void render_vectorField( mrpt::gui::CDisplayWindow3D &win,TypeReal *points, TypeReal *field, int n_points, std::vector<TypeReal> &center, bool normalize, bool clear );
void viz_octree( const std::vector<TypeReal> &cellCenters, const std::vector<TypeReal> &cellHalfSides, std::string windowName );
void render_pointCloud( mrpt::gui::CDisplayWindow3D &win, TypeReal *points, int n_points, std::vector<TypeReal> &center, double r, double g, double b, bool clear );
#endif
