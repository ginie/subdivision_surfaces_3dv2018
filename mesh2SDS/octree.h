//
//  octree.hpp
//  
//
//  Created by virginia estellers on 1/3/17.
//
//

#ifndef __octree__
#define __octree__

#include <cstring>

#include <stdio.h>
#include <cstdint>
#include <vector>
#include <utility>
#include <unordered_map>
#include <queue>
#include <bitset>
#include <iostream>
#include <algorithm>
#include <stdint.h>

#include "BBox.h"

struct TypePoint
{
    TypeReal    position[3];
};

struct OctreeNode //  bytes
{
    size_t      n_points;
    size_t      point0;
    bool        isLeaf;
    uint64_t    LocCode;     // or 64-bit, depends on max. required tree depth
    uint64_t    nodeVertices[8];
    uint64_t    neighbours[6];
};

#define MAXDEPTH 20

class Octree
{
    
private:
    size_t octreeDepth = 1;
    BoundingBox BBox;
    std::vector<TypePoint> points;
    std::vector<size_t> pointIDs;
    std::unordered_map<uint64_t, OctreeNode> Nodes;
    std::unordered_map<uint64_t, size_t> Vertices;
    std::unordered_map<uint64_t, size_t> LeafMap;
    std::vector<OctreeNode> Leafs;
    std::vector<TypeReal> vertexCoordinates;
    std::vector<size_t> Cell2Vertex;
    std::vector<int> Vertex2Cell;
    
private:
	uint64_t GetParentCode(uint64_t nodeCode );
	uint64_t GetChildCode(uint64_t nodeCode, int i);
    void setDepth( size_t depth ){ octreeDepth = depth; }
    size_t initialize( std::vector<TypeReal> &pointcoords, size_t max_depth, bool balance_tree );
    void createNode( uint64_t locationCode, OctreeNode child );
    void splitNode(OctreeNode &node);
    void depthTraversal_refineLeafs( OctreeNode &node, size_t max_depth, size_t min_points, size_t &splits );
    void printVoxel(const TypePoint &CellCenter, const TypePoint &CellSides);
    void createNodeVertices( OctreeNode &node );
    uint64_t getVertexMortonCode(TypeReal *pointcoords);
    void GetVertexCoord(uint64_t LocCode, TypePoint &vertex );
    void depthTraversal_createLeafMap( OctreeNode &node );
    void createLeafMap( void );
    void compute_Vertex2Cell(void );
    void compute_Cell2Vertex( void );
    void findCellNeighbours( OctreeNode &node );
    size_t balance( std::vector<uint64_t> leafCodes );
    
    
public:
    //constructor
    Octree( std::vector<TypeReal> &pointcoords, size_t depth, TypeReal BBoxmargins, bool balance_tree);
    Octree( const Octree &other) : octreeDepth(other.octreeDepth), BBox(other.BBox), points(other.points), pointIDs(other.pointIDs), Nodes(other.Nodes), Vertices(other.Vertices), LeafMap(other.LeafMap), Leafs(other.Leafs), vertexCoordinates(other.vertexCoordinates), Cell2Vertex(other.Cell2Vertex), Vertex2Cell(other.Vertex2Cell){}
    Octree() : octreeDepth(0), BBox(), points( std::vector<TypePoint>(0) ), pointIDs( std::vector<size_t>(0) ), Nodes( std::unordered_map<uint64_t, OctreeNode>(0) ), Vertices(std::unordered_map<uint64_t, size_t>(0)), LeafMap(std::unordered_map<uint64_t, size_t>(0)), Leafs(std::vector<OctreeNode>(0) ), vertexCoordinates(std::vector<TypeReal>(0)), Cell2Vertex(std::vector<size_t>(0)), Vertex2Cell(std::vector<int>(0)) {}
    //
    BoundingBox getBBox(){ return BBox; }; 
    TypeReal getBBoxSide( int idim ){ return BBox.getSide(idim); };
    TypeReal getBBoxMin( int idim ){ return BBox.getMin(idim); };
    TypeReal getBBoxMax( int idim ){ return BBox.getMax(idim); };
    void getNode(uint64_t locCode, OctreeNode &node);
    size_t getDepth(void);
    size_t getNPoints(void){ return points.size(); };
    size_t getNLeafs(void){ return Leafs.size(); };
    size_t getNVertex(void){ return Vertices.size(); };
    void getLinearLeaf( size_t i, OctreeNode &leaf){ leaf = Leafs[i]; };
    size_t getLeafIndex( uint64_t locCode );
    size_t getVertexIndex( uint64_t locCode );
    void getPoint(size_t i, TypePoint &point){ point = points[ pointIDs[i] ]; };
    void getPoint(size_t i, TypeReal *point){ for(int idim=0; idim<3; idim++){ point[idim]= points[ pointIDs[i] ].position[idim];} };
    size_t getPointID(size_t i){ return pointIDs[i]; };
    TypeReal getBBoxMin(size_t idim){ return BBox.getMin(idim); };
    TypeReal getBBoxMax(size_t idim){ return BBox.getMax(idim); };
    void getVertexHash( std::unordered_map<uint64_t, size_t> &outVertices ){ outVertices = std::unordered_map<uint64_t, size_t>(Vertices); };
    void getLeafHash( std::unordered_map<uint64_t, size_t> &outLeafMap ){ outLeafMap = std::unordered_map<uint64_t, size_t>(LeafMap); };
    // Morton codes
    size_t GetNodeTreeDepth(const OctreeNode &node);
    size_t GetNodeIDTreeDepth(uint64_t nodeID);
    void GetNodeCenter( const OctreeNode &node, TypePoint &CellCenter );
    uint64_t getPointMortonCode(TypeReal *pointcoords, size_t max_depth );
    void GetParentNode( const OctreeNode &node, OctreeNode &parent );
    bool getChildNode(const OctreeNode &node, int i, OctreeNode &child);
    uint64_t getPointNode( TypeReal *pointcoords );
    // grid structure
    void findNeighbours( void );
    void getCell2Vertex( std::vector< size_t > &out ){ out = Cell2Vertex; };
    void getVertex2Cell( std::vector< int > &out ){ out = Vertex2Cell; };
    void point2grid( const std::vector<TypeReal> &pValues, std::vector<TypeReal> &cValues, std::vector<TypeReal> &vValues );
    void splatting( const std::vector<TypeReal> &pValues, std::vector<TypeReal> &vValues, bool normalize );
    void balanceGrid( void );
    size_t refineOctree( size_t min_points);
    void adjust_bbox( const std::vector<TypeReal> &pointcoords, TypeReal expand );
    void getLeafOccupancy( std::vector<size_t> &leafOccupancy );
    void getLeafPointDensity( std::vector<TypeReal> &leafPDensity );
    void getVertexOccupancy( std::vector<TypeReal> &vertexOccupancy );
    void getVertexPointDensity( std::vector<TypeReal> &vertexPDensity );
    //
    bool pointInNode( const TypePoint &point, const TypePoint &CellCenter, const TypePoint &CellSides );
    bool pointInNode( const TypePoint &point, const OctreeNode &node );
    void getVoxel(const OctreeNode &node, TypePoint &CellCenter, TypePoint &HalfSides);
    void getVoxelVertices(const OctreeNode &node, std::vector<TypeReal> &vertexcoords );
    size_t getVoxels( std::vector<TypeReal> &centers, std::vector<TypeReal> &halfSides );
    size_t getVertices( std::vector<TypeReal> &vertices ){ vertices = vertexCoordinates; return vertexCoordinates.size()/3; };
    void printNeighbours( void );
    void averagePerLeaf( const std::vector<TypeReal> & point_vars, std::vector<TypeReal> & leaf_vars, std::vector<TypeReal> & leaf_weights );
};




#endif /* octree_hpp */
