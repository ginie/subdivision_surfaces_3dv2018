#include <cassert>
#include <cstdio>
#include <cfloat>
#include <algorithm>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <unordered_map>
#include <queue>
#include <utility>
#include <boost/multi_array.hpp>
#include <boost/config.hpp>
#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/functional/hash.hpp>

#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "PointcloudOp.h"
#include "kdtree2.h"


#ifndef __edgemap__
#define __edgemap__
struct pair_hash {
    template <class T1, class T2>
    std::size_t operator () (const std::pair<T1,T2> &p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        // Mainly for demonstration purposes, i.e. works but is overly simple
        // In the real world, use sth. like boost.hash_combine
        return h1 ^ h2;  
    }
};
typedef std::unordered_map< std::pair<int,int>, int, pair_hash > edgemap;
#endif


PointcloudOp::PointcloudOp(const std::vector<TypeReal> &_vertices, int estimate_geometry = 0)
{
	// initialize vertices, face_nvertex, face_vertex
	vertices =_vertices;
	size_t n_points = vertices.size()/3;
	int kNNgraph = 6;
	
	// create a kdtree to determine the nearest neighbours
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_points][3]);
	for(int ii=0; ii<n_points; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = vertices[ii*3+idim];
		}
	}
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
	tree = new kdtree2::KDTree(KdTreePoints,false); 
	tree->sort_results = true;  

	// if the pointcloud down not include normals,
	// estimate their orientation by PCA analysis
	// orient them by computing the minimim spanning tree
	std::unordered_map <std::pair <int, int>, int, boost::hash <std::pair <int, int> > > edge_map;
	std::vector< int > edge_pairs;
	int num_edges = 0;
	
	// create kNN graph
	for(int ipt=0; ipt<n_points; ipt++)
	{
		std::vector<TypeReal> query(3);
		for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = vertices[ipt*3+idim];
		}
		tree->n_nearest(query, kNNgraph + 1, KdtreeRes);

		// for each point, use the vertex and its neighbourhood to estimate the geometry of the surface
		for( int k=0; k<kNNgraph + 1; k++)
		{
			int neigh =  KdtreeRes[k].idx;
			if( neigh == ipt ){ continue; }
			std::pair<int, int> edge_ikey = std::make_pair( ipt, neigh );
			if( (edge_map.count(edge_ikey) == 0) )
			{
				edge_map.emplace( edge_ikey, num_edges );
				// symmetrize the graph
				std::pair<int, int> edge_jkey = std::make_pair( neigh, ipt );
				edge_map.emplace( edge_jkey, num_edges );
				edge_pairs.push_back(ipt);
				edge_pairs.push_back(neigh);
				num_edges += 1;
				//printf("new edge in map %d %d\n", ipt, neigh);
			}
		}
	}
	
	std::vector<int> n_neighs( n_points, 0);
	for(int ii=0; ii< num_edges; ii++)
	{
		n_neighs[ edge_pairs[2*ii] ] += 1;
		n_neighs[ edge_pairs[2*ii+1] ] += 1;
	}
	std::vector<int> start_neighs( n_points, 0);
	for(int ii=1; ii< n_points; ii++)
	{
		start_neighs[ ii ] = start_neighs[ii-1] + n_neighs[ii-1];
		//printf("start neigh point %d is %d with %d neighbours\n", ii, start_neighs[ ii ] , n_neighs[ii] );
	}
	std::vector<int> neighs(2*num_edges);
	for(int ii=0; ii< num_edges; ii++)
	{
		int v0 = edge_pairs[2*ii];
		int v1 = edge_pairs[2*ii+1];
		int indx = start_neighs[ v0 ];
		neighs[ indx ] = v1;
		start_neighs[ v0 ] += 1;
		//
		indx = start_neighs[ v1 ];
		neighs[ indx ] = v0;
		start_neighs[ v1 ] += 1;
	}
	//printf("initialized neigh_graph\n");
	
	if( estimate_geometry > 0) // create edges for the kNN graph
	{
		int indx = 0;
		TypeReal twoPi = 2*3.14159265;
		std::vector<bool> boundary(n_points, false);
		pBoundary.resize(n_points, 0);
		pCorner.resize(n_points, 0);
		pLine.resize(n_points, 0);
		pInterior.resize(n_points, 0);
		principalK1.resize(n_points, 0);
		principalD1.resize(n_points*3);
		principalK2.resize(n_points, 0);
		principalD2.resize(n_points*3);
		principalD3.resize(n_points*3);// normal direction
		confidence.resize(n_points, 0);
		for(int ipt=0; ipt<n_points; ipt++)
		{
	
			// for each point, use the vertex and its neighbourhood to estimate the geometry of the surface
			int n_neigh = n_neighs[ipt];
			Eigen::Vector3f point( vertices[3*ipt], vertices[3*ipt+1], vertices[3*ipt+2] );
			// find the centroid of each cluster
			Eigen::Vector3f centroid(0,0,0);
			Eigen::MatrixXf neigh_points( n_neigh, 3);
			Eigen::MatrixXf centered_points( n_neigh, 3);
			Eigen::MatrixXf pcentered_points( n_neigh, 3);
			Eigen::VectorXf neigh_weights(n_neigh);
			//printf("closests points to %f,%f,%f are:", points[ipt*3], points[ipt*3+1], points[ipt*3+2]);
			for( int k=0; k<n_neigh; k++)
			{
				for(int idim=0; idim<3; idim++)
				{
					neigh_points(k,idim) = vertices[ neighs[indx]*3+idim ];
				}
				Eigen::Vector3f neighp = neigh_points.row(k);
				neigh_weights[k] = ( neighp - point ).norm();
				centroid += neigh_weights[k] * neighp;
				indx += 1;
				//printf("\t %f %f %f at distance %f\n", points[ closestP[k]*3+0 ], points[ closestP[k]*3+1 ], points[ closestP[k]*3+2 ], distance2P[k] );
			}
			centroid /= neigh_weights.sum();
			neigh_weights /= neigh_weights.sum();
			
	
			// computer the covariance matrix
			Eigen::Matrix3f cov =  Eigen::Matrix3f::Zero();			
			for( int k=0; k<n_neigh; k++)
			{
				Eigen::Vector3f aux;
				for(int idim=0; idim<3; idim++)
				{
					aux[idim] = (centroid[idim] - neigh_points(k,idim) );
					centered_points(k,idim) = neigh_points(k,idim) - centroid[idim];
					pcentered_points(k,idim) = neigh_points(k,idim) - point[idim];
				}
				cov += neigh_weights[k] * ( aux *(aux.transpose()));
			}    
			
			// find principal components
			Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigensolver(cov);
			if (eigensolver.info() != Eigen::Success) abort();
			Eigen::Vector3f eigVals = eigensolver.eigenvalues();
			// sort eigenValues
			std::vector<int> map(3);
			for(int idim=0; idim<3; idim++){map[idim] = idim;}
			std::sort( map.begin(), map.end(), [&](int x,int y){ return eigVals[x]<eigVals[y];} );
			//matrix whose columns are the eigenvectors.
			Eigen::Matrix3f eigVects = eigensolver.eigenvectors();
			for(int idim=0; idim<3; idim++)
			{
				principalD1[ipt*3+idim] = eigVects(idim,map[2]);
				principalD2[ipt*3+idim] = eigVects(idim,map[1]);
			}
			principalD3[ipt*3] = principalD1[ipt*3+1]*principalD2[ipt*3+2] - principalD1[ipt*3+2]*principalD2[ipt*3+1];
			principalD3[ipt*3+1] = principalD1[ipt*3+2]*principalD2[ipt*3+0] - principalD1[ipt*3+0]*principalD2[ipt*3+2];
			principalD3[ipt*3+2] = principalD1[ipt*3+0]*principalD2[ipt*3+1] - principalD1[ipt*3+1]*principalD2[ipt*3+0];
			
			TypeReal sum = eigVals[0] + eigVals[1] + eigVals[2];
			Eigen::Vector3f lambdas( eigVals[map[0]]/sum, eigVals[map[1]]/sum, eigVals[map[2]]/sum);
			Eigen::Vector3f boundary_model(0, 1.0/3, 2.0/3);
			Eigen::Vector3f corner_model(1.0/3, 1.0/3, 1.0/3);
			Eigen::Vector3f interior_model(0, 1.0/2, 1.0/2);
			Eigen::Vector3f line_model(0, 0, 1.0);
			TypeReal dist_boundary = (lambdas - boundary_model).norm();
			TypeReal dist_interior = (lambdas - interior_model).norm();
			TypeReal dist_corner = (lambdas - corner_model).norm();
			TypeReal dist_line = (lambdas - line_model).norm();
			
			pBoundary[ipt] = std::exp( -dist_boundary );
			pCorner[ipt] = std::exp( -dist_corner );
			pInterior[ipt] = std::exp( -dist_interior );
			pLine[ipt] = std::exp( -dist_line );
			sum =  pBoundary[ipt] + pCorner[ipt] + pInterior[ipt] + pLine[ipt] ;
			pBoundary[ipt] /= sum;
			pCorner[ipt] /= sum;
			pInterior[ipt] /= sum;
			pLine[ipt] /= sum;
			if( pBoundary[ipt] > std::max( std::max(pCorner[ipt], pInterior[ipt]), pLine[ipt]) ){ pBoundary[ipt] = 1.0; }
			//printf("point %d has prob boundary %f line %f corner %f interior %f\n", ipt, pBoundary[ipt], pLine[ipt], pCorner[ipt], pInterior[ipt]);
			
			// project points and fit a parabola  
			if( estimate_geometry>1 )
			{
				Eigen::MatrixXf neigh_projection = centered_points * eigVects;
				TypeReal A11 = 0;
				TypeReal A12 = 0;
				TypeReal A22 = 0;
				TypeReal b1 = 0; TypeReal b2 = 0;
				for( int k=0; k<n_neigh; k++)
				{
					TypeReal x = neigh_projection(k,map[2]);
					TypeReal y = neigh_projection(k,map[1]);
					TypeReal z = neigh_projection(k,map[0]);			
					A11 += std::pow(x,4);
					A12 += std::pow(x*y,2);
					A22 += std::pow(y,4);
					b1 += std::pow(x,2) * z;
					b2 += std::pow(y,2) * z;
				} 
				
				// estimate principal curvatures solving system A*k = b
				TypeReal idetA = 1.0/( A11*A22 - std::pow(A12,2) );
				principalK1[ipt] = 2*std::abs( idetA* ( A22* b1 - A12*b2) );
				principalK2[ipt] = 2*std::abs( idetA* ( -A12* b1 + A11*b2) );
				
				// get confidence measures
				confidence[ipt] = 1.0;
				
			}// end if (estimate_geometry > 1)
				
			// compute boundary likelihood
			std::vector<TypeReal> angles; 
			TypeReal avrg_dist = 0;
			Eigen::MatrixXf neigh_pProjection = pcentered_points * eigVects;
			//printf("angles for point %d:", ipt);
			for( int k=0; k<n_neigh; k++)
			{
				Eigen::Vector3f aux = neigh_pProjection.row(k);
				avrg_dist += aux.norm();
				//printf("aux = ( %f, %f, %f)\n", aux[0], aux[1], aux[2] );
				float x = aux[ map[1] ];
				float y = aux[ map[2] ];
				float length = std::sqrt( x*x + y*y );
				float cos_angle = x/length;
				float angle = std::acos( cos_angle );
				if(y <0 )
				{
					angle = -angle;
				}
				//printf(" %f", angle);
				angles.push_back( angle );
			} 		
			//printf("\n");
			avrg_dist /= n_neigh;
			TypeReal centroid_dist = (centroid - point).norm();
			TypeReal pBoundary_centroid = std::min( centroid_dist/avrg_dist , (TypeReal) 1.0);
			// sort neighbours w.r.t angles
			std::sort( angles.begin(), angles.end() );
			TypeReal max_diff = 0;
			//printf("sorted angles:");
			for( int k=0; k<n_neigh; k++)
			{
				//printf(" %f", angles[k]);
				TypeReal diff = angles[ (k+1)%n_neigh ] - angles[k];
				if( k== n_neigh - 1) { diff += twoPi; }
				if( diff > max_diff ){ max_diff = diff; }
			}
			//printf("\n");
			TypeReal pBoundary_angle = (max_diff/twoPi - 1.0/n_neigh)/(0.5 - 1.0/n_neigh);
			if( pBoundary_angle> 1.0){ pBoundary_angle = 1.0;} 
			pBoundary[ipt] = pBoundary_angle;// *pBoundary_centroid *pBoundary[ipt];
			//pBoundary[ipt] = std::pow(pBoundary[ipt], 1.0/3);
			//printf("point %d/%d has %f probability to be a boundary\n", ipt, n_points,  pBoundary[ipt]);
			if( std::isnan(principalK1[ipt]) || std::isnan(principalK2[ipt]) || std::isnan(principalD1[ipt*3]) || std::isnan(principalD1[ipt*3+1]) || std::isnan(principalD1[ipt*3+2]) || std::isnan(principalD2[ipt*3]) || std::isnan(principalD2[ipt*3+1]) || std::isnan(principalD2[ipt*3+2])|| std::isnan(principalD3[ipt*3]) || std::isnan(principalD3[ipt*3+1]) || std::isnan(principalD3[ipt*3+2]) || std::isinf(principalK1[ipt]) || std::isinf(principalK2[ipt]) || std::isinf(principalD1[ipt*3]) || std::isinf(principalD1[ipt*3+1]) || std::isinf(principalD1[ipt*3+2]) || std::isinf(principalD2[ipt*3]) || std::isinf(principalD2[ipt*3+1]) || std::isinf(principalD2[ipt*3+2]) || std::isinf(principalD3[ipt*3]) || std::isinf(principalD3[ipt*3+1]) || std::isinf(principalD3[ipt*3+2]) )
			{
				//printf("nan in Jet point %ipt k1 = %f, d1 = %f %f %f, k2= %f, d2 = %f %f %f\n", principalK1[ipt], principalD1[ipt*3], principalD1[ipt*3+1], principalD1[ipt*3+2], principalK2[ipt], principalD2[ipt*3], principalD2[ipt*3+1], principalD2[ipt*3+2] );
				principalK1[ipt] = 0;
				principalD1[ipt*3] = 0;
				principalD1[ipt*3+1] = 0;
				principalD1[ipt*3+2] = 0;
				principalK2[ipt] = 0;
				principalD2[ipt*3] = 0;
				principalD2[ipt*3+1] = 0;
				principalD2[ipt*3+2] = 0;
				principalD3[ipt*3] = 0;
				principalD3[ipt*3+1] = 0;
				principalD3[ipt*3+2] = 0;    		
				confidence[ipt] = 0;
			}		
		}// end loop over points
		
		
    
		// compute weights for MST computation
		typedef std::pair<int, int> E;
		//printf("%d edges in knn graph, %d size og edge_pairs\n", num_edges, edge_pairs.size()/2);
		E edges[ num_edges ];
		TypeReal normal_weights[ num_edges ];
		TypeReal boundary_weights[ num_edges ];
		for( int iedge=0; iedge< num_edges; iedge++ )
		{
			int iv = edge_pairs[2*iedge];
			int jv = edge_pairs[2*iedge+1];
			TypeReal ndisti = 0;
			TypeReal ndistj = 0;
			for(int idim=0; idim<3; idim++)
			{
				TypeReal aux = vertices[iv*3+idim] -vertices[jv*3+idim];
				ndisti += aux* principalD3[iv*3+idim];
				ndistj += aux* principalD3[jv*3+idim];
			}
			TypeReal w = std::abs(ndisti)+std::abs(ndistj);
			normal_weights[iedge] = w;
			boundary_weights[iedge] = 2.0 - pBoundary[iv] - pBoundary[jv];
			edges[iedge] = std::make_pair( iv, jv );
			
		}

		// minimum spanning tree		
		using namespace boost;
		typedef adjacency_list < vecS, vecS, undirectedS,
		property<vertex_distance_t, TypeReal>, property < edge_weight_t, TypeReal > > Graph;

		{ // minimium spanning tree for normals
			Graph g(edges, edges + sizeof(edges) / sizeof(E), normal_weights, n_points );
			property_map<Graph, edge_weight_t>::type weightmap = get(edge_weight, g);
			std::vector < graph_traits < Graph >::vertex_descriptor > p(num_vertices(g));
			prim_minimum_spanning_tree(g, &p[0]);
			std::vector< std::vector<int> > sons(n_points, std::vector<int>(0) );
			std::vector<int> sources;
			//printf("MST has size %d out of %d = %d points\n", p.size(), n_points, num_vertices(g));
			int count = 0;
			
			for (int i = 0; i < p.size(); i++)
			{
				if (p[i] != i)
				{
					sons[ p[i] ].push_back(i);
					//printf("parent %d is %d\n", p[i], i);
				}else
				{
					sources.push_back(i);
				}
			}
			int source = -1;
			for(int ii=0; ii< sources.size(); ii++)
			{ 
				int isource = sources[ii];
				if( sons[ isource ].size() >0 )
				{ 
					source = isource; 
					sources.erase( sources.begin()+ii );
					break;
				}
			}
			std::queue<int> next_vertices;
			next_vertices.push( source );
			while( ! next_vertices.empty() )
			{
				int iv = next_vertices.front();
				for(int k=0; k< sons[iv].size(); k++)
				{
					int jv = sons[iv][k];
					//printf("\tedge %d %d in MST\n", iv, jv);
					TypeReal ip = 0;
					for(int idim=0; idim<3; idim++)
					{
						ip += principalD3[iv*3+idim] * principalD3[jv*3+idim];
					}
					if( ip < 0 )
					{
						for(int idim=0; idim<3; idim++)
						{
							principalD3[jv*3+idim] *= -1.0;
						}
					}
					next_vertices.push(jv);
				}
				next_vertices.pop();
			}
		}
		
		
		
		/*{ // minimium spanning tree for boundaries
			Graph g(edges, edges + sizeof(edges) / sizeof(E), boundary_weights, n_points );
			property_map<Graph, edge_weight_t>::type weightmap = get(edge_weight, g);
			std::vector < graph_traits < Graph >::vertex_descriptor > p(num_vertices(g));
			prim_minimum_spanning_tree(g, &p[0]);
			std::vector< std::vector<int> > sons(n_points, std::vector<int>(0) );
			std::vector<int> sources;
			//printf("MST has size %d out of %d = %d points\n", p.size(), n_points, num_vertices(g));
			int count = 0;
			
			for (int i = 0; i < p.size(); i++)
			{
				if (p[i] != i)
				{
					sons[ p[i] ].push_back(i);
					//printf("parent %d is %d\n", p[i], i);
				}else
				{
					sources.push_back(i);
				}
			}
			int source = -1;
			for(int ii=0; ii< sources.size(); ii++)
			{ 
				int isource = sources[ii];
				if( sons[ isource ].size() >0 )
				{ 
					source = isource; 
					sources.erase( sources.begin()+ii );
					break;
				}
			}
			std::queue<int> next_vertices;
			next_vertices.push( source );
			while( ! next_vertices.empty() )
			{
				int iv = next_vertices.front();
				if( sons[iv].size()< 3 ){ next_vertices.pop(); continue; }
				TypeReal avrg_pBoundary = pBoundary[iv];
				for(int k=0; k< sons[iv].size(); k++)
				{
					int jv = sons[iv][k];
					avrg_pBoundary += pBoundary[jv];
					next_vertices.push(jv);
				}
				avrg_pBoundary /= sons[iv].size();
				if( avrg_pBoundary >0.75 )
				{
					for(int k=0; k< sons[iv].size(); k++)
					{
						int jv = sons[iv][k];
						boundary[jv] = true;
					}
				}
				next_vertices.pop();
			}
		}
		for(int ii=0; ii<n_points; ii++)
		{
			if( boundary[ii] )
			{
				pBoundary[ii] = 1.0;
			}
			else{ pBoundary[ii] = 0; }
		}*/
	}

}









