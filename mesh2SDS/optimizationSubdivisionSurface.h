#ifndef __SDAPPROX__
#define __SDAPPROX__                

void normalize_variables( std::vector<TypeReal> &point_values, size_t n_points, std::vector<TypeReal> &minVars, std::vector<TypeReal> &maxVars);
void denormalize_variables( std::vector<TypeReal> &point_values, std::vector<TypeReal> minVars, std::vector<TypeReal> maxVars);


void refinedMeshGeometry( std::vector<TypeReal> &points, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &vertex_nring, std::vector<int> &vertex_0ring, std::vector<int> &vertex_ring, std::vector<int> &vertex_nfaces, std::vector<int> &vertex_0face, std::vector<int> &vertex_face, std::vector<TypeReal> &principalK1, std::vector<TypeReal> &principalK2, std::vector<TypeReal> &principalD1, std::vector<TypeReal> &principalD2, std::vector<TypeReal> &principalD3, std::vector<TypeReal> &metric);
void build_normalProjection( const std::vector<TypeReal> &normals, SpMat &Pn);
void graph_Differences( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, SpMat &regMat, SpMat &regOp, bool diagonals);
//TypeReal find_closest_Spoint(const std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const std::vector<int> &adjacency, const std::vector<int> &changeCoord, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, std::vector<TypeReal>& closestSpoint, size_t max_iter);
void estimate_diff2metrics( TypeReal *Ss, TypeReal *St, TypeReal *Sss, TypeReal *Sst, TypeReal *Stt, TypeReal *normals, TypeReal *metric, TypeReal *meanCurvature, TypeReal *principalCurvature1, TypeReal *principalCurvature2, TypeReal *principalDirection1, TypeReal *principalDirection2, size_t n_samples );
void sample_diffmetrics( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const std::vector<int> &f_params, const std::vector<TypeReal> &s_params, const std::vector<TypeReal> &t_params, std::vector<TypeReal> &spoints, std::vector<TypeReal> &normals, std::vector<TypeReal> &metric, std::vector<TypeReal> &meanCurvature, std::vector<TypeReal> &principalRadius1, std::vector<TypeReal> &principalRadius2, std::vector<TypeReal> &principalDirection1, std::vector<TypeReal> &principalDirection2 );
TypeReal KdSearch_closestParameter2Surface( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params);
void KdSearch_closestPointValue( std::vector<TypeReal> &points, std::vector<TypeReal> &point_values, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &vertex_values);

// joint optimization
TypeReal KdSearch_closestRefinedVertex( std::vector<TypeReal> &points, std::vector<TypeReal> refinedVertices, std::vector<int> &closestRvertex, SpMat &project2closestVertex);
TypeReal sqrDistance_1storderApprox( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals);

TypeReal ColorDistance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &sds_color, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> &colors, std::vector<int> &boundary_vertices);
TypeReal Distance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<int> &boundary_vertices);
void approxDistance_MM( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, int n_ref );

TypeReal Distance_trimmedLSQ( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> &weights);


TypeReal compute_gradient( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, const SpMat &Pn, const SpMat &regM, Settings D, SpVect &grad );
void lpOptimization_lBFGS(  std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals );


void computeLM_JacobianResidual( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, const SpMat &Pn, const SpMat &regM, SpMat &J, SpVect &res );
TypeReal LeastSquaresOptimization_LM( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals );
void computeLM_JacobianResidual( std::vector<TypeReal> &points, std::vector<TypeReal> &pointscolor, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, std::vector<TypeReal> &sds_color, const SpMat &Pn, const SpMat &regM, SpMat &J, SpVect &res );
TypeReal LeastSquaresOptimization_LM( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &sds_color, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> pointscolor );
TypeReal SDScolorOptimization_LM( std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &sds_color, Settings D, std::vector<TypeReal> &points, std::vector<TypeReal> pointscolor );


#endif
