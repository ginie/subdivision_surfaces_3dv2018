#include <vector>

bool triMeshDecimation(std::vector<double> invertices, std::vector<int> inface_nvertex, std::vector<int> inface_vertex, size_t	targetNVerticesDecimatedMesh, std::vector<double> &outvertices, std::vector<int> &outface_nvertex, std::vector<int> &outface_vertex, bool viz);
bool triMeshDecimation(std::vector<float> invertices, std::vector<int> inface_nvertex, std::vector<int> inface_vertex, size_t	targetNVerticesDecimatedMesh, std::vector<float> &outvertices, std::vector<int> &outface_nvertex, std::vector<int> &outface_vertex, bool viz);
