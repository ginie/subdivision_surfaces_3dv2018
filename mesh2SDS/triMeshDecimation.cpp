#define _CRT_SECURE_NO_WARNINGS
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#ifdef _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif // _CRTDBG_MAP_ALLOC

#include "mdMeshDecimator.h"
#include "triMeshDecimation.h"

using namespace MeshDecimation;
using namespace std;

void CallBack(const char * msg)
{
    cout << msg;
}

bool triMeshDecimation(std::vector<double> invertices, std::vector<int> inface_nvertex, std::vector<int> inface_vertex, size_t	targetNVerticesDecimatedMesh, std::vector<double> &outvertices, std::vector<int> &outface_nvertex, std::vector<int> &outface_vertex, bool viz = false)
{

    size_t targetNTrianglesDecimatedMesh = 2*( targetNVerticesDecimatedMesh -1 );
	Float  maxDecimationError = 1.0;
	bool success = true;

	// Load input mesh
	vector< Vec3<Float> > points;
    vector< Vec3<int> > triangles;
    int nv = invertices.size()/3;
    int nf = inface_nvertex.size();
	points.resize(nv);
	triangles.resize(nf);
	for (int p = 0; p < nv ; p++) 
	{
		points[p].X() = (Float)invertices[ 3*p ];
		points[p].Y() = (Float)invertices[ 3*p + 1];
		points[p].Z() = (Float)invertices[ 3*p + 2];
	}        
	for (int t = 0; t < nf ; ++t)
	{
		if( inface_nvertex[t]!= 3 )
		{
			printf("error, expecting triangulat input mesh\n");
			success = false;
			break;
		}
		triangles[t].X() = inface_vertex[3*t];
		triangles[t].Y() = inface_vertex[3*t+1];
		triangles[t].Z() = inface_vertex[3*t+2];
	}
    
    
	if( success )
	{
		// decimate mesh
		MeshDecimator myMDecimator;
		if( viz ){ myMDecimator.SetCallBack(&CallBack); }
		myMDecimator.SetEColManifoldConstraint(true);
		myMDecimator.Initialize(points.size(), triangles.size(), &points[0], &triangles[0]);
		myMDecimator.Decimate(targetNVerticesDecimatedMesh, 
							  targetNTrianglesDecimatedMesh, 
							  maxDecimationError);
	
		// allocate memory for decimated mesh
		vector< Vec3<Float> > decimatedPoints;
		vector< Vec3<int> > decimatedtriangles;
		decimatedPoints.resize(myMDecimator.GetNVertices());
		decimatedtriangles.resize(myMDecimator.GetNTriangles());
	
		// retreive decimated mesh
		myMDecimator.GetMeshData(&decimatedPoints[0], &decimatedtriangles[0]);
	
		nv = decimatedPoints.size();
		nf = decimatedtriangles.size();
		outvertices.resize(nv*3);
		outface_nvertex = std::vector<int>(nf, 3);
		outface_vertex.resize(nf*3);
		for(size_t v = 0; v < nv; v++)
		{
			for(int idim=0; idim<3; idim++)
			{
				outvertices[ v*3+idim] = (double)decimatedPoints[v][idim];
			}
		}
		for(size_t f = 0; f < nf; f++)
		{
			for(int ii=0; ii<3; ii++)
			{
				outface_vertex[ f*3 + ii ] = decimatedtriangles[f][ii];
			}
		}
	}
	
#ifdef _CRTDBG_MAP_ALLOC
    _CrtDumpMemoryLeaks();
#endif // _CRTDBG_MAP_ALLOC

	return success;
}


bool triMeshDecimation(std::vector<float> invertices, std::vector<int> inface_nvertex, std::vector<int> inface_vertex, size_t	targetNVerticesDecimatedMesh, std::vector<float> &outvertices, std::vector<int> &outface_nvertex, std::vector<int> &outface_vertex, bool viz = false)
{

    size_t targetNTrianglesDecimatedMesh = 2*( targetNVerticesDecimatedMesh -1 );
	Float  maxDecimationError = 1.0;
	bool success = true;

	// Load input mesh
	vector< Vec3<Float> > points;
    vector< Vec3<int> > triangles;
    int nv = invertices.size()/3;
    int nf = inface_nvertex.size();
	points.resize(nv);
	triangles.resize(nf);
	for (int p = 0; p < nv ; p++) 
	{
		points[p].X() = (Float)invertices[ 3*p ];
		points[p].Y() = (Float)invertices[ 3*p + 1];
		points[p].Z() = (Float)invertices[ 3*p + 2];
	}        
	for (int t = 0; t < nf ; ++t)
	{
		if( inface_nvertex[t]!= 3 )
		{
			printf("error, expecting triangulat input mesh\n");
			success = false;
			break;
		}
		triangles[t].X() = inface_vertex[3*t];
		triangles[t].Y() = inface_vertex[3*t+1];
		triangles[t].Z() = inface_vertex[3*t+2];
	}
    
    
	if( success )
	{
		// decimate mesh
		MeshDecimator myMDecimator;
		if( viz ){ myMDecimator.SetCallBack(&CallBack);}
		myMDecimator.SetEColManifoldConstraint(true);
		myMDecimator.Initialize(points.size(), triangles.size(), &points[0], &triangles[0]);
		myMDecimator.Decimate(targetNVerticesDecimatedMesh, 
							  targetNTrianglesDecimatedMesh, 
							  maxDecimationError);
	
		// allocate memory for decimated mesh
		vector< Vec3<Float> > decimatedPoints;
		vector< Vec3<int> > decimatedtriangles;
		decimatedPoints.resize(myMDecimator.GetNVertices());
		decimatedtriangles.resize(myMDecimator.GetNTriangles());
	
		// retreive decimated mesh
		myMDecimator.GetMeshData(&decimatedPoints[0], &decimatedtriangles[0]);
	
		nv = decimatedPoints.size();
		nf = decimatedtriangles.size();
		outvertices.resize(nv*3);
		outface_nvertex = std::vector<int>(nf, 3);
		outface_vertex.resize(nf*3);
		for(size_t v = 0; v < nv; v++)
		{
			for(int idim=0; idim<3; idim++)
			{
				outvertices[ v*3+idim] = (float)decimatedPoints[v][idim];
			}
		}
		for(size_t f = 0; f < nf; f++)
		{
			for(int ii=0; ii<3; ii++)
			{
				outface_vertex[ f*3 + ii ] = decimatedtriangles[f][ii];
			}
		}
	}
	
#ifdef _CRTDBG_MAP_ALLOC
    _CrtDumpMemoryLeaks();
#endif // _CRTDBG_MAP_ALLOC

	return success;
}


