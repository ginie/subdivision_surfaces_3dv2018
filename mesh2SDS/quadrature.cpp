//
//  quadrature.h
//  
//
//  Created by virginia estellers on 1/17/17.
//
//
#include <cstdio>
#include <vector>
#include <cmath>

#include "TypeDef.h"
#include "quadrature.h"
const double PI = std::atan(1.0)*4;

//------------- sample at quadrature-----------------------------------------------------

void quadrature_referenceInterval(size_t n_quad, std::vector<TypeReal> &qpoint, std::vector<TypeReal> &qweight)
{
    qpoint.resize(n_quad);
    qweight.resize(n_quad);
    TypeReal aux, aux1, aux2, aux3;
	switch(n_quad)
	{
	case 1:
		qpoint[0] = 0;
		qweight[0] = 2;
		break;
	case 2:
		aux = std::sqrt(1.0/3); 
		qpoint[0] = -aux; qpoint[1] = aux;
		qweight[0] = 1.f; qweight[1] = 1.f;
		break;
	case 3:
		aux = std::sqrt(3.0/5); 
		qpoint[0] = -aux; qpoint[1] = 0; qpoint[2] = aux;
		qweight[0] = 5.f/9; qweight[1] = 8.f/9; qweight[2] = 5.f/9;    	
		break;
	default:
		aux1 = 2.0 * std::sqrt(6.0/5) / 7.f;
		aux2 = std::sqrt( 3.0/7 + aux1);
		aux3 = std::sqrt( 3.0/7 - aux1);
		qpoint[0] = -aux2; qpoint[1] = -aux3; qpoint[2] = aux3; qpoint[3] = aux2;
		aux = std::sqrt(30.0);
		qweight[0] = (18.f - aux)/36; qweight[1] = (18.f + aux)/36; qweight[2] = (18.f + aux)/36; qweight[3] = (18.f - aux)/36;    	
		break;
	}
    
}

// Gaussian quadrature rule over face quad [0,1]^2
void quadrature_referenceQuad( size_t n_Uquad, size_t n_Vquad, std::vector<TypeReal> &qpoints, std::vector<TypeReal> &qweights)
{
    // quadrature in each dimension
    std::vector<TypeReal> uQpoint, vQpoint;
    std::vector<TypeReal> uQweight, vQweight;
    quadrature_referenceInterval(n_Uquad, uQpoint, uQweight);
    quadrature_referenceInterval(n_Vquad, vQpoint, vQweight);
    // tensor product construction of quadrature over quads
    qpoints.resize(n_Uquad*n_Vquad*2);
    qweights.resize(n_Uquad*n_Vquad);
    size_t count = 0;
    for(int ii=0; ii<n_Uquad; ii++)
    {
        for(int jj=0; jj<n_Vquad; jj++)
        {
            qpoints[2*count] = 0.5*( 1+uQpoint[ii] );
            qpoints[2*count+1] = 0.5*( 1 + vQpoint[jj]);
            qweights[count] = 0.25 * uQweight[ii] * vQweight[jj];
            count++;
        }
    }
}

