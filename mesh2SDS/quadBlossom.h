#include <vector>


void quadBlossom(std::vector<float> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, bool visualize);
void quadBlossom(std::vector<double> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, bool visualize);