#include <cassert>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <time.h>
#include <utility>
#include <unordered_map>



#include "TypeDef.h"
#include "BBox.h"
#include "octree.h"
#include "Settings.h"
#include "IO.h"
#include "EigenTypeDef.h"
#include "sds.h"
#include "optimizationSubdivisionSurface.h"
#include "optimizeControlMesh.h"
#include "PointcloudOp.h"
#include "quadBlossom.h"
#include "triMeshDecimation.h"
#include "MeshOp.h"


int PoissonReconstruction( std::string inputFile , const std::vector<TypeReal> &points,  const std::vector<TypeReal> &normals, std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, int depth_value, TypeReal BBexp );







int main(int argc, char *argv[]) {
  
	
	
	Settings D;
    D.parseCommandLine(argc, argv);
    if(D.visualize>0){ D.printOptions();}
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    clock_t t0 = clock();
    
    
    std::vector<TypeReal> vertices, points, normals, point_vars, vars;
    std::vector<int> face_nvertex, face_vertex;
    std::vector<int> meshFace_nvertex, meshFace_vertex, empty;
    std::vector<TypeReal> center, dummy;
    size_t n_vertex, n_faces;
	
    // read input pointcloud
    std::string format = D.cloudFile.substr(D.cloudFile.size()-3,3);
    if( format.compare( std::string("ply"))==0 ){
    	read_ply( D.cloudFile, points, meshFace_nvertex, meshFace_vertex, vars, true);
    }else{
    	printf("unknown input file type, expecting .ply\n");
    	return(0);
    }
    size_t n_points = points.size()/3;  
    size_t n_meshFaces = meshFace_nvertex.size();
    size_t size_vars = vars.size()/n_points - 3;
    if( size_vars == 0 )
    {
    	normals = vars;
    }
    else if( size_vars > 0 )
    {
    	normals.resize(3*n_points);
    	//point_vars.resize(size_vars*n_points);
    	point_vars.resize(3*n_points);
    	for(int ii=0; ii<n_points; ii++)
    	{
    		for(int idim=0; idim<3; idim++)
    		{
    			normals[ii*3+idim] = vars[ (size_vars+3)*ii + idim ];
    		}
    		/*for(int idim=0; idim<size_vars; idim++)
    		{
    			point_vars[ size_vars*ii + idim ] = vars[ (size_vars+3)*ii + 3 + idim ];
    		}*/
    		for(int idim=0; idim<3; idim++)
    		{
    			point_vars[ ii + idim*n_points ] = vars[ (size_vars+3)*ii + 3 + idim ];
    		}
    	}
    	vars.clear();
    	size_vars = 3;
    }
    else{ printf("missing normals\n");}
    printf("%lu points, normals, and %lu-dimensional vars\n", n_points, size_vars);
    
    
    
    // analize pointcloud
    PointcloudOp pointCloudOp(points, 0);
    
    // estimate normals
    if( normals.size()==0 )
    { 
    	if(D.visualize>0){ printf("estimating consistently-oriented normals...");}
    	pointCloudOp.getNormals( normals );
    	if(D.visualize>0){ printf("done\n");}
    	std::string outfilename(D.outFile);
    	outfilename.replace( outfilename.rfind('.'),  4, std::string("_pointcloud_normals.ply") );
    	std::vector< std::string > var_names(3);
    	var_names[0] = std::string("nx"); var_names[1] = std::string("ny"); var_names[2] = std::string("nz");
    	write_ply_mesh(outfilename, points, empty, empty, normals, var_names);
    }
    
    
    
             
    // create octree from pointcloud
    if(D.visualize>0){  printf("constructing octree ..."); }
    int octreeLevel0 = D.octreeLevel - D.uniR; 
	Octree tree( points, octreeLevel0, D.BBexp, false );
	if(D.visualize>0){ printf("done\n");}

	
	
	
    // compute scale
    BoundingBox BBox = tree.getBBox();
    TypeReal min_box[3] = { tree.getBBoxMin(0), tree.getBBoxMin(1), tree.getBBoxMin(2) };
    TypeReal max_box[3] = { tree.getBBoxMax(0), tree.getBBoxMax(1), tree.getBBoxMax(2) };
	TypeReal maxBBsize = std::max( std::max( tree.getBBoxSide(0), tree.getBBoxSide(1) ), tree.getBBoxSide(2) ) ;
	TypeReal scale = std::pow( tree.getBBoxSide(0) * tree.getBBoxSide(1) * tree.getBBoxSide(2), 1.0/3 );
	D.beta /= scale;  
	std::vector<TypeReal> weights(n_points, 1.0);



    


   	// read initial control mesh or fit a mesh with Poisson Surface Reconstruction Method
   	std::vector<TypeReal> boundary_points;
   	MeshOp meshOp;
   	if( D.iniFile.size()>0)
   	{
   		read_ply( D.iniFile, vertices, face_nvertex, face_vertex, dummy, true); 
   		meshOp = MeshOp(vertices, face_nvertex, face_vertex);
   		sample_boundary(vertices, face_nvertex, face_vertex, boundary_points);
		// EDGE COLLAPSE TO DESIRED NUMBER OF VERTICES
   		if( vertices.size () > D.n_controlPoints*3 )
   		{
   			std::vector<TypeReal> vertices0(vertices);
   			std::vector<int> face_nvertex0( face_nvertex);
   			std::vector<int> face_vertex0(face_vertex);
   			triMeshDecimation( vertices0, face_nvertex0, face_vertex0, D.n_controlPoints, vertices, face_nvertex, face_vertex, D.visualize>0);
   			vertices0.clear(); face_nvertex0.clear(); face_vertex0.clear();
   		}
   	}
   	else
   	{
		std::vector<TypeReal> vertices0(vertices);
		std::vector<int> face_nvertex0( face_nvertex);
		std::vector<int> face_vertex0(face_vertex);
   		// estimate an initial mesh from Poisson reconstruction and decimate it to the desired number of control points
   		PoissonReconstruction( D.cloudFile , points, normals, vertices0, face_nvertex0, face_vertex0, D.octreeLevel, D.BBexp );
   		/*std::string outfilename(D.outFile);
    		outfilename.replace( outfilename.rfind('.'),  4, std::string("_initialization.ply") );
    		write_ply_mesh(outfilename, vertices0, face_nvertex0, face_vertex0 );*/
    	
		// EDGE COLLAPSE TO DESIRED NUMBER OF VERTICES
	    	snap_to_bbox(vertices0, face_nvertex0, face_vertex0, BBox); 	
	    	meshOp = MeshOp(vertices0, face_nvertex0, face_vertex0);
	    	triMeshDecimation( vertices0, face_nvertex0, face_vertex0, D.n_controlPoints-1, vertices, face_nvertex, face_vertex, D.visualize>0);
	   	snap_to_points(vertices, face_nvertex, face_vertex, points);
	    	vertices0.clear(); face_nvertex0.clear(); face_vertex0.clear();
   	}


	// PERFECT MATCHING
	// find boundaries to ensure that we can find a perfect match
	std::vector<int> boundary_faces;
	clean_boundary( vertices, face_nvertex, face_vertex, boundary_faces, D.visualize>0 );
	if( boundary_faces.size()%2  >  0  )
	{ // split a boundary face to have an even number of triangles to match
		int iface = boundary_faces[0];
		int v0 = face_vertex[3*iface];
		int v1 = face_vertex[3*iface+1];
		int v2 = face_vertex[3*iface+2];
		int vnew = vertices.size()/3;
		for(int idim=0; idim<3; idim++)
		{
			vertices.push_back( 0.5*(vertices[ v0*3 +idim] + vertices[ v2*3 +idim] )  );
		}
		face_nvertex.push_back(3);
		face_vertex[ iface*3+2 ] = vnew;
		face_vertex.push_back( vnew );
		face_vertex.push_back( v1 );
		face_vertex.push_back( v2 );
		printf("added a boundary face to allow for a perfect match\n");
	}
	quadBlossom(vertices, face_nvertex, face_vertex, D.visualize>0);
	/*{
		std::string outfilename(D.outFile);
    		outfilename.replace( outfilename.rfind('.'),  4, std::string("_SDS_initialization.obj") );
    		//UniformRefineSurface(vertices, face_nvertex, face_vertex, 3);
    		write_mesh( outfilename, vertices, face_nvertex, face_vertex);    
   	}*/
	remove_area0Faces( vertices, face_nvertex, face_vertex);
	remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	clean_mesh( vertices, face_nvertex, face_vertex );


	// detect boundaries to define constraints
	std::vector<int> boundary_vertices;
	detect_boundary( vertices, face_nvertex, face_vertex, boundary_vertices);



	n_vertex = vertices.size()/3;
	if(D.visualize>0){printf("mesh initialized with %lu vertices and %lu faces\n", n_vertex, face_nvertex.size() );}
	if( D.uniR < 0 ){ D.uniR = std::ceil( std::log( n_points*1.0/n_vertex )/ std::log( 4.0 ) ); if(D.visualize>0){ printf("set uniR to %lu\n", D.uniR);} }

	
	
	if(D.visualize>0){ printf(" --------------- FITTING SUBDIVISION SURFACE with %lu vertices----------------------\n", vertices.size()/3);}
	// first fit the mesh from implicit reconstruction
	{		 
		std::vector<TypeReal> meshNormals, meshPoints;
		meshOp.getNormals( meshNormals );
		meshOp.getVertices( meshPoints );
		
		TypeReal settings_randomize = D.randomization;
		D.randomization = 1.0;
		if( D.alg == 0 )
		{
			Distance_MM( vertices, face_nvertex, face_vertex, D, meshPoints, meshNormals, boundary_vertices);
			
		}else if( D.alg==1 )
		{
			sqrDistance_1storderApprox( vertices, face_nvertex, face_vertex, D, meshPoints, meshNormals);	
		}else
		{
			//lpOptimization_lBFGS( vertices, face_nvertex, face_vertex, D, points, normals);	
			LeastSquaresOptimization_LM( vertices, face_nvertex, face_vertex, D, meshPoints, meshNormals);	
		}	
		D.randomization = settings_randomize;
	}

	
	
	
    TypeReal energy=-1;
    if((D.visualize>0)&&(D.uniR>0)){  printf(" --------------- MULTIRESOLUTION FIT TO POINTS----------------------\n"); }
	
	
	// optimize distance approximation
	for(int ii=0; ii<D.uniR; ii++)
	{		
		std::vector<TypeReal> centroid0s, centroid0_weights, centroid0_normals;
		tree.averagePerLeaf( points, centroid0s, centroid0_weights );
		tree.averagePerLeaf( normals, centroid0_normals, centroid0_weights );
		for(int ii=0; ii<centroid0_normals.size()/3; ii++)
		{
			TypeReal norm = 0;
			for(int idim=0; idim<3; idim++)
			{
				norm += std::pow( centroid0_normals[ii*3+idim], 2);
			}
			norm += std::sqrt(norm);
			if( norm> eps )
			{
				for(int idim=0; idim<3; idim++)
				{
					centroid0_normals[ii*3+idim] /= norm;
				}
			}
		}   

		if( D.alg == 0 )
		{
			Distance_MM( vertices, face_nvertex, face_vertex, D, centroid0s, centroid0_normals, boundary_vertices);
		}
		else if( D.alg == 1)
		{
			sqrDistance_1storderApprox( vertices, face_nvertex, face_vertex, D, centroid0s, centroid0_normals);
		}
		else{
			LeastSquaresOptimization_LM( vertices, face_nvertex, face_vertex, D, centroid0s, centroid0_normals );
		}

		
		tree.refineOctree(1);
		D.epsilon /= 10;
	}
	
	
	
	std::vector<TypeReal> sds_vars(n_vertex * size_vars, 0);
		
	if(D.visualize>0){ printf(" --------------- FITTING SUBDIVISION SURFACE TO POINTS with %lu vertices----------------------\n", vertices.size()/3);}
	
	if( D.alg == 0 )
	{
		if(D.lp>= 1.0)
		{
			if( (size_vars>0)  )
			{
				energy = ColorDistance_MM( vertices, face_nvertex, face_vertex, sds_vars, D, points, normals, point_vars, boundary_vertices);
			}else
			{
				energy = Distance_MM( vertices, face_nvertex, face_vertex, D, points, normals, boundary_vertices);
			}
		}
		else
		{
			energy = Distance_trimmedLSQ( vertices, face_nvertex, face_vertex, D, points, normals, weights);
		}
	}else if( D.alg==1 )
	{
		energy = sqrDistance_1storderApprox( vertices, face_nvertex, face_vertex, D, points, normals);	
	}else
	{
		//lpOptimization_lBFGS( vertices, face_nvertex, face_vertex, D, points, normals);
		if( size_vars>=3 )
		{
			energy = LeastSquaresOptimization_LM( vertices, face_nvertex, face_vertex, sds_vars, D, points, normals, point_vars);
		}else
		{
			energy = LeastSquaresOptimization_LM( vertices, face_nvertex, face_vertex, D, points, normals);	
		}
	}
	if( (size_vars>0) && (D.delta>0) ){  energy = SDScolorOptimization_LM( vertices, face_nvertex, face_vertex, sds_vars, D, points, point_vars ); }

		
	
   	
    
	
	
	
	
	
	
	
	
	
	
	
	
	
    
    
   	clock_t t1 = clock();
	printf("energy %f time %f\n", energy/D.randomization, (double)(t1-t0) / ((double)CLOCKS_PER_SEC));
    
    
    // output results
    if(D.visualize>0){ printf("writing out control mesh its an approximated limit surface by 3-level subdivision...");}
    std::string outfilename(D.outFile);
    
    std::vector< std::string > var_names( size_vars );
    if(size_vars>= 3){ var_names[0] = 'r'; var_names[1] = 'g'; var_names[2] = 'b'; }
    if(size_vars==4){ var_names[3] ='alpha'; }
    write_ply_mesh( D.outFile, vertices, face_nvertex, face_vertex, sds_vars, var_names);
    
    outfilename.assign(D.outFile);
    outfilename.replace( outfilename.rfind('.'),  4, std::string("_refinedControlMesh3.obj") );
    {
    	if( (size_vars==3) || (size_vars==4) )
    	{
    		UniformRefineSurface(vertices, face_nvertex, face_vertex, sds_vars, 3);
    		write_obj_coloredmesh(  outfilename, vertices, face_nvertex, face_vertex, sds_vars);
    	}
    	else
    	{
    		UniformRefineSurface(vertices, face_nvertex, face_vertex, 3);
    		write_mesh( outfilename, vertices, face_nvertex, face_vertex);
    	}
    	   	
    }    
    if(D.visualize>0){ printf("done.\n");}
    
    
    return 0;
}



