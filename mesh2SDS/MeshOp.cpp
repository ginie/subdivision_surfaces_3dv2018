#include <list>
#include <fstream>
#include <vector>

#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "MeshOp.h"





MeshOp::MeshOp(std::vector<TypeReal> _vertices, std::vector<int> _face_nvertex, std::vector<int> _face_vertex)
{
	// initialize vertices, face_nvertex, face_vertex
	vertices =_vertices;
	face_nvertex = _face_nvertex;
	face_vertex = _face_vertex;
	size_t n_points = vertices.size()/3;
	
	
	int n_ring = 1;
	std::vector<int> vertex_nNeigh, vertex_startNeigh, vertex_neigh;
	meshNeighbourhoods( face_nvertex, face_vertex, n_ring, vertex_neigh, vertex_nNeigh, vertex_startNeigh );
	
		
	principalK1.resize(n_points);
	principalD1.resize(n_points*3);
	principalK2.resize(n_points);
	principalD2.resize(n_points*3);
	principalD3.resize(n_points*3);// normal direction
	confidence.resize(n_points);
	std::vector<TypeReal> condNumber(n_points);
	for(int ipt=0; ipt<n_points; ipt++)
	{
		// for each point, use the vertex and its neighbourhood to estimate the geometry of the surface
		int n_neigh = vertex_nNeigh[ipt] + 1; 
		std::vector<size_t> closestP( n_neigh );
		closestP[0] = ipt;
		int k0 = vertex_startNeigh[ipt];
		for( int k=1; k<n_neigh; k++)
		{
			closestP[k] =  vertex_neigh[ k0 + k - 1];
		}
		
		// find the centroid of each cluster
		TypeReal centroid[3] = {0,0,0};
		Eigen::MatrixXf neigh_points( n_neigh, 3);
		Eigen::MatrixXf centered_points( n_neigh, 3);
		//printf("closests points to %f,%f,%f are:", points[ipt*3], points[ipt*3+1], points[ipt*3+2]);
		for( int k=0; k<n_neigh; k++)
		{
			for(int idim=0; idim<3; idim++)
			{
				neigh_points(k,idim) = vertices[ closestP[k]*3+idim ];
				centroid[idim] += neigh_points(k,idim);
			}
			//printf("\t %f %f %f at distance %f\n", points[ closestP[k]*3+0 ], points[ closestP[k]*3+1 ], points[ closestP[k]*3+2 ], distance2P[k] );
		}
		for(int idim=0; idim<3; idim++){ centroid[idim] /= n_neigh; }	
		

		// computer the covariance matrix
		Eigen::Matrix3f cov =  Eigen::Matrix3f::Zero();
		for( int k=0; k<n_neigh; k++)
		{
			Eigen::Vector3f aux;
			for(int idim=0; idim<3; idim++)
			{
				aux[idim] = (centroid[idim] - neigh_points(k,idim) );
				centered_points(k,idim) = neigh_points(k,idim) - centroid[idim];
			}
			cov += aux *(aux.transpose());
		}    
    
		// find principal components
		Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigensolver(cov);
		if (eigensolver.info() != Eigen::Success) abort();
		Eigen::Vector3f eigVals = eigensolver.eigenvalues();
		// sort eigenValues
		std::vector<int> map(3);
		for(int idim=0; idim<3; idim++){map[idim] = idim;}
		std::sort( map.begin(), map.end(), [&](int x,int y){ return eigVals[x]<eigVals[y];} );
		//matrix whose columns are the eigenvectors.
        Eigen::Matrix3f eigVects = eigensolver.eigenvectors();
        for(int idim=0; idim<3; idim++)
		{
			principalD1[ipt*3+idim] = eigVects(idim,map[2]);
			principalD2[ipt*3+idim] = eigVects(idim,map[1]);
		}
		principalD3[ipt*3] = principalD1[ipt*3+1]*principalD2[ipt*3+2] - principalD1[ipt*3+2]*principalD2[ipt*3+1];
		principalD3[ipt*3+1] = principalD1[ipt*3+2]*principalD2[ipt*3+0] - principalD1[ipt*3+0]*principalD2[ipt*3+2];
		principalD3[ipt*3+2] = principalD1[ipt*3+0]*principalD2[ipt*3+1] - principalD1[ipt*3+1]*principalD2[ipt*3+0];
		
		
    	// project points and fit a parabola
    	Eigen::MatrixXf neigh_projection = centered_points * eigVects;
    	TypeReal A11 = 0;
    	TypeReal A12 = 0;
    	TypeReal A22 = 0;
    	TypeReal b1 = 0; TypeReal b2 = 0;
    	for( int k=0; k<n_neigh; k++)
		{
			TypeReal x = neigh_projection(k,map[2]);
			TypeReal y = neigh_projection(k,map[1]);
			TypeReal z = neigh_projection(k,map[0]);			
			A11 += std::pow(x,4);
			A12 += std::pow(x*y,2);
			A22 += std::pow(y,4);
			b1 += std::pow(x,2) * z;
			b2 += std::pow(y,2) * z;
		} 
		
		// estimate principal curvatures solving system A*k = b
		TypeReal idetA = 1.0/( A11*A22 - std::pow(A12,2) );
    	principalK1[ipt] = 2*std::abs( idetA* ( A22* b1 - A12*b2) );
    	principalK2[ipt] = 2*std::abs( idetA* ( -A12* b1 + A11*b2) );
		
		// get confidence measures
		condNumber[ipt] = 1.0;
		confidence[ipt] = 1.0;
    	
    	if( std::isnan(principalK1[ipt]) || std::isnan(principalK2[ipt]) || std::isnan(principalD1[ipt*3]) || std::isnan(principalD1[ipt*3+1]) || std::isnan(principalD1[ipt*3+2]) || std::isnan(principalD2[ipt*3]) || std::isnan(principalD2[ipt*3+1]) || std::isnan(principalD2[ipt*3+2])|| std::isnan(principalD3[ipt*3]) || std::isnan(principalD3[ipt*3+1]) || std::isnan(principalD3[ipt*3+2]) || std::isinf(principalK1[ipt]) || std::isinf(principalK2[ipt]) || std::isinf(principalD1[ipt*3]) || std::isinf(principalD1[ipt*3+1]) || std::isinf(principalD1[ipt*3+2]) || std::isinf(principalD2[ipt*3]) || std::isinf(principalD2[ipt*3+1]) || std::isinf(principalD2[ipt*3+2]) || std::isinf(principalD3[ipt*3]) || std::isinf(principalD3[ipt*3+1]) || std::isinf(principalD3[ipt*3+2]) )
    	{
    		//printf("nan in Jet point %ipt k1 = %f, d1 = %f %f %f, k2= %f, d2 = %f %f %f\n", principalK1[ipt], principalD1[ipt*3], principalD1[ipt*3+1], principalD1[ipt*3+2], principalK2[ipt], principalD2[ipt*3], principalD2[ipt*3+1], principalD2[ipt*3+2] );
    		principalK1[ipt] = 0;
    		principalD1[ipt*3] = 0;
    		principalD1[ipt*3+1] = 0;
    		principalD1[ipt*3+2] = 0;
    		principalK2[ipt] = 0;
    		principalD2[ipt*3] = 0;
    		principalD2[ipt*3+1] = 0;
    		principalD2[ipt*3+2] = 0;
    		principalD3[ipt*3] = 0;
    		principalD3[ipt*3+1] = 0;
    		principalD3[ipt*3+2] = 0;    		
    		confidence[ipt] = 0;
    		condNumber[ipt] = 1.0;
    	}
    }	
}


/*MeshOp::MeshOp(std::vector<TypeReal> _vertices, std::vector<int> _face_nvertex, std::vector<int> _face_vertex)
{
	// initialize vertices, face_nvertex, face_vertex
	vertices =_vertices;
	face_nvertex = _face_nvertex;
	face_vertex = _face_vertex;
	size_t n_points = vertices.size()/3;
	
	
	
	// estimate geometry
	typedef double                   				 DFT;
	typedef CGAL::Simple_cartesian<DFT>     		 Data_Kernel;
	typedef Data_Kernel::Point_3    		 		 DPoint;
	typedef Data_Kernel::Vector_3     				 DVector;
	typedef CGAL::Monge_via_jet_fitting<Data_Kernel> My_Monge_via_jet_fitting;
	typedef My_Monge_via_jet_fitting::Monge_form     My_Monge_form;
	
	
	int n_ring = 1;
	std::vector<int> vertex_nNeigh, vertex_startNeigh, vertex_neigh;
	meshNeighbourhoods( face_nvertex, face_vertex, n_ring, vertex_neigh, vertex_nNeigh, vertex_startNeigh );
	
	
	// default parameter values and global variables
	unsigned int d_fitting = 2;
	unsigned int d_monge = 2;
	
	
	
	principalK1.resize(n_points);
	principalD1.resize(n_points*3);
	principalK2.resize(n_points);
	principalD2.resize(n_points*3);
	principalD3.resize(n_points*3);// normal direction
	confidence.resize(n_points);
	std::vector<TypeReal> condNumber(n_points);
	for(int ipt=0; ipt<n_points; ipt++)
	{
		// for each point, use the vertex and its neighbourhood to estimate the geometry of the surface
		int n_neigh = vertex_nNeigh[ipt] + 1; //std::max( std::min( vertex_nNeigh[ipt] + 1, 13), 7 );
		//printf("%d neighbours\n", n_neigh);
		//container for data points used in the estimation of Monge form
		std::vector<DPoint> in_points(n_neigh);  
		in_points[0] = DPoint(vertices[ ipt*3 ], vertices[ ipt*3 + 1 ], vertices[ ipt*3 + 2 ] );
		//DVector point_normal = Dvector( normals[ipt*3], normals[ipt*3+1], normals[ipt*3+2] ); 
		int k0 = vertex_startNeigh[ipt];
		
		for( int k=0; k<vertex_nNeigh[ipt]; k++)
		{
			int idpoint =  vertex_neigh[ k0 + k];
			in_points[k+1] = DPoint(vertices[ idpoint*3 ], vertices[ idpoint*3 + 1 ], vertices[ idpoint*3 + 2 ] );
		}		
		
	
		// perform the fitting
		My_Monge_form monge_form;
		My_Monge_via_jet_fitting monge_fit;
		monge_form = monge_fit(in_points.begin(), in_points.end(), d_fitting, d_monge);
		//switch min-max ppal curv/dir wrt the mesh orientation
		//monge_form.comply_wrt_given_normal(point_normal);
	
		
		DVector normal = monge_form.normal_direction();
		DVector prin1Dir = monge_form.maximal_principal_direction();
		DVector prin2Dir = monge_form.minimal_principal_direction();
		for(int idim=0; idim<3; idim++)
		{
			principalD1[ipt*3+idim] = prin1Dir[idim];
			principalD2[ipt*3+idim] = prin2Dir[idim];
			principalD3[ipt*3+idim] = normal[idim];
		}
		principalK2[ipt] = monge_form.principal_curvatures(1);
		principalK1[ipt] = monge_form.principal_curvatures(0);
		
		// get confidence measures
		condNumber[ipt] = monge_fit.condition_number();
		std::vector<DVector> PCAvectors(3);
		std::vector<DFT> PCAvalues(3);
		for(int idim=0; idim<3; idim++)
		{
			std::pair<DFT,DVector> obj;
			obj = monge_fit.pca_basis(idim);
			PCAvectors[idim] = obj.second;
			PCAvalues[idim] = obj.first;
		}
		// compute normal vector to the plane defined by the two principal components
		DFT normN = 0;
		DFT norm3 = 0;
		DFT ip = 0;
		for(int idim=0; idim<3; idim++)
		{
			int iplus = (idim+1)%3;
			int iminus = (idim-1+3)%3;
			DFT PCAnormal = PCAvectors[0][iplus] * PCAvectors[1][iminus] - PCAvectors[0][iminus] * PCAvectors[1][iplus];
			normN += std::pow(PCAnormal, 2);
			norm3 +=std::pow( PCAvectors[2][idim], 2 );
			ip += PCAvectors[2][idim] * PCAnormal;
		}
		confidence[ipt] = std::abs( ip/std::sqrt( norm3 * normN));
		if( std::isnan( confidence[ipt] )|| std::isinf( confidence[ipt] )  ){ confidence[ipt] = 0; }
		if( std::isnan( condNumber[ipt] )|| std::isinf( condNumber[ipt] )  ){ condNumber[ipt] = 1.0;  confidence[ipt] = 0; }
    	
    	if( std::isnan(principalK1[ipt]) || std::isnan(principalK2[ipt]) || std::isnan(principalD1[ipt*3]) || std::isnan(principalD1[ipt*3+1]) || std::isnan(principalD1[ipt*3+2]) || std::isnan(principalD2[ipt*3]) || std::isnan(principalD2[ipt*3+1]) || std::isnan(principalD2[ipt*3+2])|| std::isnan(principalD3[ipt*3]) || std::isnan(principalD3[ipt*3+1]) || std::isnan(principalD3[ipt*3+2]) || std::isinf(principalK1[ipt]) || std::isinf(principalK2[ipt]) || std::isinf(principalD1[ipt*3]) || std::isinf(principalD1[ipt*3+1]) || std::isinf(principalD1[ipt*3+2]) || std::isinf(principalD2[ipt*3]) || std::isinf(principalD2[ipt*3+1]) || std::isinf(principalD2[ipt*3+2]) || std::isinf(principalD3[ipt*3]) || std::isinf(principalD3[ipt*3+1]) || std::isinf(principalD3[ipt*3+2]) )
    	{
    		//printf("nan in Jet point %ipt k1 = %f, d1 = %f %f %f, k2= %f, d2 = %f %f %f\n", principalK1[ipt], principalD1[ipt*3], principalD1[ipt*3+1], principalD1[ipt*3+2], principalK2[ipt], principalD2[ipt*3], principalD2[ipt*3+1], principalD2[ipt*3+2] );
    		principalK1[ipt] = 0;
    		principalD1[ipt*3] = 0;
    		principalD1[ipt*3+1] = 0;
    		principalD1[ipt*3+2] = 0;
    		principalK2[ipt] = 0;
    		principalD2[ipt*3] = 0;
    		principalD2[ipt*3+1] = 0;
    		principalD2[ipt*3+2] = 0;
    		principalD3[ipt*3] = 0;
    		principalD3[ipt*3+1] = 0;
    		principalD3[ipt*3+2] = 0;    		
    		confidence[ipt] = 0;
    		condNumber[ipt] = 1.0;
    	}
    }	
    
    // normalize condition number and include it in confidence measure
	TypeReal minCN = std::numeric_limits<TypeReal>::max();
	TypeReal maxCN = std::numeric_limits<TypeReal>::min();
	for(int ii=0; ii<n_points; ii++)
	{
		if( minCN > condNumber[ii] ){ minCN = condNumber[ii]; }
		else if( maxCN < condNumber[ii] ){ maxCN = condNumber[ii]; }
	}
	TypeReal irange = maxCN> minCN? 1.0/( maxCN - minCN ): 1.0;
	for(int ii=0; ii<n_points; ii++)
	{
		TypeReal cond =  ( condNumber[ii]- minCN )*irange;
		confidence[ii] *= 1.0/( 1.0 + cond );
	}


}*/


void MeshOp::meshVertexIncidence( std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int>& faceStart, std::vector<int> &vertex_face, std::vector<int> &vertex_nface, std::vector<int> &vertex_startFace  )
{
	size_t n_points = *std::max_element(face_vertex.begin(),face_vertex.end()); 
	n_points += 1;
	
	// find the first-ring neighbours of a vertex
	int n_faces = face_nvertex.size();
	// create a data structure to store the faces incident to each edge
	vertex_nface = std::vector<int>( n_points, 0);
	faceStart = std::vector<int>(n_faces,0);
	int count = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		faceStart[ii] = count;
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			vertex_nface[ face_vertex[count] ] += 1;
			count += 1;
		}
	}
	count = 0;
	std::vector<int> vertexStart(n_points,0);
	for(int jj=0; jj< n_points; jj++)
	{ 
		vertexStart[jj] = count;
		count += vertex_nface[jj];
	}
	vertex_startFace = vertexStart;
	vertex_face = std::vector<int>(count);
	count = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			int iv = face_vertex[count];
			vertex_face[ vertexStart[iv] ] = ii;
			vertexStart[iv] += 1;
			count += 1;
		}
	}	
	
}


void MeshOp::meshNeighbourhoods( std::vector<int> &face_nvertex, std::vector<int> &face_vertex, int n_ring, std::vector<int> &vertexNeigh, std::vector<int> &sizeNeigh, std::vector<int> &startNeigh  )
{
	size_t n_points = *std::max_element(face_vertex.begin(),face_vertex.end()); 
	n_points += 1;
	
	// get the faces incident to each edge
	int n_faces = face_nvertex.size();
	std::vector<int> vertex_nface, vertex_startFace, vertex_face, faceStart;
	meshVertexIncidence( face_nvertex, face_vertex, faceStart, vertex_face, vertex_nface, vertex_startFace  );
	
	// for each face incident to a vertex, add its vertices to the neighburhood of the current vertex
	vertexNeigh.clear();
	sizeNeigh = std::vector<int>(n_points);
	startNeigh = std::vector<int>(n_points);
	int count = 0;
	std::vector< std::vector<int> > vertex_neigh(n_points, std::vector<int>(0));
	std::vector<int> vertex_nNeigh(n_points,0);
	for(int jj=0; jj< n_points; jj++)
	{ 
		std::vector<int> ivertexNeigh;
		for(int ii=0; ii< vertex_nface[jj]; ii++)
		{
			int face = vertex_face[count];
			for(int kk=0; kk< face_nvertex[face]; kk++)
			{
				int iv = face_vertex[ faceStart[face] +kk];
				if( iv != jj ) // add all vertices that share a face with the vertex
				{
					ivertexNeigh.push_back( iv );//
				}
			}
			count += 1;
		}
		// remove duplicates
		std::unique(ivertexNeigh.begin(), ivertexNeigh.end() );
		if(n_ring>1)
		{
			vertex_neigh[jj] = ivertexNeigh;
			// set counter
			vertex_nNeigh[jj] = ivertexNeigh.size();
		}else
		{
			sizeNeigh[jj] = ivertexNeigh.size();
			startNeigh[jj] = vertexNeigh.size();
			vertexNeigh.insert(vertexNeigh.end(), ivertexNeigh.begin(), ivertexNeigh.end());
		}
	}
	if( n_ring>1)
	{
		std::vector< std::vector<int> > vertex_neigh0(vertex_neigh);
		std::vector<int> vertex_nNeigh0(vertex_nNeigh);
		// add second-ring neighbours
		for(int jj=0; jj< n_points; jj++)
		{ 
			std::vector<int> ivertexNeigh = vertex_neigh0[jj];
			// add neighbours of all the first ring vertices
			for(int ii=0; ii< vertex_nNeigh0[jj]; ii++)
			{
				int firstringvertex = ivertexNeigh[ii];
				for(int kk=0; kk< vertex_nNeigh0[firstringvertex]; kk++)
				{
					int iv = vertex_neigh0[firstringvertex][kk];
					if( iv != jj )
					{
						ivertexNeigh.push_back( iv );
					}
					
				}
			}
			// remove duplicates
			std::unique(ivertexNeigh.begin(), ivertexNeigh.end() );
			sizeNeigh[jj] = ivertexNeigh.size();
			startNeigh[jj] = vertexNeigh.size();
			vertexNeigh.insert(vertexNeigh.end(), ivertexNeigh.begin(), ivertexNeigh.end());
		}
	}
	
}

