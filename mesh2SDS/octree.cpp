//
//  octree.cpp
//  
//
//  Created by virginia estellers on 1/3/17.
//
//


#include "octree.h"


/* Linear Octrees store a unique index called locational code in each node. Additionally, all Octree nodes are stored in a hash map which allows directly accessing any node based on its locational code. The locational code is constructed in such a way, that deriving the locational codes for any node’s parent and children based on its own locational code is feasible and fast. To avoid unnecessary hash map look-ups for children which don’t exist, the node struct can be extended by a bit-mask indicating which children have been allocated and which haven’t*/


//------------------------------------------------------------------------ constructor

Octree::Octree( std::vector<TypeReal> &pointcoords, size_t depth, TypeReal BBoxmargins=0.4, bool balance_tree=false )
{
    if(depth>MAXDEPTH)
    {
        //printf("maximum allowed octree depth %d\n", MAXDEPTH);
        depth = MAXDEPTH;
    }
    // set the bounding box of the octree
    BoundingBox BBox();
    adjust_bbox( pointcoords, BBoxmargins );
    // set the depth of the tree
    setDepth(depth);
    vertexCoordinates = std::vector<TypeReal>(0);
    Nodes = std::unordered_map<uint64_t, OctreeNode>();
    Vertices = std::unordered_map<uint64_t, size_t>();
    size_t n_leafs = initialize( pointcoords, depth, balance_tree );    
    compute_Vertex2Cell();
    compute_Cell2Vertex();
}

void Octree::balanceGrid( void ) // balance the tree and create datastructures that describe the mesh
{
	std::vector<uint64_t> leafCodes;
	for( auto it = Nodes.begin(); it != Nodes.end(); ++it )
	{
		OctreeNode node = it->second;
		if( node.isLeaf == true) { leafCodes.push_back( it->first ); }
	}
	size_t n_splits = balance( leafCodes );
	// creating leafMap
    createLeafMap();
    // creating mesh structures
	//printf("octree balanced\n");
    compute_Vertex2Cell();
    compute_Cell2Vertex();
}

// initialize octree
void Octree::adjust_bbox( const std::vector<TypeReal> &pointcoords, TypeReal expand= 0.1 )
{
	int n_points = pointcoords.size()/3;
	TypeReal mins[3];
	TypeReal maxs[3];
	TypeReal ranges[3];
	for(size_t idim=0; idim<3; idim++)
	{
		TypeReal min = pointcoords[idim];
		TypeReal max = pointcoords[idim];
		for(int ii=0; ii<n_points; ii++)
		{
			min = pointcoords[ii*3+idim] < min ? pointcoords[ii*3+idim] : min;
			max = pointcoords[ii*3+idim] > max ? pointcoords[ii*3+idim] : max;
		}
		mins[idim] = min;
		maxs[idim] = max;
		ranges[idim] = max - min;
	}
	
	TypeReal range = std::min(ranges[0], std::min(ranges[1], ranges[2] ));
	for(size_t idim=0; idim<3; idim++)
	{
		BBox.setMax(idim, maxs[idim] + 0.5*expand*range );
		BBox.setMin(idim, mins[idim] - 0.5*expand*range );
	}
}


size_t Octree::initialize( std::vector<TypeReal> &pointcoords, size_t max_depth, bool balance_tree=false )
{
    
    // initialize the pointlist vector
    int n_points = pointcoords.size()/3;
    std::vector<uint64_t> locCodes( n_points );
    // to keep track of sorting the location codes
    pointIDs.resize(n_points);
    points.resize(n_points);
    for (int i=0; i< n_points; i++)
    {
        locCodes[i] = getPointMortonCode( &(pointcoords[i*3]), max_depth );
        points[i] = (TypePoint){ pointcoords[ i*3 ], pointcoords[ i*3 + 1 ], pointcoords[ i*3 + 2] };
        pointIDs[i] = i;
    }
    //printf("got morton codes of points\n");
    // sort the location codes and keep in map the argsort
    std::sort( pointIDs.begin(), pointIDs.end(), [&](size_t x, size_t y){return locCodes[x]<locCodes[y];} );
    std::vector<uint64_t> locCodes0( locCodes );
    for (int i=0; i< n_points; i++)
    {
        locCodes[i] = locCodes0[ pointIDs[i] ];       
    }   
    //printf("applied sorting to morton codes and created pointlist\n");
    
    // find uniques to determine the size of the octree
    uint64_t current = locCodes[0];
    size_t n_leafs = 1;
    std::vector<uint64_t> leafCodes(n_points,0);
    leafCodes[0] = current;
    // count how many points fall within a leaf
    std::vector< size_t > leafSizes(n_points,0);
    size_t counter = 0;
    for( int i=0; i<n_points; i++ )
    {
        if( current != locCodes[ i ] )
        {
            current = locCodes[ i ];
            leafCodes[n_leafs] = current;
            leafSizes[n_leafs-1] = counter;
            n_leafs += 1;
            counter = 1;
        }else
        {
        	counter += 1;
        }
    }
    leafSizes[n_leafs-1] = counter;
    leafSizes.resize(n_leafs);
    leafCodes.resize(n_leafs);
    
    // reserve space for hash tables
    int maxinnernodes = std::round( ((TypeReal)8*n_leafs )/7 );
    int max_leafs = 8*n_leafs;
    int max_nodes = max_leafs + max_leafs;
    Nodes.reserve( max_nodes  );
    Vertices.reserve( max_leafs );
    LeafMap.reserve( max_leafs );
    //printf("uniques found, allocating space for %d nodes, %d leafs, %d vertices\n", max_nodes, max_leafs, max_leafs);
        
    // create root node
    OctreeNode root;
    root.n_points = 0;
    root.point0 = 0;
    root.isLeaf = false;
    root.LocCode = (uint64_t) 1;
    for(int j=0; j<6;j++){ root.neighbours[j] = (uint64_t) 0; }
    createNodeVertices(root);
    createNode( root.LocCode, root);
    //printf("created root\n");
    
    // create nodes and leafs
    size_t point0 = 0;
    std::vector<uint64_t> inner_nodes(n_leafs*8);
    size_t count = 0;
    for( int i=0; i<n_leafs; i++ )
    {
       uint64_t leafCode = leafCodes[i];
       uint64_t parentCode = GetParentCode(leafCode);
       auto iter = Nodes.find(parentCode);
       while( (iter == Nodes.end()) && (parentCode>0)  )
       {
       	   // create inner node
       	   //std::cout << "creating parent node " << parentCode << "\n";
       	   OctreeNode node;
       	   node.n_points = 0;
       	   node.point0 = 0;
       	   node.isLeaf = false;
       	   node.LocCode = parentCode;
       	   //for(int j=0; j<6;j++){ node.neighbours[j] = (uint64_t) 0; }
       	   //createNodeVertices(node);
       	   createNode( parentCode, node);
       	   inner_nodes[count] = parentCode;
       	   count += 1;
       	   parentCode = GetParentCode(parentCode);
       	   iter = Nodes.find(parentCode);
       }
       // create leaf
       //std::cout << "creating leaf node " << leafCode << "\n";
       OctreeNode leaf;
       leaf.n_points = leafSizes[i];
       leaf.point0 = point0;
       leaf.isLeaf = true;
       leaf.LocCode = leafCode;
       for(int j=0; j<6;j++){ leaf.neighbours[j] = (uint64_t) 0; }
       createNodeVertices(leaf);  
       createNode( leafCode, leaf);
       point0 += leafSizes[i];
    }
    inner_nodes.resize(count+1);
    //printf("created ancestors of leafs\n");
    
    // create empty siblings of leafs to get a full grid
    for( int i=0; i<n_leafs; i++ )
    {
       uint64_t leafCode = leafCodes[i];
       uint64_t parentCode = GetParentCode(leafCode);
       for(int j=0; j<8; j++)
       {
       	   uint64_t childCode = GetChildCode(parentCode, j);
		   const auto iter = Nodes.find(childCode);
		   if( (iter == Nodes.end())  && (parentCode>0) )
		   {
			   // create empty leaf
			   //std::cout << "creating sibling for leaf node " << leafCode << "\n";
			   OctreeNode node;
			   node.n_points = 0;
			   node.point0 = 0;
			   node.isLeaf = true;
			   leafCodes.push_back( childCode );
			   node.LocCode = childCode;
			   for(int j=0; j<6;j++){ node.neighbours[j] = (uint64_t) 0; }
			   createNodeVertices(node);
			   createNode( childCode, node);
		   }
    	}
    } 
    // create empty siblings of inner nodes> root to get a full grid
    for( int i=0; i<inner_nodes.size(); i++ )
    {
       uint64_t leafCode = inner_nodes[i];
       uint64_t parentCode = GetParentCode(leafCode);
       for(int j=0; j<8; j++)
       {
       	   uint64_t childCode = GetChildCode(parentCode, j);
		   const auto iter = Nodes.find(childCode);
		   if( (iter == Nodes.end())  && (parentCode>0) )
		   {
			   // create empty leaf
			   //std::cout << "creating sibling for inner node " << leafCode << "\n";
			   OctreeNode node;
			   node.n_points = 0;
			   node.point0 = 0;
			   node.isLeaf = true;
			   leafCodes.push_back( childCode );
			   node.LocCode = childCode;
			   for(int j=0; j<6;j++){ node.neighbours[j] = (uint64_t) 0; }
			   createNodeVertices(node);
			   createNode( childCode, node);
		   }
    	}
    }
    //printf("filling the grid\n");
    
    // balancing the tree
    if(balance_tree==true)
    { 
    	size_t n_splits = balance( leafCodes );
    	/*size_t n_splits = 1;
    	while( n_splits >0 )
    	{
    		n_splits = balance( leafCodes );
    		leafCodes.clear();
    		for( auto it = Nodes.begin(); it != Nodes.end(); ++it )
    		{
    			OctreeNode node = it->second;
    			if( node.isLeaf == true) { leafCodes.push_back( it->first ); }
    		}
    	}
    	printf("octree balanced\n");*/
    }
    
    // creating leafMap
    createLeafMap();
    
    //printf("%d cells %d vertices in octree grid\n", LeafMap.size(), Leafs.size(), Vertices.size() );
    
    return n_leafs;

    
}


// -----------------------------------------------------------------------  private

// create entries in hash table and look for items in it

void Octree::createNode( uint64_t locationCode, OctreeNode child)
{
    Nodes.insert( std::make_pair( locationCode, child) );
}


void Octree::getNode(uint64_t locCode, OctreeNode &node)
{
    const auto iter = Nodes.find(locCode);
    if( iter == Nodes.end() )
    {
        //printf("node with location key %lu not found\n", locCode);
    }
    else{
        node = iter->second;
    }
}

size_t Octree::getLeafIndex( uint64_t locCode )
{
    const auto iter = LeafMap.find(locCode);
    if( iter == LeafMap.end() )
    {
        //printf("leaf with location key %lu not found\n", locCode);
        return -1;
    }
    else{
        return iter->second;
    }
}

size_t Octree::getVertexIndex( uint64_t locCode )
{
    const auto iter = Vertices.find(locCode);
    if( iter == Vertices.end() )
    {
        //printf("Vertex with location key %lu not found\n", locCode);
        return -1;
    }
    else{
        return iter->second;
    }
}
///--------------------------------------- convert to coordinates to Morton codes


// ----Morton code properties
// getting depth from location keys
size_t Octree::GetNodeTreeDepth(const OctreeNode &node)
{
	return GetNodeIDTreeDepth(node.LocCode);
}

size_t Octree::GetNodeIDTreeDepth(uint64_t nodeID)
{
    //size_t depth;
    //for (uint64_t lc=node->LocCode, depth=0; lc!=1; lc>>=3, depth++);
    //return depth;
#if defined(__GNUC__)
    return (63-__builtin_clzll(nodeID))/3;
#elif defined(_MSC_VER)
    unsigned long long int msb;
    _BitScanReverse(&msb, nodeID);
    return msb/3;
#endif
}




// from 3D coordinates to Morton code
uint64_t Octree::getPointMortonCode(TypeReal *pointcoords, size_t max_depth )
{
    std::bitset<64> bits(0);
    bits[ max_depth*3 ] = true;
    for( size_t idim=0; idim<3; idim++)
    {
        TypeReal BBmin = BBox.getMin(idim);
        TypeReal BBside = BBox.getSide(idim);
        TypeReal octreeScale = std::pow(2,max_depth);
        int center;
        int min = 0;
        int max = std::pow(2,max_depth);
        TypeReal x = (pointcoords[idim] - BBmin)/BBside * octreeScale;
        size_t indx = 3*max_depth - idim - 1;
        for (size_t idepth=0; idepth< max_depth; idepth++)
        {
            center = ( min + max )/2;
            if ( x >= center )
            {
                min = center;
                bits[ indx ] = true;
            }else
            {
                max = center;
                bits[ indx ] = false;
            }
            indx -= 3;
        }
    }
    return bits.to_ullong();
}



// from Morton code to 3D coords
void Octree::GetNodeCenter(const OctreeNode &node, TypePoint &CellCenter )
{
    std::bitset<64> bits(node.LocCode);
    size_t depth = GetNodeTreeDepth(node);
    if(bits[ depth*3 ] == true)
    {
        for( size_t idim=0; idim<3; idim++)
        {
			int center;
			int min = 0;
			int max = std::pow(2,depth+1);
            size_t indx = 3*depth - idim - 1;
            for (size_t idepth=0; idepth<depth; idepth++)
            {
                center = ( min + max )/2;
                if (bits[ indx ] == true)
                {
                    min = center;
                }else
                {
                    max = center;
                }
                indx -= 3;
            }
            TypeReal BBmin = BBox.getMin(idim);
            TypeReal BBside = BBox.getSide(idim);
			TypeReal octreeScale = std::pow(2,depth+1);
			TypeReal x = (TypeReal)( (min + max )/2);
            CellCenter.position[idim] = x*BBside/octreeScale + BBmin;
        }
    }
}



// to determine the Morton code of vertices we use a larger Bbox and an octree of depth+1, otherwise we cannot locate the vertices with any x=x_max, y=y_max, or z=z_max
// the 000 octant of the larger BBox corresponds to the original Bbox of the octree (this is why we go a level deeper)

// 3D to Morton code
uint64_t Octree::getVertexMortonCode(TypeReal *pointcoords )
{
    std::bitset<64> bits(0);
    size_t max_depth = MAXDEPTH + 1;
    bits[ max_depth*3 ] = true;
    for( size_t idim=0; idim<3; idim++)
    {
        TypeReal BBmin = BBox.getMin(idim);
        TypeReal BBside = BBox.getSide(idim);
        BBside *= 2;
        TypeReal octreeScale = std::pow(2,max_depth);
        int center;
        int min = 0;
        int max = std::pow(2,max_depth);
        TypeReal x = (pointcoords[idim] - BBmin)/BBside * octreeScale;
        size_t indx = 3*max_depth - idim - 1;
        for (size_t idepth=0; idepth< max_depth; idepth++)
        {
            center = ( min + max )/2;
            if ( x >= center )
            {
                min = center;
                bits[ indx ] = true;
            }else
            {
                max = center;
                bits[ indx ] = false;
            }
            indx -= 3;
        }
    }
    return bits.to_ullong();
}




// Morton Code to 3D
void Octree::GetVertexCoord(uint64_t LocCode, TypePoint &vertex )
{
    std::bitset<64> bits(LocCode);
    size_t depth = MAXDEPTH + 1;
    if(bits[ depth*3 ] == true)
    {
        for( size_t idim=0; idim<3; idim++)
        {
			int center;
			int min = 0;
			int max = std::pow(2,depth);
            size_t indx = 3*depth - idim - 1;
            for (size_t idepth=0; idepth<depth; idepth++)
            {
                center = ( min + max )/2;
                if (bits[ indx ] == true)
                {
                    min = center;
                }else
                {
                    max = center;
                }
                indx -= 3;
            }
            TypeReal BBmin = BBox.getMin(idim);
            TypeReal BBside = BBox.getSide(idim);
            BBside *= 2;
			TypeReal octreeScale = std::pow(2,depth);
			TypeReal x = (TypeReal)min;
            vertex.position[idim] = x*BBside/octreeScale + BBmin;
        }
    }    
}




//--------------------------------------------------------------- splitting nodes and updating pointlist

// find cell vertices and create an index to it; if necessary, create new grid vertices (new entries in hash table)
void Octree::createNodeVertices( OctreeNode &node )
{
    /*FILE * pFile;
    pFile = fopen ("vertexMap.txt","a");*/

    
    // get cell coordinates
    TypePoint CellCenter, HalfSides, cellCenter0;
    GetNodeCenter( node, CellCenter );
    size_t  depth = GetNodeTreeDepth( node );
    TypeReal cellSide = std::pow( 0.5, depth );
    for(int idim=0; idim<3; idim++)
    {
        TypeReal sideBBox = BBox.getSide(idim);
        HalfSides.position[idim] = 0.5 * cellSide * sideBBox;
    }
    
    TypeReal cube_corners[24] = { -1.0,-1.0,-1.0,   -1.0,-1.0,1.0,   -1.0,1.0,-1.0,   -1.0,1.0,1.0,   1.0,-1.0,-1.0,   1.0,-1.0,1.0,    1.0,1.0, -1.0,   1.0,1.0,1.0 };
    TypeReal vertexcoords[3];
    for(int i=0; i<8; i++)
    {
        for(int idim=0; idim<3; idim++)
        {
            vertexcoords[idim] = CellCenter.position[idim] + cube_corners[ i*3 + idim ] * HalfSides.position[idim];
        }
        uint64_t locCode = getVertexMortonCode( &(vertexcoords[0]) );
        node.nodeVertices[i] = locCode;
        const auto iter = Vertices.find(locCode);
        if( iter == Vertices.end() )
        {
            size_t sz = Vertices.size();
            Vertices.insert( std::make_pair( locCode, sz) );
            for(int idim=0; idim<3; idim++){ vertexCoordinates.push_back( vertexcoords[idim] ); }
            /*fprintf(pFile, "%g %g %g %d\n", vertexcoords[0], vertexcoords[1], vertexcoords[2], n_vertex);*/
        }
        
        /*TypePoint vertex, vertex0;
        GetVertexCoord(locCode, vertex );
        if( std::pow(vertexcoords[0] - vertex.position[0],2) + std::pow(vertexcoords[1] - vertex.position[1],2) + std::pow(vertexcoords[2] - vertex.position[2],2) > 1e-6 )
        {
            printf("%g %g %g TO %g %g %g\n", vertexcoords[0], vertexcoords[1], vertexcoords[2], vertex.position[0], vertex.position[1], vertex.position[2]);
        }*/
    }
    /*fclose(pFile);*/
}


void Octree::getVoxelVertices(const OctreeNode &node, std::vector<TypeReal> &vertexcoords )
{
    TypePoint CellCenter, HalfSides;
    GetNodeCenter( node, CellCenter );
    size_t depth = GetNodeTreeDepth( node );
    TypeReal cellSide = std::pow( 0.5, depth );
    for(int idim=0; idim<3; idim++)
    {
        TypeReal sideBBox = BBox.getSide(idim);
        HalfSides.position[idim] = 0.5 * cellSide * sideBBox;
    }
    
    TypeReal cube_corners[24] = { -1,-1,-1,   -1,-1,1,   -1,1,-1,   -1,1,1,   1,-1,-1,   1,-1,1,    1,1, -1,   1,1,1 };
    vertexcoords.resize(24);
    for(int i=0; i<8; i++)
    {
        for(int idim=0; idim<3; idim++)
        {
            vertexcoords[i*3+idim] = CellCenter.position[idim] + cube_corners[ i*3 + idim ] * HalfSides.position[idim];
        }
    }
}

// refine node
void Octree::splitNode(OctreeNode &node)
{
    // update node and hash table
    Nodes.erase(node.LocCode);
    node.isLeaf = false;
    Nodes.insert(std::make_pair( node.LocCode, node));
    
    
    // get cell coordinates
    TypePoint CellCenter;
    GetNodeCenter( node, CellCenter );
    size_t n_points = node.n_points;
    
    // assign points to the corresponding children
    std::vector<size_t> childCounter(8,0);
    std::vector<size_t> childAssignment(n_points);
    std::vector<size_t> pointIDs_copy(n_points);
    size_t powersOfTwo[3] = { 4, 2, 1 };
    for(int j=0; j<n_points; j++)
    {
        TypePoint point =  points[ pointIDs[node.point0  + j] ];
        size_t childID = 0;
        // find octant
        for(int idim=0; idim<3; idim++)
        {
            if( point.position[idim] >= CellCenter.position[idim] )
            {
                childID += powersOfTwo[idim];
            }
        }
        childCounter[childID] += 1;
        childAssignment[j] = childID;
        pointIDs_copy[ j ] = pointIDs[node.point0  + j];
    }
    
    // create children nodes with correct number of points
    TypeReal cum_volume = 0;
    size_t current_point0 = node.point0;
    std::vector<size_t> childPoint0(8);
    std::vector<TypePoint> childCenters(8);
    std::vector<TypePoint> childSides(8);
    for (int i=0; i<8; i++)
    {
        const uint64_t locCodeChild = (node.LocCode<<3)|i;
        OctreeNode child;
        child.isLeaf = true;
        child.LocCode = locCodeChild;
        child.point0 = current_point0;
        child.n_points = childCounter[i];
        for(int j=0; j<6;j++){ child.neighbours[j] = (uint64_t) 0; }
        // update point0 for child node
        childPoint0[i] = current_point0;
        current_point0 += childCounter[i];
        // create and index child vertices
        createNodeVertices(child);
        // update hash table
        Nodes.insert(std::make_pair( locCodeChild, child));
    }

    
    // reorder the point list to store the points in the child node into child.point0 + j index
    for(int j=0; j< n_points; j++)
    {
        size_t childID = childAssignment[j];
        size_t point_indx = childPoint0[childID];
        pointIDs[point_indx] = pointIDs_copy[j];
        childPoint0[childID] += 1;
    }
    
}







//----------------------------------------------- depth traversals

//  finding parent and childdren from location keys
uint64_t Octree::GetParentCode(uint64_t nodeCode )
{
    uint64_t parentCode = nodeCode>>3;
    return parentCode;
}
void Octree::GetParentNode(const OctreeNode &node, OctreeNode &parent)
{
    const uint64_t locCodeParent = GetParentCode(node.LocCode);
    getNode(locCodeParent, parent);
}

uint64_t Octree::GetChildCode(uint64_t nodeCode, int i)
{
    const uint64_t locCodeChild = (nodeCode<<3)|i;
    return locCodeChild;
}
bool Octree::getChildNode(const OctreeNode &node, int i, OctreeNode &child)
{
    const uint64_t locCodeChild = GetChildCode(node.LocCode, i);
    getNode(locCodeChild, child);
    return (node.isLeaf==false);
}



void Octree::depthTraversal_refineLeafs( OctreeNode &node, size_t max_depth, size_t min_points, size_t &splits )
{
    size_t depth = GetNodeTreeDepth(node);
    if( node.isLeaf==true )
    {
        if( node.n_points>min_points )
        {
            splitNode(node);
            splits += 1;
        }
    }
    else
    {
        if( depth<=max_depth ) // stop the traversal at depth maxdepth
        {
            for (int i=0; i<8; i++)
            {
                OctreeNode child;
                getChildNode( node, i, child );
                depthTraversal_refineLeafs(child, max_depth, min_points, splits);
            }
        }
    }
}



size_t Octree::getDepth(void)
{
    size_t n_leafs = Leafs.size();
    size_t max_depth = 0;
    for( int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf = Leafs[i];
        size_t depth = GetNodeTreeDepth( leaf );
        max_depth = depth>max_depth? depth : max_depth;
    }
	return max_depth;
}

void Octree::depthTraversal_createLeafMap( OctreeNode &node )
{
    if( node.isLeaf )
    {
        size_t sz = LeafMap.size();
        LeafMap.insert( std::make_pair( node.LocCode, sz) );
        Leafs.push_back( node );
    }
    else
    {
        for (int i=0; i<8; i++)
        {
            OctreeNode child;
            getChildNode( node, i, child );
            depthTraversal_createLeafMap(child);
        }
    }
}

void Octree::compute_Cell2Vertex( void )
{
    

    size_t n_leafs = Leafs.size();
    Cell2Vertex = std::vector<size_t>( n_leafs*8,0 );
    for( int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf = Leafs[i];
        for(int iv=0; iv<8; iv++)
        {
            const auto iter = Vertices.find( leaf.nodeVertices[iv] );
            if( iter != Vertices.end() )
            {
                Cell2Vertex[ i*8 + iv] = iter->second;
            }
            else
            {
                //printf("vertex not found\n");
            }
        }
    }
    
    /*FILE * pFile;
    pFile = fopen ("cell2vertex.txt","w");
    for( int i=0; i<n_leafs; i++)
    {
        fprintf(pFile, "%d", i);
        for(int iv=0; iv<8; iv++)
        {
            fprintf(pFile, " %lu", Cell2Vertex[ i*8 + iv]);
        }
        fprintf(pFile, "\n");
    }
    fclose(pFile);*/
}

void Octree::getLeafOccupancy( std::vector<size_t> &leafOccupancy )
{
    
    size_t n_leafs = Leafs.size();
    leafOccupancy = std::vector<size_t>( n_leafs, 0 );
    for( int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf = Leafs[i];
        leafOccupancy[i] = leaf.n_points;
    }

}

void Octree::getLeafPointDensity( std::vector<TypeReal> &leafPDensity )
{
    
    size_t n_leafs = Leafs.size();
    leafPDensity = std::vector<TypeReal>( n_leafs, 0 );
    for( int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf = Leafs[i];
        size_t  depth = GetNodeTreeDepth( leaf );
        TypeReal cellSide = std::pow( 0.5, depth );
        TypeReal cellVolume = 1.0;
		for(int idim=0; idim<3; idim++)
		{
			TypeReal sideBBox = BBox.getSide(idim);
			cellVolume = cellVolume * (cellSide * sideBBox);
		}
        leafPDensity[i] = leaf.n_points/cellVolume;
    }

}



void Octree::averagePerLeaf( const std::vector<TypeReal> & point_vars, std::vector<TypeReal> & leaf_vars, std::vector<TypeReal> & leaf_weights )
{
	int n_points = pointIDs.size();
    int dim = point_vars.size()/n_points;
    size_t n_leafs = Leafs.size();
    int count = 0;
    leaf_vars = std::vector<TypeReal>( n_leafs*dim, 0);
    leaf_weights = std::vector<TypeReal>( n_leafs, 0);
    for( int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf = Leafs[i];
        if( leaf.n_points == 0) { continue; }
        leaf_weights[i] = leaf.n_points; 
        for(int ip=0; ip<leaf.n_points; ip++)
        {
        	int id = pointIDs[	leaf.point0 + ip ];
        	for(int idim=0; idim<dim; idim++)
        	{
        		leaf_vars[count*dim+idim] += point_vars[id*dim+ idim];
        	}
        }
        for(int idim=0; idim<dim; idim++)
        {
        	leaf_vars[count*dim+idim] /= leaf.n_points;
        }
        count += 1;
    }
    leaf_vars.resize( count*dim );
    leaf_weights.resize( count*dim );
}



void Octree::getVertexOccupancy( std::vector<TypeReal> &vertexOccupancy )
{
    size_t n_leafs = Leafs.size();
    size_t n_vertex = getNVertex();
    vertexOccupancy = std::vector<TypeReal>(n_vertex,0);
    std::vector<size_t> leafOccupancy;
    getLeafOccupancy( leafOccupancy );
    if( Cell2Vertex.size() != 8*n_leafs )
    {
    	compute_Cell2Vertex();
    }
    int count = 0;
    for( int i=0; i<n_leafs; i++)
    {
    	for(int iv=0; iv<8; iv++)
    	{
    		vertexOccupancy[ Cell2Vertex[count] ] += 0.125*(leafOccupancy[i]);
    		count += 1;
    	}
    }
}

void Octree::getVertexPointDensity( std::vector<TypeReal> &vertexPDensity )
{
    size_t n_leafs = Leafs.size();
    size_t n_vertex = getNVertex();
    vertexPDensity = std::vector<TypeReal>(n_vertex,0);
    std::vector<TypeReal> leafOccupancy;
    getLeafPointDensity( leafOccupancy );
    if( Cell2Vertex.size() != 8*n_leafs )
    {
    	compute_Cell2Vertex();
    }
    int count = 0;
    for( int i=0; i<n_leafs; i++)
    {
    	for(int iv=0; iv<8; iv++)
    	{
    		vertexPDensity[ Cell2Vertex[count] ] += 0.125*(leafOccupancy[i]);
    		count += 1;
    	}
    }

}


void Octree::compute_Vertex2Cell(void )
{
    size_t n_vertex = Vertices.size();
    Vertex2Cell = std::vector< int >( n_vertex*8, -1 );
    TypeReal step = std::pow( 0.5, octreeDepth+1);
    TypeReal cube_corners[24] = { -1.0,-1.0,-1.0,   -1.0,-1.0,1.0,   -1.0,1.0,-1.0,   -1.0,1.0,1.0,   1.0,-1.0,-1.0,   1.0,-1.0,1.0,    1.0,1.0, -1.0,   1.0,1.0,1.0 };
    for ( auto it = Vertices.begin(); it != Vertices.end(); ++it )
    {
        size_t idnode = it-> second;
        uint64_t locCode = it->first;
        TypePoint vertex;
        GetVertexCoord(locCode, vertex );
        for( int k=0; k<8; k++)
        {
            TypeReal point[3];
            for(int idim=0; idim<3; idim++)
            {
                point[idim] = vertex.position[idim] + cube_corners[ k*3 + idim ] * step;
            }
            uint64_t locNeighCode = getPointNode( &(point[0]) );
            if( locNeighCode>0) // it is not outside the octree domain
            {
                const auto iter = LeafMap.find(locNeighCode);
                if( iter != LeafMap.end() )
                {
                    Vertex2Cell[ idnode*8 + k] = iter->second;
                }
            }
        }
    }
    
    /*FILE * pFile;
    pFile = fopen ("vertex2cell.txt","w");
    for(int i=0; i<n_vertex; i++)
    {
        fprintf(pFile, "%d %d %d %d %d %d %d %d %d\n", i, Vertex2Cell[ i*8 + 0], Vertex2Cell[ i*8 + 1], Vertex2Cell[ i*8 + 2], Vertex2Cell[ i*8 + 3], Vertex2Cell[ i*8 + 4], Vertex2Cell[ i*8 + 5], Vertex2Cell[ i*8 + 6], Vertex2Cell[ i*8 + 7]);
    }
    fclose(pFile);*/
}

void Octree::createLeafMap( void )
{
    OctreeNode root;
    getNode(1, root);
    Leafs.resize(0);
    LeafMap.clear();
    depthTraversal_createLeafMap( root );
    //printf("%d=%d vertices, %d leafs after creating leaf map\n", n_vertex, Vertices.size(), Leafs.size());
}


// refine octree by splitting non-empty leafs
size_t Octree::refineOctree( size_t min_points )
{
    size_t n_leafs = Leafs.size();
    size_t splits = 0;
    for( int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf = Leafs[i];
        if( leaf.n_points>min_points )
        {
            splitNode(leaf);
            splits += 1;
        }
    }   
    
    // creating leafMap
    createLeafMap();
    // creating mesh structures
	//printf("%d cells split with more than %d points\n", splits,  min_points);
    compute_Vertex2Cell();
    compute_Cell2Vertex();
    
    
    return splits;
}



uint64_t Octree::getPointNode( TypeReal *pointcoords )
{
    
    uint64_t locCode = (uint64_t)0;
    
    if( (pointcoords[0] >= BBox.getMin(0)) && ( pointcoords[0] <= BBox.getMax(0) ) && (pointcoords[1] >= BBox.getMin(1)) && ( pointcoords[1] <= BBox.getMax(1) ) && (pointcoords[2] >= BBox.getMin(2)) && ( pointcoords[2] <= BBox.getMax(2) )  )
    {
        locCode = getPointMortonCode( &(pointcoords[0]), octreeDepth );
        // if locCode is in Nodes, this is a neighbour
        // otherwise it is the first ancestor of the node in the tree
        auto iter = Nodes.find(locCode);
        while( iter == Nodes.end() )
        {
            uint64_t parent = locCode>>3;
            iter = Nodes.find(parent);
            locCode = parent;
        }
    }
    return locCode;
}


// find neighbours of leafs
void Octree::findNeighbours( void )
{
    for(int ileaf=0; ileaf<Leafs.size(); ileaf++)
    {
        OctreeNode node = Leafs[ileaf];
        TypePoint CellCenter, HalfSides;
        getVoxel(node, CellCenter, HalfSides);
        TypeReal step = std::pow( 0.5, octreeDepth+1);
        TypeReal coef[2] = {-1.0, 1.0};
        size_t count = 0;
        for(int idim=0; idim<3; idim++)
        {
            TypeReal BBside = BBox.getSide(idim);
            TypeReal dx = HalfSides.position[idim] + step * BBside;
            for(int i=0; i<2; i++)
            {
                TypeReal face[3] = { CellCenter.position[0], CellCenter.position[1], CellCenter.position[2] };
                face[idim] += coef[i] * dx;
                // if neighbour exists
                if( (face[idim] > BBox.getMin(idim)) && ( face[idim] < BBox.getMax(idim) )  )
                {
                    uint64_t locCode0 = getPointMortonCode( &(face[0]), octreeDepth );
                    // if locCode is in Nodes, this is a neighbour
                    // otherwise it is the first ancestor of the node in the tree
                    auto iter = Nodes.find(locCode0);
                    while( iter == Nodes.end() )
                    {
                        uint64_t parent = locCode0>>3;
                        iter = Nodes.find(parent);
                        locCode0 = parent;
                    }
                    node.neighbours[count] = locCode0;
                    //printf("neighbour %d is %lu\n", count, locCode0);
                }
                else
                {
                    node.neighbours[count] = (uint64_t)0;
                    //printf( "no neighbour %d\n", count);
                }
                count += 1;
            }// end loop over i
        }// end loop over idim
        
        // update Leaf list
        Leafs[ileaf] = node;
        
        // update hash table
        Nodes.erase(node.LocCode);
        Nodes.insert(std::make_pair( node.LocCode, node));
        //printf("neighbours node %lu are: %d %d %d %d %d %d\n", node.LocCode, node.neighbours[0], node.neighbours[1], node.neighbours[2], node.neighbours[3], node.neighbours[4], node.neighbours[5]);
    }
}

void Octree::findCellNeighbours( OctreeNode &node )
{
	TypePoint CellCenter, HalfSides;
	getVoxel(node, CellCenter, HalfSides);
	TypeReal step = std::pow( 0.5, octreeDepth+1);
	TypeReal coef[2] = {-1.0, 1.0};
	size_t count = 0;
	for(int idim=0; idim<3; idim++)
	{
		TypeReal BBside = BBox.getSide(idim);
		TypeReal dx = HalfSides.position[idim] + step * BBside;
		for(int i=0; i<2; i++)
		{
			TypeReal face[3] = { CellCenter.position[0], CellCenter.position[1], CellCenter.position[2] };
			face[idim] += coef[i] * dx;
			// if neighbour exists
			if( (face[idim] > BBox.getMin(idim)) && ( face[idim] < BBox.getMax(idim) )  )
			{
				uint64_t locCode0 = getPointMortonCode( &(face[0]), octreeDepth );
				// if locCode is in Nodes, this is a neighbour
				// otherwise it is the first ancestor of the node in the tree
				auto iter = Nodes.find(locCode0);
				while( iter == Nodes.end() )
				{
					uint64_t parent = locCode0>>3;
					iter = Nodes.find(parent);
					locCode0 = parent;
				}
				node.neighbours[count] = locCode0;
				//printf("neighbour %d is %lu\n", count, locCode0);
			}
			else
			{
				node.neighbours[count] = (uint64_t)0;
				//printf( "no neighbour %d\n", count);
			}
			count += 1;
		}// end loop over i
	}// end loop over idim
	
	// update hash table
	Nodes.erase(node.LocCode);
	Nodes.insert(std::make_pair( node.LocCode, node));
	//printf("neighbours node %lu are: %d %d %d %d %d %d\n", node.LocCode, node.neighbours[0], node.neighbours[1], node.neighbours[2], node.neighbours[3], node.neighbours[4], node.neighbours[5]);
}


size_t Octree::balance( std::vector<uint64_t> leafCodes )
{
	std::queue<uint64_t> leafQueue;
	// add all the leafs to the queue
	for(int ii=0; ii< leafCodes.size(); ii++)
	{
		leafQueue.push(leafCodes[ii]);
	}
	size_t count = 0;
	while( !leafQueue.empty() )
	{
		uint64_t locCode = leafQueue.front();
		size_t depth_leaf = GetNodeIDTreeDepth(locCode);
		auto iter = Nodes.find(locCode);
		if( iter == Nodes.end() )
		{ 
			//printf("leaf code not found\n");
		}
		else
		{		
			OctreeNode leaf = iter->second;
			findCellNeighbours( leaf );
			bool split = false;
			for(int jj=0; jj<6; jj++)
			{
				uint64_t neighCode = leaf.neighbours[jj];
				if(neighCode>0)
				{
					size_t depth_neigh = GetNodeIDTreeDepth(neighCode);
					if( depth_neigh > depth_leaf + 1 ) // the cell has to be split
					{
						split = true;
						break;
					}
				}
			}// end loop neighbours inside octree domain
			if( split ) // one of its neighbours is at depth > depth_leaf + 1
			{
				count += 1;
				// split leaf and add its children and neighbours to the queue
				splitNode(leaf);
				for(int kk=0; kk<8; kk++)
				{
					uint64_t childCode = GetChildCode(locCode, kk);
					leafQueue.push( childCode );
				}
				for(int jj=0; jj<6; jj++)
				{
					uint64_t neighCode = leaf.neighbours[jj];
					if(neighCode>0)
					{
						leafQueue.push( neighCode );
					}
				}
			}// end loop neighbours inside octree domain
		}
		leafQueue.pop();
	}
	//printf("%d cells split to balance the tree\n", count);
	return count;
}


void Octree::point2grid( const std::vector<TypeReal> &pValues, std::vector<TypeReal> &cValues, std::vector<TypeReal> &vValues )
{

    size_t n_leafs = getNLeafs();
    size_t n_vertex = getNVertex();
    size_t n_points = getNPoints();
    
    size_t dimF = pValues.size()/n_points;
    // average pointValues per cell
    cValues = std::vector<TypeReal> (n_leafs*dimF, 0);
    // tranfers to pointsamples inside a cell to the cell vertices
    vValues = std::vector<TypeReal> (n_vertex*dimF, 0);
    
    // proces the non-empty leafs to initialize the frozen points and the narrow band
    for(int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf;
        getLinearLeaf(i, leaf);
        if( leaf.n_points>0 ) // the distance of non-empty leafs is the minimum distance to points inside it
        {
            // compute the value of the distance function at the cell vertices
            for(int j=0; j<leaf.n_points; j++)
            {
                TypePoint point;
                getPoint( leaf.point0 + j, point);
                size_t pointID = getPointID( leaf.point0 + j );
                // average the function values over the cell
                for(int idim=0; idim<dimF; idim++)
                {
                	cValues[ i*dimF + idim ] += pValues[ pointID*dimF + idim ]/leaf.n_points;
                }
                // distribute the sample to the cell vertices with weights decreasing with the distance to the vertex
                TypeReal vweights[8];
                TypeReal sumWeights = 0;
                for(int iv=0; iv<8; iv++)
                {
                    TypeReal dist = 0;
                    size_t vertexID = Cell2Vertex[ i*8 + iv ];
                    for(int idim=0; idim<3; idim++)
                    {
                        dist += std::pow( point.position[idim] - vertexCoordinates[vertexID*3+idim] , 2);
                    }
                    vweights[iv] = std::exp( -dist );
                    sumWeights += vweights[iv];
                }
                for(int iv=0; iv<8; iv++)
                {
                    size_t vertexID = Cell2Vertex[ i*8 + iv ];
                    for(int idim=0; idim<dimF; idim++)
                    {
                        vValues[vertexID*dimF + idim] += vweights[iv]/sumWeights * pValues[ pointID*dimF + idim ];
                    }
                }// end loop over cell vertices
            }// end loop over cell points
        }// end loop over non-empty cells
    }// end loop over grid cells
}




void Octree::splatting( const std::vector<TypeReal> &pValues, std::vector<TypeReal> &vValues, bool normalize=true )
{

    size_t n_leafs = getNLeafs();
    size_t n_vertex = getNVertex();
    size_t n_points = getNPoints();
    
    size_t dimF = pValues.size()/n_points;
    // tranfers to pointsamples inside a cell to the cell vertices
    vValues = std::vector<TypeReal> (n_vertex*dimF, 0);
    vValues = std::vector<TypeReal> (n_vertex*dimF, 0);
    
    // proces the non-empty leafs to initialize the frozen points and the narrow band
    for(int i=0; i<n_leafs; i++)
    {
        OctreeNode leaf;
        getLinearLeaf(i, leaf);
        if( leaf.n_points>0 ) // the distance of non-empty leafs is the minimum distance to points inside it
        {
            // compute the value of the distance function at the cell vertices
            for(int j=0; j<leaf.n_points; j++)
            {
                TypePoint point;
                getPoint( leaf.point0 + j, point);
                size_t pointID = getPointID( leaf.point0 + j );
                std::vector<TypeReal> ipointValues(dimF);
                memcpy( &ipointValues[0], &pValues[ pointID], dimF*sizeof(TypeReal)  );
                // distribute the sample to the cell vertices with weights decreasing with the distance to the vertex
                OctreeNode node = leaf;
                std::vector<bool> vSplat(n_vertex, false);
                TypeReal sc = 1.0;
                while( node.LocCode>1 )
                {
					TypeReal vweights[8];
					TypeReal sumWeights = 0;
					size_t vertexIDs[8];
					for(int iv=0; iv<8; iv++)
					{
						TypeReal dist = 0;
						vertexIDs[iv] = getVertexIndex(node.nodeVertices[iv]);
						for(int idim=0; idim<3; idim++)
						{
							dist += std::pow( point.position[idim] - vertexCoordinates[vertexIDs[iv]*3+idim] , 2);
						}
						vweights[iv] = std::exp( -dist );
						sumWeights += vweights[iv];
					}
					for(int iv=0; iv<8; iv++)
					{
						if(vSplat[vertexIDs[iv]] == false)
						{
							for(int idim=0; idim<dimF; idim++)
							{
								vValues[vertexIDs[iv]*dimF + idim] += vweights[iv]/sumWeights * ipointValues[ idim ];//0.125* sc * ipointValues[ idim ];
							}
							vSplat[vertexIDs[iv]] = true;
						}
					}// end loop over cell vertices
					
					// get the parent of the node
					const OctreeNode child = node;
					GetParentNode( child, node );
					sc *= 0.5;
				}
            }// end loop over cell points
        }// end loop over non-empty cells
    }// end loop over grid cells
    
    // normalize vector field
    if(normalize)
    {
		for(int i=0; i<n_vertex; i++)
		{
			TypeReal norm = 0;
			for(int idim=0; idim<dimF; idim++)
			{
				norm += std::pow(vValues[ i*dimF + idim], 2);
			}
			norm = std::sqrt(norm);
			if(norm>1e-12)
			{
				for(int idim=0; idim<dimF; idim++)
				{
					vValues[ i*dimF + idim] /= norm;
				}
			}
		}
	}
}




//---------------------------------- useful for debugging

void Octree::getVoxel(const OctreeNode &node, TypePoint &CellCenter, TypePoint &HalfSides)
{
    // get cell coordinates
    GetNodeCenter( node, CellCenter );
    size_t  depth = GetNodeTreeDepth( node );
    TypeReal cellSide = std::pow( 0.5, depth );
    for(int idim=0; idim<3; idim++)
    {
        TypeReal sideBBox = BBox.getSide(idim);
        HalfSides.position[idim] = 0.5 * cellSide * sideBBox;
    }
}

bool Octree::pointInNode( const TypePoint &point, const TypePoint &CellCenter, const TypePoint &CellSides )
{
    bool in = true;
    for(int idim=0; idim<3; idim++)
    {
        if( ( point.position[idim] < CellCenter.position[idim] - CellSides.position[idim] ) || ( point.position[idim] >= CellCenter.position[idim] + CellSides.position[idim] ) )
        {
            in = false;
            break;
        }
    }
    return in;
}

bool Octree::pointInNode( const TypePoint &point, const OctreeNode &node )
{
    // get cell coordinates
    TypePoint CellCenter, HalfSides;
    getVoxel( node, CellCenter, HalfSides );
    bool in = pointInNode( point, CellCenter, HalfSides );
    return in;  
}

void Octree::printVoxel(const TypePoint &CellCenter, const TypePoint &CellSides)
{
    // get cell coordinates
    printf("voxel [%g,%g) x [%g,%g) x [%g,%g)\n", CellCenter.position[0]-CellSides.position[0], CellCenter.position[0]+CellSides.position[0], CellCenter.position[1]-CellSides.position[1], CellCenter.position[1]+CellSides.position[1], CellCenter.position[2]-CellSides.position[2], CellCenter.position[2]+CellSides.position[2]);
}







size_t Octree::getVoxels( std::vector<TypeReal> &centers, std::vector<TypeReal> &halfSides )
{
    if(Leafs.size()==0)
    {
        createLeafMap();
    }
    size_t n_leafs = Leafs.size();
    centers.resize( n_leafs * 3 );
    halfSides.resize( n_leafs * 3 );
    TypeReal BBoxSides[3];
    for(int idim=0; idim<3; idim++)
    {
        BBoxSides[idim] = BBox.getSide(idim);
    }
    for(int i=0; i<n_leafs; i++)
    {
        OctreeNode node = Leafs[i];
        TypePoint CellCenter;
        GetNodeCenter( node, CellCenter );
        size_t  depth = GetNodeTreeDepth( node );
        TypeReal cellSide = std::pow( 0.5, depth );
        for(int idim=0; idim<3; idim++)
        {
            centers[i*3+idim] = CellCenter.position[idim];
            halfSides[i*3 + idim] = 0.5 * cellSide * BBoxSides[idim];
        }
    }
    return n_leafs;
}

void Octree::printNeighbours( void )
{
    if(Leafs.size()==0)
    {
        createLeafMap();
    }
    size_t n_leafs = Leafs.size();
    std::vector<TypeReal> centers( n_leafs * 3 );
    std::vector<TypeReal> halfSides( n_leafs * 3 );
    TypeReal BBoxSides[3];
    for(int idim=0; idim<3; idim++)
    {
        BBoxSides[idim] = BBox.getSide(idim);
    }
    
    for(int i=0; i<n_leafs; i++)
    {
        OctreeNode node = Leafs[i];
        TypePoint CellCenter;
        GetNodeCenter( node, CellCenter );
        size_t  depth = GetNodeTreeDepth( node );
        TypeReal cellSide = std::pow( 0.5, depth );
        for(int idim=0; idim<3; idim++)
        {
            centers[i*3+idim] = CellCenter.position[idim];
            halfSides[i*3 + idim] = 0.5 * cellSide * BBoxSides[idim];
        }
    }

    FILE * pFile;
    pFile = fopen ("voxels_neighbours.txt","w");
    for(int i=0; i<n_leafs; i++)
    {
        fprintf(pFile, "%g %g %g %g %g %g", centers[i*3], centers[i*3+1], centers[i*3+2], halfSides[i*3], halfSides[i*3+1], halfSides[i*3+2] );
        OctreeNode node = Leafs[i];
        //printf("cell %d neighbours ", node.LocCode);
        for(int j=0; j<6; j++)
        {
            uint64_t locCode = node.neighbours[j];
            //printf(" %d", locCode);
            if( locCode==0 )
            {
                fprintf(pFile, " %g %g %g %g %g %g", centers[i*3], centers[i*3+1], centers[i*3+2], halfSides[i*3], halfSides[i*3+1], halfSides[i*3+2] );
            }
            else
            {
                size_t indx = getLeafIndex( locCode );
                fprintf(pFile, " %g %g %g %g %g %g", centers[indx*3], centers[indx*3+1], centers[indx*3+2], halfSides[indx*3], halfSides[indx*3+1], halfSides[indx*3+2] );

            }
        }
        fprintf(pFile, "\n");
        //printf("\n");
    }
    fclose (pFile);

}


