#ifndef __SETTINGS__
#define __SETTINGS__

class Settings
{
public:

    TypeReal    epsilon;
    TypeReal 	alpha;
    TypeReal 	beta;
    TypeReal 	gamma;
    TypeReal 	delta;
    TypeReal	BBexp;
    TypeReal    randomization;
    size_t 	    uniR;
    TypeReal 	lp;
    size_t		octreeLevel; 
    size_t      quadrature;
    size_t      maxIterations;
    size_t	    solverIterations;
    size_t	    n_controlPoints;
    std::string outFile;
    std::string cloudFile;
    std::string iniFile;
    size_t		n_splits;
    size_t		n_collapses;
    size_t		collapse_step;      
    size_t		split_step;   
    int			visualize;
    int 		alg;

    Settings() :
        epsilon(TypeReal(1e-6)),
        alpha(0.1),
        beta(0.01),
        gamma(0),
        delta(1.0),
        BBexp(1.1),
        randomization(1.0),
        uniR(0),
        lp(1.3),
        quadrature(3),
        maxIterations(100),
        solverIterations(10),
        n_controlPoints(1000),
        octreeLevel(0),
        cloudFile(),
        iniFile(),
        outFile(),
        n_collapses(0),
        collapse_step(1),        
        n_splits(0),
        split_step(1),          
        visualize(0),
        alg(0)
    {
    }
    
    void error(const char *msg);
    void printOptions(void);
    void print_usage(void);
    void parseCommandLine(int argc, char **argv);
};

#endif //__SETTINGS__
