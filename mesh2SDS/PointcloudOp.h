//
//  PointcloudOp.hpp
//  
//
//  Created by virginia estellers on 1/3/17.
//
//

#ifndef PointcloudOp_hpp
#define PointcloudOp_hpp



//definition of a non-mutable lvalue property map,
//with the get function as a friend function to give it
//access to the private member


class PointcloudOp
{	// typedefs for cgal
	

	private:
		std::vector<TypeReal> vertices;
		std::vector<TypeReal> pBoundary;
		std::vector<TypeReal> pLine;
		std::vector<TypeReal> pCorner;
		std::vector<TypeReal> pInterior;
		std::vector<TypeReal> principalK1;
		std::vector<TypeReal> principalK2;
		std::vector<TypeReal> principalD1;
		std::vector<TypeReal> principalD2;
		std::vector<TypeReal> principalD3;
		std::vector<TypeReal> confidence;
		
		
	public:
		
		PointcloudOp( const PointcloudOp &other) : vertices(other.vertices), principalK1(other.principalK1), principalK2(other.principalK2), principalD1(other.principalD1), principalD2(other.principalD2), principalD3(other.principalD3), confidence(other.confidence), pBoundary(other.pBoundary), pLine(other.pLine), pCorner(other.pCorner), pInterior(other.pInterior) {}
		PointcloudOp() : vertices( std::vector<TypeReal>(0) ), principalK1( std::vector<TypeReal>(0) ), principalK2( std::vector<TypeReal>(0) ), principalD1( std::vector<TypeReal>(0) ), principalD2( std::vector<TypeReal>(0) ), principalD3( std::vector<TypeReal>(0) ), confidence( std::vector<TypeReal>(0) ), pBoundary( std::vector<TypeReal>(0) ), pLine( std::vector<TypeReal>(0) ), pCorner( std::vector<TypeReal>(0) ), pInterior( std::vector<TypeReal>(0) ) {}

		
		// initialize points and estimate geometry
		PointcloudOp(const std::vector<TypeReal> &_vertices, int estimate_geometry);
		//
		TypeReal getPrincipalK1( int id ){ return principalK1[id]; };
		TypeReal getpBoundary( int id ){ return pBoundary[id]; };
		TypeReal getPrincipalK2( int id ){ return principalK2[id]; };
		TypeReal getPrincipalD1( int id, int dim ){ return principalD1[id*3+dim]; };
		TypeReal getPrincipalD2( int id, int dim ){ return principalD2[id*3+dim]; };
		TypeReal getPrincipalD3( int id, int dim ){ return principalD3[id*3+dim]; };
		TypeReal getConfidence( int id ){ return confidence[id]; };
		void getBoundary( std::vector<TypeReal> &_pBoundary){_pBoundary = pBoundary;};
		void getLine( std::vector<TypeReal> &_pLine){_pLine = pLine;};
		void getCorner( std::vector<TypeReal> &_pCorner){_pCorner = pCorner;};
		void getInterior( std::vector<TypeReal> &_pInterior){_pInterior = pInterior;};
		void getNormals( std::vector<TypeReal> &_principalD3){_principalD3 = principalD3;};
		void getPointcloud( std::vector<TypeReal> &_vertices ){ _vertices = vertices; };
		void getGeometry( std::vector<TypeReal> &_principalK1, std::vector<TypeReal> &_principalK2, std::vector<TypeReal> &_principalD1, std::vector<TypeReal> &_principalD2, std::vector<TypeReal> &_principalD3, std::vector<TypeReal> &_confidence){ _principalK1 = principalK1; _principalK2 = principalK2; _principalD1 = principalD1; _principalD2 = principalD2; _principalD3 = principalD3; _confidence = confidence; };
};



#endif /* PointcloudOp.hpp */
 