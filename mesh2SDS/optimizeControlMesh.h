#ifndef __optimizeControlMesh__
#define __optimizeControlMesh__

//void remeshVertexRing( std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &face_metric, int ivertex, std::vector<int> &vertex_ring );
void computeError(std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings &D, std::vector<TypeReal> &quad_error, std::vector<TypeReal> &vertex_error );
void computeCurvature( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings &D, std::vector<TypeReal> &face_curvature, std::vector<TypeReal> &vertex_curvature );

// split
bool vertex_split( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &cost, int n_splits );

// collapse
bool diagonal_collapse( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &cost, int n_collapses);

void snap_to_bbox(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, BoundingBox BBox);
void snap_to_points(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &points);
void sample_boundary(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &boundary_points);

//void create_hEdgeMesh( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, CMU462::HalfedgeMesh *hEdgeMesh);

// cleaning operations for quad mesh
void clean_boundary( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &boundary_faces, bool verbose );
void detect_boundary( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &boundary_vertices);
void detect_boundary_faces( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &boundary_faces);

int remove_doublet( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
int remove_singlet( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
int remove_unreferedVertex( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
int remove_area0Faces( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex);
void clean_mesh( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex );

#endif
