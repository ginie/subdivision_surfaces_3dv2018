#include <cassert>
#include <cstdio>
#include <cfloat>
#include <algorithm>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <queue>
#include <unordered_map>
#include <utility>
#include <boost/multi_array.hpp>
#include <boost/functional/hash.hpp>

#include "TypeDef.h"
#include "BBox.h"
#include "Settings.h"
#include "EigenTypeDef.h"
#include "sds.h"
#include "kdtree2.h"
#include "optimizeControlMesh.h"


typedef std::unordered_map< std::pair<int,int>, int, boost::hash<std::pair<int, int> > > EdgeMap;

void clean_boundary( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &boundary_faces, bool verbose = false )
{
	
	// map each halfedge to the face incident to it
	EdgeMap hedge_map;
	int count = 0;
	int n_faces = face_nvertex.size();
	int n_hedges = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			int v = face_vertex[ count + jj ];
			int vplus = face_vertex[ count + ((jj+1)% face_nvertex[ii]) ];
			hedge_map.emplace( std::make_pair( v, vplus ), ii );
			n_hedges += 1;
		}
		count += face_nvertex[ii];
	}
	// detect boundary edges and boundary faces
	int n_heBoundary = 0;
	std::vector<int> face_nboundary( n_faces, 0 );
	for ( auto it = hedge_map.begin(); it != hedge_map.end(); ++it )
    {
    	std::pair<int, int> hedge = it->first;
    	int iface = it->second;
    	std::pair<int, int> hedge_opp = std::make_pair( hedge.second, hedge.first );
    	if( ( hedge_map.count(hedge_opp) == 0) )
    	{
    		n_heBoundary += 1;
    		face_nboundary[ iface ] += 1;
    	}
    }
    
    // remove any corner face with two boundary edges
    std::queue<int> faces2remove;
    for(int ii=0; ii< n_faces; ii++)
	{
		if( face_nboundary[ii] > 1 )
		{
			faces2remove.push(ii);
		}
    }
    // check if by removing a face, we create new corner face
    std::vector<bool> remove_faces( n_faces, false );
    int n_fremovals = 0;
    while( ! faces2remove.empty() )
	{
		int iface = faces2remove.front();
		for(int jj=0; jj< face_nvertex[iface]; jj++)
		{
			int v = face_vertex[ iface*3 + jj ];
			int vplus =  face_vertex[ iface*3 + ((jj+1)%face_nvertex[iface] ) ];
			std::pair<int, int> he = std::make_pair( v, vplus );
			std::pair<int, int> hedge_opp = std::make_pair( vplus, v );
			if( ( hedge_map.count(hedge_opp) != 0) )
			{ // non boundary edge, get the new boundary face
				int faceID = hedge_map.at( hedge_opp );
				face_nboundary[ faceID ] += 1;
				if( face_nboundary[faceID]>1 )
				{
					faces2remove.push(faceID);
				}
			}
			hedge_map.erase(he);
		}
		faces2remove.pop();
		n_heBoundary = -1;
		remove_faces[iface] = true;
		n_fremovals += 1;
	}
	
	// remove corner faces
	if( n_fremovals >0 )
	{
		int count = 0;
		for(int ii=0; ii< n_faces; ii++)
		{
			if( remove_faces[ ii ] == false )
			{
				face_nboundary[count/3] = face_nboundary[ii];
				face_nvertex[ count/3 ] = face_nvertex[ii];
				for(int jj=0; jj<3; jj++)
				{
					face_vertex[ count ] =  face_vertex[ ii*3+jj ];
					count += 1;
				}
			}
		}
		face_nboundary.resize(n_faces - n_fremovals);
		face_nvertex.resize( n_faces - n_fremovals, 3);
		face_vertex.resize( (n_faces - n_fremovals)*3 );
	}
	// remove unreferenced vertices
	remove_unreferedVertex( vertices, face_nvertex, face_vertex );
	if( verbose) { printf("removed %d corner faces with 2 boundary edges\n", n_fremovals); }
	
	n_faces = face_nvertex.size();
    boundary_faces.clear();
    for(int ii=0; ii< n_faces; ii++)
	{
		if( face_nboundary[ii] == 1 )
		{
			boundary_faces.push_back(ii);
		}
    }
	//printf("mesh with %d faces, %d vertices, and %d boundary faces\n", n_faces, vertices.size()/3, boundary_faces.size() );
}

void snap_to_bbox(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, BoundingBox BBox)
{
	int n_vertex = vertices.size()/3;
	std::vector<bool> inside_vertex( true, n_vertex);
	int n_outV = 0;
	for(int ii=0; ii<n_vertex; ii++)
	{
		bool in = true;
		for(int idim=0; idim<3; idim++)
		{
			if( (vertices[ii*3+idim] > BBox.getMax(idim) ) || (vertices[ii*3+idim] < BBox.getMin(idim) ) )
			{
				in = false;
				break;
			}
		}
		if( in==false ){ inside_vertex[ii] = in; n_outV += 1; }
	}
	
	int n_faces = face_nvertex.size();
	if( n_outV >0 )
	{
		std::vector<int> face_nvertex0( face_nvertex );
		std::vector<int> face_vertex0( face_vertex );
		int st = 0;
		bool in = true;
		face_nvertex.clear();
		face_vertex.clear();
		for(int ii=0; ii< n_faces; ii++)
		{
			for(int jj=0; jj< face_nvertex0[ii]; jj++)
			{
				int v = face_vertex0[ st + jj];
				if( inside_vertex[v] ==false ){ in = false; break; }
			}
			
			if( in==true )
			{
				face_nvertex.push_back(face_nvertex0[ii]) ;
				for(int jj=0; jj< face_nvertex0[ii]; jj++)
				{
					face_vertex.push_back( face_vertex0[ st + jj] );
				}
			}
			st = st + face_nvertex0[ii];
		}
		
		remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	}
}


void sample_boundary(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &boundary_points)
{
	
	
	int n_vertex = vertices.size()/3;
	int n_faces = face_nvertex.size();

	// map each halfedge to the face incident to it		
	EdgeMap hedge_map;
	std::vector<int> edge_visitor;
	int count = 0;
	int n_edges = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			int v = face_vertex[ count + jj ];
			int vplus = face_vertex[ count + ((jj+1)% face_nvertex[ii]) ];
			std::pair<int, int> key = std::make_pair( v, vplus ); 
			if( hedge_map.count(key)==0  )
			{
				hedge_map.emplace( key, n_edges );
				hedge_map.emplace( std::make_pair( vplus, v ), n_edges );
				edge_visitor.push_back(1);
				n_edges += 1;
			}else
			{
				int edgeID = hedge_map.at( key );
				edge_visitor[ edgeID ] += 1;
			}
		}
		count += face_nvertex[ii];
	}
	
	boundary_points.clear();
	std::vector<bool> edgeDone( n_edges, false);
	for ( auto it = hedge_map.begin(); it != hedge_map.end(); ++it )
	{
		std::pair<int, int> hedge = it->first;
		int iedge = it->second;
		if(edgeDone[iedge]){ continue; }
		int iv = hedge.first;
		int jv = hedge.second;
		if( edge_visitor[iedge] != 2 )
		{
			for (int idim=0; idim<3; idim++)
			{ 
				boundary_points.push_back( 0.5*( vertices[iv*3+idim] + vertices[jv*3+idim] ) );
			}
		}
		edgeDone[iedge] = true;
		
	}

}


/*void snap_to_points(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &points)
{
		
	int removed_face = 1;
	int n_points = points.size()/3;
	
	// create a KDtree of points
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_points][3]);
	for ( int ii=0; ii< n_points; ii++ )
	{
		for (int idim=0; idim<3; idim++)
		{ 
			KdTreePoints[ii][idim] = points[ii*3+idim];
		}		
	}
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
	tree = new kdtree2::KDTree(KdTreePoints,false); 
	tree->sort_results = true;			
		
	while( removed_face>0 )
	{
		
		int n_vertex = vertices.size()/3;
		int n_faces = face_nvertex.size();
		std::vector<TypeReal> face_centers(3*n_faces, 0);
		
		// map each halfedge to the face incident to it		
		EdgeMap hedge_map;
		std::vector<int> edge_visitor;
		int count = 0;
		int n_edges = 0;
		for(int ii=0; ii< n_faces; ii++)
		{
			int sz = face_nvertex[ii];
			for(int jj=0; jj< sz; jj++)
			{
				int v = face_vertex[ count + jj ];
				for(int idim=0; idim<3; idim++){
					face_centers[ii*3+idim] += vertices[3*v+idim]/sz;
				}
				int vplus = face_vertex[ count + ((jj+1)%sz) ];
				std::pair<int, int> key = std::make_pair( v, vplus ); 
				if( hedge_map.count(key)==0  )
				{
					hedge_map.emplace( key, n_edges );
					hedge_map.emplace( std::make_pair( vplus, v ), n_edges );
					edge_visitor.push_back(1);
					n_edges += 1;
				}else
				{
					int edgeID = hedge_map.at( key );
					edge_visitor[ edgeID ] += 1;
				}
			}
			count += face_nvertex[ii];
		}


		// remove empty boundary faces
		removed_face = 0;
		std::vector<int> face_nvertex0( face_nvertex );
		std::vector<int> face_vertex0( face_vertex );
		face_nvertex.clear();
		face_vertex.clear();
		count = 0;
		for(int ii=0; ii< n_faces; ii++)
		{
			bool boundary = false;
			TypeReal radius = 0;
			std::vector<TypeReal> query = {face_centers[ii*3], face_centers[ii*3+1], face_centers[ii*3+2] };
			tree->n_nearest(query, 1, KdtreeRes);
			TypeReal closestPdist = KdtreeRes[0].dis;
			
			for(int jj=0; jj< face_nvertex0[ii]; jj++)
			{
				int v = face_vertex0[ count + jj ];
				int vplus = face_vertex0[ count + ((jj+1)% face_nvertex0[ii]) ];
				std::pair<int, int> key = std::make_pair( v, vplus ); 
				int iedge = hedge_map.at(key);
				if( edge_visitor[iedge] == 1 ){ boundary = true; }
				
				TypeReal rad = 0;
				for(int idim=0; idim<3; idim++)
				{
					rad += std::pow( face_centers[ii*3 + idim] - vertices[3*v+idim], 2); 
				}
				if( rad> radius ){ radius = rad; }
				
			}
			if( (radius< closestPdist) || (boundary==false) )
			{
				face_nvertex.push_back(face_nvertex0[ii]) ;
				for(int jj=0; jj< face_nvertex0[ii]; jj++)
				{
					face_vertex.push_back( face_vertex0[ count + jj] );
				}
			}else{ removed_face += 1; }
			count += face_nvertex0[ii];
		}	
		printf("removed %d faces\n", removed_face);
		
	}
	remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	printf("snapped mesh to %d vertices, %d faces\n", vertices.size()/3, face_nvertex.size() );

}*/

/*void snap_to_points(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &points)
{
		
	int removed_face = 1;
	int n_points = points.size()/3;
	
	// create a KDtree of points
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_points][3]);
	for ( int ii=0; ii< n_points; ii++ )
	{
		for (int idim=0; idim<3; idim++)
		{ 
			KdTreePoints[ii][idim] = points[ii*3+idim];
		}		
	}
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
	tree = new kdtree2::KDTree(KdTreePoints,false); 
	tree->sort_results = true;			
	
	CMU462::HalfedgeMesh *hEdgeMesh;
	hEdgeMesh = new CMU462::HalfedgeMesh();
	create_hEdgeMesh( vertices, face_nvertex, face_vertex, hEdgeMesh);
	printf("hEdgeMesh with %d vertices, %d faces\n", hEdgeMesh->nVertices(), hEdgeMesh->nFaces() );
	
	while( removed_face>0 )
	{
		std::queue< CMU462::FaceIter > faces2delete;
		for( CMU462::FaceIter b = hEdgeMesh->boundariesBegin(); b != hEdgeMesh->boundariesEnd(); b++ )
		{ // boundary loop 
			CMU462::HalfedgeIter h0 = b->halfedge();
			CMU462::HalfedgeIter hedge= h0;
			int sz_boundary = 0;
			do
			{
				// process a boundary face
				std::vector<TypeReal> center(3, 0.0);
				int face_sz = 0;
				CMU462::HalfedgeIter hface0 = hedge->twin();
				printf("processing face of %d sides adjacent to boundary\n", face_sz);												
				CMU462::HalfedgeIter hface = hface0;
				do
				{	
					CMU462::VertexCIter v = hface->vertex();
					for(int idim=0; idim<3; idim++)
					{
						center[idim] += v->position[idim];
					}
					face_sz += 1;
					hface = hface->next();
				}while( hface != hface0 );
				printf("processing face of %d sides adjacent to boundary\n", face_sz);
				for(int idim=0; idim<3; idim++)
				{
					center[idim] /= face_sz;
				}
				
				// compute closest point to face center
				tree->n_nearest(center, 1, KdtreeRes);
				TypeReal dist2closestPoint = KdtreeRes[0].dis;	
			
				// compute face radius
				TypeReal radius = -1;
				hface = hface0;
				do
				{	
					TypeReal rad = 0;
					CMU462::VertexCIter v = hface->vertex();
					for(int idim=0; idim<3; idim++)
					{
						rad += std::pow( center[idim] - v->position[idim], 2);
					}
					if( rad> radius ){ radius = rad; }
					hface = hface->next();
				}while( hface != hface0 );
				
				// find close
				if( radius > dist2closestPoint )
				{
					//printf("boundary face with radius %f and closest point at distance %f\n", radius, dist2closestPoint);
					faces2delete.push( hface0->face() );
				}	
						
				sz_boundary += 1;
				hedge = hedge->next();
			}while( hedge != h0 );
			
		}
		removed_face = faces2delete.size();
		printf("snap %d boundary faces from mesh\n", removed_face);
		while( !faces2delete.empty() )
		{
			CMU462::FaceIter b = faces2delete.front();
			std::vector<bool> hedge_isbound;
			std::vector<CMU462::HalfedgeIter> hedge_bound;
			std::vector<CMU462::VertexIter> vertex_bound;
			
			CMU462::HalfedgeIter h0 = b->halfedge();
			CMU462::HalfedgeIter hedge= h0;
			do
			{
				vertex_bound.push_back( hedge->vertex() );
				if( hedge->twin()->isBoundary() ){ hedge_isbound.push_back(true); }else{ hedge_isbound.push_back(false); }
				hedge_bound.push_back(hedge);
				hedge = hedge->next();
			}while( hedge != h0 );
			
			int sz = hedge_isbound.size();
			for(int ii=0; ii<sz; ii++)
			{
				if( hedge_isbound[ii] )
				{
					hEdgeMesh->deleteEdge( hedge_bound[ii]->edge() );
					hEdgeMesh->deleteHalfedge( hedge_bound[ii] );
					int jj = (ii+1)%sz;
					if( hedge_isbound[jj] ){  printf("remove vertex\n"); hEdgeMesh->deleteVertex( vertex_bound[ii] ); }
				}
			}
			
			hEdgeMesh->deleteFace(b);
			faces2delete.pop();
		}
	}
	// extract vertex, face_nvertex, and face_vertex from halfege data structure
	printf("hEdgeMesh with %d vertices, %d faces\n", hEdgeMesh->nVertices(), hEdgeMesh->nFaces() );
	getMesh( hEdgeMesh, vertices, face_nvertex, face_vertex );
	remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	printf("mesh with %d vertices, %d faces\n", vertices.size()/3, face_nvertex.size() );
}*/

/*void snap_to_points(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &points)
{
		
	int removed_face = 1;
	int n_points = points.size()/3;
	
	// create a KDtree of points
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_points][3]);
	for ( int ii=0; ii< n_points; ii++ )
	{
		for (int idim=0; idim<3; idim++)
		{ 
			KdTreePoints[ii][idim] = points[ii*3+idim];
		}		
	}
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
	tree = new kdtree2::KDTree(KdTreePoints,false); 
	tree->sort_results = true;			
	
	CMU462::HalfedgeMesh *hEdgeMesh;
	hEdgeMesh = new CMU462::HalfedgeMesh();
	create_hEdgeMesh( vertices, face_nvertex, face_vertex, hEdgeMesh);
	printf("hEdgeMesh with %d vertices, %d faces\n", hEdgeMesh->nVertices(), hEdgeMesh->nFaces() );
	
	while( removed_face>0 )
	{
		std::queue< CMU462::FaceIter > faces2delete;
		for( CMU462::FaceIter b = hEdgeMesh->facesBegin(); b != hEdgeMesh->facesEnd(); b++ )
		{ // boundary loop 
			CMU462::HalfedgeIter h0 = b->halfedge();
			CMU462::HalfedgeIter hedge= h0;
			int sz = 0;
			int boundary = 0;
			// compute the location of face center
			std::vector<TypeReal> center(3, 0.0);
			do
			{
				CMU462::VertexCIter v = hedge->vertex();
				if( hedge->twin()->isBoundary() ){ boundary += 1; }
				for(int idim=0; idim<3; idim++)
				{
					center[idim] += v->position[idim];
				}
				sz += 1;
				hedge = hedge->next();
			}while( hedge != h0 );
			if(boundary > 0){ continue; }

			for(int idim=0; idim<3; idim++)
			{
				center[idim] /= sz;
			}
			// compute closest point to face center
			tree->n_nearest(center, 1, KdtreeRes);
			TypeReal dist2closestPoint = KdtreeRes[0].dis;	
			
			// compute face radius
			hedge = b->halfedge();
			TypeReal radius = -1;
			do
			{
				CMU462::VertexCIter v = hedge->vertex();
				TypeReal rad = 0;
				for(int idim=0; idim<3; idim++)
				{
					rad += std::pow( v->position[idim] - center[idim], 2);
				}
				if( rad > radius ){ radius = rad; }
				hedge = hedge->next();
			}while( hedge != h0 );
			
			// find close
			if( radius > dist2closestPoint )
			{
				//printf("boundary face with radius %f and closest point at distance %f\n", radius, dist2closestPoint);
				faces2delete.push(b);
			}
		}
		removed_face = faces2delete.size();
		printf("snap %d boundary faces from mesh\n", removed_face);
		while( !faces2delete.empty() )
		{
			CMU462::FaceIter b = faces2delete.front();
			// start at the first non-boundary edge
			CMU462::HalfedgeIter h0 = b->halfedge();
			while( h0->twin()->isBoundary() )
			{
				h0 = h0->next();
			}
			CMU462::HalfedgeIter hedge= h0;
			
			std::vector<bool> hedge_isbound;
			std::vector<CMU462::HalfedgeIter> hedge_bound;
			std::vector<CMU462::VertexIter> vertex_bound;
			bool nextIni = false;
			CMU462::HalfedgeIter newNext;
			CMU462::FaceIter boundaryFace;
			do
			{
				vertex_bound.push_back( hedge->vertex() );
				if( hedge->twin()->isBoundary() )
				{
					boundaryFace = hedge->twin()->face();
					if( nextIni = false ){ newNext = hedge->twin()->next(); nextIni = false; }
					hedge_isbound.push_back(true);
				}else{ hedge_isbound.push_back(false); }
				hedge_bound.push_back(hedge);
				hedge = hedge->next();
			}while( hedge != h0 );
			
			int sz = hedge_isbound.size();
			nextIni = false;
			for(int ii=0; ii<sz; ii++)
			{
				hedge = hedge_bound[ii];
				int jj = (ii+1)%sz;
				if( hedge_isbound[ii] )
				{
					hEdgeMesh->deleteEdge( hedge->edge() );
					hEdgeMesh->deleteHalfedge( hedge->twin() );
					hEdgeMesh->deleteHalfedge( hedge );
					if( hedge_isbound[jj] ){  printf("remove vertex\n"); hEdgeMesh->deleteVertex( vertex_bound[ii] ); }
				}else
				{
					if( hedge_isbound[jj] )
					{
						hedge->setNeighbors( newNext, hedge->twin(), hedge->vertex(), hedge->edge(), boundaryFace );
					}else{
						hedge->setNeighbors( hedge->next(), hedge->twin(), hedge->vertex(), hedge->edge(), boundaryFace );
					}
				}
			}
			
			hEdgeMesh->deleteFace(b);
			faces2delete.pop();
		}
	}
	// extract vertex, face_nvertex, and face_vertex from halfege data structure
	printf("hEdgeMesh with %d vertices, %d faces\n", hEdgeMesh->nVertices(), hEdgeMesh->nFaces() );
	getMesh( hEdgeMesh, vertices, face_nvertex, face_vertex );
	remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	printf("mesh with %d vertices, %d faces\n", vertices.size()/3, face_nvertex.size() );
}*/


// works
void snap_to_points(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &points)
{
		
	int removed_face = 1;
	int n_points = points.size()/3;
	while( removed_face > 0 )
	{
		int n_vertex = vertices.size()/3;
		int n_faces = face_nvertex.size();

		// map each halfedge to the face incident to it		
		EdgeMap hedge_map;
		std::vector<int> edge_visitor;
		int count = 0;
		int n_edges = 0;
		for(int ii=0; ii< n_faces; ii++)
		{
			for(int jj=0; jj< face_nvertex[ii]; jj++)
			{
				int v = face_vertex[ count + jj ];
				int vplus = face_vertex[ count + ((jj+1)% face_nvertex[ii]) ];
				std::pair<int, int> key = std::make_pair( v, vplus ); 
				if( hedge_map.count(key)==0  )
				{
					hedge_map.emplace( key, n_edges );
					hedge_map.emplace( std::make_pair( vplus, v ), n_edges );
					edge_visitor.push_back(1);
					n_edges += 1;
				}else
				{
					int edgeID = hedge_map.at( key );
					edge_visitor[ edgeID ] += 1;
				}
			}
			count += face_nvertex[ii];
		}
		
		// define Kdtree
		boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_edges][3]);
		std::vector<bool> edgeDone( n_edges, false);
		for ( auto it = hedge_map.begin(); it != hedge_map.end(); ++it )
		{
			std::pair<int, int> hedge = it->first;
			int iedge = it->second;
			if(edgeDone[iedge]){ continue; }
			int iv = hedge.first;
			int jv = hedge.second;
			for (int idim=0; idim<3; idim++)
			{ 
				KdTreePoints[iedge][idim] = 0.5*( vertices[iv*3+idim] + vertices[jv*3+idim] );
			}
			edgeDone[iedge] = true;
			
		}
		
		
		// for each input point, find closest edge
		std::vector<int> occ_edge(n_edges, 0);
		kdtree2::KDTree* tree;
		kdtree2::KDTreeResultVector KdtreeRes; 
		tree = new kdtree2::KDTree(KdTreePoints,false); 
		tree->sort_results = true;
		for(int ii=0; ii< n_points; ii++)
		{
			std::vector<TypeReal> query(3);
			for (int idim=0; idim<3; idim++)
			{ 
				query[idim] = points[ii*3+idim];
			}
			tree->n_nearest(query, 1, KdtreeRes);
			int closestEdge = KdtreeRes[0].idx;		
			occ_edge[ closestEdge ] += 1;
		}
		
		// remove empty boundary faces
		removed_face = 0;
		std::vector<int> face_nvertex0( face_nvertex );
		std::vector<int> face_vertex0( face_vertex );
		face_nvertex.clear();
		face_vertex.clear();
		count = 0;
		for(int ii=0; ii< n_faces; ii++)
		{
			bool boundary = false;
			int occ_face = 0;
			for(int jj=0; jj< face_nvertex0[ii]; jj++)
			{
				int v = face_vertex0[ count + jj ];
				int vplus = face_vertex0[ count + ((jj+1)% face_nvertex0[ii]) ];
				std::pair<int, int> key = std::make_pair( v, vplus ); 
				int iedge = hedge_map.at(key);
				occ_face += occ_edge[iedge];
				if( edge_visitor[iedge] != 2 ){ boundary = true; }
			}
			if( (occ_face> 0) || (boundary==false) )
			{
				face_nvertex.push_back(face_nvertex0[ii]) ;
				for(int jj=0; jj< face_nvertex0[ii]; jj++)
				{
					face_vertex.push_back( face_vertex0[ count + jj] );
				}
			}else{ removed_face += 1; }
			count += face_nvertex0[ii];
		}	
		printf("removed %d empty boundary faces\n", removed_face );
		remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	}
	printf("snap_to_points done\n");
}

/*void snap_to_points(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &points, std::vector<TypeReal> &boundary_points)
{
	
	
	int removed_face = 1;
	int n_points = points.size()/3;
	while( removed_face > 0 )
	{
		boundary_points.clear();
		int n_vertex = vertices.size()/3;
		int n_faces = face_nvertex.size();
		std::vector<int> boundary_vertices;
		detect_boundary( vertices, face_nvertex, face_vertex, boundary_vertices);
		std::vector<bool> bvertex(n_vertex, false);
		for(int ii=0; ii< boundary_vertices.size(); ii++)
		{
			bvertex[ boundary_vertices[ii] ] = true;
		}

		// define Kdtree
		// find boundary faces
		std::vector<bool> bface(n_faces, false);
		boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_faces][3]);
		int count = 0;
		for(int ii=0; ii<n_faces; ii++)
		{
			TypeReal face_center[3] = { 0.0, 0.0, 0.0};
			for(int jj=0; jj< face_nvertex[ii]; jj++)
			{
				int iv = face_vertex[ count ];
				if( bvertex[iv]==true ){ bface[ii] = true; }
				for (int idim=0; idim<3; idim++)
				{
					face_center[idim] += vertices[iv*3+idim];
				}
				count += 1;
			}
			for (int idim=0; idim<3; idim++){ KdTreePoints[ii][idim] = face_center[idim]/face_nvertex[ii]; }
		}
		
		// for each input point, find closest face
		std::vector<int> occ_face(n_faces, 0);
		kdtree2::KDTree* tree;
		kdtree2::KDTreeResultVector KdtreeRes; 
		tree = new kdtree2::KDTree(KdTreePoints,false); 
		tree->sort_results = true;
		for(int ii=0; ii< n_points; ii++)
		{
			std::vector<TypeReal> query(3);
			for (int idim=0; idim<3; idim++)
			{ 
				query[idim] = points[ii*3+idim];
			}
			tree->n_nearest(query, 1, KdtreeRes);
			int closestFace = KdtreeRes[0].idx;		
			occ_face[ closestFace ] += 1;
			
			// the point is assigned to boundary face
			if( bface[closestFace]==true )
			{
				for (int idim=0; idim<3; idim++){ boundary_points.push_back( query[idim] ); }
			}
		}
		
		// remove empty boundary faces
		removed_face = 0;
		std::vector<int> face_nvertex0( face_nvertex );
		std::vector<int> face_vertex0( face_vertex );
		face_nvertex.clear();
		face_vertex.clear();
		count = 0;
		for(int ii=0; ii< n_faces; ii++)
		{
			if( (occ_face[ii]> 0) || (bface[ii]==false) )
			{
				face_nvertex.push_back(face_nvertex0[ii]) ;
				for(int jj=0; jj< face_nvertex0[ii]; jj++)
				{
					face_vertex.push_back( face_vertex0[ count + jj] );
				}
			}else{ removed_face += 1; }
			count += face_nvertex0[ii];
		}	
		printf("removed %d empty boundary faces\n", removed_face );
		remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	}
	printf("snap_to_points done\n");
}*/


void detect_boundary( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &boundary_vertices)
{
	
	// map each halfedge to the face incident to it
	EdgeMap hedge_map;
	std::vector<int> edge_visitor;
	int count = 0;
	int n_faces = face_nvertex.size();
	int n_edges = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			int v = face_vertex[ count + jj ];
			int vplus = face_vertex[ count + ((jj+1)% face_nvertex[ii]) ];
			std::pair<int, int> key = std::make_pair( v, vplus ); 
			if( hedge_map.count(key)==0  )
			{
				hedge_map.emplace( key, n_edges );
				hedge_map.emplace( std::make_pair( vplus, v ), n_edges );
				edge_visitor.push_back(1);
				n_edges += 1;
			}else
			{
				int edgeID = hedge_map.at( key );
				edge_visitor[ edgeID ] += 1;
			}
		}
		count += face_nvertex[ii];
	}
	// detect boundary edges and boundary faces
	std::vector<int> bvertices = std::vector<int> (vertices.size()/3, 0);
	for ( auto it = hedge_map.begin(); it != hedge_map.end(); ++it )
    {
    	std::pair<int, int> hedge = it->first;
    	if( edge_visitor[it->second] != 2 )
    	{
    		bvertices[hedge.first] = 1;
    		bvertices[hedge.second] = 1;
    	}
    }
    boundary_vertices.clear();
    for(int ii=0; ii< bvertices.size(); ii++)
    {
    	if( bvertices[ii] >0 )
    	{
    		boundary_vertices.push_back( ii );
    	}
    }
    
}


void detect_boundary_faces( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int> &boundary_faces)
{
	
	EdgeMap hedge_map;
	std::vector<int> edge_visitor;
	int count = 0;
	int n_faces = face_nvertex.size();
	int n_edges = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			int v = face_vertex[ count + jj ];
			int vplus = face_vertex[ count + ((jj+1)% face_nvertex[ii]) ];
			std::pair<int, int> key = std::make_pair( v, vplus ); 
			if( hedge_map.count(key)==0  )
			{
				hedge_map.emplace( key, n_edges );
				hedge_map.emplace( std::make_pair( vplus, v ), n_edges );
				edge_visitor.push_back(1);
				n_edges += 1;
			}else
			{
				int edgeID = hedge_map.at( key );
				edge_visitor[ edgeID ] += 1;
			}
		}
		count += face_nvertex[ii];
	}
	// detect boundary boundary faces
	count = 0;
	boundary_faces.clear();
	for(int ii=0; ii< n_faces; ii++)
	{
		bool boundary = false;
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			int v = face_vertex[ count + jj ];
			int vplus = face_vertex[ count + ((jj+1)% face_nvertex[ii]) ];
			std::pair<int, int> key = std::make_pair( v, vplus ); 
			int edgeID = hedge_map.at( key );
			
			if(	edge_visitor[ edgeID ] == 1) // boundary edge
			{
				boundary = true;
				break;
			}
		}
		boundary_faces.push_back( ii );
		count += face_nvertex[ii];
	}

    
}


int mesh_datastructures( const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, EdgeMap &edge_map, std::vector<int> &edge_nface, std::vector<int> &edge_face, std::vector<bool> &vertex_boundary, std::vector<int> &vertex_nface, std::vector<int> &vertex_face, std::vector<int> &vertex_nedge, std::vector<int> &vertex_edge )
{
	// doublet is detected by a non-boundary vertex of valence 2 caused by 2 adjacent quads sharing 2 consecutive edges
	// removed by merging the two facets incident to the vertex and removing the valence-2 vertex
	int n_vertex = *std::max_element( face_vertex.begin(), face_vertex.end() );
	n_vertex += 1; 
	int n_faces = face_nvertex.size();
	
	// edges are described by pairs of vertex indexes
	// we use a hash table to get an edge ID from the pair of vertices that form the edge
	edge_map.clear();

	
	vertex_nface = std::vector<int>( n_vertex, 0);
	std::vector< std::vector<int> > vertex_face0( n_vertex, std::vector<int>(0) );
	// keep track of edges incident to a vertex
	std::vector< std::vector<int> > vertex_edge0( n_vertex, std::vector<int>(0) );
	
	// create a data structure to store the faces incident to each edge
	int edge_count = 0;
	edge_nface.clear();
	std::vector< std::vector<int> > edge_faces( 0, std::vector<int>(0) );
	for(int ii=0; ii< n_faces; ii++)
	{
		
		for(int jj=0; jj< 4; jj++)
		{
			int i0 = face_vertex[ ii*4 + jj];
			int i1 = face_vertex[ ii*4 + ( (jj+1)%4 )];
			int edgeID;
			// add edge to the edge map (hash table with vertex pairs as keys)
			std::pair<int, int> edge_ikey = std::make_pair( i0, i1 );
			if( (edge_map.count(edge_ikey) == 0) )
            {
            	edge_map.emplace( edge_ikey, edge_count );
            	std::pair<int, int> edge_jkey = std::make_pair( i1, i0 );
            	edge_map.emplace( edge_jkey, edge_count );
            	edge_nface.push_back(1);
            	edge_faces.push_back( std::vector<int>(1, ii) );
            	edgeID = edge_count;
            	edge_count += 1;
            	
            }else
            {// the edge already exists, so add this face to its incidence
            	edgeID = edge_map.at( edge_ikey );
            	edge_nface[ edgeID] += 1;
            	edge_faces[edgeID].push_back( ii );
			}
			// keep track of faces and edges incident to a vertex
			vertex_nface[i0] += 1;
			vertex_face0[ i0 ].push_back( ii );
            vertex_edge0[i0].push_back( edgeID );
            vertex_edge0[i1].push_back( edgeID );			
		}
	}

	// assemble edge_face
	// the faces incident to an edge are stores in a array containing the faces incident to the edge
	edge_face.clear(); 
	for(int ii=0; ii< edge_count; ii++)
	{
		for(int jj=0; jj<edge_nface[ii]; jj++)
		{
			edge_face.push_back( edge_faces[ii][jj] );
		}
		//edge_faces[ii].clear();	
	}
	
	// detect edges and vertices in the boundary
	int n_boundaryedges = 0;
	std::vector<bool> edge_boundary( edge_count, false );
	vertex_boundary = std::vector<bool>( n_vertex, false );
	for ( auto it = edge_map.begin(); it != edge_map.end(); ++it )
    {
    	std::pair<int, int> edge_key = it->first;
    	int iedge = it->second;
    	if( edge_nface[iedge] == 1 )
    	{
    		edge_boundary[ iedge ] = true;
    		vertex_boundary[ edge_key.first] = true;
    		vertex_boundary[ edge_key.second] = true;
    		n_boundaryedges += 1;
    	}
    }
    //printf("%d/%d edges in the boundary\n", n_boundaryedges, edge_map.size());
    int n_boundaryvertex = 0;
    for(int ii=0; ii<n_vertex; ii++)
	{
		if( vertex_boundary[ii]==true){ n_boundaryvertex += 1; }
	}
	//printf("%d/%d vertices in the boundary\n", n_boundaryvertex, n_vertex);
	
	
	vertex_face.clear();
	for(int ii=0; ii< n_vertex; ii++)
	{
		std::vector<int> vfaces = vertex_face0[ii];
		vertex_face.insert(vertex_face.end(), vfaces.begin(), vfaces.end());
	}
	
	vertex_edge.clear();
	vertex_nedge = std::vector<int>( n_vertex );	
	for(int ii=0; ii< n_vertex; ii++)
	{
		std::vector<int> vedges = vertex_edge0[ii];
		std::sort( vedges.begin(), vedges.end() );
		auto it = std::unique( vedges.begin(), vedges.end() );
		vertex_nedge[ii] = std::distance( vedges.begin(), it );
		vertex_edge.insert( vertex_edge.end(), vedges.begin(), it );
	}
	
	
	int n_regular = 0;
	int n_boundary = 0;
	int n_nonMani = 0;
	for(int ii=0; ii< edge_nface.size(); ii++)
	{
		if( edge_nface[ii] == 1 ){ n_boundary += 1; }
		else if( edge_nface[ii]>2){ n_nonMani += 1; }
		else{ n_regular += 1; }
	}

	return n_nonMani;
}






// remove unreferenced vertices
int remove_unreferedVertex( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
	int n_vertex = vertices.size()/3;
	int n_faces = face_nvertex.size();
	
	std::vector<bool> vertexRef( n_vertex, false );
	int count = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj< face_nvertex[ii]; jj++)
		{
			vertexRef[ face_vertex[count] ] = true;
			count += 1;
		}
	}
	
	
	if( count < n_vertex )
	{
		std::vector<int> v2v(n_vertex, -1);
		std::vector<TypeReal> vertices0( vertices );
		vertices.resize(count*3);
		count = 0;
		for(int ii=0; ii< n_vertex; ii++)
		{
			if( vertexRef[ii] == true )
			{
				v2v[ii] = count;
				for(int idim=0; idim<3; idim++)
				{
					vertices[ 3*count + idim] = vertices0[ii*3+idim];
				}
				count += 1;
			}
		}
		//printf("removing %d/%d unreferenced vertices\n", n_vertex-count, n_vertex);
		
		for(int ii=0; ii< face_vertex.size(); ii++)
		{
			int oldVertex = face_vertex[ii];
			face_vertex[ii] = v2v[ oldVertex ];
		}
	}
	return n_vertex-count;	
}
          




//------------------------------------------------------------------------------


void computeError(std::vector<TypeReal> &points, std::vector<TypeReal> &normals, std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings &D, std::vector<TypeReal> &face_error, std::vector<TypeReal> &vertex_error )                                               
{

	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();   
    int n_points = points.size()/3;
    int quad_samples = D.quadrature * D.quadrature;
    TypeReal alpha = D.alpha;
    TypeReal beta = D.beta/n_faces * n_points;
    
    
	
	// sample surface at quadrature
	std::vector<TypeReal> S, Ss, St, qWeights;
	qSampleLimitSurface(vertices, face_nvertex, face_vertex, D.quadrature, S, Ss, St, qWeights);
     

	// compute surface area
	std::vector<TypeReal> face_area(n_faces, 0);
	TypeReal surface_area = 0;
	int qcount = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj<quad_samples; jj++)
		{
			TypeReal dist = 0;
			TypeReal ip = 0;			
			TypeReal E, F, G; E = 0; F = 0; G = 0;
			for(int idim=0; idim<3; idim++)
			{
				// compute first fondamental form I
				//[  s_uu \dot n   S_uv \dot n ] = [ E F ]
				//[  s_vu \dot n   S_vv \dot n ]   [ F G ]
				E += Ss[qcount*3+idim]*Ss[qcount*3+idim];
				F += Ss[qcount*3+idim]*St[qcount*3+idim];
				G += St[qcount*3+idim]*St[qcount*3+idim];	
			}
			// compute metric and curvatures
			TypeReal metric = E * G - std::pow( F, 2 );
			face_area[ii] += std::sqrt(metric) * qWeights[qcount];
			surface_area += std::sqrt(metric) * qWeights[qcount];                                  
			qcount += 1;
		}
	}
   
	
	// point 2 surface distance
	face_error = std::vector<TypeReal> (n_faces, 0);
	vertex_error = std::vector<TypeReal> (n_vertex, 0);
	// surface 2 point distance
	{
		int n_qpoints = S.size()/3;
		boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_qpoints][3]);

		for(int ii=0; ii<n_qpoints; ii++)
		{
			for (int idim=0; idim<3; idim++)
			{
				KdTreePoints[ii][idim] = S[ii*3+idim];
			}
		}
		kdtree2::KDTree* tree;
		kdtree2::KDTreeResultVector KdtreeRes; 
		tree = new kdtree2::KDTree(KdTreePoints,false); 
		tree->sort_results = true;
		for(int ii=0; ii< n_points; ii++)
		{
			std::vector<TypeReal> query(3);
			for (int idim=0; idim<3; idim++)
			{ 
				query[idim] = points[ii*3+idim];
			}
			tree->n_nearest(query, 1, KdtreeRes);
			int idclosestSPoint = KdtreeRes[0].idx;		
			int iface =  idclosestSPoint/quad_samples;
			
			TypeReal dist = 0;
			TypeReal ips = 0;	
			TypeReal ipt = 0;		
			for(int idim=0; idim<3; idim++)
			{
				// normalize it
				ips += Ss[idclosestSPoint*3+idim]* normals[ii*3+ idim];
				ipt += St[idclosestSPoint*3+idim]* normals[ii*3+ idim];
				dist += std::pow( S[idclosestSPoint*3+idim] - points[ii*3+idim], 2 );				
			}
			//face_curvature[iface] += D.beta * std::abs(ip);
			TypeReal dp = std::pow( dist, 0.5*D.lp );
			TypeReal ip = std::pow( std::abs(ips), D.lp ) + std::pow( std::abs(ipt), D.lp ); 
			TypeReal add = dp + alpha * ip;
			face_error[iface] += add;
			for(int kk=0; kk<4; kk++)
			{
				vertex_error[ face_vertex[iface*4+kk] ] += add  * face_area[ iface ];
			}
		}
	}
	
	
	// regularizer
	for(int ii=0; ii< n_faces; ii++)
	{
		int vs[4];
		for(int jj=0; jj<4; jj++)
		{
			vs[jj] = face_vertex[ii*4 + jj]	;	
		}
		for(int jj=0; jj<4; jj++)
		{
			TypeReal side = 0;
			for(int idim=0; idim<3; idim++)
			{
				side += std::pow( vertices[ vs[ (jj+1)%4 ]*3 + idim] - vertices[ vs[ jj ]*3 + idim], 2); 
			}
			face_error[ii] += beta * side;
			vertex_error[ vs[jj] ] += beta * side;
		}
	}


}






void computeCurvature( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, Settings &D, std::vector<TypeReal> &face_curvature,  std::vector<TypeReal> &vertex_curvature )                                               
{
	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();   
    
    
 	// sample surface at quadrature
 	vertex_curvature = std::vector<TypeReal>(n_vertex, 0);
 	face_curvature = std::vector<TypeReal>(n_faces, 0);
	std::vector<TypeReal> S, Ss, St, Sss, Sst, Stt, qWeights;
	qSampleD2LimitSurface(vertices, face_nvertex, face_vertex, D.quadrature, S, Ss, St, Sss, Sst, Stt, qWeights);
	int quad_samples = std::pow(D.quadrature,2);
	int count = 0;	
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj<quad_samples; jj++)
		{
			TypeReal n[3];
			// compute normal direction
			n[0] = Ss[count*3+1]*St[count*3+2] - Ss[count*3+2]*St[count*3+1];
			n[1] = Ss[count*3+2]*St[count*3] - Ss[count*3]*St[count*3+2];
			n[2] = Ss[count*3]*St[count*3+1] - Ss[count*3+1]*St[count*3];
			TypeReal inorm = ( std::pow(n[0],2) + std::pow(n[1],2) + std::pow(n[2],2) );
			if( inorm< 1e-16 )
			{
				inorm = 1;
			}else{ inorm = 1.0/std::sqrt(inorm);}
			TypeReal E, F, G; E = 0; F = 0; G = 0;
			TypeReal e, f, g; e = 0; f = 0; g = 0;
			for(int idim=0; idim<3; idim++)
			{
				// normalize it
				n[idim] *= inorm;
				// compute first fondamental form I
				//[  s_uu \dot n   S_uv \dot n ] = [ E F ]
				//[  s_vu \dot n   S_vv \dot n ]   [ F G ]
				E += Ss[count*3+idim]*Ss[count*3+idim];
				F += Ss[count*3+idim]*St[count*3+idim];
				G += St[count*3+idim]*St[count*3+idim];
				// compute second fondamental form II
				//[  s_uu \dot n   S_uv \dot n ] = [ e f ]
				//[  s_vu \dot n   S_vv \dot n ]   [ f g ]
				e += n[idim]*Sss[count*3+idim];
				f += n[idim]*Sst[count*3+idim];
				g += n[idim]*Stt[count*3+idim];
			}
			// compute metric and curvatures
			TypeReal metric = E * G - std::pow( F, 2 );
			if( std::isnan(metric) ==true){ printf("nan for metric at sample %d\n", ii);}
			//gCurvature[ii] = (f * g - std::pow( f, 2 ))/metric;
			TypeReal meanCurvature = 0.5*( e * G + g * E - 2* f * F )/metric;
			
			TypeReal add = std::abs(meanCurvature) * qWeights[count];
			face_curvature[ii] += add;
			for(int kk=0; kk<4; kk++)
			{
				vertex_curvature[ face_vertex[ii*4+kk] ] += add;
			}
			
			count += 1;
		}
	}

}



/*=-----------------------------------------------------------------------------*/

TypeReal cost_quad( TypeReal p0x, TypeReal p0y, TypeReal p0z, TypeReal p1x, TypeReal p1y, TypeReal p1z, TypeReal p2x, TypeReal p2y, TypeReal p2z, TypeReal p3x, TypeReal p3y, TypeReal p3z )
{
	// measure of quad quality, the smalles the cost the higher the quad quality
	// our cost measures how far the quad angles deviate from 90 degrees and how curved the quad
	TypeReal cost = 0;
	
	// vectors between quad vertices
	TypeReal v01[3] = { p1x - p0x, p1y - p0y, p1z - p0z }; // vector from v0 to v1
	TypeReal v12[3] = { p2x - p1x, p2y - p1y, p2z - p1z }; // vector from v1 to v2
	TypeReal v23[3] = { p3x - p2x, p3y - p2y, p3z - p2z }; // vector from v2 to v3
	TypeReal v30[3] = { p0x - p3x, p0y - p3y, p0z - p3z }; // vector from v3 to v0
	TypeReal normals[3*4];
	
	TypeReal norm01 = 0; TypeReal ip0 = 0;
	TypeReal norm12 = 0; TypeReal ip1 = 0;
	TypeReal norm23 = 0; TypeReal ip2 = 0;
	TypeReal norm30 = 0; TypeReal ip3 = 0;
	for(int idim=0; idim<3; idim++)
	{
		ip0 -= v01[idim]*v30[idim];
		ip1 -= v12[idim]*v01[idim];
		ip2 -= v23[idim]*v12[idim];
		ip3 -= v30[idim]*v23[idim];
		normals[idim] = -v01[ (idim+1)%3 ] * v30[ (idim+2)%3 ] + v01[ (idim+2)%3 ] * v30[ (idim+1)%3 ];
		normals[3+idim] = -v12[ (idim+1)%3 ] * v01[ (idim+2)%3 ] + v12[ (idim+2)%3 ] * v01[ (idim+1)%3 ];
		normals[6+idim] = -v23[ (idim+1)%3 ] * v12[ (idim+2)%3 ] + v23[ (idim+2)%3 ] * v12[ (idim+1)%3 ];
		normals[9+idim] = -v30[ (idim+1)%3 ] * v23[ (idim+2)%3 ] + v30[ (idim+2)%3 ] * v23[ (idim+1)%3 ];
		norm01 += std::pow(v01[idim], 2);
		norm12 += std::pow(v12[idim], 2);
		norm23 += std::pow(v23[idim], 2);
		norm30 += std::pow(v30[idim], 2);
	}
	norm01 = std::sqrt( norm01 );
	norm12 = std::sqrt( norm12 );
	norm23 = std::sqrt( norm23 );
	norm30 = std::sqrt( norm30 );
	
	std::vector<TypeReal> cangles(4);
	TypeReal eps = 1e-6; // cosinus of quad angles
	cangles[0] = std::abs(ip0)/(norm01*norm30 + eps);
	cangles[1] = std::abs(ip1)/(norm01*norm12 + eps);
	cangles[2] = std::abs(ip2)/(norm12*norm23 + eps);
	cangles[3] = std::abs(ip3)/(norm23*norm30 + eps);
	
	
	cost = cangles[0];
	int imax_angle = 0;
	for(int ii=1; ii<4; ii++)
	{
		if( cost> cangles[ii] ){ cost = cangles[ii]; imax_angle = ii; }
	}
	
	TypeReal ip = 0;
	int iopposite = (imax_angle +2)%4;
	TypeReal norm_max = 0;
	TypeReal norm_opp = 0;
	for(int idim=0; idim<3; idim++)
	{
		ip += normals[ imax_angle*3 + idim] * normals[ iopposite*3 + idim];
		norm_max += std::pow(normals[ imax_angle*3 + idim], 2);
		norm_opp += std::pow(normals[ iopposite*3 + idim], 2);
	}
	ip /= ( eps + std::sqrt(norm_max * norm_opp) );
	
	return cost + ( 1.0 - ip );
}



bool vertex_split( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &cost, int n_splits=1 )
{


        int n_faces = face_nvertex.size();
        int n_vertex = vertices.size()/3;

        // create an array with the vertex degree and the faces incident to each vertex
        std::vector<double> cost_stars(n_vertex, -1);
        std::vector<int> vdegree(n_vertex, 0);
        std::vector< std::vector<int> > faces_invertex( n_vertex, std::vector<int>(0) );
        int count = 0;
        for(int ii=0; ii<n_faces; ii++)
        {
                for(int jj=0; jj<face_nvertex[ii]; jj++)
                {
                        int iv = face_vertex[count];
                        faces_invertex[ iv].push_back( ii );
                        vdegree[iv] += 1;
                        count += 1;
                }
        }
        // split a vertex ( with valence > 3) into two to create a new quad
        for(int ii=0; ii<n_vertex; ii++)
        {
                if( vdegree[ii] <4 ){ cost[ii] = -1; }
        }


        bool ret;
        for( int isplit=0; isplit< n_splits; isplit++)
        {
                int ivertex = std::distance( cost.begin(), std::max_element( cost.begin(), cost.end()));
                if( cost[ ivertex] == -1 )
                {
                        ret = false;
                        break;
                }else
                {
                        cost[ ivertex ] = -1;
                        cost.push_back( -1) ;
                        vertices.resize(n_vertex*3+3);
                        int degree = vdegree[ivertex];
                        //printf("splitting vertex %d, with degree %d\n", ivertex, degree);

                        // get the vertices in the ring of the vertex to be split
                        std::vector<int> vring0( degree*3 );
                        std::vector<int> incident_faces = faces_invertex[ ivertex ];
                        count = 0; // count the vertices in vring0
                        for( int ii=0; ii< degree; ii++ )
                        {
                                int iface = incident_faces[ii];
                                int start = -1;
                                for(int jj=0; jj<4; jj++)
                                {
                                        if( face_vertex[ 4*iface + jj ] == ivertex ){ start = jj; break;}
                                }
                                for(int jj=1; jj<4; jj++)
                                {
                                        int indx = (start+jj)%4;
                                        vring0[ count ] = face_vertex[ 4*iface + indx ];
                                        count += 1;
                                }
                        }// the vertices in the ring are not ordered consistently with the orientation of the faces
                        std::vector<int> vring( 2*degree, -1 ); // sort them
                        vring[0] = vring0[0];
                        vring[1] = vring0[1];
                        count = 2; // count the vertices in vring
                        int endv = vring0[2];
                        while( endv != vring[0] )
                        {
							for( int ii=0; ii< degree; ii++ )
							{
									if( vring0[ ii*3 ] == endv )
									{ // this is the next face to be added to the ring
											vring[count] = vring0[ii*3];
											count += 1;
											vring[count] = vring0[ii*3+1];
											endv = vring0[ii*3+2];
											count += 1;
									}
							}
						}
                        // do not split vertices in the 2-ring neighbourhood of ivertex
                        if( n_splits>1 )
                        {
                                for( int kk=0; kk< vring.size(); kk++ )
                                {
                                        int jvertex = vring[kk]; // vertex in 1 ring neighbourhood
                                        cost[ jvertex ] = -1;

                                        /*for( int ii=0; ii< vdegree[jvertex]; ii++ )
                                        {
                                                int iface = faces_invertex[ jvertex ][ii];
                                                for(int jj=0; jj<4; jj++) //  kvertex 2-ring neighbourhood
                                                {
                                                        int kvertex = face_vertex[ 4*iface + jj ];
                                                        cost[ kvertex ] = -1;
                                                }
                                        }*/
                                }
                        }

                        // find the optimal shift
                        TypeReal opt_split = std::numeric_limits<TypeReal>::max();
                        std::vector<int> new_optfaces(4 * degree);
                        std::vector<TypeReal> optvertices;
                        std::vector<int> central_optface(4);
                        int iopt = 0;
                        for( int irot = 0; irot< degree; irot++ )
                        {
                                std::rotate(vring.begin(), vring.begin()+2, vring.end());
                                // compute position and id of new vertices
                                int v0new = ivertex;
                                int v1new = n_vertex;
                                TypeReal p0new[3];
                                TypeReal p1new[3];
                                int idr0 = 0;
                                int idv0 = vring[idr0];
                                int idr2 = degree;
                                if( idr2 %2 == 1 ){ idr2 += 1; }
                                int idv2 = vring[idr2];
                                int idr1 = idr2/ 2;
                                if( idr1 % 2 ==1 ){ idr1 += 1; }
                                int idv1 = vring[ idr1 ];
                                int idr3 = idr2 + ( degree*2 - idr2 )/2;
                                if( idr3 % 2 ==1 ){ idr3 += 1; }
                                int idv3 = vring[ idr3];
                                std::vector<int> central_face = {  v0new, idv1, v1new, idv3 };
                                for(int idim=0; idim<3; idim++)
                                {
                                        p0new[idim] = 0.5*( vertices[ivertex*3 + idim] + vertices[ idv1*3 + idim]  );
                                        p1new[idim] = 0.5*( vertices[ivertex*3 + idim] + vertices[ idv3*3 + idim]  );
                                }
                                // identify new faces
                                std::vector<int> new_faces(4 * degree);
                                count = 0;
                                bool v0 = true;
                                std::vector<TypeReal> quad_vlocs;
                                for(int ii=0; ii< degree; ii++)
                                {
                                        new_faces[ii*4] = vring[count];
                                        count += 1;
                                        new_faces[ii*4+1] = vring[count];
                                        count += 1;
                                        new_faces[ii*4+2] = vring[count%(2*degree)];
                                        if( v0 ){ new_faces[ii*4+3] = v0new;}else{ new_faces[ii*4+3] = v1new; }
                                        if( new_faces[ii*4+2] == idv1 ){ v0 = false; }else if( new_faces[ii*4+2] == idv3 ){ v0 = true; }
                                }

                                // modify the location of new vertices
                                for(int idim=0; idim<3; idim++)
                                {
                                        vertices[v0new*3+idim] = p0new[idim];
                                        vertices[v1new*3+idim] = p1new[idim];;
                                }

                                TypeReal cost_split = 0;
                                for( int kk=0; kk< new_faces.size()/4; kk++ )
                                {
                                        cost_split  +=  cost_quad( vertices[ 3*new_faces[kk*4] ], vertices[ 3*new_faces[kk*4] +1], vertices[ 3*new_faces[kk*4] +2],
                                                                                           vertices[ 3*new_faces[kk*4+1] ], vertices[ 3*new_faces[kk*4+1] +1], vertices[ 3*new_faces[kk*4+1] +2],
                                                                                           vertices[ 3*new_faces[kk*4+2] ], vertices[ 3*new_faces[kk*4+2] +1], vertices[ 3*new_faces[kk*4+2] +2],
                                                                                           vertices[ 3*new_faces[kk*4+3] ], vertices[ 3*new_faces[kk*4+3] +1], vertices[ 3*new_faces[kk*4+3] +2]);
                                }
                                cost_split  +=  cost_quad(  vertices[ 3*central_face[0] ], vertices[ 3*central_face[0] +1], vertices[ 3*central_face[0] +2],
                                                                                        vertices[ 3*central_face[1] ], vertices[ 3*central_face[1] +1], vertices[ 3*central_face[1] +2],
                                                                                        vertices[ 3*central_face[2] ], vertices[ 3*central_face[2] +1], vertices[ 3*central_face[2] +2],
                                                                                        vertices[ 3*central_face[3] ], vertices[ 3*central_face[3] +1], vertices[ 3*central_face[3] +2]);

                                if( cost_split< opt_split )
                                {
                                        opt_split = cost_split;
                                        new_optfaces = new_faces;
                                        optvertices = vertices;
                                        central_optface = central_face;
                                }
                        }
                        // modify the description of vertices
                        vertices = optvertices;
                        // modify the description of faces
                        count = 0;
                        for(int ii=0; ii<n_faces; ii++)
                        {
                                int v0 = face_vertex[ii*4];
                                int v1 = face_vertex[ii*4+1];
                                int v2 = face_vertex[ii*4+2];
                                int v3 = face_vertex[ii*4+3];
                                if( (v0==ivertex) || (v1==ivertex) || (v2==ivertex) || (v3==ivertex) )
                                {
                                        // the face has to be modified
                                        for(int jj=0; jj<4; jj++)
                                        {
                                                face_vertex[ii*4+jj] = new_optfaces[ count*4+jj ];
                                        }
                                        count += 1;
                                }
                        }
                        // add new face
                        face_nvertex.push_back(4);
                        for(int ii=0; ii<4; ii++)
                        {
                                face_vertex.push_back( central_optface[ii] );
                        }
                        faces_invertex.push_back( std::vector<int>(0) );
                        n_vertex += 1;
                        ret= true;
                }
        }
        return ret;
}










int remove_area0Faces( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	int n_vertex = vertices.size()/3;
	int n_faces = face_nvertex.size();
	std::vector<int> face_0vertex(n_faces);
	std::vector<bool> remove_face( n_faces, false );
	int count = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		face_0vertex[ii] = count;
		TypeReal Tarea = 0;
		std::vector<int> verts;
		verts.insert( verts.end(), face_vertex.begin() + face_0vertex[ii], face_vertex.begin() + face_0vertex[ii] + face_nvertex[ii] );
		bool repeated_vertex = false;
		for(int jj=0; jj< verts.size(); jj++)
		{
			for(int kk=jj+1; kk< verts.size(); kk++)
			{
				if( verts[jj] == verts[kk] )
				{
					repeated_vertex = true;
					break;
				}
			}
		}
		if( repeated_vertex ){ remove_face[ii] = true; }
		
		count += face_nvertex[ii];
	}
	
	count = 0;
	for(int ii=n_faces-1; ii>= 0; ii--)
	{
		if( remove_face[ii] == true )
		{
			//printf("removing face %d: %d %d %d %d\n", ii, face_vertex[ face_0vertex[ii] ], face_vertex[ face_0vertex[ii] +1], face_vertex[ face_0vertex[ii] +2], face_vertex[ face_0vertex[ii] +3] );
			count += 1;
			face_nvertex.erase( face_nvertex.begin()+ii );
			face_vertex.erase( face_vertex.begin()+ face_0vertex[ii], face_vertex.begin()+ face_0vertex[ii]+ face_nvertex[ii] );
		}
	}
	return count;	
}









bool diagonal_collapse( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<TypeReal> &cost, int n_collapses=1 )
{

	int n_vertex = vertices.size()/3;
	int n_faces = face_nvertex.size();
	TypeReal maxCost = std::numeric_limits<TypeReal>::max();
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	if( cost.size() != n_faces )
	{
		//printf("computing the cost\n");
		cost = std::vector<TypeReal>(n_faces, maxCost);
		for(int ii=0; ii< n_faces; ii++)
		{
			TypeReal n0[3];
			TypeReal n1[3];
			int v0 = face_vertex[ii*4];
			int v1 = face_vertex[ii*4+1];
			int v2 = face_vertex[ii*4+2];
			int v3 = face_vertex[ii*4+3];				
			for( int idim=0; idim<3; idim++)
			{
				int k = (idim+1)%3;
				int l = (idim+2)%3;
				n0[idim] = (vertices[ v1*3 + k ] - vertices[ v0*3 + k ]) * (vertices[ v3*3 + l ] - vertices[ v0*3 + l ]) - 	(vertices[ v1*3 + l ] - vertices[ v0*3 + l ]) * (vertices[ v3*3 + k ] - vertices[ v0*3 + k ]);
				n1[idim] = (vertices[ v1*3 + l ] - vertices[ v2*3 + l ]) * (vertices[ v3*3 + k ] - vertices[ v2*3 + k ]) - 	(vertices[ v1*3 + k ] - vertices[ v2*3 + k ]) * (vertices[ v3*3 + l ] - vertices[ v2*3 + l ]);
			}
			// compute the area of the two triangles and the angle
			TypeReal norm1 = 0;
			TypeReal norm2 = 0;
			TypeReal ip = 0;
			for( int idim=0; idim<3; idim++)
			{
				norm1 += std::pow( n0[idim], 2);
				norm2 += std::pow( n1[idim], 2);
				ip += n0[idim] * n1[idim];
			}
			norm1 = std::sqrt(norm1);
			norm2 = std::sqrt(norm2);
			// the cost is the are weighted by a curvature proxy
			if( norm1*norm2 > eps ){ ip = ip/(norm1*norm2); }
			cost[ ii ] = 0.5*(  norm1 + norm2 ) * ( 1.0 - ip );
		}
	}else
	{
		for(int ii=0; ii< n_faces; ii++)
		{
			if( std::isnan(cost[ii]) || std::isinf( cost[ii] ) ){ cost[ii] = maxCost; }
		}
	}

	
	// collapse a quad trhough its shortest diagonal
	// by merging the two vertices ivertex < jvertex that define the diagonal 
	// 1. compute the location of a new vertex: for simplicity the midpoint of ivertex and jvertex
	// 2. mark the quad to be removed from the face_vertex list
	// 3. update the position of ivertex
	// 4. traverse the face_vertex list and update the indices of the vertex as follows
	//						- if vertex id == jvertex, substitute it for ivertex	
	bool ret;
	int n_collapsed = 0;
	for( int icollapse=0; icollapse< n_collapses; icollapse++)
	{
		int iquad = std::distance( cost.begin(), std::min_element( cost.begin(), cost.end()));
		//printf("mincost %f, maxcost %f\n", *std::min_element(cost.begin(), cost.end()), *std::max_element(cost.begin(), cost.end()) );

		if( cost[ iquad ] == maxCost )
		{ 
			ret = false;
			break;
		}else
		{
			//printf("collapsing quad %d with cost %f\n", iquad, cost[iquad] );
			cost[ iquad ] = maxCost;
			
			// get the shortest diagonal of the quad to define ivertex < jvertex
			int ivertex, jvertex;
			TypeReal diag1 = 0;
			TypeReal diag2 = 0;
			int v0 = face_vertex[ iquad*4 ];
			int v1 = face_vertex[ iquad*4 + 1];
			int v2 = face_vertex[ iquad*4 + 2];
			int v3 = face_vertex[ iquad*4 + 3];
			for( int idim=0; idim<3; idim++)
			{
				diag1 += std::pow( vertices[ v0*3+idim] - vertices[ v2*3+idim], 2 );
				diag2 += std::pow( vertices[ v1*3+idim] - vertices[ v3*3+idim], 2 );
			}
			if( diag1 < diag2 )
			{
				if( v0 < v2 ){ ivertex = v0; jvertex = v2;}
				else{ ivertex = v2; jvertex = v0; }
			}else{
				if( v1 < v3 ){ ivertex = v1; jvertex = v3;}
				else{ ivertex = v3; jvertex = v1; }
			}
			//printf("ivertex %d, jvertex %d\n", ivertex, jvertex);
			
			
			// 1) and 3) compute the location of the new vertex and update the vertex list
			for( int idim=0; idim<3; idim++)
			{
				vertices[ ivertex*3 + idim] = 0.25*( vertices[ v0*3+idim] + vertices[ v1*3+idim] + vertices[ v2*3+idim] + vertices[ v3*3+idim] );	
			}
			
			// 4 update the face_vertex list
			for(int jj=0; jj<4; jj++)
			{
				face_vertex[ iquad*4 + jj] = -1;
			}
			
			for( int ii=0; ii< n_faces; ii++ )
			{
				bool in_ring = false;
				for(int jj=0; jj<face_nvertex[ii]; jj++)
				{
					int iv = face_vertex[ ii*4+jj ];
					if( iv == jvertex )
					{
						 face_vertex[ ii*4+jj ] = ivertex;
					}
					if( (iv==v0) || (iv==v1) || (iv==v2) || (iv==v3) ){ in_ring = true; }
				}  
				if( in_ring ) // prevent collapsing quads that have changed shape
				{
					cost[ ii ] = maxCost;	
				}
			}

			n_collapsed += 1;
			ret = true;
		}
	}
	
	
	// remove empty faces
	//printf("%d/%d quads collapsed\n", n_collapsed, n_faces);
	if( n_collapsed>0 )
	{
		int count = 0;
		std::vector<int> face_vertex0(face_vertex);
		face_nvertex.resize( n_faces-n_collapsed);
		face_vertex.resize( (n_faces-n_collapsed)*4 );
		for( int ii=0; ii< n_faces; ii++ )
		{
			if( face_vertex0[ii*4] != -1 )
			{  
				for(int jj=0; jj<4; jj++)
				{
					face_vertex[ count ] = face_vertex0[ii*4+jj];
					count += 1;
				}
			}
		}
		
		
		// remove unreferenced vertices
		remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	}
	
	return ret;
}




/*------------------- cleaning operations for quad mesh ------------------------*/

// remove doublet
int remove_doublet( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
	// doublet is detected by a non-boundary vertex of valence 2 caused by 2 adjacent quads sharing 2 consecutive edges
	// removed by merging the two facets incident to the vertex and removing the valence-2 vertex
	int n_vertex = vertices.size()/3; 
	int n_faces = face_nvertex.size();
	
	std::vector<bool> vertex_boundary;
	EdgeMap edge_map;
	std::vector<int> edge_face, edge_nface, vertex_nface, vertex_face, vertex_nedge, vertex_edge;
	mesh_datastructures( face_nvertex, face_vertex, edge_map, edge_nface, edge_face, vertex_boundary, vertex_nface, vertex_face, vertex_nedge, vertex_edge );

	std::vector<int> vertex_0face(n_vertex, 0);
	std::vector<int> vertex_0edge(n_vertex, 0);
	for(int ii=1; ii<n_vertex; ii++)
	{
		vertex_0face[ii] = vertex_0face[ii-1] + vertex_nface[ii-1];
		vertex_0edge[ii] = vertex_0edge[ii-1] + vertex_nedge[ii-1];
	}

	
	// detect vertices with valence 2 that are not on the boundary
	bool stop = false;
	int n_doublets = 0;
	for(int ii=n_vertex-1; ii>=0; ii--)
	{
		if( (vertex_nedge[ii]==2) &&( vertex_boundary[ii] == false ) ) // vertex has valence 2
		{
		  // it is a doublet and needs to be removed
			
			//stop = false;
			//printf("doubled detected at vertex %d with valence %d\n", ii, vertex_nedge[ii] );
			n_doublets += 1;
			// merge 2 faces incident to vertex
			int iface = vertex_face[ vertex_0face[ii] ];
			int jface = vertex_face[ vertex_0face[ii] + 1];
			if(  iface > jface )
			{ 
				int aux = iface;
				iface = jface;
				jface = aux;
			}
			int ivertex, jvertex; // local index of vertex ii in the incident faces
			std::vector<int> oldface_verts(8);
			for(int jj=0; jj<4; jj++)
			{
			  int iv = face_vertex[iface*4+ jj];
			  oldface_verts[jj] = iv;
			  int jv = face_vertex[jface*4+ jj];
			  oldface_verts[4+jj] = jv;
			  if( iv == ii )
			  {
				  ivertex = jj;
			  }
			  if( jv == ii )
			  {
				  jvertex = jj;
			  }		
			  //printf("iface %d\n", iv);
			  //printf("jface %d\n", jv);
			}
			std::vector<int> verts(4); // vertices of new faces, correctly oriented
			for(int jj=0; jj<3; jj++)
			{
				int offset = (ivertex+jj+1)%4;
				verts[jj] = oldface_verts[offset];
			}
			int offset = (jvertex+2)%4;
			verts[3] = oldface_verts[4 + offset];
			//printf("new face %d %d %d %d\n", verts[0], verts[1], verts[2], verts[3]);
		  
			// update valence of vertices in the ring 
			vertex_nedge[ verts[0] ] -= 1;
			vertex_nedge[ verts[2] ] -= 1;
			vertex_nedge[ii] = 0;
			//printf("update valence vertices %d %d to %d %d\n", verts[0], verts[2], vertex_nedge[ verts[0] ], vertex_nedge[ verts[2] ]);
		  
			// remove old faces of the face_vertex list and add the new one
			n_faces -= 1;
			for(int jj=0; jj<4; jj++)
			{
			  face_vertex[iface*4+jj] = verts[jj];
			}
			face_vertex.erase( face_vertex.begin() + jface*4, face_vertex.begin() + jface*4 + 4 );
			face_nvertex.erase( face_nvertex.begin() + jface );
			
		  
		  // remove vertex
		  n_vertex -= 1;
		  vertices.erase( vertices.begin() + ii*3, vertices.begin() + ii*3+3);
		  
		  for(int jj=0; jj<face_vertex.size(); jj++)
		  {
			  if( face_vertex[jj] == ii ){ printf("ERROR, vertex %d should be unreferenced but appear in face %d\n", ii, jj/4); }
			  else if( face_vertex[jj] > ii ){ face_vertex[jj] -= 1; }
		  }
		  
		  

		  break;
		}// doublet
		
	}// end loop over n_vertex
	return n_doublets;
}

// remove singlet
int remove_singlet( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex)
{
	// doublet is detected by a non-boundary vertex of valence 2 caused by 2 adjacent quads sharing 2 consecutive edges
	// removed by merging the two facets incident to the vertex and removing the valence-2 vertex
	int n_vertex = vertices.size()/3; 
	int n_faces = face_nvertex.size();
	
	std::vector<bool> vertex_boundary;
	EdgeMap edge_map;
	std::vector<int> edge_face, edge_nface, vertex_nface, vertex_face, vertex_nedge, vertex_edge;
	mesh_datastructures( face_nvertex, face_vertex, edge_map, edge_nface, edge_face, vertex_boundary, vertex_nface, vertex_face, vertex_nedge, vertex_edge );

	std::vector<int> vertex_0face(n_vertex, 0);
	std::vector<int> vertex_0edge(n_vertex, 0);
	for(int ii=1; ii<n_vertex; ii++)
	{
		vertex_0face[ii] = vertex_0face[ii-1] + vertex_nface[ii-1];
		vertex_0edge[ii] = vertex_0edge[ii-1] + vertex_nedge[ii-1];
	}

	
	// detect vertices with valence 2 that are not on the boundary
	bool stop = false;
	int n_singlets = 0;
	for(int ii=n_vertex-1; ii>=0; ii--)
	{
		if( (vertex_nedge[ii]==1) &&( vertex_boundary[ii] == false ) ) // vertex has valence 2
		{
		  // it is a singlet and needs to be removed
			
			//stop = false;
			//printf("doubled detected at vertex %d with valence %d\n", ii, vertex_nedge[ii] );
			n_singlets += 1;
			// remove faces incident to vertex
			int iface = vertex_face[ vertex_0face[ii] ];
			face_vertex.erase( face_vertex.begin() + iface*4, face_vertex.begin() + iface*4 + 4 );
			face_nvertex.erase( face_nvertex.begin() + iface );
			n_faces -= 1;
			
			// remove vertex ii
			n_vertex -= 1;
			vertices.erase( vertices.begin() + ii*3, vertices.begin() + ii*3+3);
			for(int jj=0; jj<face_vertex.size(); jj++)
			{
			  if( face_vertex[jj] == ii ){ printf("ERROR, vertex %d should be unreferenced but appear in face %d\n", ii, jj/4); }
			  else if( face_vertex[jj] > ii ){ face_vertex[jj] -= 1; }
			}

		  break;
		}// singlet
		
	}// end loop over n_vertex
	return n_singlets;
}


void clean_mesh( std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex )
{
		int n_doublets = 0;
		int inc = 1;
		while( inc>0)
		{
			while( inc>0 )
			{ 
				inc = remove_doublet( vertices, face_nvertex, face_vertex);
				n_doublets += inc;
			}
			//if( n_doublets>0 ){ printf("%d doublets removed\n", n_doublets); };
			
			int n_singlets = 0;
			inc = 1;
			while( inc>0 )
			{ 
				inc = remove_singlet( vertices, face_nvertex, face_vertex);
				n_singlets += inc;
			}
			//if( n_singlets>0 ){ printf("%d singlets removed\n", n_singlets); }
		}
}
