#include <cassert>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <time.h>
#include <utility>

#include "TypeDef.h"
#include "octree.h"
#include "Settings.h"
#include "IO.h"
#include "EigenTypeDef.h"
#include "sds.h"
#include "optimizationSubdivisionSurface.h"
#include "optimizeControlMesh.h"
#include "MeshOp.h"
#include "quadBlossom.h"
#include "triMeshDecimation.h"










int main(int argc, char *argv[]) {
  
	
	
	Settings D;
	bool close_holes = true;
    D.parseCommandLine(argc, argv);
    if(D.visualize>0){ D.printOptions();}
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    clock_t t0 = clock();
    
    
    std::vector<TypeReal> vertices, points, sdfPoints;
    std::vector<int> face_nvertex, face_vertex;
    std::vector<int> meshFace_nvertex, meshFace_vertex, sdfFace_nvertex, sdfFace_vertex;
    std::vector<TypeReal> center, dummy;
    size_t n_vertex, n_faces;
	
    // read input mesh
    std::string format = D.cloudFile.substr(D.cloudFile.size()-3,3);
    if( format.compare( std::string("off"))==0 ){
    	read_off( D.cloudFile, points, meshFace_nvertex, meshFace_vertex);    
    }else if( format.compare( std::string("ply"))==0 ){
    	read_ply( D.cloudFile, points, meshFace_nvertex, meshFace_vertex, dummy, true);
    }else{
    	printf("unknown input file type, expecting .ply or .off\n");
    	return(0);
    }
    size_t n_points = points.size()/3;  
    size_t n_meshFaces = meshFace_nvertex.size();
    
    
    
    // compute scale
    TypeReal points_min[3] = {points[0], points[1], points[2]};
    TypeReal points_max[3] = {points[0], points[1], points[2]};
    for(int ii=0; ii< n_points; ii++)
	{
		for(int idim=0; idim<3; idim++)
		{
			if( points[ii*3+idim] < points_min[idim] ){ points_min[idim] = points[ii*3+idim]; }
			else if( points[ii*3+idim] > points_max[idim] ){ points_max[idim] = points[ii*3+idim]; }
		}
	}
    TypeReal volumeBBox = ( points_max[0] - points_min[0] ) * ( points_max[1] - points_min[1] ) * ( points_max[2] - points_min[2] );
    TypeReal scale = std::pow( volumeBBox, 1.0/3 );
	D.beta /= scale;  
    
 	// compute length of mesh edges
    if( D.octreeLevel <= 0 )
	{
		TypeReal average_dist = 0;
		int count = 0;
		// compute the avreage length of the edges of the original mesh
		// useful to determine the length of the octree at maximum depth
		TypeReal min_tside = std::numeric_limits<TypeReal>::max();
		for(int ii=0; ii< meshFace_nvertex.size(); ii++)
		{
			int nsides = meshFace_nvertex[ii];
			TypeReal average_side = 0;
			for(int jj=0; jj<nsides ; jj++)
			{
				int iv = meshFace_vertex[ count + jj];
				int jv = meshFace_vertex[ count + ((jj+1)%nsides )];
				TypeReal dist = 0;
				for(int idim=0; idim<3; idim++)
				{
					dist += std::pow( points[iv*3+ idim] - points[jv*3+idim], 2 );
				}
				average_side += std::sqrt(dist);
			}
			average_side /= nsides;
			if( average_side < min_tside ){ min_tside = average_side; }
			count += nsides;
		}
		D.octreeLevel = std::floor( std::log( scale / min_tside) / std::log(2.0) );
	}
	if( D.octreeLevel > 9 ){ D.octreeLevel = 9; }
	if(D.visualize>0){ printf("octree depth set to %lu\n", D.octreeLevel);}
 

	

    n_points = points.size()/3;  
    n_meshFaces = meshFace_nvertex.size();
    MeshOp meshOp(points, meshFace_nvertex, meshFace_vertex); 
    std::vector<TypeReal> normals;
    meshOp.getNormals( normals );
    if(D.visualize>0){ printf("input mesh read with %lu points and %lu faces\n", n_points, n_meshFaces);}

	
	
    std::vector<TypeReal> weights(n_points, 1.0);
	
	

    // create octree from pointcloud 
    int octreeLevel = D.octreeLevel;
    int octreeLevel0 = octreeLevel - D.uniR; 
	Octree tree( points, octreeLevel0, 1.1, false );
    


   	// read initial control mesh
	if( D.iniFile.size()>0)
	{
		read_ply( D.iniFile, vertices, face_nvertex, face_vertex, dummy, true); 
		if( vertices.size () > D.n_controlPoints*3 )
		{
			std::vector<TypeReal> vertices0(vertices);
			std::vector<int> face_nvertex0( face_nvertex);
			std::vector<int> face_vertex0(face_vertex);
			triMeshDecimation( vertices0, face_nvertex0, face_vertex0, D.n_controlPoints - 1, vertices, face_nvertex, face_vertex, D.visualize>0);
		}
	}
	else
	{
		triMeshDecimation( points, meshFace_nvertex, meshFace_vertex, D.n_controlPoints-1, vertices, face_nvertex, face_vertex, D.visualize>0);
	}
	// find boundaries to ensure that we can find a perfect match
	std::vector<int> boundary_faces;
	clean_boundary( vertices, face_nvertex, face_vertex, boundary_faces, D.visualize>0 );
	if( boundary_faces.size()%2  >  0  )
	{ // split a boundary face to have an even number of triangles to match
		int iface = boundary_faces[0];
		int v0 = face_vertex[3*iface];
		int v1 = face_vertex[3*iface+1];
		int v2 = face_vertex[3*iface+2];
		int vnew = vertices.size()/3;
		for(int idim=0; idim<3; idim++)
		{
			vertices.push_back( 0.5*(vertices[ v0*3 +idim] + vertices[ v2*3 +idim] )  );
		}
		face_nvertex.push_back(3);
		face_vertex[ iface*3+2 ] = vnew;
		face_vertex.push_back( vnew );
		face_vertex.push_back( v1 );
		face_vertex.push_back( v2 );
		printf("added a boundary face to allow for a perfect match\n");
	}   	
	quadBlossom(vertices, face_nvertex, face_vertex, D.visualize>0);
	remove_area0Faces( vertices, face_nvertex, face_vertex);
	remove_unreferedVertex( vertices, face_nvertex, face_vertex);
	clean_mesh( vertices, face_nvertex, face_vertex );


	// detect boundaries
	std::vector<int> boundary_vertices;
	detect_boundary( vertices, face_nvertex, face_vertex, boundary_vertices);
	
	n_vertex = vertices.size()/3;
	if(D.visualize>0){printf("mesh initialized with %lu vertices and %lu faces\n", n_vertex, face_nvertex.size() );}

	
	//vertex_stats( vertices,  face_nvertex,  face_vertex );
	if( D.uniR < 0 ){ D.uniR = std::ceil( std::log( n_points*1.0/n_vertex )/ std::log( 4.0 ) ); if(D.visualize>0){ printf("set uniR to %lu\n", D.uniR);} }

	
	TypeReal energy=-1;
    if((D.visualize>0)&&(D.uniR>0)){  printf(" --------------- MULTIRESOLUTION FIT ----------------------\n"); }
	
	
	// optimize distance approximation
	for(int ii=0; ii<D.uniR; ii++)
	{		
		std::vector<TypeReal> centroid0s, centroid0_weights, centroid0_normals;
		tree.averagePerLeaf( points, centroid0s, centroid0_weights );
		tree.averagePerLeaf( normals, centroid0_normals, centroid0_weights );
		for(int ii=0; ii<centroid0_normals.size()/3; ii++)
		{
			TypeReal norm = 0;
			for(int idim=0; idim<3; idim++)
			{
				norm += std::pow( centroid0_normals[ii*3+idim], 2);
			}
			norm += std::sqrt(norm);
			if( norm> eps )
			{
				for(int idim=0; idim<3; idim++)
				{
					centroid0_normals[ii*3+idim] /= norm;
				}
			}
		}   

		if( D.alg == 0 )
		{
			//approxDistance_MM( vertices, face_nvertex, face_vertex, D, centroid0s, centroid0_normals, ii);
			Distance_MM( vertices, face_nvertex, face_vertex, D, centroid0s, centroid0_normals, boundary_vertices);
		}
		else if( D.alg == 1)
		{
			sqrDistance_1storderApprox( vertices, face_nvertex, face_vertex, D, centroid0s, centroid0_normals);
		}
		else{
			LeastSquaresOptimization_LM( vertices, face_nvertex, face_vertex, D, centroid0s, centroid0_normals );
		}

		
		tree.refineOctree(1);
		D.epsilon /= 10;
	}
	
	
	
	
		
	if(D.visualize>0){ printf(" --------------- FITTING SUBDIVISION SURFACE  with %lu vertices----------------------\n", vertices.size()/3);}
	
	
	if( D.alg == 0 )
	{
		energy = Distance_MM( vertices, face_nvertex, face_vertex, D, points, normals, boundary_vertices);

	}else if( D.alg==1 )
	{
		energy = sqrDistance_1storderApprox( vertices, face_nvertex, face_vertex, D, points, normals);	
	}else
	{
		lpOptimization_lBFGS( vertices, face_nvertex, face_vertex, D, points, normals);	
		energy = LeastSquaresOptimization_LM( vertices, face_nvertex, face_vertex, D, points, normals);	
	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* ------------------------ adaptively refine the control mesh and re-fit the subdivision surface--------------*/
    for( int kk=0; kk< std::max( D.n_splits, D.n_collapses); kk++ )
    {
    	std::vector<TypeReal> face_error, vertex_error;
    	computeError(points, normals, vertices, face_nvertex, face_vertex, D, face_error, vertex_error);
    	if( kk<D.n_splits){
    		bool success = vertex_split( vertices, face_nvertex, face_vertex, vertex_error, D.split_step );
    	}
    	if( kk<D.n_collapses){
    		bool success = diagonal_collapse( vertices, face_nvertex, face_vertex, face_error, D.collapse_step );
    	}

    	// clean control mesh
		int area0_faces = remove_area0Faces( vertices, face_nvertex, face_vertex);
		if( area0_faces>0 ){ remove_unreferedVertex( vertices, face_nvertex, face_vertex); }
		clean_mesh( vertices, face_nvertex, face_vertex );	
    	
		if( D.alg == 0 )
		{
			energy = Distance_MM( vertices, face_nvertex, face_vertex, D, points, normals, boundary_vertices);
		}else if( D.alg==1 )
		{
			energy = sqrDistance_1storderApprox( vertices, face_nvertex, face_vertex, D, points, normals);	
		}else
		{
			energy = LeastSquaresOptimization_LM( vertices, face_nvertex, face_vertex, D, points, normals);	
		}
   	
    }
  
    
    
    
    
    
    
    
    
    
    
    
    
   	clock_t t1 = clock();
	printf("energy %f time %f\n", energy, (double)(t1-t0) / ((double)CLOCKS_PER_SEC));
    
    
    // output results
    if(D.visualize>0){ printf("writing out control mesh its an approximated limit surface by 3-level subdivision...");}
    std::string outfilename(D.outFile);
    write_mesh( D.outFile, vertices, face_nvertex, face_vertex);
    outfilename.assign(D.outFile);
    
    outfilename.assign(D.outFile);
    outfilename.replace( outfilename.rfind('.'),  4, std::string("_refinedControlMesh3.obj") );
    {
    	UniformRefineSurface(vertices, face_nvertex, face_vertex, 3);
    	write_mesh( outfilename, vertices, face_nvertex, face_vertex);
    }
    
    if(D.visualize>0){ printf("done.\n");}
    
    
    return 0;
}



