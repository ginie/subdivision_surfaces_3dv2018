#ifndef __SDS__
#define __SDS__
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <cfloat>
#include <algorithm>
#include <cmath>
#include <limits>
#include <queue>
#include <opensubdiv/far/topologyDescriptor.h>
#include <opensubdiv/far/primvarRefiner.h>
#include <opensubdiv/far/patchTableFactory.h>
#include <opensubdiv/far/patchMap.h>
#include <opensubdiv/far/ptexIndices.h>
#include <opensubdiv/far/stencilTable.h>
#include <opensubdiv/far/stencilTableFactory.h>



#include "TypeDef.h"
#include "quadrature.h"

#include "EigenTypeDef.h"
#include "linearAlgebra.h"

#include "sds.h"

/// CLASSES
// Vertex container implementation.
struct Vertex {

    // Minimal required interface ----------------------
    Vertex() { }

    void Clear( void * =0 ) {
         point[0] = point[1] = point[2] = 0.0f;
    }
    

    void AddWithWeight(Vertex const & src, TypeReal weight) {
        point[0] += weight * src.point[0];
        point[1] += weight * src.point[1];
        point[2] += weight * src.point[2];
    }

    TypeReal point[3];
};


    // vertex value typedef
struct VertexValue {
	
	// Basic 'uv' layout channel
	TypeReal value;
	
	// Minimal required interface ----------------------
	
	void Clear()
	{
		value = 0;
	}
	

	void AddWithWeight(VertexValue const & src, float weight)
	{
		value += weight * src.value;
	}

};

struct VertexRGB {
	
	// Basic 'uv' layout channel
	TypeReal value[3];
	
	// Minimal required interface ----------------------
	
	void Clear()
	{
		value[0] = 0; value[1] = 0; value[2] = 0;
	}
	

	void AddWithWeight(VertexRGB const & src, float weight)
	{
		value[0] += weight * src.value[0];
		value[1] += weight * src.value[1];
		value[2] += weight * src.value[2];
	}

};

typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
//OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE;
//OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY;
OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_AND_CORNER; 





//------------------------------------------------------------------------------






void UniformParameterSample( size_t n_faces, size_t s_samples, size_t t_samples, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params)
{
    TypeReal delta_s = 1.0/s_samples;
    TypeReal delta_t = 1.0/t_samples;
    size_t n_samples = s_samples * t_samples;
    f_params.resize(n_samples*n_faces);
    s_params.resize(n_samples*n_faces);
    t_params.resize(n_samples*n_faces);
    size_t count = 0;
    for (size_t face=0; face<n_faces; face++)
    {
        for (size_t s_sample=0; s_sample<s_samples; s_sample++)
        {
            for (size_t t_sample=0; t_sample<t_samples; t_sample++)
            {
                f_params[count] = face;
                s_params[count] = (0.5 + s_sample)*delta_s;
                t_params[count] = (0.5 + t_sample)*delta_t;
                count += 1;
            }
        }
    }
}



void quadratureParameterSample( size_t n_faces, size_t quadrature, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params, std::vector<TypeReal> &q_weights)
{
	// define weights for Gauss quadrature rule
    std::vector<TypeReal> qpoints, qweights;
    quadrature_referenceQuad( quadrature, quadrature, qpoints, qweights);
    size_t n_samples = quadrature * quadrature;
    
    f_params.resize(n_samples*n_faces);
    s_params.resize(n_samples*n_faces);
    t_params.resize(n_samples*n_faces);
    q_weights.resize(n_samples*n_faces);
    size_t count = 0;
    for (size_t face=0; face<n_faces; face++)
    {
        for (size_t isample=0; isample<n_samples; isample++)
        {
			f_params[count] = face;
			s_params[count] = qpoints[isample*2];
			t_params[count] = qpoints[isample*2+1];
			q_weights[count] = qweights[isample];
			count += 1;
        }
    }
}



//------------------------------------------------------------------------------





void vertex_stats( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex )
{
    size_t n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
    
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    Descriptor desc;
    desc.numVertices = n_vertex;
    desc.numFaces = n_faces;
    desc.numVertsPerFace = &(face_nvertex[0]);
    desc.vertIndicesPerFace = &(face_vertex[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(0));
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(0);
    
    // allocate matrices
    size_t nbins = 12;
    size_t count = 0;
    std::vector<int> vertex_valence(n_vertex);
    std::vector<int> valence_hist(nbins, 0);
    for( int iv=0; iv< n_vertex; iv++ )
    {
        OpenSubdiv::Far::ConstIndexArray edges = refLastLevel.GetVertexEdges (iv);
        size_t val = edges.size();
        vertex_valence[iv] = val;
        if( val  <= nbins ){
        	valence_hist[ val-1  ] += 1;
        }
    	else{
    		valence_hist[ nbins-1 ] += 1;
    	}
    }
    
    for( int iv=0; iv< nbins; iv++ )
    {
        if(valence_hist[iv]>0){
            printf("%d vertices with valence %d\n", valence_hist[iv], iv+1 );
        }
    }
}

void find_extraordinaryVertex( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &eVertex )
{
    size_t n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
    
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    Descriptor desc;
    desc.numVertices = n_vertex;
    desc.numFaces = n_faces;
    desc.numVertsPerFace = &(face_nvertex[0]);
    desc.vertIndicesPerFace = &(face_vertex[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(0));
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(0);
    
    // allocate matrices
    eVertex.clear();
    for( int iv=0; iv< n_vertex; iv++ )
    {
        OpenSubdiv::Far::ConstIndexArray edges = refLastLevel.GetVertexEdges (iv);
        size_t val = edges.size();
        if( val  != 4 )
        {
        	eVertex.push_back(iv); 
        }
    }
    
}

void find_valenceVertex( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &eVertex )
{
    size_t n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
    
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    Descriptor desc;
    desc.numVertices = n_vertex;
    desc.numFaces = n_faces;
    desc.numVertsPerFace = &(face_nvertex[0]);
    desc.vertIndicesPerFace = &(face_vertex[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(0));
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(0);
    
    // allocate matrices
    eVertex.resize(n_vertex);
    for( int iv=0; iv< n_vertex; iv++ )
    {
        OpenSubdiv::Far::ConstIndexArray edges = refLastLevel.GetVertexEdges (iv);
        size_t val = edges.size();
        eVertex[iv] = val; 
    }
    
}


//------------------------------------------------------------------------------


// for each face store in an adjancency matrix adjacency the ids of faces that are its neighbours
// the order of the faces is splus(0), tplus(1), sminus(2), tminus(3) and works only for quads
// for each neighbouring face we need to check that the coordinate system does not chnage (it is consistent with the face_vertex information)
// we store this information in matrix changeCoord where 0=identyty, 1 = s->t, t->s, 2 = s->-s t->-t, 3 = s->t, t-> -s
void compute_params_adjacency( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &adjacency, std::vector<int> &changeCoord )
{
    size_t n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
    
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE);
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    OpenSubdiv::Far::TopologyDescriptor desc;
    desc.numVertices = n_vertex;
    desc.numFaces = n_faces;
    desc.numVertsPerFace = &(face_nvertex[0]);
    desc.vertIndicesPerFace = &(face_vertex[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<OpenSubdiv::Far::TopologyDescriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<OpenSubdiv::Far::TopologyDescriptor>::Options(type, options));
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(0));
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(0);
    
    // allocate matrices
    adjacency.resize(4*n_faces);
    size_t count = 0;
    for( int iface=0; iface< n_faces; iface++ )
    {
        if( face_nvertex[iface]!= 4){ printf("ERROR computing adjacency: non-quad face\n"); break; }
        int vs[4];
        vs[0] = face_vertex[count];
        vs[1] = face_vertex[count+1];
        vs[2] = face_vertex[count+2];
        vs[3] = face_vertex[count+3];
        count += face_nvertex[iface];
        // face_vertex[1], face_vertex[2] define the edge (1, t) splus
        OpenSubdiv::Far::Index edge_splus = refLastLevel.FindEdge(vs[1], vs[2]);
        OpenSubdiv::Far::ConstIndexArray faces = refLastLevel.GetEdgeFaces( edge_splus );
        if( faces[0] == iface){
            adjacency[ iface*4 ] = faces[1];
        }else{
            adjacency[ iface*4 ] = faces[0];
        }
        // face_vertex[2], face_vertex[3] define the edge (s, 1) tplus
        OpenSubdiv::Far::Index edge_tplus = refLastLevel.FindEdge(vs[2], vs[3]);
        faces = refLastLevel.GetEdgeFaces( edge_tplus );
        if( faces[0] == iface){
            adjacency[ iface*4 + 1] = faces[1];
        }else{
            adjacency[ iface*4 + 1] = faces[0];
        }
        // face_vertex[0], face_vertex[3] define the edge (0, t) sminus
        OpenSubdiv::Far::Index edge_sminus = refLastLevel.FindEdge(vs[0], vs[3]);
        faces = refLastLevel.GetEdgeFaces( edge_sminus );
        if( faces[0] == iface){
            adjacency[ iface*4 + 2] = faces[1];
        }else{
            adjacency[ iface*4 + 2] = faces[0];
        }
        // face_vertex[0], face_vertex[1] define the edge (s, 0) tminus
        OpenSubdiv::Far::Index edge_tminus = refLastLevel.FindEdge(vs[0],vs[1]);
        faces = refLastLevel.GetEdgeFaces( edge_tminus );
        if( faces[0] == iface){
            adjacency[ iface*4 + 3] = faces[1];
        }else{
            adjacency[ iface*4 + 3] = faces[0];
        }
        //printf("adjacency %d: %d %d %d %d\n", iface, adjacency[4*iface], adjacency[4*iface+1], adjacency[4*iface+2], adjacency[4*iface+3]);
    }
    
    
    // find how the face parametric axes change
    changeCoord.resize(4*n_faces);
    for( int iface=0; iface< n_faces; iface++ )
    {
        // for each neighbouring face determine the change of coordinates
        // the possibilities are 0: (s,t) -> (-s,-t)
        //						 1: (s,t) -> (-t, s)
        //						 2: (s,t) -> (s, t)
        //						 3: (s,t) -> (t,-s)
        for( int edge_orig=0; edge_orig<4; edge_orig++)
        {
            // find which is the id of the shared edge w.r.t. neighbouring face
            int neigh_face = adjacency[4*iface+edge_orig];
            int edge_dest = 0;
            while( adjacency[4*neigh_face + edge_dest] != iface ){ edge_dest += 1;}
            //if(edge_dest>= 4){ printf("faces with more than 4 edges\n");}
            // identify the transform
            changeCoord[iface*4 + edge_orig] = ((edge_dest-edge_orig) % 4 + 4) % 4; // make sure it is positive
        }
    }
}

//------------------------------------------------------------------------------

int reparametrize_sample( const std::vector<int> &adjacency, const std::vector<int> &changeCoord, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, size_t ipoint )
{
    size_t n_points = fparams.size();
    int counter = 0;
    int change_coord = 4;
    if((ipoint<0) || (ipoint>n_points))
    {
        for (ipoint=0; ipoint< n_points; ipoint++)
        {
            // if we step outside the current face, we set the point into the shared boundary of the corresponding neighbouring face (associated with neighbouring face, not current one)
            if( (sparams[ipoint]>=1.0) || (sparams[ipoint]<=0.0) || (tparams[ipoint]>=1.0) || (tparams[ipoint]<=0.0) )
            {
            	counter += 1;
                int iface = fparams[ipoint];
                // find neighbouring face
                /*if( sparams[ipoint]>=1.0 ) // splus is in adjacency 0
                {
                    fparams[ipoint] = adjacency[iface*4];
                    change_coord = changeCoord[iface*4];
                    sparams[ipoint] = 1.0f;//std::fmod( sparams[ipoint], (TypeReal)1.0);
                }
                else// sminus is in adjacency 2
                {
                    fparams[ipoint] = adjacency[iface*4+2];
                    change_coord = changeCoord[iface*4+2];
                    sparams[ipoint] = 0.0f;//std::fmod( 1.0 + std::fmod( sparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
                }
                if( tparams[ipoint]>= 1.0 ) // tplus is in adjacency 1
                {
                    fparams[ipoint] = adjacency[iface*4+1];
                    change_coord = changeCoord[iface*4+1];
                    tparams[ipoint] = 1.0;//std::fmod( tparams[ipoint], (TypeReal)1.0);
                }
                else// tminus is in adjacency 3
                {
                    fparams[ipoint] = adjacency[iface*4+3];
                    change_coord = changeCoord[iface*4+3];
                    tparams[ipoint] = 0.0f;//std::fmod( 1.0 + std::fmod( tparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
                }*/
                if( sparams[ipoint]>=1.0 ) // splus is in adjacency 0
                {
                    fparams[ipoint] = adjacency[iface*4];
                    change_coord = changeCoord[iface*4];
                    sparams[ipoint] = std::fmod( sparams[ipoint], (TypeReal)1.0);
                }
                else// sminus is in adjacency 2
                {
                    fparams[ipoint] = adjacency[iface*4+2];
                    change_coord = changeCoord[iface*4+2];
                    sparams[ipoint] = std::fmod( 1.0 + std::fmod( sparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
                }
                if( tparams[ipoint]>= 1.0 ) // tplus is in adjacency 1
                {
                    fparams[ipoint] = adjacency[iface*4+1];
                    change_coord = changeCoord[iface*4+1];
                    tparams[ipoint] = std::fmod( tparams[ipoint], (TypeReal)1.0);
                }
                else// tminus is in adjacency 3
                {
                    fparams[ipoint] = adjacency[iface*4+3];
                    change_coord = changeCoord[iface*4+3];
                    tparams[ipoint] = std::fmod( 1.0 + std::fmod( tparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
                }       
                // apply change of coordinates
                switch( change_coord )
                {
                    case 0:
                        sparams[ipoint] = 1.0 - sparams[ipoint];
                        tparams[ipoint] = 1.0 - tparams[ipoint];
                        break;
                    case 1:
                        sparams[ipoint] = 1.0 - tparams[ipoint];
                        tparams[ipoint] = sparams[ipoint];
                    case 3:
                        sparams[ipoint] = tparams[ipoint];
                        tparams[ipoint] = 1.0 - sparams[ipoint];
                        break;
                }
            }
        }
    }
    else
    {
		// if we step outside the current face, we set the point into the shared boundary of the corresponding neighbouring face (associated with neighbouring face, not current one)
		if( (sparams[ipoint]>=1.0) || (sparams[ipoint]<=0.0) || (tparams[ipoint]>=1.0) || (tparams[ipoint]<=0.0) )
		{
			counter += 1;
			int iface = fparams[ipoint];
			// find neighbouring face
			/*if( sparams[ipoint]>=1.0 ) // splus is in adjacency 0
			{
				fparams[ipoint] = adjacency[iface*4];
				change_coord = changeCoord[iface*4];
				sparams[ipoint] = 1.0f;//std::fmod( sparams[ipoint], (TypeReal)1.0);
			}
			else// sminus is in adjacency 2
			{
				fparams[ipoint] = adjacency[iface*4+2];
				change_coord = changeCoord[iface*4+2];
				sparams[ipoint] = 0.0f;//std::fmod( 1.0 + std::fmod( sparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
			}
			if( tparams[ipoint]>= 1.0 ) // tplus is in adjacency 1
			{
				fparams[ipoint] = adjacency[iface*4+1];
				change_coord = changeCoord[iface*4+1];
				tparams[ipoint] = 1.0;//std::fmod( tparams[ipoint], (TypeReal)1.0);
			}
			else// tminus is in adjacency 3
			{
				fparams[ipoint] = adjacency[iface*4+3];
				change_coord = changeCoord[iface*4+3];
				tparams[ipoint] = 0.0f;//std::fmod( 1.0 + std::fmod( tparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
			}*/
			if( sparams[ipoint]>=1.0 ) // splus is in adjacency 0
			{
				fparams[ipoint] = adjacency[iface*4];
				change_coord = changeCoord[iface*4];
				sparams[ipoint] = std::fmod( sparams[ipoint], (TypeReal)1.0);
			}
			else// sminus is in adjacency 2
			{
				fparams[ipoint] = adjacency[iface*4+2];
				change_coord = changeCoord[iface*4+2];
				sparams[ipoint] = std::fmod( 1.0 + std::fmod( sparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
			}
			if( tparams[ipoint]>= 1.0 ) // tplus is in adjacency 1
			{
				fparams[ipoint] = adjacency[iface*4+1];
				change_coord = changeCoord[iface*4+1];
				tparams[ipoint] = std::fmod( tparams[ipoint], (TypeReal)1.0);
			}
			else// tminus is in adjacency 3
			{
				fparams[ipoint] = adjacency[iface*4+3];
				change_coord = changeCoord[iface*4+3];
				tparams[ipoint] = std::fmod( 1.0 + std::fmod( tparams[ipoint], (TypeReal)1.0), (TypeReal)1.0);
			} 
            // apply change of coordinates
			switch( change_coord )
			{
				case 0:
					sparams[ipoint] = 1.0 - sparams[ipoint];
					tparams[ipoint] = 1.0 - tparams[ipoint];
					break;
				case 1:
					sparams[ipoint] = 1.0 - tparams[ipoint];
					tparams[ipoint] = sparams[ipoint];
				case 3:
					sparams[ipoint] = tparams[ipoint];
					tparams[ipoint] = 1.0 - sparams[ipoint];
					break;
			}
		}
               
    }
    return counter;
}




//-------------------------------------- apply the subdivision rules to the input mesh

void UniformRefineSurface(std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, int n_uniformRef)
{
    
    int nverts = vertices.size()/3;
    int nfaces = facenverts.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE);
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Uniformly refine the TOPOLOGY up to 'maxlevel'
    refiner->RefineUniform(OpenSubdiv::Far::TopologyRefiner::UniformOptions(n_uniformRef));
    
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    Vertex *verts = (Vertex*)std::malloc( refiner->GetNumVerticesTotal() *sizeof(Vertex) );
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));
    
    // Interpolate vertex primvar data at each level
    OpenSubdiv::Far::PrimvarRefiner primvarRefiner(*refiner);
    
    Vertex * src = verts;
    for (int level = 1; level <= n_uniformRef; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        primvarRefiner.Interpolate(level, src, dst);
        src = dst;
    }
    
    // return the current vertices and faces
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(n_uniformRef);
    
    nverts = refLastLevel.GetNumVertices();
    nfaces = refLastLevel.GetNumFaces();
    
    // copy vertex information
    int firstOfLastVerts = refiner->GetNumVerticesTotal() - nverts;
    vertices.resize(nverts*3);
    memcpy(&vertices[0], &verts[firstOfLastVerts], nverts*3*sizeof(TypeReal));
    
    
    // copy face information
    facenverts = std::vector<int>(nfaces,0);
    faceverts.clear();
    for (int face = 0; face < nfaces; face++)
    {
        
        OpenSubdiv::Far::ConstIndexArray fverts = refLastLevel.GetFaceVertices(face);
        
        // all refined Catmark faces should be quads
        assert(fverts.size()==4);
        
        facenverts[face] = fverts.size();
        for (int vert=0; vert<fverts.size(); vert++)
        {
            faceverts.push_back(fverts[vert]);
        }
    }
    
    free(verts);
}



void UniformRefineSurface(std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, std::vector<TypeReal> &vertexValues, int n_uniformRef)
{
    
    int nverts = vertices.size()/3;
    int nfaces = facenverts.size();
    int dim_values = vertexValues.size()/nverts;

    //int n_uniformRef = 1;
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE);
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Uniformly refine the TOPOLOGY up to 'maxlevel'
    refiner->RefineUniform(OpenSubdiv::Far::TopologyRefiner::UniformOptions(n_uniformRef));

    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    Vertex *verts = (Vertex*)std::malloc( refiner->GetNumVerticesTotal() *sizeof(Vertex) );
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));
    
    // Interpolate vertex primvar data at each level
    OpenSubdiv::Far::PrimvarRefiner primvarRefiner(*refiner);
    
    Vertex * src = verts;
    for (int level = 1; level <= n_uniformRef; ++level)
    {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        primvarRefiner.Interpolate(level, src, dst);
        src = dst;
    }
    
    
    
    // return the current vertices and faces
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(n_uniformRef);
    
    int nrverts = refLastLevel.GetNumVertices();
    int nrfaces = refLastLevel.GetNumFaces();
    
    // copy vertex information
    int firstOfLastVerts = refiner->GetNumVerticesTotal() - nrverts;
    vertices.resize(nrverts*3);
    memcpy(&vertices[0], &verts[firstOfLastVerts], nrverts*3*sizeof(TypeReal));
    free(verts);
    
    
    // copy face information
    facenverts = std::vector<int>(nrfaces,0);
    faceverts.clear();
    for (int face = 0; face < nrfaces; face++)
    {
        OpenSubdiv::Far::ConstIndexArray fverts = refLastLevel.GetFaceVertices(face);
        
        // all refined Catmark faces should be quads
        assert(fverts.size()==4);
        
        facenverts[face] = fverts.size();
        for (int vert=0; vert<fverts.size(); vert++)
        {
            faceverts.push_back(fverts[vert]);
        }
    }
    
    
    std::vector<TypeReal> rVertexValues(nrverts*dim_values);
    for(int idim=0; idim<dim_values; idim++)
    {
    	VertexValue *vverts = (VertexValue*)std::malloc( refiner->GetNumVerticesTotal() *sizeof(VertexValue) );
    	for(int ii=0; ii< nverts; ii++)
    	{
    		vverts[ii].value = vertexValues[ii + idim*nverts];
    	}
    	
		VertexValue * vsrc = vverts;
		for (int level = 1; level <= n_uniformRef; ++level)
		{    
			VertexValue * vdst = vsrc + refiner->GetLevel(level-1).GetNumVertices();
			primvarRefiner.Interpolate(level, vsrc, vdst);
			vsrc = vdst;
		}
		for(int ii=0; ii< nrverts; ii++)
    	{
    		rVertexValues[ii + idim*nrverts] = vverts[firstOfLastVerts + ii].value;
    	}
		free(vverts);
    }
    

    
    vertexValues = rVertexValues;
    
    
}

/* ------------------------------------ subdivision as linear operators on the control vertices ----------------------------*/

void UniformRefineSurface( std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, int n_uniformRef, std::vector<TypeReal> &Rvertices, std::vector<int> &Rfacenverts, std::vector<int> &Rfaceverts, SpMat &A, SpMat &A3)
{
    
    int nverts = vertices.size()/3;
    int nverts0 = nverts;
    int nfaces = facenverts.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE);
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Uniformly refine the TOPOLOGY up to 'maxlevel'
    refiner->RefineUniform(OpenSubdiv::Far::TopologyRefiner::UniformOptions(n_uniformRef));

        
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    Vertex *verts = (Vertex*)std::malloc( refiner->GetNumVerticesTotal() *sizeof(Vertex) );
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));
    
    
    // Interpolate vertex primvar data at each level
    Vertex * src = verts;
    for (int level = 1; level <= n_uniformRef; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // return the current vertices and faces
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(n_uniformRef);
    
    nverts = refLastLevel.GetNumVertices();
    nfaces = refLastLevel.GetNumFaces();
        
    // get refined vertices and faces
    int firstOfLastVerts = refiner->GetNumVerticesTotal() - nverts;
    Rvertices = std::vector<TypeReal>(nverts*3);
    memcpy(&Rvertices[0], &verts[firstOfLastVerts], nverts*3*sizeof(TypeReal));    
    // copy face information
    Rfacenverts = std::vector<int>(nfaces,0);
    Rfaceverts.clear();
    for (int face = 0; face < nfaces; face++)
    {
        
        OpenSubdiv::Far::ConstIndexArray fverts = refLastLevel.GetFaceVertices(face);
        
        // all refined Catmark faces should be quads
        assert(fverts.size()==4);
        
        Rfacenverts[face] = fverts.size();
        for (int vert=0; vert<fverts.size(); vert++)
        {
            Rfaceverts.push_back(fverts[vert]);
        }
    }
    
    // Use the FarStencilTable factory to create cascading stencil table
    OpenSubdiv::Far::StencilTableFactory::Options st_options;
    st_options.generateIntermediateLevels = false;
    st_options.factorizeIntermediateLevels = false;
    st_options.generateOffsets = true;
    OpenSubdiv::Far::StencilTable const * stenciltab = OpenSubdiv::Far::StencilTableFactory::Create(*refiner, st_options);    
    // Allocate vertex primvar buffer (1 stencil for each vertex)
    const int nstencils = stenciltab->GetNumStencils();
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    //printf("nstencils %d, nverts %d, nverts0 %d\n", nstencils, nverts, nverts0);
   
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    std::vector<size_t> rows, cols;
    std::vector<TypeReal> vals;
	std::vector<int> sizes = stenciltab->GetSizes();
	std::vector<OpenSubdiv::Far::Index> indices = stenciltab->GetControlIndices();
	std::vector<float> weights = stenciltab->GetWeights();
	std::vector<OpenSubdiv::Far::Index>  offsets = stenciltab->GetOffsets();
	int i0 = nstencils - nverts;
	for (int i=0; i<nverts; i++)
	{
		OpenSubdiv::Far::Index offset = offsets[i+i0];
		std::queue<std::pair<int, TypeReal> > auxQueue;
		// For each element in the array, add the coef's contribution
		for (int j = 0; j < sizes[i+i0]; j++)
		{
			int indx = indices[offset+j];
			TypeReal val =  weights[offset+j];
			if( indx >= nverts0 )
			{
				auxQueue.push( std::make_pair(indx-nverts0 , val) );
			}
			else
			{
				rows.push_back(i);
				cols.push_back( indx);
				vals.push_back( val );
			}
		}
		// for indices that are auxiliary intermediate points, accumulate the contributions
		while( !auxQueue.empty() )
		{
			std::pair<int, TypeReal> obj = auxQueue.front();
			int indx0 = obj.first;
			TypeReal val0 = obj.second;
			offset = offsets[indx0];
			for (int k = 0; k < sizes[indx0]; k++)
			{
				TypeReal val = val0*weights[ offset + k ];
				int indx = indices[ offset + k ];
				if( indx <nverts0 )
				{
					rows.push_back(i);
					cols.push_back( indx );
					vals.push_back( val );
				}else
				{
					auxQueue.push( std::make_pair(indx-nverts0, val) );
				}
			}
			/*size_t size_st = st[ind_offset].GetSize();
			OpenSubdiv::Far::Index const *st_indices = st[ind_offset].GetVertexIndices();
			float const *st_weights = st[ind_offset].GetWeights();
			for (int k = 0; k < size_st; k++)
			{
				TypeReal val = val0 * st_weights[ k ];
				int indx = st_indices[ k ];
				if( indx <nverts0 )
				{
					rows.push_back(i);
					cols.push_back( indx );
					vals.push_back( val );
				}else
				{
					auxQueue.push( std::make_pair(indx, val) );
				}
			}*/
			auxQueue.pop();
		}
	
	}    
	//printf("nstencils %d, nverts %d\n", nstencils, nverts);
	//printf("maxindices %d, nvert0s %d\n", *std::max_element(cols.begin(),cols.end()), nverts0);
    A.resize( nverts, nverts0 );
    build_spsMatrix( rows, cols, vals, A );    
    
    A3.resize( 3*nverts, 3*nverts0 );   
    std::vector<T> tripletList;// list of non-zeros coefficients
    for(size_t j=0; j<rows.size(); j++)
    {
    	for(int idim=0; idim<3; idim++)
    	{
    		tripletList.push_back(T(3*rows[j]+idim,3*cols[j]+idim,vals[j]));
        }
    }
    A3.setFromTriplets(tripletList.begin(), tripletList.end());
    
    
    
    free(verts);
    delete refiner;
    delete stenciltab;
}


void UniformRefineSurfaceOperator( std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, std::vector<TypeReal> &Rvertices, std::vector<int> &Rfacenverts, std::vector<int> &Rfaceverts, std::vector<int> &face2Face, SpMat &A, SpMat &A3)
{
    
    int nverts = vertices.size()/3;
    int nverts0 = nverts;
    int nfaces = facenverts.size();
    int n_uniformRef = 1;
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    face2Face.clear();
    for (int face = 0; face < nfaces; face++)
    {
    	for(int ii=0; ii< facenverts[face]; ii++)
    	{
    		face2Face.push_back(face);
        }
    }
    
    // Instantiate a FarTopologyRefiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Uniformly refine the TOPOLOGY up to 'maxlevel'
    refiner->RefineUniform(OpenSubdiv::Far::TopologyRefiner::UniformOptions(n_uniformRef));

        
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    Vertex *verts = (Vertex*)std::malloc( refiner->GetNumVerticesTotal() *sizeof(Vertex) );
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));
    
    
    // Interpolate vertex primvar data at each level
    Vertex * src = verts;
    for (int level = 1; level <= n_uniformRef; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // return the current vertices and faces
    OpenSubdiv::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(n_uniformRef);
    
    nverts = refLastLevel.GetNumVertices();
    nfaces = refLastLevel.GetNumFaces();
        
    // get refined vertices and faces
    int firstOfLastVerts = refiner->GetNumVerticesTotal() - nverts;
    Rvertices = std::vector<TypeReal>(nverts*3);
    memcpy(&Rvertices[0], &verts[firstOfLastVerts], nverts*3*sizeof(TypeReal));    
    // copy face information
    Rfacenverts = std::vector<int>(nfaces,0);
    Rfaceverts.clear();
    for (int face = 0; face < nfaces; face++)
    {
    	OpenSubdiv::Far::ConstIndexArray fverts = refLastLevel.GetFaceVertices(face);
        
        // all refined Catmark faces should be quads
        assert(fverts.size()==4);
        
        Rfacenverts[face] = fverts.size();
        for (int vert=0; vert<fverts.size(); vert++)
        {
            Rfaceverts.push_back(fverts[vert]);
        }
    }
    
    // Use the FarStencilTable factory to create cascading stencil table
    OpenSubdiv::Far::StencilTableFactory::Options st_options;
    st_options.generateIntermediateLevels = false;
    st_options.factorizeIntermediateLevels = false;
    st_options.generateOffsets = true;
    OpenSubdiv::Far::StencilTable const * stenciltab = OpenSubdiv::Far::StencilTableFactory::Create(*refiner, st_options);    
    // Allocate vertex primvar buffer (1 stencil for each vertex)
    const int nstencils = stenciltab->GetNumStencils();
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    //printf("nstencils %d, nverts %d, nverts0 %d\n", nstencils, nverts, nverts0);
   
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    std::vector<size_t> rows, cols;
    std::vector<TypeReal> vals;
	std::vector<int> sizes = stenciltab->GetSizes();
	std::vector<OpenSubdiv::Far::Index> indices = stenciltab->GetControlIndices();
	std::vector<float> weights = stenciltab->GetWeights();
	std::vector<OpenSubdiv::Far::Index>  offsets = stenciltab->GetOffsets();
	int i0 = nstencils - nverts;
	for (int i=0; i<nverts; i++)
	{
		OpenSubdiv::Far::Index offset = offsets[i+i0];
		std::queue<std::pair<int, TypeReal> > auxQueue;
		// For each element in the array, add the coef's contribution
		for (int j = 0; j < sizes[i+i0]; j++)
		{
			int indx = indices[offset+j];
			TypeReal val =  weights[offset+j];
			if( indx >= nverts0 )
			{
				auxQueue.push( std::make_pair(indx-nverts0 , val) );
			}
			else
			{
				rows.push_back(i);
				cols.push_back( indx);
				vals.push_back( val );
			}
		}
		// for indices that are auxiliary intermediate points, accumulate the contributions
		while( !auxQueue.empty() )
		{
			std::pair<int, TypeReal> obj = auxQueue.front();
			int indx0 = obj.first;
			TypeReal val0 = obj.second;
			offset = offsets[indx0];
			for (int k = 0; k < sizes[indx0]; k++)
			{
				TypeReal val = val0*weights[ offset + k ];
				int indx = indices[ offset + k ];
				if( indx <nverts0 )
				{
					rows.push_back(i);
					cols.push_back( indx );
					vals.push_back( val );
				}else
				{
					auxQueue.push( std::make_pair(indx-nverts0, val) );
				}
			}
			auxQueue.pop();
		}
	
	}    
	//printf("nstencils %d, nverts %d\n", nstencils, nverts);
	//printf("maxindices %d, nvert0s %d\n", *std::max_element(cols.begin(),cols.end()), nverts0);
    A.resize( nverts, nverts0 );
    build_spsMatrix( rows, cols, vals, A );    
    
    A3.resize( 3*nverts, 3*nverts0 );   
    std::vector<T> tripletList;// list of non-zeros coefficients
    for(size_t j=0; j<rows.size(); j++)
    {
    	for(int idim=0; idim<3; idim++)
    	{
    		tripletList.push_back(T(3*rows[j]+idim,3*cols[j]+idim,vals[j]));
        }
    }
    A3.setFromTriplets(tripletList.begin(), tripletList.end());
    
    
    
    free(verts);
    delete refiner;
    delete stenciltab;
}




void UniformRefineSurfaceOperators(std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, int n_ref, std::vector<TypeReal> &rvertices, std::vector<int> &rface_nvertex, std::vector<int> &rface_vertex, SpMat &A, SpMat &A3)
{
	int nverts = vertices.size()/3;
	// create refinement matrices from uniform stencils and refine the surface
	// A_k \in R^{ n_atlevelk x n_0 )
	rvertices = vertices;
    rface_nvertex = facenverts;
    rface_vertex = faceverts;    
    A = SpMat(nverts,nverts);
    A3 = SpMat(3*nverts,3*nverts);
    std::vector<TypeReal> ones(nverts,1.0);
    build_spsDiagMatrix(std::vector<TypeReal>(3*nverts,1.0), A3 );
    std::vector<int> face2Face;
    for( int ii=0; ii< n_ref; ii++)
    {
    	SpMat A0(A);
    	SpMat A03(A3);
    	SpMat Ar, A3r;
    	std::vector< TypeReal> vertices0(rvertices);
    	std::vector<int> facenverts0(rface_nvertex);
    	std::vector<int> faceverts0(rface_vertex);
    	UniformRefineSurfaceOperator( vertices0, facenverts0, faceverts0, rvertices, rface_nvertex, rface_vertex, face2Face, Ar, A3r);
    	A = Ar * A0;
    	A3 = A3r * A03;
    }


}







//--------------------------------------------------- sample the surface and its derivatives at input parametric coordinates ---------------------------


void SampleLimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& Ss, std::vector<TypeReal>& St)
{
    
    int nverts = vertices.size()/3;
    size_t nfaces = facenverts.size();
    size_t nsamples = f_params.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE);
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 3;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));
    
    
    // Generate a set of OpenSubdiv::Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);
    
    // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();
    
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));
    
    // Adaptive refinement to isolate extraordinary vertices, may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    
    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // Evaluate local points from interpolated vertex primvars.
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    S.resize(3*nsamples);
    Ss.resize(3*nsamples);
    St.resize(3*nsamples);
    float pWeights[16], dsWeights[16], dtWeights[16];
    for (size_t isample=0; isample<nsamples; isample++)
    {
        // Locate the patch corresponding to the face ptex idx and (s,t)
        TypeReal s = s_params[isample];
        TypeReal t = t_params[isample];
        //printf("f_params[isample] = %d, s = %f, t=%f\n", f_params[isample], s, t);
        OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( f_params[isample], s, t );
        assert(handle);
        
        // Evaluate the patch weights, identify the CVs and compute the limit frame
        // the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
        // compute the weights of these linear combinations
        patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights);
        // get the control variables for this patch
        OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);
        
        //Compute the weights for the coordinates and the derivatives wrt the control vertices
        //Set all weights to zero
        std::vector<TypeReal> vect_wc(nverts, 0.f);
        std::vector<TypeReal> vect_wu1(nverts, 0.f);
        std::vector<TypeReal> vect_wu2(nverts, 0.f);
        for (int cv = 0; cv < cvs.size(); ++cv)
        {
            for(size_t idim=0; idim<3; idim++)
            {
                S[isample*3 + idim] += pWeights[cv]* verts[cvs[cv]].point[idim];
                Ss[isample*3 + idim] += dsWeights[cv]* verts[cvs[cv]].point[idim];
                St[isample*3 + idim] += dtWeights[cv]* verts[cvs[cv]].point[idim];
            }
        }
        
    }
    
}

void SampleD2LimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& Ss, std::vector<TypeReal>& St, std::vector<TypeReal>& Sss, std::vector<TypeReal>& Sst, std::vector<TypeReal>& Stt )
{
    
    int nverts = vertices.size()/3;
    size_t nfaces = facenverts.size();
    size_t nsamples = f_params.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE);
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 3;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));
    
    
    // Generate a set of OpenSubdiv::Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);
    
    // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();
    
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));
    
    // Adaptive refinement to isolate extraordinary vertices, may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    
    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // Evaluate local points from interpolated vertex primvars.
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    S.resize(3*nsamples);
    Ss.resize(3*nsamples);
    St.resize(3*nsamples);
    Sss.resize(3*nsamples);
    Sst.resize(3*nsamples);
    Stt.resize(3*nsamples);
    float pWeights[16], dsWeights[16], dtWeights[16], dssWeights[16], dstWeights[16], dttWeights[16];
    for (size_t isample=0; isample<nsamples; isample++)
    {
        // Locate the patch corresponding to the face ptex idx and (s,t)
        TypeReal s = s_params[isample];
        TypeReal t = t_params[isample];
        //printf("f_params[isample] = %d, s = %f, t=%f\n", f_params[isample], s, t);
        OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( f_params[isample], s, t );
        assert(handle);
        
        // Evaluate the patch weights, identify the CVs and compute the limit frame
        // the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
        // compute the weights of these linear combinations
        patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights, dssWeights, dstWeights, dttWeights);
        // get the control variables for this patch
        OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);
        

        //Compute the weights for the coordinates and the derivatives wrt the control vertices
        //Set all weights to zero
        for (int cv = 0; cv < cvs.size(); ++cv)
        {
            for(size_t idim=0; idim<3; idim++)
            {
                S[isample*3 + idim] += pWeights[cv]* verts[cvs[cv]].point[idim];
                Ss[isample*3 + idim] += dsWeights[cv]* verts[cvs[cv]].point[idim];
                St[isample*3 + idim] += dtWeights[cv]* verts[cvs[cv]].point[idim];
                Sss[isample*3 + idim] += dssWeights[cv]* verts[cvs[cv]].point[idim];
                Sst[isample*3 + idim] += dstWeights[cv]* verts[cvs[cv]].point[idim];
                Stt[isample*3 + idim] += dttWeights[cv]* verts[cvs[cv]].point[idim];
            }
        }
        
    }
    
}



/* ------------------------------------ sample as linear operators ----------------------------*/


void SampleLimitBasis(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, SpMat &L, SpMat &Ds, SpMat &Dt, bool sample_normals)
{
    
    int nverts = vertices.size()/3;
    size_t nfaces = facenverts.size();
    size_t nsamples = f_params.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 0;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));
     
    
    // Generate a set of OpenSubdiv::Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;//ENDCAP_NONE;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);
    
   // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    //printf("getting number of refiner verices and local points\n");
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();
     
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));  
    
    
    
    //Get all the stencils from the patchTable (necessary to obtain the weights for the LocalPoints)
    //printf("getting stencils\n");
    OpenSubdiv::Far::StencilTable const *stenciltab = patchTable->GetLocalPointStencilTable();
    // Allocate vertex primvar buffer (1 stencil for each vertex)
    const int nstencils = stenciltab->GetNumStencils();
    //if( nverts != stenciltab->GetNumControlVertices() 	){ printf("nverts %d != stenciltab->GetNumControlVertices() %d\n", nverts, stenciltab->GetNumControlVertices());}
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    
    // Adaptive refinement may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    //printf("nRefinedLevels is %d\n", nRefinedLevels);
    
    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // Evaluate local points from interpolated vertex primvars.
    //printf("Evaluate local points from interpolated vertex primvars\n");
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    //printf("Create a OpenSubdiv::Far::PatchMap to help locating patches in the table\n");
    
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    //printf("Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces\n");
    
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    std::vector<size_t> Lrows, Lcols;
    std::vector<TypeReal> Lvals, Dsvals, Dtvals;
    float pWeights[20], dsWeights[20], dtWeights[20];
    for (size_t isample=0; isample<nsamples; isample++)
    {
        // Locate the patch corresponding to the face ptex idx and (s,t)
        TypeReal s = s_params[isample];
        TypeReal t = t_params[isample];
        //printf("f_params[isample] = %d/%d, s = %f, t=%f\n", f_params[isample], nfaces, s, t);
        OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( f_params[isample], s, t );
        assert(handle);
        
        // Evaluate the patch weights, identify the CVs and compute the limit frame
        // the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
        // compute the weights of these linear combinations
        patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights);
        
        // get the control variables for this patch
        OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);
        //printf("%d active variables in the patch\n", cvs.size() );
        
        
        //Compute the weights for the coordinates and the derivatives wrt the control vertices
        //Set all weights to zero
        std::vector<TypeReal> vect_wc(nverts, 0.f);
        std::vector<TypeReal> vect_wu1(nverts, 0.f);
        std::vector<TypeReal> vect_wu2(nverts, 0.f);
        for (int cv = 0; cv < cvs.size(); ++cv)
        {
            if (cvs[cv] < nverts)
            {
                vect_wc[ cvs[cv] ] += pWeights[cv];
                vect_wu1[ cvs[cv] ] += dsWeights[cv];
                vect_wu2[ cvs[cv] ] += dtWeights[cv];
            }
            else
            {
            	const unsigned int ind_offset = cvs[cv] - nverts;
            	//printf("pWeights %f dsWeights %f dtWeights %f to be stored at coefficient indx %d/%d\n", pWeights[cv], dsWeights[cv], dtWeights[cv], cvs[cv], ind_offset, nstencils );
     
                //Look at the stencil associated to this local point and distribute its weight over the control vertices
                size_t size_st = st[ind_offset].GetSize();
                OpenSubdiv::Far::Index const *st_ind = st[ind_offset].GetVertexIndices();
                float const *st_weights = st[ind_offset].GetWeights();
                for (size_t s = 0; s < size_st; s++)
                {
                    vect_wc[ st_ind[s] ] += pWeights[cv]*st_weights[s];
                    vect_wu1[ st_ind[s]] += dsWeights[cv]*st_weights[s];
                    vect_wu2[ st_ind[s]] += dtWeights[cv]*st_weights[s];
                }
            }
        }
        //Store the weights into Eigen triplets
        for (int cv=0; cv<nverts; cv++)
        {
            if (vect_wc[cv] != 0.f)
            {
                Lcols.push_back(cv);// xcomponent
                Lrows.push_back(isample);
                Lvals.push_back(vect_wc[cv] ); 
                Dsvals.push_back( vect_wu1[cv] ); 
                Dtvals.push_back( vect_wu2[cv] ); 
            }
        }
        
        
    }
    // create sparse matrix L that evaluates the limit surface at parameter values
    L.resize( nsamples, nverts );
    build_spsMatrix(Lrows, Lcols, Lvals, L);
    //printf("L constructed\n");
    
    if(sample_normals==true)
    {
        // create sparse matrix L that evaluates the tangent vector of limit surface at parameter values
        Ds.resize( nsamples, nverts );
        Dt.resize( nsamples, nverts );
        build_spsMatrix(Lrows, Lcols, Dsvals, Ds);
        build_spsMatrix(Lrows, Lcols, Dtvals, Dt);
        //printf("D constructed\n");
    }
}



void SampleLimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, SpMat &L, SpMat &Ds, SpMat &Dt, bool sample_normals)
{
    
    int nverts = vertices.size()/3;
    size_t nfaces = facenverts.size();
    size_t nsamples = f_params.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 0;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));
     
    
    // Generate a set of OpenSubdiv::Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;//ENDCAP_NONE;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);
    
   // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    //printf("getting number of refiner verices and local points\n");
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();
     
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));  
    
    
    
    //Get all the stencils from the patchTable (necessary to obtain the weights for the LocalPoints)
    //printf("getting stencils\n");
    OpenSubdiv::Far::StencilTable const *stenciltab = patchTable->GetLocalPointStencilTable();
    // Allocate vertex primvar buffer (1 stencil for each vertex)
    const int nstencils = stenciltab->GetNumStencils();
    //if( nverts != stenciltab->GetNumControlVertices() 	){ printf("nverts %d != stenciltab->GetNumControlVertices() %d\n", nverts, stenciltab->GetNumControlVertices());}
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    
    // Adaptive refinement may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    //printf("nRefinedLevels is %d\n", nRefinedLevels);
    
    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // Evaluate local points from interpolated vertex primvars.
    //printf("Evaluate local points from interpolated vertex primvars\n");
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    //printf("Create a OpenSubdiv::Far::PatchMap to help locating patches in the table\n");
    
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    //printf("Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces\n");
    
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    std::vector<size_t> Lrows, Lcols;
    std::vector<TypeReal> Lvals, Dsvals, Dtvals;
    float pWeights[20], dsWeights[20], dtWeights[20];
    for (size_t isample=0; isample<nsamples; isample++)
    {
        // Locate the patch corresponding to the face ptex idx and (s,t)
        TypeReal s = s_params[isample];
        TypeReal t = t_params[isample];
        //printf("f_params[isample] = %d/%d, s = %f, t=%f\n", f_params[isample], nfaces, s, t);
        OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( f_params[isample], s, t );
        assert(handle);
        
        // Evaluate the patch weights, identify the CVs and compute the limit frame
        // the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
        // compute the weights of these linear combinations
        patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights);
        
        // get the control variables for this patch
        OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);
        //printf("%d active variables in the patch\n", cvs.size() );
        
        
        //Compute the weights for the coordinates and the derivatives wrt the control vertices
        //Set all weights to zero
        std::vector<TypeReal> vect_wc(nverts, 0.f);
        std::vector<TypeReal> vect_wu1(nverts, 0.f);
        std::vector<TypeReal> vect_wu2(nverts, 0.f);
        for (int cv = 0; cv < cvs.size(); ++cv)
        {
            if (cvs[cv] < nverts)
            {
                vect_wc[ cvs[cv] ] += pWeights[cv];
                vect_wu1[ cvs[cv] ] += dsWeights[cv];
                vect_wu2[ cvs[cv] ] += dtWeights[cv];
            }
            else
            {
            	const unsigned int ind_offset = cvs[cv] - nverts;
            	//printf("pWeights %f dsWeights %f dtWeights %f to be stored at coefficient indx %d/%d\n", pWeights[cv], dsWeights[cv], dtWeights[cv], cvs[cv], ind_offset, nstencils );
     
                //Look at the stencil associated to this local point and distribute its weight over the control vertices
                size_t size_st = st[ind_offset].GetSize();
                OpenSubdiv::Far::Index const *st_ind = st[ind_offset].GetVertexIndices();
                float const *st_weights = st[ind_offset].GetWeights();
                for (size_t s = 0; s < size_st; s++)
                {
                    vect_wc[ st_ind[s] ] += pWeights[cv]*st_weights[s];
                    vect_wu1[ st_ind[s]] += dsWeights[cv]*st_weights[s];
                    vect_wu2[ st_ind[s]] += dtWeights[cv]*st_weights[s];
                }
            }
        }
        //Store the weights into Eigen triplets
        for (int cv=0; cv<nverts; cv++)
        {
            if (vect_wc[cv] != 0.f)
            {
                Lcols.push_back(cv*3);// xcomponent
                Lcols.push_back(cv*3+1); //ycomponent
                Lcols.push_back(cv*3+2); //zcomponent
                Lrows.push_back(isample*3);
                Lrows.push_back(isample*3+1);
                Lrows.push_back(isample*3+2);
                Lvals.push_back(vect_wc[cv] ); Lvals.push_back(vect_wc[cv] ); Lvals.push_back(vect_wc[cv] );
                Dsvals.push_back( vect_wu1[cv] ); Dsvals.push_back( vect_wu1[cv] ); Dsvals.push_back( vect_wu1[cv] );
                Dtvals.push_back( vect_wu2[cv] ); Dtvals.push_back( vect_wu2[cv] ); Dtvals.push_back( vect_wu2[cv] );
            }
        }
        
        
    }
    // create sparse matrix L that evaluates the limit surface at parameter values
    L.resize( 3*nsamples, 3*nverts );
    build_spsMatrix(Lrows, Lcols, Lvals, L);
    //printf("L constructed\n");
    
    if(sample_normals==true)
    {
        // create sparse matrix L that evaluates the tangent vector of limit surface at parameter values
        Ds.resize( 3*nsamples, 3*nverts );
        Dt.resize( 3*nsamples, 3*nverts );
        build_spsMatrix(Lrows, Lcols, Dsvals, Ds);
        build_spsMatrix(Lrows, Lcols, Dtvals, Dt);
        //printf("D constructed\n");
    }
}


void SampleD2LimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, SpMat &L, SpMat &Ds, SpMat &Dt, SpMat &Dss, SpMat &Dst, SpMat &Dtt )
{
    
    int nverts = vertices.size()/3;
    size_t nfaces = facenverts.size();
    size_t nsamples = f_params.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE);
    //options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 0;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));
    
    
    // Generate a set of OpenSubdiv::Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);
    
    // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();
    
    
    //Get all the stencils from the patchTable (necessary to obtain the weights for the LocalPoints)
    OpenSubdiv::Far::StencilTable const *stenciltab = patchTable->GetLocalPointStencilTable();
    const int nstencils = stenciltab->GetNumStencils();
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    
    
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));
    
    // Adaptive refinement may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    
    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // Evaluate local points from interpolated vertex primvars.
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    std::vector<size_t> Lrows, Lcols;
    std::vector<TypeReal> Lvals, Dsvals, Dtvals, Dssvals, Dstvals, Dttvals;
    float pWeights[16], dsWeights[16], dtWeights[16], dssWeights[16], dstWeights[16], dttWeights[16];
    for (size_t isample=0; isample<nsamples; isample++)
    {
        // Locate the patch corresponding to the face ptex idx and (s,t)
        TypeReal s = s_params[isample];
        TypeReal t = t_params[isample];
        //printf("f_params[isample] = %d, s = %f, t=%f\n", f_params[isample], s, t);
        OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( f_params[isample], s, t );
        assert(handle);
        
        // Evaluate the patch weights, identify the CVs and compute the limit frame
        // the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
        // compute the weights of these linear combinations
        patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights, dssWeights, dstWeights, dttWeights);

        // get the control variables for this patch
        OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);
        
        //Compute the weights for the coordinates and the derivatives wrt the control vertices
        //Set all weights to zero
        std::vector<TypeReal> vect_wc(nverts, 0.f);
        std::vector<TypeReal> vect_wu1(nverts, 0.f);
        std::vector<TypeReal> vect_wu2(nverts, 0.f);
        std::vector<TypeReal> vect_wu11(nverts, 0.f);
        std::vector<TypeReal> vect_wu12(nverts, 0.f);   
        std::vector<TypeReal> vect_wu22(nverts, 0.f); 
        for (int cv = 0; cv < cvs.size(); ++cv)
        {
            if (cvs[cv] < nverts)
            {
                vect_wc[ cvs[cv] ] += pWeights[cv];
                vect_wu1[ cvs[cv] ] += dsWeights[cv];
                vect_wu2[ cvs[cv] ] += dtWeights[cv];
                vect_wu11[ cvs[cv] ] += dssWeights[cv];
                vect_wu12[ cvs[cv] ] += dstWeights[cv];    
                vect_wu22[ cvs[cv] ] += dttWeights[cv];
            }
            else
            {
                const unsigned int ind_offset = cvs[cv] - nverts;
                //Look at the stencil associated to this local point and distribute its weight over the control vertices
                size_t size_st = st[ind_offset].GetSize();
                OpenSubdiv::Far::Index const *st_ind = st[ind_offset].GetVertexIndices();
                float const *st_weights = st[ind_offset].GetWeights();
                for (size_t s = 0; s < size_st; s++)
                {
                    vect_wc[ st_ind[s] ] += pWeights[cv]*st_weights[s];
                    vect_wu1[ st_ind[s]] += dsWeights[cv]*st_weights[s];
                    vect_wu2[ st_ind[s]] += dtWeights[cv]*st_weights[s];
                    vect_wu11[ st_ind[s]] += dssWeights[cv]*st_weights[s];
                    vect_wu12[ st_ind[s]] += dstWeights[cv]*st_weights[s];   
                    vect_wu22[ st_ind[s]] += dttWeights[cv]*st_weights[s];  
                }
            }
        }
        //Store the weights into Eigen triplets
        for (int cv=0; cv<nverts; cv++)
        {
            if (vect_wc[cv] != 0.f)
            {
                Lcols.push_back(cv*3);// xcomponent
                Lcols.push_back(cv*3+1); //ycomponent
                Lcols.push_back(cv*3+2); //zcomponent
                Lrows.push_back(isample*3);
                Lrows.push_back(isample*3+1);
                Lrows.push_back(isample*3+2);
                Lvals.push_back(vect_wc[cv] ); Lvals.push_back(vect_wc[cv] ); Lvals.push_back(vect_wc[cv] );
                Dsvals.push_back( vect_wu1[cv] ); Dsvals.push_back( vect_wu1[cv] ); Dsvals.push_back( vect_wu1[cv] );
                Dtvals.push_back( vect_wu2[cv] ); Dtvals.push_back( vect_wu2[cv] ); Dtvals.push_back( vect_wu2[cv] );
                Dssvals.push_back( vect_wu11[cv] ); Dssvals.push_back( vect_wu11[cv] ); Dssvals.push_back( vect_wu11[cv] );
                Dstvals.push_back( vect_wu12[cv] ); Dstvals.push_back( vect_wu12[cv] ); Dstvals.push_back( vect_wu12[cv] );  
                Dttvals.push_back( vect_wu22[cv] ); Dttvals.push_back( vect_wu22[cv] ); Dttvals.push_back( vect_wu22[cv] ); 
            }
        }
        
        
    }
    // create sparse matrix L that evaluates the limit surface at parameter values
    L.resize( 3*nsamples, 3*nverts );
    build_spsMatrix(Lrows, Lcols, Lvals, L);
    

	// create sparse matrix L that evaluates the tangent vector of limit surface at parameter values
	Ds.resize( 3*nsamples, 3*nverts );
	Dt.resize( 3*nsamples, 3*nverts );
	build_spsMatrix(Lrows, Lcols, Dsvals, Ds);
	build_spsMatrix(Lrows, Lcols, Dtvals, Dt);

		// create sparse matrix L that evaluates the tangent vector of limit surface at parameter values
	Dss.resize( 3*nsamples, 3*nverts );
	Dst.resize( 3*nsamples, 3*nverts );
	Dtt.resize( 3*nsamples, 3*nverts );
	build_spsMatrix(Lrows, Lcols, Dssvals, Dss);
	build_spsMatrix(Lrows, Lcols, Dstvals, Dst);
	build_spsMatrix(Lrows, Lcols, Dttvals, Dtt);
    
}


/*--------------------------------- quadrature sample --------------------------*/






void qSampleD2LimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, size_t n_quad, std::vector<TypeReal> &S, std::vector<TypeReal> &Ss, std::vector<TypeReal> &St, std::vector<TypeReal> &Sss, std::vector<TypeReal> &Sst, std::vector<TypeReal> &Stt, std::vector<TypeReal> &qWeights)
{
	size_t n_faces = facenverts.size(); 
	std::vector<TypeReal> s_params, t_params;
	std::vector<int> f_params;
	quadratureParameterSample( n_faces, n_quad, f_params, s_params, t_params, qWeights);
	SampleD2LimitSurface(vertices, facenverts, faceverts, f_params, s_params, t_params, S, Ss, St, Sss, Sst, Stt);   
}




void qSampleLimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, size_t n_quad, std::vector<TypeReal> &S, std::vector<TypeReal> &Ss, std::vector<TypeReal> &St, std::vector<TypeReal> &qWeights)
{
	size_t n_faces = facenverts.size(); 
	std::vector<TypeReal> s_params, t_params;
	std::vector<int> f_params;
	quadratureParameterSample( n_faces, n_quad, f_params, s_params, t_params, qWeights);
	SampleLimitSurface(vertices, facenverts, faceverts, f_params, s_params, t_params, S, Ss, St);    
}

TypeReal area_surface(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex )                                               
{

	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();   
    size_t quadrature = 3;
    size_t quad_samples =  quadrature * quadrature;

	// sample surface at quadrature
	std::vector<TypeReal> S, Ss, St, qWeights;
	std::vector<TypeReal> s_params, t_params;
	std::vector<int> f_params;
	quadratureParameterSample( n_faces, quadrature, f_params, s_params, t_params, qWeights);
	SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, S, Ss, St);
    
	// compute surface area to normalize alpha
	TypeReal surface_area = 0;
	int count = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj<quad_samples; jj++)
		{
			TypeReal dist = 0;
			TypeReal ip = 0;			
			TypeReal E, F, G; E = 0; F = 0; G = 0;
			for(int idim=0; idim<3; idim++)
			{
				// compute first fondamental form I
				//[  s_uu \dot n   S_uv \dot n ] = [ E F ]
				//[  s_vu \dot n   S_vv \dot n ]   [ F G ]
				E += Ss[count*3+idim]*Ss[count*3+idim];
				F += Ss[count*3+idim]*St[count*3+idim];
				G += St[count*3+idim]*St[count*3+idim];	
			}
			// compute metric and curvatures
			TypeReal metric = E * G - std::pow( F, 2 );
			surface_area += std::sqrt(metric) * qWeights[count];                                  
			count += 1;
		}
	}
	
	return surface_area;
}
#endif
