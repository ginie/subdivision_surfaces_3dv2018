//
//  MeshOp
//  
//
//  Created by virginia estellers on 1/3/17.
//
//

#ifndef __MeshOp__
#define __MeshOp__



//definition of a non-mutable lvalue property map,
//with the get function as a friend function to give it
//access to the private member


class MeshOp
{	// typedefs for cgal
	
	private:
		std::vector<TypeReal> vertices;
		std::vector<TypeReal> principalK1;
		std::vector<TypeReal> principalK2;
		std::vector<TypeReal> principalD1;
		std::vector<TypeReal> principalD2;
		std::vector<TypeReal> principalD3;
		std::vector<TypeReal> confidence;
		std::vector<int> 	  face_nvertex;
		std::vector<int> 	  face_vertex;
		void meshNeighbourhoods( std::vector<int> &face_nvertex, std::vector<int> &face_vertex, int n_ring, std::vector<int> &vertexNeigh, std::vector<int> &sizeNeigh, std::vector<int> &startNeigh  );
		void meshVertexIncidence( std::vector<int> &face_nvertex, std::vector<int> &face_vertex, std::vector<int>& faceStart, std::vector<int> &vertex_face, std::vector<int> &vertex_nface, std::vector<int> &vertex_startFace  );
		
	public:
		MeshOp( const MeshOp &other) : vertices(other.vertices), face_nvertex(other.face_nvertex), face_vertex(other.face_vertex), principalK1(other.principalK1), principalK2(other.principalK2), principalD1(other.principalD1), principalD2(other.principalD2), principalD3(other.principalD3), confidence(other.confidence){}
		MeshOp() : vertices( std::vector<TypeReal>(0) ), face_nvertex( std::vector<int>(0) ), face_vertex( std::vector<int>(0) ), principalK1( std::vector<TypeReal>(0) ), principalK2( std::vector<TypeReal>(0) ), principalD1( std::vector<TypeReal>(0) ), principalD2( std::vector<TypeReal>(0) ), principalD3( std::vector<TypeReal>(0) ), confidence( std::vector<TypeReal>(0) ) {}

		
		// initialize vertices, face_nvertex, face_vertex, create KDtree, and estimate geometry
		MeshOp(std::vector<TypeReal> _vertices, std::vector<int> _face_nvertex, std::vector<int> _face_vertex);
		//
		TypeReal getPrincipalK1( int id ){ return principalK1[id]; };
		TypeReal getPrincipalK2( int id ){ return principalK2[id]; };
		TypeReal getPrincipalD1( int id, int dim ){ return principalD1[id*3+dim]; };
		TypeReal getPrincipalD2( int id, int dim ){ return principalD2[id*3+dim]; };
		TypeReal getPrincipalD3( int id, int dim ){ return principalD3[id*3+dim]; };
		TypeReal getConfidence( int id ){ return confidence[id]; };
		
		void getMesh( std::vector<TypeReal> &_vertices, std::vector<int> &_face_nvertex, std::vector<int> &_face_vertex ){ _vertices = vertices; _face_nvertex = face_nvertex; _face_nvertex = face_vertex; };
		void getGeometry( std::vector<TypeReal> &_principalK1, std::vector<TypeReal> &_principalK2, std::vector<TypeReal> &_principalD1, std::vector<TypeReal> &_principalD2, std::vector<TypeReal> &_principalD3, std::vector<TypeReal> &_confidence){ _principalK1 = principalK1; _principalK2 = principalK2; _principalD1 = principalD1; _principalD2 = principalD2; _principalD3 = principalD3; _confidence = confidence; };
		void getNormals( std::vector<TypeReal> &_principalD3){_principalD3 = principalD3;};
		void getVertices( std::vector<TypeReal> &_points){_points = vertices;};

};



#endif
 