#include <cassert>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <climits>
#include <cstdlib>
#include <cmath>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <time.h>

#include "TypeDef.h"
#include "IO.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "geometrySDS.h"




class Settings_Geometry
{
public:

    size_t		n_samples; 
    size_t      quadrature;
    std::string outFile;
    std::string sdsFile;
    bool		curvatures;
    bool 		sample_area;

    Settings_Geometry() :
    	n_samples(1000),
        quadrature(3),
        curvatures(false),
        sample_area(false),
        sdsFile(),
        outFile()
    {
    }
    
    void error(const char *msg)	{
		fprintf(stderr,"sds | ERROR | %s\n",(msg)?msg:"");
		std::exit(0);
	}


	void printOptions(void){
		printf("       -quad|-quadrature  <%lu>\n", quadrature);
		printf("     -n_samp|-n_samples  <%lu>\n", n_samples);
		if( sample_area )
		{
			printf("-sample proportional to area\n");
		}else{
			printf("-sample proportional to curvature\n");
		}
		if( curvatures )
		{
			printf("-sample normals and principal directions\n");
		}else{
			printf("-sample only normals\n");
		}
	}

	void print_usage(void){
		fprintf(stderr,"USAGE\n");
		fprintf(stderr,"sampleSDSgeometry [options] sdsFile outFile\n");
		printOptions();
	}
	
    void parseCommandLine(int argc, char **argv){
        if (argc==1)
		{
			print_usage();
			exit(0);
		}
		int aux = 0;
		for (int i=1; i<argc; ++i)
		{
			if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); exit(0); }
			else if (!strcmp(argv[i],"-quad")  || !strcmp(argv[i],"-quadrature"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(quadrature))<1) { error("parsing -quadrature"); }
			}
			else if (!strcmp(argv[i],"-n_samp")  || !strcmp(argv[i],"-n_samples"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(n_samples))<1) { error("parsing -n_samples"); }
			}     
			else if (!strcmp(argv[i],"-sample_area") )
			{
				sample_area = true;
			}    
			else if (!strcmp(argv[i],"-curvatures") )
			{
				curvatures = true;
			}  
			else if (argv[i][0]=='-') { error("unknown option"); }
			else if (sdsFile.size()==0) { sdsFile = std::string(argv[i]); }
			else if (outFile.size()==0) { outFile = std::string(argv[i]); }
		}
		if (sdsFile.size()==0)              { error("no inFile");  }
		if (outFile.size()==0)                { error("no outFile"); }
    }
    
    
};





int main(int argc, char *argv[]) {
  
	
	Settings_Geometry D;
    D.parseCommandLine(argc, argv);
    D.printOptions();
     
    clock_t t0 = clock();
    
    
    std::vector<TypeReal> vertices;
    std::vector<int> face_nvertex, face_vertex;

    // read input control mesh
    std::string format = D.sdsFile.substr(D.sdsFile.size()-3,3);
    if( format.compare( std::string("ply"))==0 ){
    	std::vector<TypeReal> dummy;
    	read_ply( D.sdsFile, vertices, face_nvertex, face_vertex, dummy, true);
    }else{
    	printf("unknown input file type, expecting .ply\n");
    	return(0);
    }
    int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();
    printf("control mesh of subdivision surface has %d vertices %d faces\n", n_vertex, n_faces);

	size_t quadrature = D.quadrature;
    std::vector<TypeReal> quad_metric;
    TypeReal Smetric;
    if( D.sample_area )
    {
    	
    	Smetric = quad_surface(vertices, face_nvertex, face_vertex, quadrature, quad_metric );
    }else
    {
    	Smetric = quad_GaussCurvature(vertices, face_nvertex, face_vertex, quadrature, quad_metric );
		Smetric = 0;
		for(int ii = 0; ii< n_faces; ii++ )
		{
			quad_metric[ii] = std::abs(quad_metric[ii]);
			Smetric += std::abs(quad_metric[ii]);
		}
    } 
    

    // sample proportional to quad area
    TypeReal density = D.n_samples/ Smetric;
    std::vector<int> f_params;
	std::vector<TypeReal> s_params, t_params;
    for(int ii = 0; ii< n_faces; ii++ )
    {
    	int n_dimsamples = (int) std::ceil( std::sqrt( quad_metric[ii] * density ) );
    	if( n_dimsamples <1 ){ n_dimsamples = 1;}
    	
    	// uniformly sample the quad
    	TypeReal delta_s = 1.0/n_dimsamples;
    	TypeReal delta_t = delta_s;
    	for (int s_sample=0; s_sample<n_dimsamples; s_sample++)
        {
            for (int t_sample=0; t_sample<n_dimsamples; t_sample++)
            {
            	f_params.push_back(ii);
				s_params.push_back( (0.5 + s_sample)*delta_s );
				t_params.push_back( (0.5 + t_sample)*delta_t );
            }
        }
    }

    std::vector<TypeReal> points, normals, metric;
    std::vector<TypeReal> principalCurvature1, principalCurvature2, principalDirection1, principalDirection2;
    if( D.curvatures )
    {
    	get_FrenetFrame(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, points, normals, metric, principalCurvature1, principalCurvature2, principalDirection1, principalDirection2 );
    }else
    {
    	 get_normals(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, points, normals, metric );
    }


    
    
    
    
    
    
   	clock_t t1 = clock();
   	printf("computations took %f seconds\n", (double)(t1-t0) / ((double)CLOCKS_PER_SEC));
    
    // output results
    std::string outfilename(D.outFile);
    std::vector<int> empty(0);
    std::vector< std::string > properties;
    properties.clear(); properties.push_back("nx"); properties.push_back("ny"); properties.push_back("nz");
    write_ply_mesh( outfilename.append("_normals.ply"), points, empty, empty, normals, properties );
    
    if( D.curvatures )
    {
		outfilename.assign(D.outFile);
		//properties.clear(); properties.push_back("nx"); properties.push_back("ny"); properties.push_back("nz");
		write_ply_mesh( outfilename.append("_principalDirection1.ply"), points, empty, empty, principalDirection1, properties );    
	
		outfilename.assign(D.outFile);
		//properties.clear(); properties.push_back("nx"); properties.push_back("ny"); properties.push_back("nz");
		write_ply_mesh( outfilename.append("_principalDirection2.ply"), points, empty, empty, principalDirection2, properties );    
	
	
		std::vector<TypeReal> meanK( principalCurvature1.size() );
		std::vector<TypeReal> GaussK( principalCurvature1.size() );
		for(int ii=0; ii< principalCurvature1.size(); ii++)
		{
			meanK[ii] = 0.5*( principalCurvature1[ii] + principalCurvature2[ii] );
			GaussK[ii] = ( principalCurvature1[ii] * principalCurvature2[ii] );
		}
		outfilename.assign(D.outFile);
		properties.clear(); properties.push_back("GaussCurvature"); 
		write_ply_mesh( outfilename.append("_GaussCurvature.ply"), points, empty, empty, GaussK, properties );    
	
		outfilename.assign(D.outFile);
		properties.clear(); properties.push_back("meanCurvature"); 
		write_ply_mesh( outfilename.append("_meanCurvature.ply"), points, empty, empty, meanK, properties );    
    }
    
    
    return 0;
}



