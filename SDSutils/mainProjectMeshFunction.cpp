#include <cassert>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

#include <cfloat>
#include <algorithm>
#include <queue> 
#include <map>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <limits>
#include <time.h>




#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "IO.h"
#include "mesh2sdsProjection.h"





class Settings_Project
{
public:

    size_t      quadrature;
    size_t      uniformRefinement;
    std::string outFile;
    std::string meshFile;
    std::string functionFile;
    std::string sdsFile;
    int			visualize;
public:
    Settings_Project() :
        uniformRefinement(0),
        quadrature(4),
        sdsFile(),
        functionFile(),
        meshFile(),
        outFile(),
        visualize(0)
    {
    }
    
	void error(const char *msg)
	{
		fprintf(stderr,"SDSproject | ERROR | %s\n",(msg)?msg:"");
		exit(0);
	}
	
	
	void printOptions(void)
	{
		printf("       -uniR|-uniREf  <%lu>\n", uniformRefinement);  
		printf("       -quad|-quadrature  <%lu>\n", quadrature);
	}
	
	void print_usage(void)
	{
		fprintf(stderr,"USAGE\n");
		fprintf(stderr,"  SDSProject [options] meshPlyFile meshFunctiontxtFile sdsPlyFile outtxtFile\n");
		printOptions();
	}
	void parseCommandLine(int argc, char **argv)
	{
		if (argc==1)
		{
			print_usage();
			exit(0);
		}
		
		for (int i=1; i<argc; ++i)
		{
			if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); exit(0); }
			else if (!strcmp(argv[i],"-quad")  || !strcmp(argv[i],"-quadrature"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(quadrature))<1) { error("parsing -quadrature"); }
			}
			else if (!strcmp(argv[i],"-uniR")  || !strcmp(argv[i],"-uniRef"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(uniformRefinement))<1) { error("parsing -uniformRefinement"); }
			} 
			else if (!strcmp(argv[i],"-VIZ") || !strcmp(argv[i],"-viz"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%d",&(visualize))<0) { error("parsing -viz"); }
			} 			
			else if (argv[i][0]=='-') { error("unknown option"); }
			else if (sdsFile.size()==0) { sdsFile = std::string(argv[i]); }
			else if (meshFile.size()==0) { meshFile = std::string(argv[i]); }
			else if (functionFile.size()==0) { functionFile = std::string(argv[i]); }
			else if (outFile.size()==0) { outFile = std::string(argv[i]); }
		}
		if (sdsFile.size()==0)              { error("no sdsFile");  }		
		if (meshFile.size()==0)                { error("no meshFile"); }
		if (functionFile.size()==0)              { error("no functionFile");  }
		if (outFile.size()==0)                { error("no outFile"); }
	}
   
};




void real2rgb( TypeReal *v, TypeReal *rgb, int sz)
{
	TypeReal vmax = v[0];
	TypeReal vmin = v[0];
	for(int ii=0; ii< sz; ii++)
	{
		if( v[ii] < vmin ){ vmin = v[ii]; }
		if( v[ii] > vmax ){ vmax = v[ii]; }
	}
	double dv = vmax - vmin;
	//printf("color range %f %f\n", vmin, vmax);
	for(int ii=0; ii< sz; ii++)
	{
		rgb[ii*3] = 1.0;
		rgb[ii*3+1] = 1.0;
		rgb[ii*3+2] = 1.0;
				
		if( v[ii] < (vmin + 0.25 * dv) )
		{
		  rgb[ii*3] = 0;
		  rgb[ii*3+1] = 4 * (v[ii] - vmin) / dv;
		}
		else if( v[ii] < (vmin + 0.5 * dv) )
		{
		  rgb[ii*3] = 0;
		  rgb[ii*3+2] = 1 + 4 * (vmin + 0.25 * dv - v[ii]) / dv;
		}
		else if( v[ii] < (vmin + 0.75 * dv) )
		{
		  rgb[ii*3] = 4 * (v[ii] - vmin - 0.5 * dv) / dv;
		  rgb[ii*3+2] = 0;
		} 
		else
		{
		  rgb[ii*3+1] = 1 + 4 * (vmin + 0.75 * dv - v[ii]) / dv;
		  rgb[ii*3+2] = 0;
		}
	}
	
}



int main(int argc, char *argv[]) {
  
	Settings_Project D;
    D.parseCommandLine(argc, argv);
    D.printOptions();
    std::string outfilename(D.outFile);
    std::vector<TypeReal> vertices, vertex_vars;
    std::vector<int> face_nvertex, face_vertex;
   
	
    // read input mesh
    read_ply( D.sdsFile, vertices, face_nvertex, face_vertex, vertex_vars, true);
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();   
	vertex_stats( vertices,  face_nvertex,  face_vertex );
	std::vector<int> eVertices;
	find_extraordinaryVertex( vertices, face_nvertex, face_vertex, eVertices );
	  
    
    // read input mesh to determine the location of the verices
    std::vector<TypeReal> meshVertices, vFunction, projectedFunction, dummy;
    std::vector<int> meshFace_nvertex, meshFace_vertex;
    read_ply( D.meshFile, meshVertices, meshFace_nvertex, meshFace_vertex, dummy, true);
    //read_off( D.meshFile, meshVertices, meshFace_nvertex, meshFace_vertex);
    
    // read the input segmentation: this is described by the values of a function evaluated at the location of the mesh vertices
    FILE *mFile;
    vFunction.resize( meshVertices.size()/3 );
    std::ifstream file(D.functionFile);
    printf("reading input segmentation...\n");
    TypeReal in;
    int ii=0;
    while(file >> in)
    {
    	vFunction[ii] = in;
    	ii += 1;
    }
    printf("\t done\n");
    // project it into the basis
   //projectVertexFunction( meshVertices, vFunction, vertices, face_nvertex, face_vertex, projectedFunction, D.quadrature );
   if( D.uniformRefinement>0)
   {
   	   UniformRefineSurface( vertices, face_nvertex, face_vertex, D.uniformRefinement );
   	   n_vertex = vertices.size()/3;
   }
   projectedFunction.resize(n_vertex);
   interpolateVertexFunction( meshVertices, vFunction, &vertices[0], &projectedFunction[0], n_vertex );
 
    
    // outfule segmentation
    std::string outFile(D.outFile);
    mFile = fopen (outFile.c_str(),"w"); 
    fprintf(mFile,"%d\n", n_vertex);
    for (int ii=0; ii< n_vertex; ii++)
    {
		fprintf(mFile,"%f\n", projectedFunction[ii]);
	}
    fclose(mFile);
    
    
    // visualize projected function
	std::vector< TypeReal> vcolors( projectedFunction.size()*3 );
	real2rgb( &projectedFunction[0], &vcolors[0], projectedFunction.size());
	outfilename.assign(D.outFile);
    outfilename.replace( outfilename.rfind('.'),  4, std::string("_refinedControlMesh3.obj") );
    write_obj_coloredmesh(  outfilename.append(".obj"), vertices, face_nvertex, face_vertex, vcolors);	
     
    /*std::vector<TypeReal> vertexValues(projectedFunction);
	std::vector<TypeReal> cpvertices(vertices);
	std::vector<int> cpface_nvertex( face_nvertex);
	std::vector<int> cpface_vertex(face_vertex);    
	UniformRefineSurface( cpvertices, cpface_nvertex, cpface_vertex, vertexValues, 3);
	
	// transfer to faces
	n_faces = cpface_nvertex.size();
	int count = 0;
	std::vector<TypeReal> faceValues(n_faces, 0);
	for(int ii=0; ii<n_faces; ii++)
	{
		for(int jj=0; jj<cpface_nvertex[ii]; jj++)
		{
			//faceValues[ii] += refinedValues[ cpface_vertex[count] ];
			faceValues[ii] += vertexValues[ cpface_vertex[count] ];
			count += 1;
		}
	}*/
        
    return 0;
}

