#ifndef __geometrySDS__
#define __geometrySDS__



void compute_FrenetFrame( TypeReal *Ss, TypeReal *St, TypeReal *Sss, TypeReal *Sst, TypeReal *Stt, TypeReal *normals, TypeReal *metric, TypeReal *principalCurvature1, TypeReal *principalCurvature2, TypeReal *principalDirection1, TypeReal *principalDirection2, size_t n_samples );
void compute_normals( TypeReal *Ss, TypeReal *St, TypeReal *normals, TypeReal *metric, size_t n_samples );

//
void get_normals(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, std::vector<int> &f_params, std::vector<TypeReal>& s_params, std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& normals, std::vector<TypeReal>& metric );
void get_FrenetFrame(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, std::vector<int> &f_params, std::vector<TypeReal>& s_params, std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& normals, std::vector<TypeReal>& metric, std::vector<TypeReal> &principalCurvature1, std::vector<TypeReal> &principalCurvature2, std::vector<TypeReal> &principalDirection1, std::vector<TypeReal> &principalDirection2 );

//
TypeReal quad_surface(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_area ) ;                                             

TypeReal quad_meanCurvature(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_curvature );                                               

TypeReal quad_GaussCurvature(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_curvature );                                              

TypeReal quad_maxCurvature(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_curvature );



#endif
