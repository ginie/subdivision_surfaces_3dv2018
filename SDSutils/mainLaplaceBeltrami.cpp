#include <cassert>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cfloat>
#include <algorithm>
#include <queue> 
#include <map>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <limits>
#include <time.h>



#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "LaplaceBeltrami.h"
#include "IO.h"


//#include <SymGEigsSolver.h>
//#include <MatOp/SparseSymMatProd.h>
//#include <MatOp/SparseCholesky.h>
//#include <MatOp/SparseRegularInverse.h>



class Settings_LB
{
public:

    size_t      quadrature;
    size_t      nEigenVectors;
    std::string outFile;
    std::string meshFile;
    int			visualize;
public:
    Settings_LB() :
        quadrature(4),
        nEigenVectors(3),
        meshFile(),
        outFile(),
        visualize(0)
    {
    }
    
	// class constructor and functions
	void error(const char *msg){
		fprintf(stderr,"SDSEigen | ERROR | %s\n",(msg)?msg:"");
		exit(0);
	}


	void printOptions(void){
		printf("         -oFile|-o  %s\n", outFile.c_str());
		printf("       -sdsFile|-sds %s\n", meshFile.c_str());		
		printf("       -quad|-quadrature  <%lu>\n", quadrature);
		printf("       -nEig|-nEigenVectors  <%lu>\n", nEigenVectors);
	}

	void print_usage(void){
		fprintf(stderr,"USAGE\n");
		fprintf(stderr,"  SDSEigen [options] -sdsFile meshFile -oFile outFile\n");
		printOptions();
	}
	
	void parseCommandLine(int argc, char **argv)
	{
		if (argc==1)
		{
			print_usage();
			exit(0);
		}
		
		for (int i=1; i<argc; ++i)
		{
			if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); exit(0); }
			else if (!strcmp(argv[i],"-quad")  || !strcmp(argv[i],"-quadrature"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(quadrature))<1) { error("parsing -quadrature"); }
			}
			else if (!strcmp(argv[i],"-nEig")  || !strcmp(argv[i],"-nEigenVectors"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(nEigenVectors))<1) { error("parsing -nEigenVectors"); }
			}       
			else if (!strcmp(argv[i],"-VIZ") || !strcmp(argv[i],"-viz"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%d",&(visualize))<0) { error("parsing -viz"); }
			}    
			else if (!strcmp(argv[i],"-sds") || !strcmp(argv[i],"-sdsFile"))
			{
				i++;
				meshFile = std::string(argv[i]);
			}
			else if (!strcmp(argv[i],"-o") || !strcmp(argv[i],"-outFile"))
			{
				i++;
				outFile = std::string(argv[i]);
			}			
			else if (argv[i][0]=='-') { error("unknown option"); }
		}
		if (meshFile.size()==0)              { error("no inFile");  }
		if (outFile.size()==0)                { error("no outFile"); }
	}
    
};



int main(int argc, char *argv[]) {
  
	Settings_LB D;
    D.parseCommandLine(argc, argv);
    D.printOptions();
    std::string outfilename(D.outFile);
    std::vector<TypeReal> vertices, vertex_vars;
    std::vector<int> face_nvertex, face_vertex;
   
	
    // read input mesh
    read_ply( D.meshFile, vertices, face_nvertex, face_vertex, vertex_vars, true);
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();   
	
    
    int n_eig = D.nEigenVectors;
    SpMat stiffM, massM, A, rstiffM, rmassM;
    LaplaceBeltrami(vertices, face_nvertex, face_vertex, stiffM, massM, D.quadrature);
 	
    
    /* to interface with matlab, you can store the non-zero entries of the mass and stiffness matrices, the control mesh, and the subdivision schemes */
    FILE *mFile;
    std::string outFile(D.outFile);
    mFile = fopen (outFile.append("_massMatrix.txt").c_str(),"w"); 
    fprintf(mFile, "%d %d %d\n", massM.nonZeros(), massM.rows(), massM.cols());
	for(int k=0; k<massM.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(massM,k); it; ++it)
        {
            fprintf(mFile, "%d %d %f\n", it.row(), it.col(), it.value());
        }
    }
    fclose(mFile);
    outFile = std::string(D.outFile);
    mFile = fopen (outFile.append("_stiffnessMatrix.txt").c_str(),"w"); 
    fprintf(mFile, "%d %d %d\n", stiffM.nonZeros(), stiffM.rows(), stiffM.cols());
	for(int k=0; k<stiffM.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(stiffM,k); it; ++it)
        {
            fprintf(mFile, "%d %d %f\n", it.row(), it.col(), it.value());
        }
    }
    fclose(mFile);    

    
    
    
    int n_refvisualization = 3;
    // compute control mesh of refined surface (used as proxy for the subdivision surface for visualization) and the interpolation matrix for this mesh
	// create refinement matrices from uniform stencils and refine the surface
	// A_k \in R^{ n_atlevelk x n_0 )
	A.resize(n_vertex,n_vertex);
	std::vector<TypeReal> rvertices(vertices);
    std::vector<int> rface_nvertex(face_nvertex); 
    std::vector<int> rface_vertex(face_vertex);    
    std::vector<TypeReal> ones(n_vertex,1.0);
    build_spsDiagMatrix(ones, A );
    for( int ii=0; ii< n_refvisualization; ii++)
    {
    	SpMat A0(A);
    	SpMat Ar, A3;
    	std::vector< TypeReal> vertices0(rvertices);
    	std::vector<int> facenverts0(rface_nvertex);
    	std::vector<int> faceverts0(rface_vertex);
    	UniformRefineSurface( vertices0, facenverts0, faceverts0, 1, rvertices, rface_nvertex, rface_vertex, Ar, A3);
    	A = Ar * A0;
    	
    	if( ii = n_refvisualization-1)
    	{
			outFile = std::string(D.outFile);
			outFile.append("_refine");
			outFile.append(std::to_string(ii+1));
			outFile.append(".txt");
			mFile = fopen (outFile.c_str(),"w"); 
			fprintf(mFile, "%d %d %d\n", A.nonZeros(), A.rows(), A.cols());
			for(int k=0; k<A.outerSize(); ++k)
			{
				for (SpMat::InnerIterator it(A,k); it; ++it)
				{
					fprintf(mFile, "%d %d %f\n", it.row(), it.col(), it.value());
				}
			}
			fclose(mFile); 
			outFile = std::string(D.outFile);
			outFile.append("_ref");
			outFile.append(std::to_string(ii+1));
			outFile.append(".off");
			mFile = fopen (outFile.c_str(),"w"); 
			write_off_mesh( outFile, rvertices, rface_nvertex, rface_vertex);
		}

    }
    
   
	

    {
	outfilename = std::string(D.meshFile);
	outfilename.replace( outfilename.rfind('.'),  4, std::string(".off") );
	write_off_mesh( outfilename, vertices, face_nvertex, face_vertex);
	}
    
    
    
    
    
    // Construct matrix operation object using the wrapper classes	
	/*SpVect eigVals = SpVect::Zero(n_eig);
	Mat eigVects = Mat::Zero(n_vertex, n_eig);
	SpMat shift_stiffM = stiffM + 1e-3 * massM;
	// Construct generalized eigen solver object, requesting the largest three generalized eigenvalues
	Spectra::SparseSymMatProd<TypeReal> op1(massM);
	Spectra::SparseCholesky<TypeReal> op2(shift_stiffM);
	Spectra::SymGEigsSolver<TypeReal, Spectra::LARGEST_MAGN, Spectra::SparseSymMatProd<TypeReal>, Spectra::SparseCholesky<TypeReal>, Spectra::GEIGS_CHOLESKY > geigs(&op1, &op2, n_eig, 2*n_eig);
	// Initialize and compute
	geigs.init();
	int nconv = geigs.compute();
	// Retrieve results
	if(geigs.info() != Spectra::SUCCESSFUL)
	{ 
		printf("geigs.info FAIL");		
	}else
	{
		eigVals = geigs.eigenvalues();
		eigVects = geigs.eigenvectors();
		for(int kk = 0; kk<n_eig; kk++ )
		{
			printf("%f\n", kk, 1.0/eigVals[kk] -1e-3);
		} 
	}

	
    
    
    int count = 0;
    std::vector<mrpt::gui::CDisplayWindow3D> windows(n_eig); 
    for(int kk = 0; kk<n_eig; kk++ )
    {
    	//SpVect vertexValues(n_vertex);
    	std::vector<TypeReal> vertexValues(n_vertex);
		for(int ii=0; ii<n_vertex; ii++)
		{
			vertexValues[ii] = eigVects(ii,kk);
		}
		std::vector<TypeReal> cpvertices(vertices);
		std::vector<int> cpface_nvertex( face_nvertex);
		std::vector<int> cpface_vertex(face_vertex);    
		UniformRefineSurface( cpvertices, cpface_nvertex, cpface_vertex, vertexValues, 3);
		
		// transfer to faces
		n_faces = cpface_nvertex.size();
		count = 0;
		std::vector<TypeReal> faceValues(n_faces, 0);
		for(int ii=0; ii<n_faces; ii++)
		{
			for(int jj=0; jj<cpface_nvertex[ii]; jj++)
			{
				//faceValues[ii] += refinedValues[ cpface_vertex[count] ];
				faceValues[ii] += vertexValues[ cpface_vertex[count] ];
				count += 1;
			}
		}
		render_colorControlMesh( windows[kk], cpvertices, cpface_nvertex, cpface_vertex, faceValues, false, false);
		windows[kk].setWindowTitle( std::to_string(1.0/eigVals[kk] - 1e-3) );
	}
	std::cin.ignore();*/
    
    
    return 0;
}

