#include <cassert>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cfloat>
#include <algorithm>
#include <queue> 
#include <map>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <utility>
#include <boost/multi_array.hpp>
#include <limits>
#include <time.h>


#include "kdtree2.h"
#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "IO.h"



class Settings_evaluate
{
public:

    std::string 			outFile;
    std::string 			meshFile;
    std::string				pointsFile;
    int						visualize;
public:
    Settings_evaluate() :
        meshFile(),
        pointsFile(),
        outFile(),
        visualize(0)
    {
    }
    
	// class constructor and functions
	void error(const char *msg){
		fprintf(stderr,"sampleTMeshBasis | ERROR | %s\n",(msg)?msg:"");
		exit(0);
	}


	void printOptions(void){
		printf("          -pFile|-p  %s\n", pointsFile.c_str());
		printf("          -oFile|-o  %s\n", outFile.c_str());
		printf("       -meshFile|-m %s\n", meshFile.c_str());
	}

	void print_usage(void){
		fprintf(stderr,"USAGE\n");
		fprintf(stderr,"  sampleTMeshBasis -meshFile meshFile -oFile outFile -pFile samplePoints\n");
		printOptions();
	}
	
	void parseCommandLine(int argc, char **argv)
	{
		if (argc==1)
		{
			print_usage();
			exit(0);
		}
		
		for (int i=1; i<argc; ++i)
		{
			if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); exit(0); }
			else if (!strcmp(argv[i],"-p") || !strcmp(argv[i],"-pointsFile"))
			{
				i++;
				pointsFile = std::string(argv[i]);
			}        
			else if (!strcmp(argv[i],"-m") || !strcmp(argv[i],"-meshFile"))
			{
				i++;
				meshFile = std::string(argv[i]);
			}
			else if (!strcmp(argv[i],"-o") || !strcmp(argv[i],"-outFile"))
			{
				i++;
				outFile = std::string(argv[i]);
			}
		}
		if (meshFile.size()==0)              { error("no meshFile");  }
		if (pointsFile.size()==0)            { error("no pointsFile");  }
		if (outFile.size()==0)               { error("no outFile"); }
	}
    
};



void UniformTriangleSample( size_t s_samples, size_t t_samples, std::vector<TypeReal> &w_params)
{
    TypeReal delta_s = 1.0/s_samples;
    TypeReal delta_t = 1.0/t_samples;
    size_t n_samples = s_samples * t_samples;
    w_params.resize(n_samples*3);
    size_t count = 0;
    for (size_t s_sample=0; s_sample<s_samples; s_sample++)
	{
		TypeReal r1 = (0.5 + s_sample)*delta_s;
		TypeReal sqrt_r1 = std::sqrt(r1);
		for (size_t t_sample=0; t_sample<t_samples; t_sample++)
		{
			TypeReal r2 = (0.5 + t_sample)*delta_t;
			w_params[3*count] = 1.0 - sqrt_r1;
			w_params[3*count+1] = sqrt_r1* (1.0 - r2);
			w_params[3*count+1] = r2*sqrt_r1;
			count += 1;
		}
	}
}




Vect3 closesPointOnTriangle( const Vect3 *triangle, const Vect3 &sourcePosition )
{
    Vect3 edge0 = triangle[1] - triangle[0];
    Vect3 edge1 = triangle[2] - triangle[0];
    Vect3 v0 = triangle[0] - sourcePosition;

    TypeReal a = edge0.dot( edge0 );
    TypeReal b = edge0.dot( edge1 );
    TypeReal c = edge1.dot( edge1 );
    TypeReal d = edge0.dot( v0 );
    TypeReal e = edge1.dot( v0 );

    TypeReal det = a*c - b*b;
    TypeReal s = b*e - c*d;
    TypeReal t = b*d - a*e;

    if ( s + t < det )
    {
        if ( s < 0 )
        {
            if ( t < 0 )
            {
                if ( d < 0 )
                {
                    s = std::min( std::max( -d/a, (TypeReal) 0 ), (TypeReal) 1.0 );
                    t = 0;
                }
                else
                {
                    s = 0;
                    t = std::min( std::max( -e/c, (TypeReal) 0 ), (TypeReal) 1.0 );
                }
            }
            else
            {
                s = 0;
                t = std::min( std::max( -e/c, (TypeReal) 0 ), (TypeReal) 1.0 );
            }
        }
        else if ( t < 0 )
        {
            s = std::min( std::max( -d/a, (TypeReal) 0 ), (TypeReal) 1.0 );
            t = 0;
        }
        else
        {
            TypeReal invDet = 1.0 / det;
            s *= invDet;
            t *= invDet;
        }
    }
    else
    {
        if ( s < 0 )
        {
            TypeReal tmp0 = b+d;
            TypeReal tmp1 = c+e;
            if ( tmp1 > tmp0 )
            {
                TypeReal numer = tmp1 - tmp0;
                TypeReal denom = a-2*b+c;
                s = std::min( std::max( numer/denom, (TypeReal) 0 ), (TypeReal) 1.0 );
                t = 1.0 - s;
            }
            else
            {
                t = std::min( std::max( -e/c, (TypeReal) 0 ), (TypeReal) 1.0 );
                s = 0;
            }
        }
        else if ( t < 0 )
        {
            if ( a+d > b+e )
            {
                TypeReal numer = c+e-b-d;
                TypeReal denom = a-2*b+c;
                s = std::min( std::max( numer/denom, (TypeReal) 0 ), (TypeReal) 1.0 );
                t = 1.0 - s;
            }
            else
            {
                s = std::min( std::max( -e/c, (TypeReal) 0 ), (TypeReal) 1.0 );
                t = 0.0 ;
            }
        }
        else
        {
            TypeReal numer = c+e-b-d;
            TypeReal denom = a-2*b+c;
            s = std::min( std::max( numer/denom, (TypeReal) 0 ), (TypeReal) 1.0 );
            t = 1.0 - s;
        }
    }
    Vect3 coord;
    coord[0] = 1.0 - s - t;
    coord[1] = s;
    coord[2] = t;
    return coord;
}

void KdSearch_closestTriangleParameter2Surface( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &v_params, std::vector<TypeReal> &w_params)
{

    size_t n_points = points.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t n_vertex = vertices.size()/3;
    // Generate uniform samples on each face
    size_t s_samples = 5;
    size_t t_samples = 5;
    size_t n_facesamples = s_samples * t_samples;
    size_t n_samples =  n_facesamples * n_faces;
    int count = 0;
    int pcount = 0;
    std::vector<TypeReal> weights;
    UniformTriangleSample( s_samples, t_samples, weights);
    std::vector<int> sample_vertex(n_samples*3);
    // create search data structures
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_samples][3]);
    for(int ii=0; ii< n_faces; ii++)
    {
    	// sample this face
    	int nvs = face_nvertex[ii];
    	std::vector<int> vIDs(nvs);
    	for(int jj=0; jj< face_nvertex[ii]; jj++)
    	{
    		vIDs[jj] =  face_vertex[ count ];
    		count += 1;
    	}
    	for(int jj=0; jj< n_facesamples; jj++)
    	{
    		for(int idim=0; idim<3; idim++){ KdTreePoints[pcount][idim] = 0; }
    		for(int kk=0; kk<nvs; kk++)
    		{
    			sample_vertex[ pcount*3 + kk] = vIDs[kk];
    			for(int idim=0; idim<3; idim++)
    			{
    				KdTreePoints[pcount][idim] += weights[jj*nvs + kk] * vertices[ vIDs[kk]*3 + idim ];
    			}
    		}
    		pcount += 1;
    	}
    }
	// notice it is in C-standard layout. 
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
    tree = new kdtree2::KDTree(KdTreePoints,false); 
    tree->sort_results = true;
    
    v_params.resize(3*n_points);
    w_params.resize(3*n_points);
    for( size_t ii=0; ii< n_points; ii++ )
    {
    	// for each point, find the closest surface sample
    	std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = points[ii*3+idim];
		}
		tree->n_nearest(query, 1, KdtreeRes);
		size_t c0sample = KdtreeRes[0].idx;	
		Vect3 triangle[3];
		Vect3 point;
		for(int idim=0; idim<3; idim++)
		{
			point[idim] = query[idim];
		}
		for(int iv=0; iv<3; iv++)
		{
			int v = sample_vertex[c0sample*3+iv];
			v_params[ii*3+iv] = v;
			for(int idim=0; idim<3; idim++)
			{
				triangle[iv][idim] = vertices[ 3*v + idim ];
			}
		}
		Vect3 coords = closesPointOnTriangle( triangle, point );
		for(int idim=0; idim<3; idim++)
		{
			w_params[ii*3+ idim] = coords[idim];
		}
    }
}

















int main(int argc, char *argv[]) {
  
	Settings_evaluate D;
    D.parseCommandLine(argc, argv);
    D.printOptions();
    std::string outfilename(D.outFile);
    std::vector<TypeReal> vertices, vertex_vars, points;
    std::vector<int> face_nvertex, face_vertex, dummy0, dummy1;
   
	
    // read input mesh
    read_ply( D.meshFile, vertices, face_nvertex, face_vertex, vertex_vars, true);
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();  
	
    // read points to be sampled
    std::string format = D.pointsFile.substr(D.pointsFile.size()-3,3);
    if( format.compare( std::string("ply"))==0 ){
    	read_ply( D.pointsFile, points, dummy0, dummy1, vertex_vars, false);
    }else{
    	printf("unknown input file type, expecting .ply\n");
    	return(0);
    }
    size_t n_points = points.size()/3;  
    printf("%lu input points to sample read\n", n_points);

    
    // find closest point
    std::vector<int> v_params;
    std::vector<TypeReal> w_params;
    KdSearch_closestTriangleParameter2Surface( points, vertices, face_nvertex, face_vertex, v_params, w_params);
    printf("sample parameters determined\n");
    
    // sample the subdivision spline basis functions at that point
    SpMat L(n_points, n_vertex);
    std::vector<T> tripletList;// list of non-zeros coefficients   
    for(int ii=0; ii<n_points; ii++)
    {
    	for(int jj=0; jj<3; jj++)
    	{
    		tripletList.push_back(T(ii, v_params[ii*3+jj], w_params[ii*3+jj] ) );
    	}
    }
    L.setFromTriplets(tripletList.begin(), tripletList.end());
    printf("basis sampled\n");
    
    /* to interface with matlab, you can store the non-zero entries of the mass and stiffness matrices, the control mesh, and the subdivision schemes */
    FILE *mFile;
    std::string outFile(D.outFile);
    mFile = fopen (outFile.append("_sampleBasis.txt").c_str(),"w"); 
    fprintf(mFile, "%lu %lu %lu\n", L.nonZeros(), L.rows(), L.cols());
	for(int k=0; k<L.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(L,k); it; ++it)
        {
            fprintf(mFile, "%lu %lu %f\n", it.row(), it.col(), it.value());
        }
    }
    fclose(mFile);
   

    
    
    return 0;
}

        
       