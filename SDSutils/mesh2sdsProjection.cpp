#include <vector>
#include <cmath>
#include <cstdio>
#include <boost/multi_array.hpp>



#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "mesh2sdsProjection.h"
#include "kdtree2.h"



void interpolateVertexFunction( const std::vector<TypeReal> &vertex, const std::vector<TypeReal> &vFunction, TypeReal *sPoints, TypeReal *sFunction, int n_points )
{
	int n_vertex = vertex.size()/3;
	int kNN = 1;
	
    // create search data structures
    // Kdtree with vertices
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_vertex][3]);
	for(int ii=0; ii<n_vertex; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = vertex[ii*3+idim];
		}
	}
	// notice it is in C-standard layout. 
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
    tree = new kdtree2::KDTree(KdTreePoints,false); 
    tree->sort_results = true;

	
	
	// for each sample, determine the value of the function at the samples by linearly interpolating the samples from its k-NN vertices
	for(int ii=0; ii<n_points; ii++)
	{
		std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] =sPoints[ii*3+idim];
		}
		tree->n_nearest(query, kNN, KdtreeRes);
		
		TypeReal intValue = 0;
		TypeReal sumWeights = 0;
		for( int jj=0; jj< kNN; jj++ )
		{
			size_t neighID = KdtreeRes[jj].idx;
			TypeReal weight = 0;
			for(int idim=0; idim<3; idim++)
			{
				weight += std::pow( query[idim] - vertex[neighID*3+idim], 2);
			}
			weight = std::sqrt(weight);
			weight = std::exp(-weight);
			intValue += weight * vFunction[neighID]; 
			sumWeights += weight;
		}
		sFunction[ii] = intValue/sumWeights;
	}
}

void projectVertexFunction( const std::vector<TypeReal> &points, const std::vector<TypeReal> &vFunction, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &projFunction,size_t quadrature )
{
	int n_vertex = vertices.size()/3;
	int n_faces = face_nvertex.size();
    size_t quad_samples =  quadrature * quadrature;

	// sample surface at quadrature
	printf("computing quadrature samples...");
	std::vector<TypeReal> s_params, t_params, qWeights;
	std::vector<int> f_params;
	quadratureParameterSample( n_faces, quadrature, f_params, s_params, t_params, qWeights);
	SpMat qL, qDs, qDt, qLt;
	SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, qL, qDs, qDt, true);
    int n_qpoints = qWeights.size();
    printf("done\n");
    qLt = qL.transpose();
    n_qpoints = quad_samples *n_faces;
    
    std::vector<TypeReal> sPoints( (n_qpoints+n_vertex)*3 );
    Eigen::Map< SpVect >  qPoints(&sPoints[0], n_qpoints*3);
    for(int idim=0; idim<3; idim++)
    {
    	SpVect coord(n_vertex);
    	for(int ii=0; ii<n_vertex; ii++)
    	{
    		coord[ii] = vertices[ii*3+idim];
    		sPoints[3*n_qpoints +ii*3+idim] =  coord[ii];
    	}
    	SpVect y = qL * coord;
    	for(int ii=0; ii<n_qpoints; ii++)
    	{
    		qPoints[ii*3+idim] = y[ii];
    	}    	
    }
    //printf("%d quadrature points computed\n", n_qpoints);
    
    // project into the basis
    printf("computing matrix...");
    SpMat qW(n_qpoints, n_qpoints);
    build_spsDiagMatrix( qWeights, qW );
    SpMat massM = qLt * qW * qL;
    printf("done\n");
    
    
    // compute rhs
    printf("computing rhs...");
    std::vector<TypeReal> sFunction(n_qpoints+n_vertex);
    interpolateVertexFunction( points, vFunction, &sPoints[0], &sFunction[0], n_qpoints+n_vertex );
    Eigen::Map< SpVect > y(&sFunction[0], n_qpoints);
    Eigen::Map< SpVect > x0(&sFunction[n_qpoints], n_vertex);
    SpVect rhs = qLt * qW * y;
    printf("done\n");
    
    // solve the problem
    printf("solving linear system...");
    projFunction.resize(n_vertex);
    Eigen::Map< SpVect > x(&projFunction[0], n_vertex);
    Eigen::ConjugateGradient<SpMat > solver;
    solver.setMaxIterations(500);
    //Eigen::SparseLU<SpMat> solver;
    solver.compute(massM);
    x = solver.solveWithGuess(rhs, x0);
    printf("done\n");

	
}



/*void projectVertexFunction( const std::vector<TypeReal> &points, const std::vector<TypeReal> &vFunction, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &projFunction, size_t quadrature )
{
   int n_vertex = vertices.size()/3;
   size_t n_faces = face_nvertex.size();
   
   std::vector<TypeReal> q_weights;
   std::vector<int> f_params;
   std::vector<TypeReal> s_params, t_params;
   quadratureParameterSample( n_faces, quadrature, f_params, s_params, t_params,  q_weights);
   SpMat qL, qDs, qDt, qLt;
   Eigen::Map<SpVect> qWeights( &q_weights[0], q_weights.size() );
   printf("computing quadrature samples...");
   SampleLimitSurface(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, qL, qDs, qDt, 1);
   int n_qpoints = qWeights.size();
   printf("done\n");
   qLt = qL.transpose();

   std::vector<TypeReal> sPoints( (n_qpoints+n_vertex)*3 );
   Eigen::Map< SpVect >  qPoints(&sPoints[0], n_qpoints*3);
   for(int idim=0; idim<3; idim++)
   {
       SpVect coord(n_vertex);
       for(int ii=0; ii<n_vertex; ii++)
       {
           coord[ii] = vertices[ii*3+idim];
           sPoints[3*n_qpoints +ii*3+idim] =  coord[ii];
       }
       SpVect y = qL * coord;
       for(int ii=0; ii<n_qpoints; ii++)
       {
           qPoints[ii*3+idim] = y[ii];
       }
   }
   //printf("%d quadrature points computed\n", n_qpoints);

   // project into the basis
   printf("computing matrix...");
   SpMat qW(n_qpoints, n_qpoints);
   build_spsDiagMatrix( qWeights, qW );
   SpMat massM = qLt * qW * qL;
   printf("done\n");


   // compute rhs
   printf("computing rhs...");
   std::vector<TypeReal> sFunction(n_qpoints+n_vertex);
   interpolateVertexFunction( points, vFunction, &sPoints[0], &sFunction[0], n_qpoints+n_vertex );
   Eigen::Map< SpVect > y(&sFunction[0], n_qpoints);
   Eigen::Map< SpVect > x0(&sFunction[n_qpoints], n_vertex);
   SpVect rhs = qLt * qW * y;
   printf("done\n");

   // solve the problem
   printf("solving linear system...");
   projFunction.resize(n_vertex);
   Eigen::Map< SpVect > x(&projFunction[0], n_vertex);
   Eigen::ConjugateGradient<SpMat > solver;
   solver.setMaxIterations(500);
   //Eigen::SparseLU<SpMat> solver;
   solver.compute(massM);
   x = solver.solveWithGuess(rhs, x0);
   printf("done\n");


}*/
