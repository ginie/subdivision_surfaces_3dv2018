#include <cassert>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cfloat>
#include <algorithm>
#include <queue> 
#include <map>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <utility>
#include <boost/multi_array.hpp>
#include <limits>
#include <time.h>


#include "kdtree2.h"
#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "IO.h"
#include "geometrySDS.h"



class Settings_evaluate
{
public:

    std::string 			outFile;
    std::string 			meshFile;
    std::string				pointsFile;
    int						visualize;
public:
    Settings_evaluate() :
        meshFile(),
        pointsFile(),
        outFile(),
        visualize(0)
    {
    }
    
	// class constructor and functions
	void error(const char *msg){
		fprintf(stderr,"sampleSDSBasis | ERROR | %s\n",(msg)?msg:"");
		exit(0);
	}


	void printOptions(void){
		printf("         -pFile|-p  %s\n", pointsFile.c_str());
		printf("         -oFile|-o  %s\n", outFile.c_str());
		printf("       -sdsFile|-sds %s\n", meshFile.c_str());
	}

	void print_usage(void){
		fprintf(stderr,"USAGE\n");
		fprintf(stderr,"  sampleSDSBasis -sdsFile meshFile -oFile outFile -pFile samplePoints\n");
		printOptions();
	}
	
	void parseCommandLine(int argc, char **argv)
	{
		if (argc==1)
		{
			print_usage();
			exit(0);
		}
		
		for (int i=1; i<argc; ++i)
		{
			if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); exit(0); }
			else if (!strcmp(argv[i],"-p") || !strcmp(argv[i],"-pointsFile"))
			{
				i++;
				pointsFile = std::string(argv[i]);
			}        
			else if (!strcmp(argv[i],"-sds") || !strcmp(argv[i],"-sdsFile"))
			{
				i++;
				meshFile = std::string(argv[i]);
			}
			else if (!strcmp(argv[i],"-o") || !strcmp(argv[i],"-outFile"))
			{
				i++;
				outFile = std::string(argv[i]);
			}
		}
		if (meshFile.size()==0)              { error("no sdsFile");  }
		if (pointsFile.size()==0)            { error("no pointsFile");  }
		if (outFile.size()==0)               { error("no outFile"); }
	}
    
};



void KdSearch_closestParameter2Surface( std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params)
{

    size_t n_points = points.size()/3;
    size_t n_faces = face_nvertex.size();
    size_t n_vertex = vertices.size()/3;
    // Generate uniform samples on each face
    size_t s_samples = 5;
    size_t t_samples = 5;
    size_t n_samples = s_samples * t_samples * n_faces;
    std::vector<int> Sf_params;
    std::vector<TypeReal> Ss_params, St_params;
    UniformParameterSample( n_faces, s_samples, t_samples, Sf_params, Ss_params, St_params);
    TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    
    // sample points (and normals) of the surface
    std::vector<TypeReal> Spoints, Ss, St;
    SampleLimitSurface(vertices, face_nvertex, face_vertex, Sf_params, Ss_params, St_params, Spoints, Ss, St);

    // create search data structures
	boost::multi_array<TypeReal,2> KdTreePoints(boost::extents[n_samples][3]);
	for(int ii=0; ii<n_samples; ii++)
	{
		for (int idim=0; idim<3; idim++)
		{
			KdTreePoints[ii][idim] = Spoints[ii*3+idim];
		}
	}
	// notice it is in C-standard layout. 
	kdtree2::KDTree* tree;
	kdtree2::KDTreeResultVector KdtreeRes; 
    tree = new kdtree2::KDTree(KdTreePoints,false); 
    tree->sort_results = true;
    
    f_params.resize(n_points);
    s_params.resize(n_points);
    t_params.resize(n_points);
    for( size_t ii=0; ii< n_points; ii++ )
    {
    	// for each point, find the closest surface sample
    	std::vector<TypeReal> query(3);
    	for (int idim=0; idim<3; idim++)
		{ 
			query[idim] = points[ii*3+idim];
		}
		tree->n_nearest(query, 2, KdtreeRes);
		size_t c0sample = KdtreeRes[0].idx;	
		size_t c1sample = KdtreeRes[1].idx;
		TypeReal c = 0;
		if(  Sf_params[c0sample] == Sf_params[c1sample] )
		{
			TypeReal ip0 = 0;
			TypeReal ip1 = 0;
			for(int idim=0; idim<3; idim++)
			{
				TypeReal s0p = points[ii*3+idim] - Spoints[c0sample*3+idim];
				TypeReal s1p = points[ii*3+idim] - Spoints[c1sample*3+idim];
				TypeReal s0s1 = Spoints[c1sample*3+idim] - Spoints[c0sample*3+idim];
				ip0 += s0p * s0s1;
				ip1 += s1p * (-s0s1);
			}
			if( (ip0 > 0) && (ip1>0) )
			{
				c = ip0/std::max( ip0+ip1, eps );
			}			
		}
		f_params[ii] = Sf_params[c0sample];
		s_params[ii] = (1-c) * Ss_params[c0sample] + c * Ss_params[c1sample];
		t_params[ii] = (1-c) * St_params[c0sample] + c * St_params[c1sample]; 
        // in the first case, this is only an upper bound
        

    }
}


TypeReal find_closest_Spoint(const std::vector<TypeReal> &points, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, const std::vector<int> &adjacency, const std::vector<int> &changeCoord, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, std::vector<TypeReal>& closestSpoint, size_t max_iter = 10)
{
    // evaluate the residual
    size_t n_vertex = vertices.size()/3;
    size_t n_points = points.size()/3;
    size_t n_faces = face_nvertex.size();
    
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
    	//printf("point %d sample at %d/%d, %f %f\n", ipoint, fparams[ipoint], n_faces, sparams[ipoint], tparams[ipoint] );
    	if( std::isnan(fparams[ipoint]) ) { printf("nan fparams %lu\n", ipoint); }
    	if( std::isnan(sparams[ipoint]) ) { printf("nan sparams %lu\n", ipoint); }
    	if( std::isnan(tparams[ipoint]) ) { printf("nan tparams %lu\n", ipoint); }
    	if( (fparams[ipoint]>=n_faces) || (fparams[ipoint]<0) ) { printf("fparams %lu outside domain\n", ipoint); }
    	if( (sparams[ipoint]> 1.0) || (sparams[ipoint]<0) ) { printf("sparams %lu outside domain\n", ipoint); }
    	if( (tparams[ipoint]>1.0) || (tparams[ipoint]<0) ) { printf("tparams %lu outside domain\n", ipoint); }
    }
    
    //printf("%d vertex, %d points, size params %d %d %d", n_vertex, n_points, fparams.size(), sparams.size(), tparams.size() );
    std::vector<int> f_(fparams);
    std::vector<TypeReal> s_(sparams);
    std::vector<TypeReal> t_(tparams);
    std::vector<TypeReal> lambda( n_points, 2e0 );
    std::vector<TypeReal> S, Ss, St;
    SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
    std::vector<TypeReal> energy(n_points,  0 );
    for (size_t ipoint=0; ipoint< n_points; ipoint++)
    {
    	for(int idim=0; idim<3; idim++)
    	{
    		energy[ipoint] += std::pow(points[ipoint*3+idim] - S[ipoint*3+idim],2);
    	}
    }
    std::vector<bool> stop( n_points, false );
    std::vector<TypeReal> Js(n_points*3);
    std::vector<TypeReal> Jt(n_points*3);
    std::vector<TypeReal> Hss(n_points);
    std::vector<TypeReal> Htt(n_points);
    std::vector<TypeReal> Hst(n_points);
    for( size_t iter=0; iter< max_iter; iter++ )
    {
        // sample the surface
        if(iter>0)
        {
        	SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, S, Ss, St);
        }
        for (size_t ipoint=0; ipoint< n_points; ipoint++)
        {
            if(stop[ipoint]==false)
            {
                TypeReal resx = points[ipoint*3] - S[ipoint*3];
                TypeReal resy = points[ipoint*3+1] - S[ipoint*3+1];
                TypeReal resz = points[ipoint*3+2] - S[ipoint*3+2];
                
                // update damping factor
                TypeReal energy_point = resx*resx + resy*resy + resz*resz;
                if( (energy[ipoint] >= energy_point) || (iter==0) )
                {
                    // energy decreases, so update the fotnote parameters and Hessian approximation for the next iteration
                    lambda[ipoint] = 0.5*lambda[ipoint];
                    s_[ipoint] = sparams[ipoint];
                    t_[ipoint] = tparams[ipoint];
                    f_[ipoint] = fparams[ipoint];
                    energy[ipoint] = energy_point;
                    // compute the MINUS gradient of f w.r.t parameters (s,t)
                    Js[ipoint] = Ss[ipoint*3] * resx + Ss[ipoint*3+1] * resy + Ss[ipoint*3+2] * resz;
                    Jt[ipoint] = St[ipoint*3] * resx + St[ipoint*3+1] * resy + St[ipoint*3+2] * resz;
                    // cheack stopping criterion
                    if( Js[ipoint]*Js[ipoint] + Jt[ipoint]*Jt[ipoint] < 1e-6* energy_point )
                    {
                        stop[ipoint] = true;
                        //printf("stopped point %lu at iteration %lu with squared distance %f and normGrad %f\n", ipoint, iter, energy_point, Js*Js + Jt*Jt );
                    }
                    Hss[ipoint] = Ss[ipoint*3]*Ss[ipoint*3] + Ss[ipoint*3+1]*Ss[ipoint*3+1] + Ss[ipoint*3+2]*Ss[ipoint*3+2];
                    Hst[ipoint] = Ss[ipoint*3]*St[ipoint*3] + Ss[ipoint*3+1]*St[ipoint*3+1] + Ss[ipoint*3+2]*St[ipoint*3+2];
                    Htt[ipoint] = St[ipoint*3]*St[ipoint*3] + St[ipoint*3+1]*St[ipoint*3+1] + St[ipoint*3+2]*St[ipoint*3+2];
                    
                }else
                {
                    // try smaller damping factor, the Hessian approximation remains the previous one but the linear system changes
                    sparams[ipoint] = s_[ipoint];
                    tparams[ipoint] = t_[ipoint];
                    fparams[ipoint] = f_[ipoint];
                    lambda[ipoint] = lambda[ipoint]*2;
                }
                // solve system
                TypeReal Hss_k = (1+ lambda[ipoint]) * Hss[ipoint];
                TypeReal Htt_k = (1+ lambda[ipoint]) * Htt[ipoint];
                TypeReal Hst_k = Hst[ipoint];
                TypeReal detH = Hss_k*Htt_k - Hst_k*Hst_k;
                TypeReal idetH = std::abs(detH)>1e-12 ? 1.0/detH : 1.0;
                // solve linear system to obtain descent direction (qs, qt)
                TypeReal qs = idetH* ( Htt_k*Js[ipoint] - Hst_k*Jt[ipoint] );
                TypeReal qt = idetH* ( Hss_k*Jt[ipoint] - Hst_k*Js[ipoint] );
                // update
                sparams[ipoint] = sparams[ipoint]+ qs;
                tparams[ipoint] = tparams[ipoint]+ qt;
                reparametrize_sample( adjacency, changeCoord, fparams, sparams, tparams, ipoint );
                //printf("sample assigned point %f %f in face %d\n", sparams[ipoint], tparams[ipoint], fparams[ipoint]);
            }
        }
        //printf("newton iteration %d/%d finished\n", iter, max_iter);
    }
    
    // output the closest point in the surface
    SampleLimitSurface(vertices, face_nvertex, face_vertex, fparams, sparams, tparams, closestSpoint, Ss, St);
    //printf("return the closest point in the surface\n");

    TypeReal squared_distance = 0;
    for(size_t ipoint=0; ipoint< n_points; ipoint++){
        squared_distance += std::pow(  points[ipoint*3] - closestSpoint[ipoint*3], 2 ) + std::pow(  points[ipoint*3+1] - closestSpoint[ipoint*3+1], 2 ) + std::pow(  points[ipoint*3+2] - closestSpoint[ipoint*3+2], 2 );
    }
    //printf("energy by optimization %f\n", squared_distance);
    return squared_distance;
}














int main(int argc, char *argv[]) {
  
	Settings_evaluate D;
    D.parseCommandLine(argc, argv);
    D.printOptions();
    std::string outfilename(D.outFile);
    std::vector<TypeReal> vertices, vertex_vars, points;
    std::vector<int> face_nvertex, face_vertex, dummy0, dummy1;
   
	
    // read input mesh
    read_ply( D.meshFile, vertices, face_nvertex, face_vertex, vertex_vars, true);
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();  
	
    // read points to be sampled
    std::string format = D.pointsFile.substr(D.pointsFile.size()-3,3);
    if( format.compare( std::string("ply"))==0 ){
    	read_ply( D.pointsFile, points, dummy0, dummy1, vertex_vars, false);
    }
    else if( format.compare( std::string("off"))==0 ){
    	read_off( D.pointsFile, points, dummy0, dummy1, false );
    }else{
    	printf("unknown input file type, expecting .ply\n");
    	return(0);
    }
    size_t n_points = points.size()/3;  
    printf("%lu input points to sample read\n", n_points);

    
    // find closest point
    std::vector<int> f_params;
    std::vector<TypeReal> s_params, t_params;
    KdSearch_closestParameter2Surface( points, vertices, face_nvertex, face_vertex, f_params, s_params, t_params);
    std::vector<TypeReal> closestSpoint;
    std::vector<int> adjacency, changeCoord;
    compute_params_adjacency( vertices, face_nvertex, face_vertex, adjacency, changeCoord );
    find_closest_Spoint(points, vertices, face_nvertex, face_vertex, adjacency, changeCoord, f_params, s_params, t_params, closestSpoint, 100);
    
    printf("sample parameters determined\n");
    
    // sample the subdivision spline basis functions at that point
    SpMat L, Ds, Dt;
    SampleLimitBasis(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, L, Ds, Dt, false);
    printf("basis sampled\n");
    
    /* to interface with matlab, you can store the non-zero entries of the mass and stiffness matrices, the control mesh, and the subdivision schemes */
    FILE *mFile;
    std::string outFile(D.outFile);
    mFile = fopen (outFile.append("_sampleBasis.txt").c_str(),"w"); 
    fprintf(mFile, "%lu %lu %lu\n", L.nonZeros(), L.rows(), L.cols());
	for(int k=0; k<L.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(L,k); it; ++it)
        {
            fprintf(mFile, "%lu %lu %f\n", it.row(), it.col(), it.value());
        }
    }
    fclose(mFile);
    
    
    
    
    std::vector<TypeReal> S, normals, metric, principalCurvature1, principalCurvature2, principalDirection1, principalDirection2;
    get_FrenetFrame(vertices, face_nvertex, face_vertex, f_params, s_params, t_params, S, normals, metric, principalCurvature1, principalCurvature2, principalDirection1, principalDirection2 );
    
    {
    	// mean curvature
		std::string outFile(D.outFile);
		std::ofstream outfile(outFile.append("_sampleMeanCurvature.txt").c_str());
		for (int ii=0; ii<principalCurvature1.size(); ii++)
		{
			outfile << principalCurvature1[ii] + principalCurvature2[ii] << "\n" ;
		}
		outfile.close();
	}
    
	{
		// Gauss curvature
		std::string outFile(D.outFile);
		std::ofstream outfile(outFile.append("_sampleGaussCurvature.txt").c_str());
		for (int ii=0; ii<principalCurvature1.size(); ii++)
		{
			outfile << principalCurvature1[ii] * principalCurvature2[ii] << "\n" ;
		}
		outfile.close();
	}

	{
		// Gauss curvature
		std::string outFile(D.outFile);
		std::ofstream outfile(outFile.append("_sampleMetric.txt").c_str());
		for (int ii=0; ii<metric.size(); ii++)
		{
			outfile << metric[ii] << "\n" ;
		}
		outfile.close();
	}    
    
    /*SpVect x(n_vertex);
    SpVect y(n_vertex);
    SpVect z(n_vertex);
    for( int ii=0; ii<n_vertex; ii++)
    {
    	x[ii] = vertices[ii*3];
    	y[ii] = vertices[ii*3+1];
    	z[ii] = vertices[ii*3+2];
    }
    SpVect Sx = L*x;
    SpVect Sy = L*y;
    SpVect Sz = L*z;
    std::vector<TypeReal> Ssamples( 3*n_points );
    std::vector<TypeReal> Sfield( 3*n_points );
    for( int ii=0; ii<n_points; ii++)
    {
    	Ssamples[ii*3] = Sx[ii];
    	Ssamples[ii*3+1] = Sy[ii];
    	Ssamples[ii*3+2] = Sz[ii];
    	Sfield[ii*3] = points[3*ii] - Sx[ii];
    	Sfield[ii*3+1] = points[3*ii+1] - Sy[ii];
    	Sfield[ii*3+2] = points[3*ii+2] - Sz[ii];
    }

    std::string filename(D.outFile);
    filename.append("_sampleField.ply");
    std::vector<std::string> var_names = { std::string("nx"), std::string("ny"), std::string("nz")};
    std::vector<int> dummy;
    write_ply_mesh( filename, Ssamples, dummy, dummy, Sfield, var_names);*/

    
    return 0;
}

        
       