#include <cassert>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

#include <cfloat>
#include <algorithm>
#include <queue> 
#include <map>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <limits>
#include <time.h>




#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "IO.h"




class Settings_Subdivide
{
public:

    size_t      uniformRefinement;
    std::string outFile;
    std::string sdsFile;
    int			visualize;
public:
    Settings_Subdivide() :
    	uniformRefinement(0),
        sdsFile(),
        outFile(),
        visualize(0)
    {
    }
    
	void error(const char *msg)
	{
		fprintf(stderr,"subdivide | ERROR | %s\n",(msg)?msg:"");
		exit(0);
	}
	
	
	void printOptions(void)
	{
		printf("       -uniR|-uniREf  <%lu>\n", uniformRefinement);  
	}
	
	void print_usage(void)
	{
		fprintf(stderr,"USAGE\n");
		fprintf(stderr,"  subdivide [options] sdsPlyFile outFile\n");
		printOptions();
	}
	void parseCommandLine(int argc, char **argv)
	{
		if (argc==1)
		{
			print_usage();
			exit(0);
		}
		
		for (int i=1; i<argc; ++i)
		{
			if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); exit(0); }
			else if (!strcmp(argv[i],"-uniR")  || !strcmp(argv[i],"-uniRef"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(uniformRefinement))<1) { error("parsing -uniformRefinement"); }
			} 
			else if (!strcmp(argv[i],"-VIZ") || !strcmp(argv[i],"-viz"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%d",&(visualize))<0) { error("parsing -viz"); }
			}    
			else if (argv[i][0]=='-') { error("unknown option"); }
			else if (sdsFile.size()==0) { sdsFile = std::string(argv[i]); }
			else if (outFile.size()==0) { outFile = std::string(argv[i]); }
		}
		if (sdsFile.size()==0)              { error("no sdsFile");  }		
		if (outFile.size()==0)                { error("no outFile"); }
	}
   
};




void real2rgb( TypeReal *v, TypeReal *rgb, int sz)
{
	TypeReal vmax = v[0];
	TypeReal vmin = v[0];
	for(int ii=0; ii< sz; ii++)
	{
		if( v[ii] < vmin ){ vmin = v[ii]; }
		if( v[ii] > vmax ){ vmax = v[ii]; }
	}
	double dv = vmax - vmin;
	//printf("color range %f %f\n", vmin, vmax);
	for(int ii=0; ii< sz; ii++)
	{
		rgb[ii*3] = 1.0;
		rgb[ii*3+1] = 1.0;
		rgb[ii*3+2] = 1.0;
				
		if( v[ii] < (vmin + 0.25 * dv) )
		{
		  rgb[ii*3] = 0;
		  rgb[ii*3+1] = 4 * (v[ii] - vmin) / dv;
		}
		else if( v[ii] < (vmin + 0.5 * dv) )
		{
		  rgb[ii*3] = 0;
		  rgb[ii*3+2] = 1 + 4 * (vmin + 0.25 * dv - v[ii]) / dv;
		}
		else if( v[ii] < (vmin + 0.75 * dv) )
		{
		  rgb[ii*3] = 4 * (v[ii] - vmin - 0.5 * dv) / dv;
		  rgb[ii*3+2] = 0;
		} 
		else
		{
		  rgb[ii*3+1] = 1 + 4 * (vmin + 0.75 * dv - v[ii]) / dv;
		  rgb[ii*3+2] = 0;
		}
	}
	
}



int main(int argc, char *argv[]) {
  
	Settings_Subdivide D;
    D.parseCommandLine(argc, argv);
    D.printOptions();
    std::string outfilename(D.outFile);
    std::vector<TypeReal> vertices, vertex_vars;
    std::vector<int> face_nvertex, face_vertex;
   
	
    // read input mesh
    read_ply( D.sdsFile, vertices, face_nvertex, face_vertex, vertex_vars, true);
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();   
	  
    
    UniformRefineSurface( vertices, face_nvertex, face_vertex, D.uniformRefinement );
   	n_vertex = vertices.size()/3;
    
    
    // visualize projected function
    write_mesh(  outfilename, vertices, face_nvertex, face_vertex);	
     
        
    return 0;
}

