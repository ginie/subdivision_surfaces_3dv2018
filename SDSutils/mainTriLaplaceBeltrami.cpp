#include <cassert>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cfloat>
#include <algorithm>
#include <queue> 
#include <map>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <limits>
#include <time.h>



#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "IO.h"


//#include <SymGEigsSolver.h>
//#include <MatOp/SparseSymMatProd.h>
//#include <MatOp/SparseCholesky.h>
//#include <MatOp/SparseRegularInverse.h>



class Settings_LB
{
public:

    size_t      quadrature;
    size_t      nEigenVectors;
    std::string outFile;
    std::string meshFile;
    int			visualize;
public:
    Settings_LB() :
        nEigenVectors(3),
        meshFile(),
        outFile(),
        visualize(0)
    {
    }
    
	// class constructor and functions
	void error(const char *msg){
		fprintf(stderr,"SDSEigen | ERROR | %s\n",(msg)?msg:"");
		exit(0);
	}


	void printOptions(void){
		printf("       -nEig|-nEigenVectors  <%lu>\n", nEigenVectors);
	}

	void print_usage(void){
		fprintf(stderr,"USAGE\n");
		fprintf(stderr,"  triMeshEigen [options] meshFile outFile\n");
		printOptions();
	}
	
	void parseCommandLine(int argc, char **argv)
	{
		if (argc==1)
		{
			print_usage();
			exit(0);
		}
		
		for (int i=1; i<argc; ++i)
		{
			if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"-help")) { print_usage(); exit(0); }
			else if (!strcmp(argv[i],"-nEig")  || !strcmp(argv[i],"-nEigenVectors"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%lu",&(nEigenVectors))<1) { error("parsing -nEigenVectors"); }
			}       
			else if (!strcmp(argv[i],"-VIZ") || !strcmp(argv[i],"-viz"))
			{
				if ((++i)>=argc || sscanf(argv[i],"%d",&(visualize))<0) { error("parsing -viz"); }
			}    
			else if (argv[i][0]=='-') { error("unknown option"); }
			else if (meshFile.size()==0) { meshFile = std::string(argv[i]); }
			else if (outFile.size()==0) { outFile = std::string(argv[i]); }
		}
		if (meshFile.size()==0)              { error("no inFile");  }
		if (outFile.size()==0)                { error("no outFile"); }
	}
    
};



void triangularMesh_gradient( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, SpMat &grad, SpMat &div, SpMat &Ac, SpMat &cotanLaplacian )
{

	// The gradient operator grad is implemented as sparse matrix.
	// The gradient is computed with piecewise linear finite elements over the mesh. We assume a triangular mesh
	// Let f index the faces of the mesh and i the vertices, then grad(u)_f = \frac{1}{2 A_f} \sum_{ i \in f } u_i ( N_f \crossProduct e_i )
	// where N_f if the face normal, e_i is the edge opposite to vertex i, and A_f is the area of the face

	int n_faces = face_nvertex.size();
	int n_vertex = vertices.size()/3;
	
	// For each face, compute un-normalized normal through the formula e1 \crossProduct e2
	// Compute the area of each face as half the norm of the cross product.
	// Compute the set of unit-norm normals to each face.
	std::vector<TypeReal> face_normals( 3*n_faces );
	std::vector<TypeReal> face_areas( n_faces );
	std::vector<TypeReal> vertex_areas( n_vertex, 0 );
	for(int ii=0; ii< n_faces; ii++)
	{
		int ivertex = face_vertex[ ii*3];
		int jvertex = face_vertex[ ii*3 +1 ];
		int kvertex = face_vertex[ ii*3 + 2];
		Vect3 edge1, edge2;
		Eigen::Map<Vect3> normal(&face_normals[ii*3], 3);
		for(int idim=0; idim<3; idim++)
		{
			edge1[idim] = vertices[ jvertex*3 + idim] - vertices[ ivertex*3 + idim];
			edge2[idim] = vertices[ kvertex*3 + idim] - vertices[ jvertex*3 + idim];
		}
		normal = edge1.cross(edge2);
		face_areas[ii] = normal.norm();
		face_areas[ii] *= 0.5;
		normal.normalize();
		vertex_areas[ ivertex ] += face_areas[ii];
		vertex_areas[ jvertex ] += face_areas[ii];
		vertex_areas[ kvertex ] += face_areas[ii];
	}
	
	
	//Sparse matrix with area f the faces around each vertex
	std::vector<T> tripletList;
	for(int ii=0; ii< n_vertex; ii++)
	{
		vertex_areas[ii] /= 3.;
		tripletList.push_back( T( ii, ii, vertex_areas[ii] ) );
	}
	Ac.setFromTriplets( tripletList.begin(), tripletList.end() );

	//Populate the sparse entries of the matrices for the gradient operator
	tripletList.clear();
	std::vector<T> tripletListB;
	for(int ii=0; ii< n_faces; ii++)
	{	
		for(int jj=0; jj<3; jj++)
		{
			// indexes opposite to jj
			int ivertex = face_vertex[ ii*3 + jj ];
			int jvertex = face_vertex[ ii*3 + ((jj+1)%3) ];
			int kvertex = face_vertex[ ii*3 + ((jj+2)%3) ];
			Vect3 edge;
			for(int idim=0; idim<3; idim++)
			{
				edge[idim] = vertices[ kvertex*3 + idim] - vertices[ jvertex*3 + idim];
			}
			Eigen::Map<Vect3> normal(&face_normals[ii*3], 3);
			// vector N_f \crossProduct e_i
			Vect3 vals = normal.cross( edge );
			for(int idim=0; idim<3; idim++)
			{
				tripletList.push_back( T(ii*3+idim, ivertex, vals[idim]/( 2*face_areas[ii] ) ) );
				tripletListB.push_back( T(ivertex, ii*3+idim, vals[idim] ) );
			}
		}
	}
	grad.setFromTriplets( tripletList.begin(), tripletList.end() );
	div.setFromTriplets( tripletListB.begin(), tripletListB.end() );
	
	
	
	// cotan weights Laplacian
	tripletList.clear();
	for(int ii=0; ii< n_faces; ii++)
	{	
		for(int jj=0; jj<3; jj++)
		{
			int ivertex = face_vertex[ ii*3 + jj ];
			int jvertex = face_vertex[ ii*3 + ((jj+1)%3) ];
			int kvertex = face_vertex[ ii*3 + ((jj+2)%3) ];
			// edges adjacent to the vertex
			Vect3 edge1, edge2;
			for(int idim=0; idim<3; idim++)
			{
				edge1[idim] = vertices[ jvertex*3 + idim] - vertices[ ivertex*3 + idim];
				edge2[idim] = vertices[ kvertex*3 + idim] - vertices[ ivertex*3 + idim];
			}
			// compute cotangent
			edge1.normalize();
			edge2.normalize();
			Vect3 crossP = edge1.cross(edge2);
			TypeReal cos = edge1.dot(edge2);
			TypeReal sin = crossP.norm();
			TypeReal cot = 1.0;
			if( std::abs(sin) > std::numeric_limits<TypeReal>::epsilon() )
			{
				cot = cos/sin;
			}
			tripletList.push_back( T(jvertex, kvertex, cot ) );
			tripletList.push_back( T(kvertex, jvertex, cot ) );
		}
	}
	
	SpMat W( n_vertex, n_vertex );
	W.setFromTriplets( tripletList.begin(), tripletList.end() );
	
	SpVect ones = SpVect::Constant( n_vertex, 1.0);
	SpMat Wt = W.transpose();
	SpVect sumW = W * ones;
	SpMat diag( n_vertex, n_vertex);
	build_spsDiagMatrix( sumW, diag );
	cotanLaplacian = diag - W;
	
	// cotanLaplacian should be equal to div*grad
	SpMat res = cotanLaplacian - div*grad;
	res.pruned();
	printf("res should be zero, it has norm %f, and relative norm %f\n", res.squaredNorm(), res.norm()/ cotanLaplacian.norm() );
	
}







int main(int argc, char *argv[]) {
  
	Settings_LB D;
    D.parseCommandLine(argc, argv);
    D.printOptions();
    std::string outfilename(D.outFile);
    std::vector<TypeReal> vertices, vertex_vars;
    std::vector<int> face_nvertex, face_vertex;
   
	
    // read input mesh
    read_ply( D.meshFile, vertices, face_nvertex, face_vertex, vertex_vars, true);
    size_t n_vertex = vertices.size()/3;
    size_t n_faces = face_nvertex.size();   
	
    
    int n_eig = D.nEigenVectors;
	// computre differential operators over the mesh
	SpMat grad( 3*n_faces, n_vertex);
	SpMat div( n_vertex, 3*n_faces );
	SpMat massM( n_vertex, n_vertex );
	SpMat stiffM( n_vertex, n_vertex );
	triangularMesh_gradient( vertices, face_nvertex, face_vertex, grad, div, massM, stiffM );
 	
    
    /* to interface with matlab, you can store the non-zero entries of the mass and stiffness matrices, the control mesh, and the subdivision schemes */
    FILE *mFile;
    std::string outFile(D.outFile);
    mFile = fopen (outFile.append("_massMatrix.txt").c_str(),"w"); 
    fprintf(mFile, "%d %d %d\n", massM.nonZeros(), massM.rows(), massM.cols());
	for(int k=0; k<massM.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(massM,k); it; ++it)
        {
            fprintf(mFile, "%d %d %f\n", it.row(), it.col(), it.value());
        }
    }
    fclose(mFile);
    outFile = std::string(D.outFile);
    mFile = fopen (outFile.append("_stiffnessMatrix.txt").c_str(),"w"); 
    fprintf(mFile, "%d %d %d\n", stiffM.nonZeros(), stiffM.rows(), stiffM.cols());
	for(int k=0; k<stiffM.outerSize(); ++k)
    {
        for (SpMat::InnerIterator it(stiffM,k); it; ++it)
        {
            fprintf(mFile, "%d %d %f\n", it.row(), it.col(), it.value());
        }
    }
    fclose(mFile);    
    
    
    return 0;
}

