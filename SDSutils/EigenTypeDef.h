#ifndef __EIGENTYPEDEF__
#define __EIGENTYPEDEF__


#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Eigenvalues>
#include "TypeDef.h"

typedef Eigen::Triplet<TypeReal> T;
typedef Eigen::SparseMatrix<TypeReal, Eigen::ColMajor, int> SpMat;
typedef Eigen::MatrixXf Mat;
//typedef Eigen::MatrixXd Mat;
typedef Eigen::VectorXf SpVect;
//typedef Eigen::VectorXd SpVect;
typedef Eigen::Vector3f Vect3;

#endif
