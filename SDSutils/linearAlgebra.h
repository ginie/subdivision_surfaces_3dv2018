#ifndef __LINEARALGEBRA__
#define __LINEARALGEBRA__

//#include "TypeDef.h"
// include eigen to solve linear systems                                        
#define EIGEN_SUPERLU_SUPPORT
#include <Eigen/SparseLU>
#include <Eigen/SparseCholesky>



// eigen utilities
void build_spsMatrix(const std::vector<size_t> &M_rows, const std::vector<size_t> &M_cols, const std::vector<TypeReal> &M_vals, Eigen::SparseMatrix<TypeReal> &M );
void build_spsDiagMatrix(const std::vector<TypeReal> &M_vals, Eigen::SparseMatrix<TypeReal> &M );
void build_spsDiagMatrix(const SpVect &M_vals, Eigen::SparseMatrix<TypeReal> &M );
Mat pseudoinverse(const Mat &mat, TypeReal tolerance);
#endif
