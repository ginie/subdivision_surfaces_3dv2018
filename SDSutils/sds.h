#ifndef __SDS__
#define __SDS__




//-
void quadratureParameterSample( size_t n_faces, size_t quadrature, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params, std::vector<TypeReal> &q_weights);
void UniformParameterSample( size_t n_faces, size_t s_samples, size_t t_samples, std::vector<int> &f_params, std::vector<TypeReal> &s_params, std::vector<TypeReal> &t_params);

//
void vertex_stats( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex );
void find_extraordinaryVertex( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &eVertex );
void find_valenceVertex( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &eVertex );

//
void compute_params_adjacency( const std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<int> &adjacency, std::vector<int> &changeCoord );
int reparametrize_sample( const std::vector<int> &adjacency, const std::vector<int> &changeCoord, std::vector<int> &fparams, std::vector<TypeReal>& sparams, std::vector<TypeReal>& tparams, size_t ipoint );

//
void UniformRefineSurface(std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, int n_uniformRef);
void UniformRefineSurface(std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, std::vector<TypeReal> &vertexValues, int n_uniformRef);
void UniformRefineSurfaceOperators(std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, int n_ref, std::vector<TypeReal> &rvertices, std::vector<int> &rface_nvertex, std::vector<int> &rface_vertex, SpMat &A, SpMat &A3);
void UniformRefineSurfaceOperator( std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, std::vector<TypeReal> &Rvertices, std::vector<int> &Rfacenverts, std::vector<int> &Rfaceverts, std::vector<int> &face2Face, SpMat &A, SpMat &A3);
void UniformRefineSurface( std::vector<TypeReal> &vertices, std::vector<int> &facenverts, std::vector<int> &faceverts, int n_uniformRef, std::vector<TypeReal> &Rvertices, std::vector<int> &Rfacenverts, std::vector<int> &Rfaceverts, SpMat &A, SpMat &A3);


//
void SampleLimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& Ss, std::vector<TypeReal>& St);
void SampleD2LimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& Ss, std::vector<TypeReal>& St, std::vector<TypeReal>& Sss, std::vector<TypeReal>& Sst, std::vector<TypeReal>& Stt );
void SampleLimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, SpMat &L, SpMat &Ds, SpMat &Dt, bool sample_normals);
void SampleD2LimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, SpMat &L, SpMat &Ds, SpMat &Dt, SpMat &Dss, SpMat &Dst, SpMat &Dtt );
void SampleLimitBasis(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, const std::vector<int> &f_params, const std::vector<TypeReal>& s_params, const std::vector<TypeReal>& t_params, SpMat &L, SpMat &Ds, SpMat &Dt, bool sample_normals);

//
void qSampleD2LimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, size_t n_quad, std::vector<TypeReal> &S, std::vector<TypeReal> &Ss, std::vector<TypeReal> &St, std::vector<TypeReal> &Sss, std::vector<TypeReal> &Sst, std::vector<TypeReal> &Stt, std::vector<TypeReal> &qWeights);
void qSampleLimitSurface(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, size_t n_quad, std::vector<TypeReal> &S, std::vector<TypeReal> &Ss, std::vector<TypeReal> &St, std::vector<TypeReal> &qWeights);
TypeReal area_surface(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex );

#endif
