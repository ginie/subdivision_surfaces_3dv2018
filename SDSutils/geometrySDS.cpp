#include <cstdlib>
#include <vector>
#include <cfloat>
#include <algorithm>
#include <limits>
#include <cmath>
#include <cstdio>
#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "sds.h"
#include "geometrySDS.h"


void compute_FrenetFrame( TypeReal *Ss, TypeReal *St, TypeReal *Sss, TypeReal *Sst, TypeReal *Stt, TypeReal *normals, TypeReal *metric, TypeReal *principalCurvature1, TypeReal *principalCurvature2, TypeReal *principalDirection1, TypeReal *principalDirection2, size_t n_samples )
{
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
    for (int ii=0; ii< n_samples; ii++)
    {
        TypeReal n[3];
        // compute normal direction
        n[0] = Ss[ii*3+1]*St[ii*3+2] - Ss[ii*3+2]*St[ii*3+1];
        n[1] = Ss[ii*3+2]*St[ii*3] - Ss[ii*3]*St[ii*3+2];
        n[2] = Ss[ii*3]*St[ii*3+1] - Ss[ii*3+1]*St[ii*3];
        TypeReal inorm = ( std::pow(n[0],2) + std::pow(n[1],2) + std::pow(n[2],2) );
        if( inorm< 1e-16 )
        {
            inorm = 1;
        }else{ inorm = 1.0/std::sqrt(inorm);}
        TypeReal E, F, G; E = 0; F = 0; G = 0;
        TypeReal e, f, g; e = 0; f = 0; g = 0;
        for(int idim=0; idim<3; idim++)
        {
            // normalize it
            n[idim] *= inorm;
            normals[ii*3+idim] = n[idim];
            // compute first fondamental form I
            //[  s_uu \dot n   S_uv \dot n ] = [ E F ]
            //[  s_vu \dot n   S_vv \dot n ]   [ F G ]
            E += Ss[ii*3+idim]*Ss[ii*3+idim];
            F += Ss[ii*3+idim]*St[ii*3+idim];
            G += St[ii*3+idim]*St[ii*3+idim];
            // compute second fondamental form II
            //[  s_uu \dot n   S_uv \dot n ] = [ e f ]
            //[  s_vu \dot n   S_vv \dot n ]   [ f g ]
            e += n[idim]*Sss[ii*3+idim];
            f += n[idim]*Sst[ii*3+idim];
            g += n[idim]*Stt[ii*3+idim];
        }
        // compute metric and curvatures
        TypeReal det1FF =  E * G - std::pow( F, 2 ); 
        TypeReal det2FF = e *g - std::pow( f, 2 );
        metric[ii] = det1FF;
        if( std::isnan(det1FF) ==true){ printf("nan for metric at sample %d\n", ii);}
        //gCurvature[ii] = (e * g - std::pow( f, 2 ))/det1FF;
        //meanCurvature[ii] = 0.5*( e * G + g * E - 2* f * F )/det1FF;
        
        // compute the shape operator ( I^{-1} II ) and its eigen-decomposition
        TypeReal idet1FF = 1.0/ std::max(det1FF,eps);
        TypeReal A11 = ( e * G - f * F ) * idet1FF;
        TypeReal A12 = ( f * G - g * F) * idet1FF;
        TypeReal A21 = ( f * E - e * F ) * idet1FF;
        TypeReal A22 = ( g * E - f * F ) * idet1FF;
        TypeReal det = det2FF * idet1FF;
        TypeReal trace = A11 + A22;
        TypeReal res = std::pow( 0.5 * trace, 2 ) - det;
        res = std::sqrt(std::max(res, eps) );
        TypeReal k1 = 0.5*trace;
        TypeReal k2 = 0.5*trace;
        if(res>0)
        {
            k1 += res;
            k2 -= res;
        }
        TypeReal coef1[2] = {1.0, 0};
        TypeReal coef2[2] = {0.0, 1.0};
        if( (A21!= 0) && ( k1!= k2) ){
            coef1[0] = k1 - A22;
            coef1[1] = A21;
            coef2[0] = k2 - A22;
            coef2[1] = A21;
        }
        else if ((A12 != 0) && (k1!=k2))
        {
            coef1[1] = k1 - A11;
            coef1[0] = A12;
            coef2[1] = k2 - A11;
            coef2[0] = A12;
        }
        // compute principal radius
        principalCurvature1[ii] = k1;
        principalCurvature2[ii] = k2;
        if( std::isnan(k1) ==true){ printf("nan for k1 at sample %d\n", ii);}
        if( std::isnan(k2) ==true){ printf("nan for k2 at sample %d\n", ii);}
        TypeReal norm1 = 0;
        TypeReal norm2 = 0;
        
        // compute principal directions
        for(int idim=0; idim<3; idim++)
        {
            principalDirection1[ii*3+idim] = coef1[0] *Ss[ii*3+idim] + coef1[1] *St[ii*3+idim];
            principalDirection2[ii*3+idim] = coef2[0] *Ss[ii*3+idim] + coef2[1] *St[ii*3+idim];
            norm1 += std::pow( principalDirection1[ii*3+idim], 2);
            norm2 += std::pow( principalDirection2[ii*3+idim], 2);
        }
        norm1 = std::sqrt( std::max(norm1, eps) );
        norm2 = std::sqrt( std::max(norm2, eps) );
        // normalize them
        for(int idim=0; idim<3; idim++)
        {
            principalDirection1[ii*3+idim] /= std::max(norm1, eps);
            principalDirection2[ii*3+idim] /= std::max(norm2, eps);
        }
        if( (std::isnan(principalDirection1[ii*3]) ==true) || (std::isnan(principalDirection1[ii*3+1]) ==true) || (std::isnan(principalDirection1[ii*3+2]) ==true) ){ printf("nan for principal direction 1 at sample %d\n", ii);}
        if( (std::isnan(principalDirection2[ii*3]) ==true) || (std::isnan(principalDirection2[ii*3+1]) ==true) || (std::isnan(principalDirection2[ii*3+2]) ==true) ){ printf("nan for principal direction 2 at sample %d\n", ii);}
    }
}

 

void compute_normals( TypeReal *Ss, TypeReal *St, TypeReal *normals, TypeReal *metric, size_t n_samples )
{
	TypeReal eps = std::numeric_limits<TypeReal>::epsilon();
	int count = 0;
    for (int ii=0; ii< n_samples; ii++)
    {
        TypeReal n[3];
        // compute normal direction
        n[0] = Ss[ii*3+1]*St[ii*3+2] - Ss[ii*3+2]*St[ii*3+1];
        n[1] = Ss[ii*3+2]*St[ii*3] - Ss[ii*3]*St[ii*3+2];
        n[2] = Ss[ii*3]*St[ii*3+1] - Ss[ii*3+1]*St[ii*3];
        TypeReal norm = std::pow(n[0],2) + std::pow(n[1],2) + std::pow(n[2],2);
        TypeReal inorm = 1;
        if( norm > eps ){ inorm =1.0/std::sqrt(norm); }
        TypeReal E = 0;
        TypeReal F = 0;
        TypeReal G = 0;
        for(int idim=0; idim<3; idim++)
        {
            // normalize it
            n[idim] *= inorm;
            normals[count] = n[idim];
            // compute first fondamental form I
            //[  s_uu \dot n   S_uv \dot n ] = [ E F ]
            //[  s_vu \dot n   S_vv \dot n ]   [ F G ]
            E += Ss[count]*Ss[count];
            F += Ss[count]*St[count];
            G += St[count]*St[count];
            count += 1;
        }
        // compute metric and curvatures
        TypeReal det1FF =  E * G - std::pow( F, 2 ); 
        metric[ii] = std::max( det1FF, eps );
     }
}


void get_normals(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, std::vector<int> &f_params, std::vector<TypeReal>& s_params, std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& normals, std::vector<TypeReal>& metric )
{
	std::vector<TypeReal> Ss, St;
	SampleLimitSurface(vertices, facenverts, faceverts, f_params, s_params, t_params, S, Ss, St);
	int n_points = S.size()/3;
	normals.resize(3*n_points);
	metric.resize(n_points);
	compute_normals( &Ss[0], &St[0], &normals[0], &metric[0], Ss.size()/3 );

}

void get_FrenetFrame(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, std::vector<int> &f_params, std::vector<TypeReal>& s_params, std::vector<TypeReal>& t_params, std::vector<TypeReal>& S, std::vector<TypeReal>& normals, std::vector<TypeReal>& metric, std::vector<TypeReal> &principalCurvature1, std::vector<TypeReal> &principalCurvature2, std::vector<TypeReal> &principalDirection1, std::vector<TypeReal> &principalDirection2 )
{
	std::vector<TypeReal> Ss, St;
	std::vector<TypeReal> Sss, Sst, Stt;
	SampleD2LimitSurface(vertices, facenverts, faceverts, f_params, s_params, t_params, S, Ss, St, Sss, Sst, Stt);
	int n_points = S.size()/3;
	normals.resize(3*n_points);
	metric.resize(n_points);
	principalCurvature1.resize(n_points);
	principalCurvature2.resize(n_points);
	principalDirection1.resize(3*n_points);
	principalDirection2.resize(3*n_points);
	compute_FrenetFrame( &Ss[0], &St[0], &Sss[0], &Sst[0], &Stt[0], &normals[0], &metric[0], &principalCurvature1[0], &principalCurvature2[0], &principalDirection1[0], &principalDirection2[0], Ss.size()/3 );

}



TypeReal quad_surface(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_area )                                               
{

	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();   
    size_t quad_samples =  quadrature * quadrature;

    quad_area = std::vector<TypeReal>( n_faces, 0 );
	std::vector<TypeReal> S, Ss, St, qWeights;
	qSampleLimitSurface(vertices, face_nvertex, face_vertex, quadrature, S, Ss, St, qWeights);
	std::vector<TypeReal> normals( quad_samples*n_faces*3 );
    std::vector<TypeReal> metric(quad_samples*n_faces);
	compute_normals( &Ss[0], &St[0], &normals[0], &metric[0], quad_samples * n_faces );
	
	
	// compute surface area
	int count = 0;
	TypeReal acc = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj<quad_samples; jj++)
		{
			TypeReal qarea = std::sqrt(metric[count]) * qWeights[count]; 
			quad_area[ii] += qarea;   
			acc += qarea;
			//printf("quad %d, count %d area %g %g %g\n", ii, count, quad_area[ii], metric[count], qWeights[count]);
			count += 1;
		}
	}
	return acc;
}




TypeReal quad_meanCurvature(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_curvature )                                               
{

	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();   
    size_t quad_samples =  quadrature * quadrature;

    quad_curvature = std::vector<TypeReal>( n_faces, 0 );
	// sample surface at quadrature
	std::vector<TypeReal> S, Ss, St, Sss, Sst, Stt, qWeights;
	qSampleD2LimitSurface(vertices, face_nvertex, face_vertex, quadrature, S, Ss, St, Sss, Sst, Stt, qWeights);
    std::vector<TypeReal> metric(quad_samples*n_faces);
	std::vector<TypeReal> principalCurvature1( quad_samples*n_faces );
    std::vector<TypeReal> principalCurvature2( quad_samples*n_faces );
    std::vector<TypeReal> normals( quad_samples*n_faces*3 );
	std::vector<TypeReal> principalDirection1( quad_samples*n_faces*3 );
    std::vector<TypeReal> principalDirection2( quad_samples*n_faces*3 );
	compute_FrenetFrame( &Ss[0], &St[0], &Sss[0], &Sst[0], &Stt[0], &normals[0], &metric[0], &principalCurvature1[0], &principalCurvature2[0], &principalDirection1[0], &principalDirection2[0], quad_samples * n_faces );

	

	
	// compute surface accu,ulated curvature
	int count = 0;
	TypeReal acc = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj<quad_samples; jj++)
		{
			TypeReal Km = 0.5*( principalCurvature1[count] + principalCurvature2[count] ) * qWeights[count]; 
			quad_curvature[ii] += Km;   
			acc += Km;
			count += 1;
		}
	}
	
	return acc;
}


TypeReal quad_GaussCurvature(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_curvature )                                               
{

	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();   
    size_t quad_samples =  quadrature * quadrature;

    quad_curvature = std::vector<TypeReal>( n_faces, 0 );
	// sample surface at quadrature
	std::vector<TypeReal> S, Ss, St, Sss, Sst, Stt, qWeights;
	qSampleD2LimitSurface(vertices, face_nvertex, face_vertex, quadrature, S, Ss, St, Sss, Sst, Stt, qWeights);
    std::vector<TypeReal> metric(quad_samples*n_faces);
	std::vector<TypeReal> principalCurvature1( quad_samples*n_faces );
    std::vector<TypeReal> principalCurvature2( quad_samples*n_faces );
    std::vector<TypeReal> normals( quad_samples*n_faces*3 );
	std::vector<TypeReal> principalDirection1( quad_samples*n_faces*3 );
    std::vector<TypeReal> principalDirection2( quad_samples*n_faces*3 );
	compute_FrenetFrame( &Ss[0], &St[0], &Sss[0], &Sst[0], &Stt[0], &normals[0], &metric[0], &principalCurvature1[0], &principalCurvature2[0], &principalDirection1[0], &principalDirection2[0], quad_samples * n_faces );

	

	
	// compute surface accu,ulated curvature
	int count = 0;
	TypeReal acc = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj<quad_samples; jj++)
		{
			TypeReal Kg = ( principalCurvature1[count] * principalCurvature2[count] ) * qWeights[count]; 
			quad_curvature[ii] += Kg; 
			acc += Kg;
			count += 1;
		}
	}
	return acc;
}


TypeReal quad_maxCurvature(std::vector<TypeReal> &vertices, std::vector<int> &face_nvertex, std::vector<int> &face_vertex, size_t quadrature, std::vector<TypeReal> &quad_curvature )                                               
{

	int n_vertex = vertices.size()/3;
    int n_faces = face_nvertex.size();   
    size_t quad_samples =  quadrature * quadrature;

    quad_curvature = std::vector<TypeReal>( n_faces, 0 );
	// sample surface at quadrature
	std::vector<TypeReal> S, Ss, St, Sss, Sst, Stt, qWeights;
	qSampleD2LimitSurface(vertices, face_nvertex, face_vertex, quadrature, S, Ss, St, Sss, Sst, Stt, qWeights);
    std::vector<TypeReal> metric(quad_samples*n_faces);
	std::vector<TypeReal> principalCurvature1( quad_samples*n_faces );
    std::vector<TypeReal> principalCurvature2( quad_samples*n_faces );
    std::vector<TypeReal> normals( quad_samples*n_faces*3 );
	std::vector<TypeReal> principalDirection1( quad_samples*n_faces*3 );
    std::vector<TypeReal> principalDirection2( quad_samples*n_faces*3 );
	compute_FrenetFrame( &Ss[0], &St[0], &Sss[0], &Sst[0], &Stt[0], &normals[0], &metric[0], &principalCurvature1[0], &principalCurvature2[0], &principalDirection1[0], &principalDirection2[0], quad_samples * n_faces );

	

	
	// compute surface accu,ulated curvature
	int count = 0;
	TypeReal acc = 0;
	for(int ii=0; ii< n_faces; ii++)
	{
		for(int jj=0; jj<quad_samples; jj++)
		{
			TypeReal K1 = ( principalCurvature1[count] ) * qWeights[count]; 
			quad_curvature[ii] += K1;  
			acc += K1;
			count += 1;
		}
	}
	
	return acc;
}


