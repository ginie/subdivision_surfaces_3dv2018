#include <opensubdiv/far/topologyDescriptor.h>
#include <opensubdiv/far/primvarRefiner.h>
#include <opensubdiv/far/patchTableFactory.h>
#include <opensubdiv/far/patchMap.h>
#include <opensubdiv/far/ptexIndices.h>
#include <opensubdiv/far/stencilTable.h>
#include <opensubdiv/far/stencilTableFactory.h>
#include <cstdio>

#include <queue> 


#include "TypeDef.h"
#include "EigenTypeDef.h"
#include "linearAlgebra.h"
#include "quadrature.h"
#include "sds.h"

/// CLASSES

void LaplaceBeltrami(std::vector<TypeReal> &vertices, const std::vector<int> &facenverts, const std::vector<int> &faceverts, SpMat &stiffM, SpMat &massM, size_t n_quad)
{
 // Vertex container implementation.
	struct Vertex {
	
		// Minimal required interface ----------------------
		Vertex() { }
	
		void Clear( void * =0 ) {
			 point[0] = point[1] = point[2] = 0.0f;
		}
	
		void AddWithWeight(Vertex const & src, TypeReal weight) {
			point[0] += weight * src.point[0];
			point[1] += weight * src.point[1];
			point[2] += weight * src.point[2];
		}
	
		TypeReal point[3];
	};
	struct VertexValue {
	
		// Minimal required interface ----------------------
		void Clear() {
			value=0.0f;
		}
	
		void AddWithWeight(VertexValue const & src, float weight) {
			value += weight * src.value;
		}
	
		// Basic 'uv' layout channel
		float value;
	};
	
	typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
	OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_NONE;
	//OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY;
	//OpenSubdiv::Sdc::Options::VtxBoundaryInterpolation BoundaryOption = OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_AND_CORNER; 


    int nverts = vertices.size()/3;
    int nverts0 = nverts;
    size_t nfaces = facenverts.size();
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;
    
    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;
    
    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(BoundaryOption);
    
    Descriptor desc;
    desc.numVertices = nverts;
    desc.numFaces = nfaces;
    desc.numVertsPerFace = &(facenverts[0]);
    desc.vertIndicesPerFace = &(faceverts[0]);
    
    
    // quadrature over reference quad
    std::vector<TypeReal> qpoints, qweights;
    quadrature_referenceQuad( n_quad, n_quad, qpoints, qweights);
    int n_quadface = qweights.size();
    
    
    // Instantiate a FarTopologyRefiner from the descriptor.
    OpenSubdiv::Far::TopologyRefiner * refiner = OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc, OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));
    
    // Adaptively refine seems to be necessary to evaluate the surface at any point
    int maxIsolation = 0;
    refiner->RefineAdaptive(OpenSubdiv::Far::TopologyRefiner::AdaptiveOptions(maxIsolation));
     
    
    //printf("generating far patch table\n");
    // Generate a set of Far::PatchTable that we will use to evaluate the
    // surface limit
    OpenSubdiv::Far::PatchTableFactory::Options patchOptions;
    patchOptions.endCapType = OpenSubdiv::Far::PatchTableFactory::Options::ENDCAP_BSPLINE_BASIS;//ENDCAP_NONE;
    OpenSubdiv::Far::PatchTable const * patchTable = OpenSubdiv::Far::PatchTableFactory::Create(*refiner, patchOptions);
    
    
    
   // Compute the total number of points we need to evaluate patchtable.
    // we use local points around extraordinary features.
    //printf("getting number of refiner verices and local points\n");
    size_t nRefinerVertices = refiner->GetNumVerticesTotal();
    size_t nLocalPoints = patchTable->GetNumLocalPoints();
     
    // Create a buffer to hold the position of the refined verts and
    // local points, then copy the coarse positions at the beginning.
    std::vector<Vertex> verts(nRefinerVertices + nLocalPoints);
    memcpy(&verts[0], &vertices[0], nverts*3*sizeof(TypeReal));  
    
    
    
    //Get all the stencils from the patchTable (necessary to obtain the weights for the LocalPoints)
    //printf("getting stencils\n");
    OpenSubdiv::Far::StencilTable const *stenciltab = patchTable->GetLocalPointStencilTable();
    // Allocate vertex primvar buffer (1 stencil for each vertex)
    const int nstencils = stenciltab->GetNumStencils();
    if( nverts0 != stenciltab->GetNumControlVertices() 	){ printf("nverts %d != stenciltab->GetNumControlVertices() %d\n", nverts, stenciltab->GetNumControlVertices() );}
    nverts = stenciltab->GetNumControlVertices();
    OpenSubdiv::Far::Stencil *st = new OpenSubdiv::Far::Stencil[nstencils];
    for (int i = 0; i < nstencils; i++)
    {
        st[i] = stenciltab->GetStencil(i);
    }
    
    // Adaptive refinement may result in fewer levels than maxIsolation.
    int nRefinedLevels = refiner->GetNumLevels();
    //printf("nRefinedLevels is %d\n", nRefinedLevels);
    
    // Interpolate vertex primvar data : they are the control vertices
    // of the limit patches (see far_tutorial_0 for details)
    Vertex * src = &verts[0];
    for (int level = 1; level < nRefinedLevels; ++level) {
        Vertex * dst = src + refiner->GetLevel(level-1).GetNumVertices();
        OpenSubdiv::Far::PrimvarRefiner(*refiner).Interpolate(level, src, dst);
        src = dst;
    }
    
    // Evaluate local points from interpolated vertex primvars.
    //printf("Evaluate local points from interpolated vertex primvars\n");
    patchTable->ComputeLocalPointValues(&verts[0], &verts[nRefinerVertices]);
    
    // Create a OpenSubdiv::Far::PatchMap to help locating patches in the table
    OpenSubdiv::Far::PatchMap patchmap(*patchTable);
    //printf("Create a OpenSubdiv::Far::PatchMap to help locating patches in the table\n");
    
    // Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces.
    OpenSubdiv::Far::PtexIndices ptexIndices(*refiner);
    //printf("Create a OpenSubdiv::Far::PtexIndices to help find indices of ptex faces\n");
    
    //if(nfaces != ptexIndices.GetNumFaces()){ printf("number of faces and ptexIndices.GetNumFaces() mistmach"); }
    nfaces = ptexIndices.GetNumFaces();
    //printf("nfaces %d ptexIndices.GetNumFaces %d\n", facenverts.size(), nfaces);
    
    // create triplets to define sparse matrix L that evaluates the limit surface at parameter values
    std::vector<size_t> rows, cols;
    std::vector<TypeReal> Mvals, Svals;
    float pWeights[20], dsWeights[20], dtWeights[20];
    for (size_t iface=0; iface<nfaces; iface++)
    {

    	for( int iq=0; iq< n_quadface; iq++)
    	{
			// Locate the patch corresponding to the face ptex idx and (s,t)
			TypeReal s = qpoints[iq*2];
			TypeReal t = qpoints[iq*2+1];
			OpenSubdiv::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch( iface, s, t );
			assert(handle);
			
			
			// Evaluate the patch weights, identify the CVs and compute the limit frame
			// the value of the surface and its partial derivatives (w.r.t parametric variables s, t) is a linear combination of the value of the control points
			// compute the weights of these linear combinations
			patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights);
			
			// get the control variables for this patch
			OpenSubdiv::Far::ConstIndexArray cvs = patchTable->GetPatchVertices(*handle);
			
			
			//Compute the weights for the coordinates and the derivatives wrt the control vertices
			//Set all weights to zero
			std::vector<TypeReal> vect_wc(nverts0, 0.f);
			std::vector<TypeReal> vect_wu1(nverts0, 0.f);
			std::vector<TypeReal> vect_wu2(nverts0, 0.f);
			TypeReal S[3] = {0, 0, 0};
			TypeReal Ss[3] = {0, 0, 0};
			TypeReal St[3] = {0, 0, 0};
			std::queue<std::pair<int, TypeReal> > auxQueue;
			std::queue<TypeReal> auxDu;
			std::queue<TypeReal> auxDv;
			for (int cv = 0; cv < cvs.size(); ++cv)
			{
				// evaluate the surface and tangent
				for(size_t idim=0; idim<3; idim++)
				{
					S[idim] += pWeights[cv]* verts[cvs[cv]].point[idim];
					Ss[idim] += dsWeights[cv]* verts[cvs[cv]].point[idim];
					St[idim] += dtWeights[cv]* verts[cvs[cv]].point[idim];
				}
				
				// get the value of the active basis
				if (cvs[cv] < nverts0)
				{
					vect_wc[ cvs[cv] ] += pWeights[cv];
					vect_wu1[ cvs[cv] ] += dsWeights[cv];
					vect_wu2[ cvs[cv] ] += dtWeights[cv];
				}
				else
				{
					//printf("add2queue %d, %f, %f, %f\n", cvs[cv], pWeights[cv], dsWeights[cv], dtWeights[cv] );
					auxQueue.push( std::make_pair(cvs[cv], pWeights[cv]) );
					auxDu.push( dsWeights[cv] );
					auxDv.push( dtWeights[cv] );
				}
			}
			
			// for indices that are auxiliary intermediate points, accumulate the contributions
			while( !auxQueue.empty() )
			{
				std::pair<int, TypeReal> obj = auxQueue.front();
				TypeReal du0 = auxDu.front();
				TypeReal dv0 = auxDv.front();
				int indx0 = obj.first;
				TypeReal val0 = obj.second;
				const unsigned int ind_offset = indx0 - nverts0;
				size_t size_st = st[ind_offset].GetSize();
				OpenSubdiv::Far::Index const *st_ind = st[ind_offset].GetVertexIndices();
				float const *st_weights = st[ind_offset].GetWeights();
				for (size_t s = 0; s < size_st; s++)
				{
					int indx = st_ind[s];
					if( indx <nverts0 )
					{
						vect_wc[ indx ] += val0*st_weights[s];
						vect_wu1[ indx ] += du0*st_weights[s];
						vect_wu2[ indx ] += dv0*st_weights[s];
					}else
					{
						auxQueue.push( std::make_pair(indx, val0*st_weights[s]) );
						auxDu.push( du0*st_weights[s] );
						auxDv.push( dv0*st_weights[s] );
						//printf("add2queue %d, %f, %f, %f\n", indx, val, du0*st_weights[s], dv0*st_weights[s]  );
						
					}
				}
				auxQueue.pop();
				auxDu.pop();
				auxDv.pop();
			}

			
			// compute the metric tensor, its inverse and the are element
			Eigen::Map< Eigen::Vector3f > Su(&Ss[0], 3);
			Eigen::Map< Eigen::Vector3f > Sv(&St[0], 3);
			Eigen::Matrix2f G(2,2);
			G(0,0) = Su.dot(Su);
			G(1,0) = Sv.dot(Su);
			G(0,1) = G(1,0);
			G(1,1) = Sv.dot(Sv);
			// compute the psudoinverse instead of the inverse to protect against badly conditioned quads
			Eigen::Matrix2f iG = pseudoinverse(G, (TypeReal) 1e-4); 
			TypeReal detG = G.determinant();
			TypeReal area = std::sqrt(detG);
			
			
			// add it to the assembly of stiffness and mass matrices
			TypeReal mweight = qweights[iq] * area;
			for (int cv=0; cv<nverts0; cv++)
			{
				if (vect_wc[cv] != 0.f)
				{
					Eigen::Vector2f Du(2); Du[0] = vect_wu1[cv]; Du[1] = vect_wu2[cv];
					for (int cw=0; cw<nverts0; cw++)
					{
						if (vect_wc[cw] != 0.f)
						{
							Eigen::Vector2f Dv(2); Dv[0] = vect_wu1[cw]; Dv[1] = vect_wu2[cw];
							rows.push_back(cv);
							cols.push_back(cw);
							Mvals.push_back( vect_wc[cv]*vect_wc[cw] * mweight);
							Svals.push_back( mweight * Du.dot( iG*Dv) );		
						}
					}
				}
			}
		}// end quadrature over face
    }// end loop over face
    
    // create sparse stiffness and mass matrices
    massM.resize( nverts0, nverts0 );
    stiffM.resize( nverts0, nverts0 );
    build_spsMatrix( rows, cols, Mvals, massM );
    build_spsMatrix( rows, cols, Svals, stiffM );
    
    SpMat massMt = massM.transpose();
    massM = 0.5*( massM + massMt );
    SpMat stiffMt = stiffM.transpose();
    stiffM = 0.5*( stiffM + stiffMt );

}





