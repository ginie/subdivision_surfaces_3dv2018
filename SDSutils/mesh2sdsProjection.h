#ifndef __mesh2sdsProject__
#define __mesh2sdsProject__

void interpolateVertexFunction( const std::vector<TypeReal> &vertex, const std::vector<TypeReal> &vFunction, TypeReal *sPoints, TypeReal *sFunction, int n_points );
void projectVertexFunction( const std::vector<TypeReal> &points, const std::vector<TypeReal> &vFunction, std::vector<TypeReal> &vertices, const std::vector<int> &face_nvertex, const std::vector<int> &face_vertex, std::vector<TypeReal> &projFunction, size_t quadrature );
#endif