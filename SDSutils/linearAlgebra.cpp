#include "TypeDef.h"
#include "EigenTypeDef.h"
#include <vector>
// include eigen to solve linear systems
#include <Eigen/OrderingMethods>
#define EIGEN_SUPERLU_SUPPORT
#include <Eigen/SparseLU>
#include <Eigen/SparseCholesky>
#include<Eigen/SparseQR>
#include "linearAlgebra.h"


void build_spsMatrix(const std::vector<size_t> &M_rows, const std::vector<size_t> &M_cols, const std::vector<TypeReal> &M_vals, Eigen::SparseMatrix<TypeReal> &M )
{
    std::vector<T> tripletList;// list of non-zeros coefficients
    
    for(size_t j=0; j<M_rows.size(); j++)
    {
        tripletList.push_back(T(M_rows[j],M_cols[j],M_vals[j]));
    }
    
    M.setFromTriplets(tripletList.begin(), tripletList.end());
}
void build_spsDiagMatrix(const std::vector<TypeReal> &M_vals, Eigen::SparseMatrix<TypeReal> &M )
{
    std::vector<T> tripletList;// list of non-zeros coefficients
    
    for(size_t j=0; j<M_vals.size(); j++)
    {
        tripletList.push_back(T(j,j,M_vals[j]));
    }
    
    M.setFromTriplets(tripletList.begin(), tripletList.end());
}
void build_spsDiagMatrix(const SpVect &M_vals, Eigen::SparseMatrix<TypeReal> &M )
{
    std::vector<T> tripletList;// list of non-zeros coefficients
    
    for(size_t j=0; j<M_vals.size(); j++)
    {
        tripletList.push_back(T(j,j,M_vals[j]));
    }
    
    M.setFromTriplets(tripletList.begin(), tripletList.end());
}
Mat pseudoinverse(const Mat &mat, TypeReal tolerance = 1e-6) // choose appropriately
{
    TypeReal Scalar;
    Eigen::JacobiSVD<Mat> svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
    const Mat &singularValues = svd.singularValues();
    Mat singularValuesInv(mat.cols(), mat.rows());
    singularValuesInv.setZero();
    for (unsigned int i = 0; i < singularValues.size(); ++i) {
        if (singularValues(i) > tolerance)
        {
            singularValuesInv(i, i) = ((TypeReal) 1.0)/ singularValues(i);
        }
        else
        {
            singularValuesInv(i, i) = ((TypeReal) 0.0);
        }
    }
    return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
}